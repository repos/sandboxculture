pdf:
	pandoc src/*.md \
	-o thesis.pdf \
	--bibliography=src/sandboxculture.bib \
	--csl=src/chicago-fullnote-bibliography.csl \
	-H src/preamble.tex \
	--template=src/template.tex \
	-V fontsize=12pt \
	-V papersize=a4paper \
	-V documentclass:report \
	--normalize \
	--number-sections \
	--smart \
	--latex-engine=xelatex \
	--verbose
