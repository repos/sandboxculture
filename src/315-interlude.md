Interlude {.unnumbered}
=========

\setcounter{footnote}{0}

As shown in the second part of the dissertation,
art and cultural platforms can thrive on techno legal constraints.
It also explains why some elements of proto free culture,
discussed in Part 1,
have translated into very diverse practices.
Therefore the populating of free culture is not always specific to free culture,
and is more likely to relate to the nature of the environments in which they emerge.
Yet,
free culture supporters,
with their desire to protect such environments by an over-articulation or principles and rules,
overlook the fact that the very failure of this attempt does not prevent cultural development,
but is instead an important component,
a veritable *fruit défendu*,
from which new practices and transactions will be fed.
Of course,
the hypothesis brought to the fore by the free culture argument,
is that eventually cultural constraints will be so strong and repressive,
that all these practices will end up stifled and at the service of a commodification process that harvests the work of artists [@BerryMoss:art].
However,
the same logic offered by free culture when pushed to the extreme,
leads to another form of commodification provoked by the endless techno legal possibilities of rationalising cultural productions which become a highly contained disposable material,
by making the incorrect generalisation that culture can be broken down into engineered blocks of things that can be combined and recombined to create new objects and products.
^[
To be fair,
Berry's and Moss' views,
especially in relation to Creative Commons,
changed quickly and radically a year after their paper I am referencing above.
See [@BerryMoss:librecommons].
Their argument remains however a very good illustration of how such alternatives are perceived at first and become quickly viral and popular within the cutural field.
But once its mechanics are more apparent,
other interpretations become possible, 
leading possibly to disenchantment,
which is why I will next introduce the sandbox analogy to explore this particular process,
and why free culture in general can be so ambivalent despite its precise techno-legal articulation.
]

In sum,
the techno-legal free software template has helped form constraints and inspire a wide range of artistic practices discussed in Part 2,
but this same template can also limit the cultural scope and the intention of those who appropriate it,
thus making free culture unfit for generalisation,
or at least creating more problems than it solves when adopted blindly.
Another aspect that I wanted to reflect upon was what happens to cultural production when it adopts a model that is essentially derived from engineering.
If on the one hand,
engineering methods of prototyping and pipelining opened up new way to engage with the making of artistic work,
from the writerly command line to notions of source code brutalism,
these methods cannot be decoupled from a certain approach to organise and categorise digital information in their systems of execution and distribution.
This led me to explain that in such systems,
once again, 
free cultural practices that rely on precise definitions cannot accommodate all sorts of cultural expressions,
which I illustrated by taking free software art as an extreme example of runtime incompatibility.
It also struck me that the question of source,
which I argued was the core foundation of software freedom,
was impossible to articulate simply once transposed to non-software cultural expressions,
making free culture fragile,
threatening to become nothing more than another form of file sharing system.
What is more,
these problems of translation show that the core value of these systems are only revolving around the questions of access to data and the potentiality of transformation of such data.


With these limits exposed I wanted to highlight the paradox of the free culture constraint,
as both liberating and entrapping,
raising fences to protect practices and at the same excluding others.
Building upon this idea of walled gardens,
I will use the last two chapters of this thesis to draw a model of such systems that take into account these conflicts and paradoxes,
and demonstrate how these paradoxes allow free culture to sustain itself and evolve through time.
In the next chapter I will attend to the kind of environments,
or platforms,
that are created by free culture.
I will take the notion of remix, which is popularly used in the free culture narrative as a point of departure,
to illustrate that the question of access can be highly manipulative and deceptive.
By doing so,
it is my intention to highlight how techno-legal frameworks provide an inconspicuous social cohesion and a set of rules,
which only become visible and questionable when the systems of belief they create are challenged by a conflictual event.
For this,
I introduce the term sandbox as a way to describe and refer to such platforms.
The idea of cultural sandboxing exists in the neighborhood of other notions such as discourse [@Foucault:archeo],
fields [@BourdieuWacquant:champs],
subcultural [@Hebdige:subculture] and post-subcultural models [@MuggletonWeinzierl:postsub],
ideological state apparatus [@Althusser:positions, Idéologie et appareil Idéologique d’État (AIE)],
art platforms [@Goriunova:platforms],
and also Heidegger's *Gestell* [@Heidegger:tech, The question concerning technology (1954)] amongst others.
However,
these are also markedly different and I find it necessary to coin the term sandbox to highlight the very specific type of framing made by software environments and licensing,
and the containment they provide,
therefore building upon the notions of foundation and territoriality hinted in the second part of the thesis.


If the impact of rationalisation,
commodification,
and normalisation on culture has already been addressed extensively in the literature,
with the sandbox model I want to show more precisely how this operates and is implemented at a techno-legal level,
at a time where the role of software in society,
and its underlying algorithms,
is increasingly scrutinised.
This is why the idea of sandboxing also relates to the mix that Kelty had noted between operating systems and social systems,
while inspired by the tag line "Operating Systems and Social Systems" of the first edition of the Wizard of OS conference in 1999 [@Kelty:bits, pp. 36-43].
However unlike Kelty,
inside the sandbox,
I do not limit the mix only to hackers,
but any users,
given the omnipresence of today’s operating systems and increasingly complex terms of service (TOS) in all sorts of appliances.
In that sense, 
if there was a point in time where such a mix might have been observed remotely and limited to some hacker subculture,
such distance is definitively questionable with today’s 24/7 network connectivity for most,
and will be eventually made irrelevant with the upcoming of the so-called Internet of Things (IoT)---and regardless how this will be marketed and promoted in the coming decades---which will impose such a mix on everyone.
As part of my argument I will both use examples from operating systems and free culture licensing,
to show how this particular sandboxing operates both at a software and legal level,
and provides an update to the notion of blackboxing [In reference to @Latour:pandora].
I will discuss the ubiquity of sandboxing,
where legal and technological openness does not necessarily equate with user empowerment and technological literacy.

\newpage
