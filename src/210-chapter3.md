Art Libre
=========

Free Art Incentives
-------------------


As explained in Chapter 1,
while the late nineties brought to life some experiments on the creation of generalised proto-free culture licenses,
this transposition first went through a direct use of the GPL for non-software creations,
articulated most notably by Stutz in 1997 [@Stutz:compgnu].
However,
this transposition was not only noticed by digital practitioners,
or for that matter anyone close to free and open source software circles.
In fact,
in the same year of the publication of Stutz's text,
copyleft was mentioned by a law scholar as a valid framework for collaborative artworks in which artists would pass "each work from one artist to another" [@Heffan:copyleft, p. 1448].
That remark is particularly significant not because of the collaborative dimension applied to art,
but because of the explicit concern to provide a legal validity to certain practices whose origins are buried in the depths of art history.
Indeed,
the idea of works passing from one artist to another and the questions of copies,
derivative works,
appropriation,
or plagiarism,
are nothing new [See @Cramer:anticopy].
So,
why a sudden interest in such practices at the turn of this century?


The reason is simple and the culprits are not the methods used in artistic practices,
but rather their practice within the digital realm.
With the fast adoption of personal computers and the democratisation of the Internet throughout the nineties,
this "convergence of media technologies and of digital computing" [@Manovich:newmediaguide, p. 14]  eventually became known as *new media*,
a term made popular by Russian media theorist Lev Manovich [@Manovich:language],
yet the newness of which was in fact very much indebted to video and television [@Murphy:tv].
However,
if the emergence of new media practices were instrumental in broadening computer related art practices,
it also challenged the intellectual property frameworks that were adjusted for *older* media only.
As French Philosopher Jacques Derrida noted in 1995,
the rise of electronic media must necessarily be accompanied by juridical and political transformations [@Derrida:mal, p. 35],
and from the perspective of copyright laws in the age of an Internet becoming increasingly commercial,
the GPL copyright hack provided an interesting model for lawyers,
then urged to revise their approach to intellectual property to accommodate different strands of practices that will eventually be associated with new media.
In particular,
the notion of originality and creativity became confronted with the tools and techniques of file sharing and digital data processing,
in a system where everything is a copy [See @Lessig:recreativity].
For instance in the US,
and under the 1976 US Copyright Act,
the only recognised artistic collaborative work was the joint work,
in which different contributions were meant to be "merged into inseparable or interdependent parts of a unitary whole" [@congress:1976, 101. Definitions].
Said differently,
the joint work assumed both the existence of an agreement to develop a final work,
as well as a commonness of the intention behind the creation of that work.
This made perfect sense in the context of the print-based copyright doctrine,
but clearly does not work for digital environments where the romantic understanding of authorship was challenged by the dense network of branches,
copies,
and processes inherent to software-driven networked collaboration,
where files,
texts,
bits of code and whatever digital bytes could end up at any time as part of an online assemblage or collage.
Net art,
with and without the dot,
will be particularly exemplary in showing the impact of the net on the difficulty of framing a particular discourse,
a canon,
a definition,
a movement,
or simply establish a static viewpoint of such a dynamic environment [See @Bosma:netart].


Intellectual property scholar Margaret Chon in her 1996 article *New Wine Bursting From Old Bottles*,
had already used the 1992 work *ChainArt* by media artist Bonnie Mitchell,
to demonstrate this particular limit of the copyright doctrine [@Chon:wine].
Mitchell's pre-WWW project,
which seems rather trivial by today's standards,
was a chain in which her students and fellow artists were invited to modify a digital image,
and pass it to someone else via e-mail and File Transfer Protocol (FTP) servers [@Mitchell:connections].
The work is interesting because it makes extremely explicit the dichotomy between digital media as a telecommunication carrier,
and digital media as artistic material [For a discussion about this particular distinction, see @Cramer:anti, p. 12-14].
But according to Mitchell,
the whole system and its different iterations are the work itself,
not just the final image at the end of the chain [@Chon:wine].
The work exists as a collection of derived,
reused and,
some would more easily say today *remixed*,
individual elements that could not be flattened down into one single joint work,
in a work of digital media conflation.
Being hard to pin down,
Chon noted the legal consequence of such chained media work in its impossible protection,
and challenge to properly credit,
under the limited copyright rules [@Chon:wine].


Following the juridical literature,
the work was then picked up by intellectual property attorney Ira V. Heffan in 1997,
who used it as an ideal example of artistic works that could greatly benefit from both the GPL,
and the creative use of its copyleft mechanism that would encourage "the creation of collaborative works by strangers" [@Heffan:copyleft, p. 1513].
Although Heffan's conclusion is legally sound,
and basically provides,
unknowingly it seems,
an academic echo to Stutz's own conclusion and generalisation regarding the GPL and non-software works published the same year,
^[
See Chapter 2.
]
it does nevertheless miss an important point,
which is the cultural dimension of such networked practices.
Said differently,
and as net art would illustrate further,
what is not taken into account is the artistic desire to reflect upon the nature of information in the age of computer networks.
Even if it seems to conveniently fill a legal gap,
using the GPL for art practices cannot be reduced to a mere desire to collaborate for the sake of collaborating,
and to make a collage for the sole intent of glueing things together,
out of its own consideration.
This cultural aspect emerged from the early debates about control and freedom of information and speech,
as the Internet started to spread to more households throughout the mid- and late nineties [@Moschovitis:internet, Chapter 7: Living on Internet time: 1995-1998].
Artists did not wait for the approval of lawyers to claim the GPL,
they used it for specific purposes unrelated to the need to make their work legally reasoned for the net.
If there had already been discussions---most notably in the context of hypertextual literature---on the need for new copyright models,
like Ted Nelson's idea of context and credit preserving automated *transcopyright* system [@Nelson:transcopy],
the adaptability of new media practices--for instance with the mix of code and writing found in what American artist Alan Sondheim described as codeworks [@Sondheim:codework]---allowed artists to reflect as much in the production of media as in its distribution in regard to intellectual property.
Eventually alternative copyright disclaimers found their way into these works,
such as the permissive copy statement found in the file header of the 2002 code poetry *london.pl* by British artist Graham Harwood [@Harwood:london],
but moreso in the use of the GPL in the 2003 software art *pngreader* by textz.com and Project GNUtenberg [@gnutenberg:pngreader].
So in fact,
many artists adopted the GPL---and other free and open source software licenses---early on to augment their work with a statement,
both derived and appropriated from the free software template,
but adapted to these new practices and intents.
To be sure,
at a time where there were no Creative Commons licenses,
no free culture definition,
at the dawn of peer-to-peer (P2P) file sharing,
and with intellectual property frameworks which were unable to apprehend the fast expansion of net culture,
the last thing artists looking at the GPL were interested in was the question of collaborative or cooperative infrastructures.
These were already in place and in use.
What mattered instead was the paratextual ability of free software licensing to colour a work as a strategy to criticise,
comment,
communicate a motivation for their practice,
essentially creating "a proxy for,
or even generative of,
the artwork’s very substance" [@AdamsonGoddard:statements, p. 363]:
in a nutshell an artist's statement in the age of new media and codeworks.


Most notably in 2000,
Mirko Vidovic used the free software definition to create the GNUArt project [@Vidovic:gnuart],
a call to develop what he refers to as free art
^[
Not to be mistaken with the GNU Art section of the FSF website that is a collection of logos, marks and artworks to be used as graphical assets for the visual identity of the GNU project.
],
an effort to ensure the independence and freedom of artists,
liberated from the control of existing collecting societies,
producers,
and publishers,
while at the same time allowing them to interface with an audience and suggest new ways to deal with the sustainability of their practice in an independent way [@puces:gnuart].
The project uses the free software model as a way of putting forward the notion of a work protected from external and exclusive control,
a work which,
according to the GNUArt author,
would then be free to circulate and evolve [@Vidovic:gnuart].
By consciously choosing the GPL as a means of creation and distribution,
the different artists who contributed to GNUArt voluntarily engaged with the issue of commodity and culture.
They used the free software template to implement an apparatus that was not specific to software freedom,
but specific to artistic freedom mixed with social and political concerns [@3boom:sono].
Such efforts were very much connected with a growing trend in the late nineties for media activism in the arts,
in particular in the scene of tactical media [@CAE:digitalresistance].
Once again,
these practices came into existence because of new media,
because of this Internet *thing*,
which ultimately permitted art practices to escape from their historically constructed autonomy.
To illustrate this,
German critic Florian Cramer notably compares the difference between the "simulation of corporate entities" [@Cramer:misunderstandings, p. 131] within the safety and artificiality of the art system,
using as an example Res Ingold's *Ingold Airlines* [@MAE:ingold],
to the same strategy deployed over the Internet by The Yes Men,
leading to their intervention and spoof as spokespeople for the World Trade Organization (WTO) in 1999 [@yesmen:film].
This is not an isolated event,
and what codeworks,
new media art,
net art,
and other nineties networked media practices will demonstrate is the potential to liberate artistic critique,
from decades of white cube taming and commercial domestication.


This is why a system like GNUArt can also be understood as "a process of copying that offers dominant culture minimal material for recuperation by recycling the same images,
actions,
and sounds into radical discourse" [@CAE:recombinant, p. 152],
to apply to this project the words from performance art and tactical media collective Critical Art Ensemble (CAE).
In this context,
another interpretation of free software licensing and art suddenly emerges:
the GPL is not just a tool to make juridically well-grounded endless digital media transformation,
it can be a materialist and performative flagship for recombining dreams of the digital resistance,
close to that envisioned by the CAE collective,
and shared by other groups at the time.
Here,
I am not musing about,
or conveniently interpreting a project like GNUArt as an act of resistance.
As with with Stallman's lost community and the Symbolics episode,
it is important to look at the human dimension of these projects,
the anecdotes and footnotes inside the official stories.
In the case of GNUArt,
bearing the same name,
Vidovic's father Mirko Vidović,
is a Croatian writer who was associated with the Movement of Independent Intellectuals in Yugoslavia,
opposing Tito's regime in a quest for freedom of speech.
Despite having easily obtained his French nationality by marriage in 1962,
and exiled to France so as to pursue his literary work and studies,
he is arrested in the late sixties during a visit to his birthplace in Western Bosnia.
There,
he will be tried and imprisoned for the earlier publication of his book of poetry.
Two years later,
his sentence is extended with his refusal to testify against members of the Croatian Spring,
a Yugoslavian political movement in favour of democratic and economic reforms.
In total,
Vidović will spend more than five years in the Yugoslav Gulag camps,
before his release and return to France toward the end of the seventies [See @Vidovic:yugoslavia].
With such a lineage,
it is easy to perceive a particular sensibility towards any forms of legal restriction,
a sensibility that would be eventually embodied in the GNUArt project itself
^[
Even if paradoxically the use of the GPL in art will also set in motion other forms of equally restricting and disciplinary legal structures,
as I will explain in Part 3.
].


Even though GNUArt emerged outside of the new media art and tactical media scene,
there is a clear extension into the digital domain of an ambition to make art engage with intellectual and cultural resistance.
It is not just about making gigantic chains or collages or networked *cadavres exquis*.
Especially here,
it is both rooted in a discourse of libertarian free speech which opposes authoritarian entities,
and the pragmatic desire to develop an autonomous and empowering practice,
in which its participants can build upon each other’s ideas and techniques by combining and cooperating within a common pool,
a library, 
once again,
of projects and materials,
be it software or art.
This project is a crucial step to understanding the different paths of evolutionary transitions in the development of free culture.
It is indeed not just the first and yet most articulate transposition of the FSF engineering freedom directed towards a politically driven artistic freedom by the means of licensing,
it is also a demonstration of the capability of the free software template to be adapted and appropriated by others,
so they can also set up their own *Gemeinschaft* emulation.
Every other effort during the proto-free culture era should be understood similarly,
and not simply as a direct transposition of the free software discourse.
As a matter of fact,
in the last section of this chapter I will discuss Stallman's struggle to understand the connections made in the case of free art.
But before this,
I will first come back to the second part of the free art genesis,
which is also very illustrative of the apparently endless adaptability of the free software template.


Licence Art Libre
-----------------


While Vidovic was the first to articulate the term *art libre* [@Vidovic:artlibre],
and writing about the need for a Free Art License as early as 1998 [@Vidovic:freeart],
we have to wait until the year 2000 for the work of a few lawyers and artists,
namely Mélanie Clément-Fontaine,
David Geraud,
Isabelle Vodjdani,
and Antoine Moreau,
as well as the feedback from the participants of a free art centred mailing list,
to see the creation of such a Licence Art Libre [@CopyleftAttitude:lal1.0] (LAL),
also commonly referred to as the Free Art License as I stated earlier in the introduction to the second part of this dissertation.
The resulting document is made to be an artistic equal to the GPL,
yet articulated specifically for the creation of free art under the French jurisdiction,
making the FAL explicitly tailored for the French equivalent of copyright laws:
*le droit d'auteur*.
This localism is however not an issue,
indeed it is important to understand that the 1886 Berne Convention for the Protection of Literary and Artistic Works,
give to the FAL an international scope,
by assuring the respect and protection of the licensed work in all the states involved in the agreement
^[
168 at the time of this writing.
].
This is not specific to the FAL:
the GPL,
written within US copyright laws,
also benefits from this international copyright treaty,
which explains why its popularity extended beyond US jurisdiction.
And the same can be said for any copyright based licenses developped within the jurisdiction of any the Berne Convention signatories.
That said,
building a license under a different juridical medium,
as with code,
enables different specificities and interpretations,
and as for the notion of copyleft,
it becomes in this particular context *la gauche d'auteur*,
so as to emphasize in a playful way the leftist tone of the project.


Alongside this,
it's probably with this project that the license reinforces its role as an artist's statement the most.
In the FAL,
the copyleft principle is described as "la liberté contre le libéralisme" [@Moreau:artlibre.org],
freedom against liberalism.
This is an important matter,
because while both the GNUArt free art licensing and the FAL free art licensing work as artistic statements,
they do differ greatly in their intention.
As a matter of fact,
Vidovic and Moreau did correspond by email during the early days of the creation of the FAL,
regarding the merging of their distinct efforts;
but,
just like with the OKD and freedom defined,
despite the apparent overlap in scope this merging never occured due to diverging opinions and lack of interest to federate both projects
^[
This exchange was confirmed to me by both Vidovic and Moreau.
].
Indeed,
if there is a connection between Vidovic's free art and a type of cyber-libertarianism that can manifest itself in both left-wing and right-wing politics,
as well as a relation with the fundamental notion of artistic freedom in the constitutions of some European countries
^[
In particular Germany,
Austria,
and
Switzerland.
], 
the lineage of free art as enabled by the FAL is however of a different nature.
In particular when the FAL,
as I said earlier,
is announced by its authors as freedom against liberalism,
it must be understood from the viewpoint of French left-wing politics where liberalism is used broadly,
to refer to capitalist systems and various strands of economic liberalism,
including neoliberalism as well as classic liberalism.


To be sure,
in this context,
free art appeared as yet another iteration in an ongoing,
but more traditional,
struggle to articulate the position of the artist and their work within the art market and the cultural institutions that control it.
It is very interesting to see for example that the term *art libre* appeared in France during the eighteenth century *artisanat* as a workaround to defend the circulation of designs outside of the market control ruled by guilds [@Miller:silks, pp. 279-280],
therefore trying to both escape and benefit from the Renaissance split between craft and art.
But it is even more striking that the concept of *artistes libres* was used to describe specific modes of art production,
as part of a broader analysis in the cultural transformation in France,
regarding the structuring of its artistic field at the end of the nineteenth century.
According to French art historian Marie-Claude Genet-Delacroix,
under the French Third Republic,
it is thus possible to make a distinction between three distinct structures of art production:
academic art,
official art,
and free art,
*art libre*,
the latter being represented in her research with the painters Odilon Redon and Paul Cézanne [@GenetDelacroix:vie, p. 41].
For Genet-Delacroix,
free art is used as a term to show that the works of such artists were free from the official and academic institutions of the time,
not from the market [@GenetDelacroix:vie, p.42].
She however notes that at the same time these artists,
who were largely ignored or disregarded by the authoritative circuits of exhibitions and awards at the time,
were able to reverse the terms of the dominant art market by substituting economics with friendship,
knowledge,
and passion for art,
precisely because they were free from institutional constraints [@GenetDelacroix:vie, p.60].


So when Moreau writes that free art is freedom against liberalism,
it must be understood from the perspective of artists resisting the dynamics of the contemporary art market,
and the creation of the FAL belongs to a prolonged inner artistic dialogue about what defines the object of art,
its economics,
and its values.
In a way,
as an artistic response,
free art is similar in its function to conceptualism,
at least in the early days of the latter,
before some of its manifestations became commodified by gallery dealers in the mid seventies,
given the commercial value of works presenting both formal novelty and radical pretension [@Taylor:avant, p.34].
This is why in free art,
the constraint of the license becomes a means of liberation of the artwork and the artist's practice,
from any possible future appropriations other than those permitted by the license.
For the artists involved in the project this is not an art movement or genre,
it is an attitude,
thereby giving the name *Copyleft Attitude* to the artists meetings from which the FAL came. 
With their license,
rules stand on their own and provide a framework for a creative process that is meant to inspire a collectivist approach to producing works,
thus encouraging an artistic alternative to the gallery and contemporary art market diktat on artwork production and distribution.
So even stronger than a statement,
the FAL is a rationalised,
functional,
and official divorce letter from the artists,
to the contemporary art world of curators,
gallerists,
and collectors after centuries of proto-art libre infidelity.


Another important aspect of this latest iteration of free art is also the constraint generated by the license.
Having inherited the "playful cleverness" [@Stallman:fsfreesociety, p. 17] of the copyright hack,
the FAL became both an artistic critique and a system to make art.
In this respect,
its practice boils down to a new variation of the constraint system used by groups such as the Ouvroir de littérature potentielle (OuLiPo),
a sixties-born group of writers and mathematicians focussed on the creation of literary structures,
in which systems of constraints are used to promote and inspire creation
^[
Such rules are either novel,
and developed within the group,
or themselves belong to a much broader cultural context and history.
This is the case of the lipogram writing,
where one or several specific letters are obviated.
See [@Perec:lipogram].
].
The group dynamic itself is driven by rules,
and in the same way that *Cent mille milliards de poèmes* [@Queneau:cent] was the emblematic 1961 OuLiPo call and manifestation of creative rules to encourage a practice of constraint-based writing,
the launch of the FAL is similarly a call for legally-constrained art,
and set the rules for the Copyleft Attitude community that will subsequently use the license to produce new works and recombine others as part of a collective effort (Figure \ref{fig:freeartnet}).
But here the collective practice is centrally dictated,
and unlike GNUArt that aimed to spread and test early on the idea of a boundless free art practice,
which subsequently might lead to a bottom-up emergence of decentralised collectives and cooperation,
the FAL works differently as an artistic top-down gift to other artists,
to invite them to engage and participate in this game
^[
For an analysis of some examples of communication within the group,
and the relationship with the works created and appropriated,
how they relate, inform and influence each others,
see [@Bruge:artlibre].
].


\afterpage{%
\begin{figure}[h]
	\caption{Reuse and appropriation between FAL/LAL artists}
    \includegraphics[width=\textwidth]{art_libre_flow} 
	\fnote{Diagram: Charlotte Bruge, 2003}
    \label{fig:freeartnet}
\end{figure}
\clearpage
}


Finally,
given the cultural context in which the document emerged,
and despite the fact that---as I said in the previous chapter---a license is not a contract,
it is difficult not to frame the FAL within broader artistic practices that use the contract as a means of institutional critique.
From Marcel Duchamp's 1924 *Monte Carlo Bond* and the 1971 work *The Artist's Contract*  by art dealer and curator Seth Siegelaub,
to some of the more recent works from artists Carey Young,
Jill Magid,
or the collective Superflex,
to name a very few,
artists have a long history of using the contract of aesthetics and the aesthetics of the contract in order to playfully destabilize institutional order and rationality [See @McClean:contract].
So in that sense,
I find it very hard to not link free art to such forms of artistic critique,
where intellectual property is turned inside out to reveal other legal and ontological narratives.
However,
even though Moreau notes that it is conceptually possible to attribute an artistic nature to the FAL,
just as it is according to him possible to do so for any other things,
he insists that it is above all a legal document like any others.
For him,
the fact that it was produced by artists and relates to the art domain,
could indeed provide an artistic element to the work,
yet it was not their intention,
and in fact a particular attention was given to make sure the Free Art License was created to be a common useful tool [See @Moreau:phd].




Usefulness of Legal Constraints as Safe Haven
---------------------------------------------


To sum-up what I have discussed so far,
the FAL works both as a system to make free art within the Copyleft Attitude group,
in a way that is meant to be useful and playful,
and also as a means to generate an artistic critique of contemporary art practices and economics.
In that sense it exists as a coherent system,
that just like free software,
allows a community to materialise its ideals and support their matching social structure and practices.
But beyond its critical and constituent usefulness,
its artistic usefulness remains unexplained. 


As I have argued in the previous section,
there is a link between free art and constraint art systems such as the works of OuLiPo.
However,
I do not want this connection to be superficially understood from the perspective of the trivial playfulness in creating rules to collaborate on a potentially never ending networked canvas.
As I said earlier,
this type of functional usefulness was not an artistic motivation to start with,
but was a specific juridical speculation made by law scholars regarding the potential of using the GPL for digital artworks.
French poet Raymond Queneau,
co-founder of OuLiPo had already advised in his 1938 essay *Qu'est-ce que l'art?* [@Queneau:letters, p. 36] that artists should not stop at the proper execution of rules for art's sake,
and such a position could also be traced back to his generational connection with movements such as Dada and surrealism,
that saw *le jeu* as "a code word for rebellion" [@Toloudis:impulse, p. 149],
and also predates the ludic frameworks of the Situationist International [@IS:manifeste].
Said differently,
there is and there must be something else expressed within these artistic games:


> To simply perform one's task well is to reduce art to a game,
> the novel to a chess match,
> the poem to a puzzle.
> It's not enough to say, nor to say well;
> the thing must be worth saying.
> But what is worth saying?
> There's no getting around it:
> that which is useful. [@Queneau:letters, p. 36]


So what is the artistic usefulness of free art?
To understand this,
it is first necessary to take into consideration the notion of liberation as found in free software discourse.
For example,
it is common that free software supporters use the term *liberation* when they aim at freeing a software from its original intellectual property framework [@Kuhn:liberation].
Such an analogy can get particularly mucky specially when Stallman himself uses the register of the French Revolution to articulate various free software arguments,
from using stereotypically the motto "Liberté, Égalité, Fraternité" [@FSF:motto; @Stallman:libegfra] to naming a specific license clause as "Liberty or Death" [@Stallman:fsfreesociety, p. 160],
another social revolution related wording that bares a strong link with *The Terror*,
the violent post-revolutionary period in France at the end of the eighteenth century [@Wahnich:terror, The Emotions in the demand for Terror].
This is not entirely specific to Stallman though,
I have already shown in the first chapter how the UNIX community had appropriated the New Hampshire motto,
"LIVE FREE OR DIE - UNIX".
^[
See Chapter 1, Section 4.
]


But in fact software is hardly ever liberated,
because its source code is tightly locked and hidden away both legally and technologically.
Most of the time the process of liberation is instead a complete reconstruction from scratch of closed source and proprietary software.
In the same way that GNU is not a liberated UNIX but in reality a free software Unix-like operating system,
a software such as the GNU Image Manipulation Program (GIMP) is not a liberated Photoshop,
but a free software raster graphics editor
^[
All that said,
it does not mean that such liberations are without risk.
If these software re-creations build upon the principle that copyright only protects a particular implementation and not an idea,
the ideas behind copyrighted implementations can be patented.
To be more explicit,
the role of a software patent application is to articulate the software *invention* in the form of pseudo-code,
flowcharts,
or algorithm descriptions.
After that,
and regardless of the programming language used,
the author of a software that would provide a feature already patented would need to have the authorisation of the patent holder to distribute their software.
For instance in 2006,
Peter Kirchgessner was forced to stop distributing his GIMP image mosaic plug-in because he had received cease and desist request from a patent holder who argued the plug-in was infringing his patent rights,
[See @Kirchgessner:mosaic].
For more information on software patents and free software, see [@Lucarini:patent].
].
English critical theorist Matthew Fuller even argued that as a result,
such forms of liberation are free yet not related to free thought given their submissive relationship with standards set by others [@Fuller2003, p. 25].
Here again the territoriality implied by the division between the *Gemeinschaft* and *Gesellschaft* is visible:
software freedom is first a framework to write free software in a different realm to the one in which it may find its inspiration.
However if anyone is free to *use* the free software template to write free software,
the *usefulness* of the latter,
and therefore whether or not it can be part of the GNU project,
is left to be judged solely by Stallman [@FSF:eval].
Here,
utility is not relative to the value of the software for its user or author 
^[
Where usefulness depends on the context, see [@Knuth:literate, Computer programming as an art].
],
but it is on the contrary relative to the software environment itself,
that is to say the common and functional components,
such as the GUI,
the kernel,
the text editor,
the shell,
the C library,
etc [@Stallman:fsfreesociety, p.10],
so as to provide a "coherent" operating system [@FSF:eval].
However this definition quickly reaches the problem of a lack of definition for these *commonness* and *coherent* functionalities,
allowing software *usefulness* to be something very arbitratry.
In the end,
it is possible to classify free software in three categories:
first, GNU software,
that is "useful programs" approved by Stallman to be part of GNU [@FSF:eval],
and for which copyright may be transferred to the FSF if the authors want the organisation to enforce the GPL for them;
second,
regular "useful free software" [@FSF:directorywiki],
that is free software submitted to the Free Software Directory (FSD) and "reviewed and approved and published by administrators" [@FSF:FSDentry] of the directory but not part of GNU;
third,
everything else released under a free software license and which I would call *in the wild* free software,
or maybe even *free* free software,
given their existence outside of the FSF walled garden or for which the FSD seal of approval was denied.


It is astonishing that such a hierarchy presents a similar structure to that I described earlier with the classification of art during the French Third Republic,
namely academic art,
official art,
and free art.
It shows again that as I argued in the previous chapter,
the most interesting property of free software is its ability to recursively appropriate the structures it criticises,
and provide a template model for others to do the same.
So it is not surprising that free art also works similarly on three nested levels:
first,
free art produced as interactions between the members of the Copyleft Attitude group [@Bruge:artlibre, A.3 Le Réseau Créatif];
second,
any free art that has been submitted to the free art directory "Liste des Oeuvres"
^[
More precisely the website http://oeuvres.artlibre.org that has been active between 2005 and 2014.
],
and moderated by Copyleft Attitude members;
and third,
every other works released under the FAL and that I would also call *in the wild* free art or *free* free art.
However,
if at first this seems to be a direct mapping of free software ontology onto free art,
it is rather yet another proof that when the free software template is appropriated,
this process becomes an opportunity to make this model work for other ideological manifestations.
So,
in the case of free art the functional usefulness is not used as a criteria to categorise the works created.
It is replaced instead by questions of morals and ethics as a means,
according to Moreau,
of gaining freedom from and keeping at a distance a libertarian conception of freedom.
^[
Email to author, November 13th, 2015.
]
Said differently,
anyone is free to create new or engage with existing free art works at any of these three levels,
yet their acceptance and visibility within the group will not be based on their usefulness---that could be understood here as either the value they represent in terms of contributing to a coherent body of work, 
or as the ability of the work to interface with or be used with others---but instead whether or not they match the aesthetics and idelogical perspectives of those who have initiated the FAL.


However,
that does not mean that there is no functional usefulness in free art,
but the latter exists at a higher level.
Indeed,
the usefulness resides in its ability to liberate art from artistic criteria [@Moreau:phd, p.549],
to emphasize "surrartistiques" [@Moreau:phd, p.554] practices,
that is to say practices that do not have any *a priori* notion of what art can be [@Moreau:phd, p.554].
So unlike anti-art which recognises its existence as within the boundaries of art with which it establishes a dialogue [@Cramer:anti, p. 24],
free art aims to settle elsewhere,
in a way that is neither supra-artistic,
nor autonomist [@Moreau:phd, p.553].
Thus the usefulness of free art is in its ability to create a space for such *surrartistiques* practices.


The logical next question is why these practices would need to be protected in such a way.
When it comes to the place held by artistic practices within culture,
Moreau points that "[l]'art est à la culture ce que l'inter-dit est aux dits" [@Moreau:interdit].
So while avoiding establishing a strict hierarchy within culture,
he nonetheless considers art as a particular expression,
by arguing that the latter is to culture what the inter-said is to what is said.
Here,
he is referring to a concept from French psychoanalyst Jacques Lacan [See @Lacan:subversion] which addresses the discontinuity of the signifier.
Lacan used the term discontinuity to describe the existence of in-between spaces,
as well as the holes in the structure of language,
that give clues to the presence of hidden truths,
as well as their forbidden characteristics by making a wordplay with the French adjective *interdit*.
Thus according to Moreau,
art is working in a similar manner with culture,
with the former helping to perceive the underlying structures of the latter.
But it is also,
almost in a psychoanalytical sense,
a staging moment,
an impulse of avant-garde nature that eventually radiates throughout other cultural fields. 


> This distinction is essential to understand the conflictual relationship between art and culture,
> and appreciate its necessary martial game.
> This allows to better grasp that the exploration of limits,
> that is peculiar to art,
> sets into motion the cultural edifice at the risk of collapsing it.
> Also,
> it seems to me that the artistic practices that must inscribe themselves in the limited frame of culture cannot be subject to any cultural policymaking.
> The breach that the budding artist will create in the cultural field must be accepted and appreciated.
> Except a grain falls into the ground and dies,
> it is a whole harvest that becomes sterile.
> The young artist is a grower who denies productivist and positive culture.
> Far from being nihilist,
> they are the critical and constructive telling of what makes culture alive.^[
My translation.
Original text: "La distinction est indispensable pour comprendre le rapport conflictuel qui existe entre l'art et la culture et en apprécier le nécessaire jeu martial. Cela permet de mieux saisir que l'exploration des limites propre à l'art met en branle, au risque de l'écroulement, l'édifice culturel. Aussi, il me semble que les pratiques artistiques qui doivent s'inscrirent dans le cadre borné de la culture ne peuvent faire l'objet d'une politique culturelle. Qu'il faut accepter et apprécier la faille que l'artiste en herbe va créer dans le champ culturel. Car si le grain ne meurt, c'est toute une récolte qui devient stérile. Le jeune artiste est un cultivateur qui nie la culture productiviste et positive. Loin d'être nihiliste, il est le révélateur critique et constructif de ce qui fait la culture vivante"
[@Moreau:interdit].
]


For Moreau,
the dynamics of the contemporary art market and the institutions that support it are a threat to a culture that is alive,
and in his analysis this very life depends on the ability of the artist to constantly challenge and renew its foundation.
This is why the idea of art that escapes art criteria is important in the free art discourse,
because these criteria are understood as imposed rules that will dictate the art practice,
which in this case would contribute to the failure of cultural renewal.
In the above quote in particular,
he plays with the notion of field theory,
*la théorie des champs*,
by French sociologist Pierre Bourdieu [@BourdieuWacquant:champs],
in order to make an analogy between the sterile harvest of a field from which the exploratory practice of the youthful cultivator is denied,
with the becoming of a productivist cultural field that cannot perceive the fundamental role of the artist,
and the usefulness of the cracks,
pits,
and various disruptions created in the process. 
Because this role is sensed as threatened,
the FAL exists to secure and enforce this space in-between,
so as to make room,
and to create protective and nurturing pockets of artistic freedom in the interstices of a fabric woven by the politics of the culture industry.


If the FAL is indeed a useful tool,
for Copyleft Attitude it is not a tool for useful productivity,
that is to say an efficient and productivist cooperation,
as seen with the engineering and prototyping cultural properties of the GPL and the focus on promoting *useful* programs,
it is instead a tool to critically engage with these modes of production within the cultural field.
In a strange twist,
I think that without realising it,
free art is not a cultural juxtaposition of free software but its cultural counterpoint,
maybe an unknowing opponent.
Free art really is a space of intervention,
that seem to share some utopian similarities with the Temporary Autonomous Zone (TAZ),
an idea developed in 1991 by American anarchist writer Hakim Bey,
who then promoted the techno culture-derived idea of creating autonomous territories at the edges of control structures,
literally operating as *zones inter-dites* in the network [See @Bey:taz].
However,
something is very different here,
and Moreau does not have a very high opinion of such undefined approaches which,
according to him are basically ignorant of the subtleties and reality of the network [@Moreau:phd, p.243].
This difference is the strict territoriality generated by the constituent effect of the couple definition-license.
Here,
free art as a counter-hegemonic force,
goes beyond the performativity and resistance by trying to plant its own ideological state apparatus [In reference to @Althusser:ideologie].


Similar to the copyleft hack from Stallman,
that creates software freedom by establishing a relationship with the same copyright that confines it,
Moreau and the participants of Copyleft Attitude,
challenge the spectacle by becoming visible and legitimate within its own apparatus.
Moreau,
who remains highly critical of the impact made by the work from French theorist Guy Debord [@Moreau:phd, 3.1.1.2.2.1 Revue Potlatch, anti-copyright.],
does not believe in an ephemeral,
poetical,
invisible intervention.
Instead,
in order to truly establish a territory of artistic freedom,
the FAL supporters seek legitimacy,
and the license is,
in its initial design,
promoted as nothing other than a useful tool to claim and protect an artistic territory.
So if the analogy with the cultural field,
quoted above,
seems at first to promote a neoliberal argument of the necessary disruptive role of the artist,
it actually operates differently,
because by creating a safe haven for these practices it radically differs from the process of creative destruction [In reference to @Schumpeter:capitalism] that is associated with the economic instrumentation of artists.
Similarly,
this is also the reason why free art bares little resemblance to anti-copyright practices,
and not just because of their divergent legal anatomy [@Cramer:anticopy],
but simply in terms of tactics.
If Debord was a drifter,
Moreau wants to be a settler.


To be sure,
here I mean that in order to become useful and resist the evanescence of former artistic reflection on authorship and intellectual property,
free art needs to make a pact with the devil. 
In particular if on the one hand the digital aesthetics modelled by CAE,
inspired by Lautréamont's ideas [@CAE:recombinant],
proposed a practice against an authoritarian capitalisation of culture and for the free circulation of ideas within the network,
free art on the other hand had to make legitimate the machine responsible for this very same authoritarian capitalisation in order to implement such practices in legal code.
While anti-copyright practices intended to ignore legal constraints,
free art instead tries to manipulate the legal system to occupy permanently the cultural landscape,
and is establishing,
just like free software,
a kingdom within the kingdom,
in the form of yet another *Gemeinschaft* emulation.
Here thus appears a paradox.
Using terms from American activist David Bollier,
in order to support the creation of an "unregimented work space" [@Bollier:silent, p. 9]---as part of a process to reclaim the commons and to enable creative endeavours---the regimenting and bureaucratisation of such space is  unavoidable.
That said,
the control over such spaces can be implemented differently to enable,
not a complete freedom for any sort of experiments within universal commons,
but instead favour singular practices that are only common and meaningful for a few.
This is the reason why for instance,
free software and free art are structured in an opposite inverted way:
the FSF sets a broad ethical context with Stallman's GNU manifesto,
in which the question of functional usefulness of GPL'ed software permits the organisation and classification of its production,
while Copyleft Attitude sets a broad functional framework with the FAL,
in which the question of ethics is both articulated within and help structure the works created in this work space.



Artistic Freedom versus Software Freedom
----------------------------------------


So far in this chapter,
I have shown with GNUArt and the FAL two types of cultural appropriation of the free software template applied to the artistic domain.
Referring back to the previous chapter,
I have also explained the affiliation between all these appropriations and free software,
during the cultural diffusion of the latter,
and how such link became further articulated in the aggregative Creative Commons and deliberative free culture definition.
However,
in the process irreconciliable differences have appeared.
As I have shown already in the sections above,
the very transposition of free software to free art shows that notions like usefulness,
purposes,
and intentions vary greatly between the FSF,
GNUArt,
and Copyleft Attitude.
What is more,
this particular discrepancy is very illustrative of my argument showing that such software freedom has been successful in a broader context,
not because of its underlying ideology,
but because of its formula,
its template for emulating communities,
and the way it can abstract and generalise a struggle,
so that it can be applied to other cultural contexts and ideologies.


That said,
it does not mean that Stallman was completely disconnected from the development of these projects.
As with the OpenContent license from Wiley who consulted both Stallman and Raymond at the time of its creation
^[
See Chapter 1.
],
Vidovic and Moreau both corresponded extensively with the leader of the FSF,
before,
during,
and after the development of their respective projects
^[
Mails from Vidovic and Moreau to author, 2013-2014.
].
At the time,
Stallman who solely used verbatim copying permissions for the publishing of his writing---and who is now also using non-free culture CC NonCommercial and Noderivs CC licenses [@Stallman:homepage]---was troubled by the very principle of porting his software freedom to another cultural domain such as art,
and even questioned the necessity and viability of such an idea.
In particular,
Vidovic and Stallman coresponded extensively on that matter even before the creation of GNUArt.
^[
Mail to author,
May 8th 2014.
]
In Stallman's view,
it seems that cultural expressions are strictly split between objects of entertainment and tools to get things done,
with the latter possibly helping us understand the fixation on the usefulness of software.
This dichotomy should not be shocking however,
as I have shown in the first chapter,
his post-scarcity society as envisioned in the GNU manifesto is essentially a binary world where leisure alternates with work,
and this separation is naturally echoed in his definition of culture:
entertainment or software useful for working.


> For novels,
> and in general for works that are used for entertainment,
> noncommercial verbatim redistribution may be sufficient freedom for the readers.
> Computer programs,
> being used for functional purposes (to get jobs done),
> call for additional freedoms beyond that,
> including the freedom to publish an improved version. [@Stallman:fsfreesociety, p. 87]


Generally speaking and as opposed to free art,
in his view cultural expressions are *not equal*,
hence the need to classify and order them.
Such an approach also differs from the broad one in the Freedom Defined project,
where according to their FAQ,
the free culture definition concerns "works of the human mind (and craft)" [@FD:FAQ],
a category comprehending "art works, free software works, free hardware design, machine design, whatever" [@FD:FAQ],
and that is,
according to the project,
"a well-defined philosophical concept". [@FD:FAQ]
Similarly,
CC treats *works* in a broad manner,
and the CC licenses can be applied to "any type of work, including educational resources, music, photographs, databases, government and public sector information, and many other types of material." [@CC:faq2016]
Stallman however does not see cultural works and expressions as equal.
He makes a notable distinction between:
functional works,
like the software that I use to write this thesis;
representative works,
such as the text of the thesis;
and finally,
what he calls aesthetic or entertaining works,
which following the same line of example,
would be the unlikely derived dramatisation of my research into a TV series.
However,
it would be a misunderstanding to think such a classification means that Stallman only cares for mass produced forms of entertainment.
Despite his crude programmatic simplification of culture,
he is also known for sharing openly his distaste for the Hollywood film industry and the like [See @Maciag:community, 1:19:00],
and is quite supportive of the filk music scene [@Stallman:fsong],
a folk derived participatory music genre linked to science-fiction fan communities,
in which derivative,
interpretative,
re-enactive,
and all sorts of appropriative transformations are central to this social music genre [See @Jenkins:filking].
As a result it would seem surprising for Stallman to not understand the notion of free art,
given the attempt of the latter to try to address cultural commodification.


It is interesting that unlike Freedom Defined or CC for which cultural works and expressions are equal,
and for which it is up to the author to pick whatever license may fit,
Stallman's classification instead implies that depending on the type of work or cultural expression created,
there is an optimal license to be used.
Stallman's reluctance to embrace something like free art is therefore linked to his personal beliefs and pragmatic concerns:
first,
the fear of misrepresentation or misinterpretation of one's thoughts thus protected with verbatim and non-derivative clauses,
which can be linked back to his prototyping habit to iteratively perfect the free software definition,
and carefully maintain throughout the years a growing FSF glossary which defines and explains the words he is using and those that he is not [See @FSF:words];
and second,
the problem of economics,
that is simply the issue of sustaining the production of free cultural non-software works,
for which he cannot think of any suitable model,
and explaining why regarding his category of aesthetic or entertaining works,
he believes that a reform of copyright is ultimately needed instead [See @Stallman:share].
Of course on the first point,
Stallman holds a conservative position that seems to be from another time,
yet given his knowledge of the digital medium he used to express,
publish and distribute his ideas,
I think his stance is more closely related to questions of verifiability and authenticity of documents and transactions,
as discussed in crypto-anarchist and cypherpunk circles,
rather than it is a disregard for all sorts of creative literary transformations.
Regarding the second point---the financial sustainability of free cultural production---while the development of working business plans appeared in the nineties for free software [@YoungRohm:redhat],
this is not the case for non-software and non-hardware free culture which is still struggling today to showcase convincing models of economic sustainability [@Unknown:sustain].


In the end,
the articulation of cultural freedom versus software freedom is also part of a larger prototyping process,
where nothing is set is stone or definitive,
hence today's official position of the FSF to neither clearly oppose nor endorse the notion of free art,
as expressed on their website:


> We don't take the position that artistic or entertainment works must be free,
> but if you want to make one free,
> we recommend the Free Art License. [@FSF:licensesintro]


While the FSF texts tend to strongly push forward their doctrine---as shown in the way assurance and desire to rally are articulated in the GNU manifesto---what such non-position shows,
is the ideological incompatibility of the many groups and individuals that are too often conflated into one cohesive whole.

\newpage
