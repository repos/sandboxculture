The Practice of Free-Range Free Culture
=======================================

Mattin - Production
-------------------


Basque noise and improvisation artist Mattin,
is know for his multiple music projects and identities,
as well as for a very broad range of noise related sonic experimentations and performances (Figure \ref{fig:mattin}).
There are quite a few printed and online interviews about his work as a musician in that regard,
but this is not what I want to discuss here.
Instead,
what motivated me to talk with Mattin was his very particular attention to the means of production and the active role,
mission even,
that he believes an author should have in relation to cultural productions,
and how such role relates to the use of free software.

Mattin started to use free software in 2004 while participating in Metabolik HackLab Bilbao in the Basque Country.
^[
The text from this section is based on a semi structured interview with Mattin,
on the 23rd of January, 2014, Berlin, Germany.
]
He became further engaged with these tools in contact with fellow noise artist and musician Julien Ottavi from the French APO33 collective in Nantes,
and by attending other workshops organised by other artist collectives busy with the use and the development of free software for their practice.
This is how I met Mattin for the first time,
as part of a 2004 Pure Data workshop that I was co-teaching and held at Mute magazine in London,
but it took us a decade to start a discussion about the meaning and context of the things that we were interested in. 
For Mattin,
there exists notably a clear separation between the tools used for the creation of,
the work of art itself,
its performance,
and its reproduction.
In particular,
his main concern lies in the artist's engagement with the productive apparatus.


To give a bit of context to Mattin's thought,
in his 1934 essay *The Author as Producer* [@Benjamin:producer],
German critic Walter Benjamin offered an understanding of authorship in the light of,
and at the meeting point,
of different Marxist traditions at the time.
In his text,
Benjamin especially opposed two situations, one in which the author supplies a productive apparatus without changing it,
and one in which the author engineers this apparatus to change it as part of a counter-hegemonic effort.
For Mattin,
who believes that working artists should be concerned with the means of their artistic production,
this essay has become a central reference,
and the reason why he sees a connection between Benjamin's critique of cultural production with the ideas of transparency,
openness,
and participation in the way free software is put together,
distributed,
and possibly modified by autonomous communities.
The latter can be at first surprising,
given that Mattin is not a software developer,
and in his case this revolutionary engineering position of the author could turn out supplying unknowingly another production apparatus masqueraded as a counter-hegemonic effort.
When I asked him about the way he might be romanticising free software production in one of his text [@Mattin:anti],
he admitted a certain ambivalence towards the principle of cooperation and socialisation found in free software.
If on the one hand he believes that they have the potential to challenge the autonomy of art,
used here in the context of the self-governance of art production,
he also believes on the other hand that there is a darker side to such novel forms of cooperation,
in which these efforts are in fact means of survival in disguise.
He explained that in his opinion people nowadays are forced to collaborate on software,
given economic pressure and thus reducing such co-actions to methods for individual gains.


\afterpage{%
\begin{figure}[h]
	\caption{Mattin at make art festival}
	\centering
    \includegraphics[width=0.8\textwidth]{mattin} 
	\fnote{Photo: Olivier Laruelle, 2009, CC0}
    \label{fig:mattin}
\end{figure}
\clearpage
}


Here again the spectre of Benjamin,
and his mixed views regarding the rise of technological means of art reproduction [@Benjamin:workofart],
seems to resurface,
and this may be the reason why,
if Mattin sees the usefulness of software freedom regarding the means of production of his work,
at the same time he refuses to engage with free culture when it comes to the dissemination and distribution of his production.
This is particularly visible with his music label *Free Software Series* started in 2007 [@Mattin:FSS],
where he invites noise,
experimental,
and improvisation sound artists,
who use and write free software as part of their practice.
The resulting works are available online via the non-profit public domain and free culture supporter online digital library Internet Archives,
and a limited edition of CDs are also produced.
Each release follows a template in which the invited artists list the software and tools they use.
But what is striking with *Free Software Series* is that unlike many other classic labels or net labels,
where contracts and licenses are imposed on the artist,
with this label they are free to choose how they want to legally publish their work.
As a result,
in spite of using a core free culture reference with the term free software,
Mattin's label is also typified by a miscellanea of licenses,
non-free personal and fantasy licenses,
pseudo-legal statements,
free culture licenses,
and sometimes more simply,
a single *anti-copyright* notice written on the back cover,
which also happens to be Mattin's way of distributing his own work.
Mattin accepts the legal pragmatism of free software licenses when applied to the creation of software tools,
but refuses to apply the same principle to his work,
or impose it on the fellow artists of his label.
According to him,
the notion of licenses presuppose too much about what a work of art is supposed to do,
what is an author,
a producer,
and this is simply incompatible with a practice that constantly questions this framework.
As a result,
the label is not just a platform to showcase a certain genre of work,
but also a strategy to preserve their *aura* [In reference to @Benjamin:workofart],
or at least diminish their loss,
given the agonistic pluralism of possible means of reproduction it presents.


So in the context of a free-range proto-free or free cultural art practice,
authorship needs to be scrutinised particularly in relationship to capitalism.
As Mattin mentioned to me,
these modes of distribution and reproduction imply the need to enforce and police the use of his work,
and also reveals the secret obsession of artists wondering about how they will be treated by posterity.
So instead of accepting without hesitation a normalised notion of authorship,
he seeks to trigger discussion.
Connecting back with the particular strategy seen earlier of the FAL to defuse the spectacular via legitimacy and visibility,
for Mattin the recuperation of the spectacle is part of the game,
and has been completely internalised.
He has no problem with being partly absorbed as part of a cultural agenda,
and does not see the need to resist it with licenses,
what matters is the impact noise has on a system in terms of artistic freedom and self-organisation [@Mattin:anti, p. 173].
So while Mattin refers to Benjamin,
and given his appreciation for Debord's work [See @Warburton:mattin],
maybe the engineering author would be best described in his case as a saboteur author.



Nina Paley - Product
--------------------


One thing that the cultural appropriation of free software by artists demonstrates very well is the distinction between tools and what they are used for.
This is an important point that is not necessarily visible when free culture becomes an over generalising umbrella for all things free and open.
Therefore when looking at artists operating within the proto-free or free culture discourse,
it is essential to find where these notions of openness and freedom are articulated in their individual practice.
If some practitioners,
like Mattin,
concentrate essentially on the production pipeline to elaborate their critique of the culture industry,
others do the exact opposite and instead put the emphasis on the means of distribution and publishing,
which is the case for American cartoonist and animator Nina Paley.
^[
This section is based on a semi structured interview with Paley,
on the 12th of April 2013, Madrid, Spain.
]
Paley's work needs no introduction for anyone familiar with free culture.
Her 2008 multi awards animated film *Sita Sings the Blues* (Figure \ref{fig:sita}) became the archetype of free culture success,
and more particularly the validity and relevance of CC licenses.
For the first time,
a free cultural work gained significant popularity and was screened within traditional film industry circuits.
When I asked her how she ended up releasing such a work under a free culture license,
what she told me would provide the perfect fit for a Creative Commons PR success story or an anecdote from a Lessig essay.
But as it turns out,
Paley knew very little about free software and free culture during the production of her feature film.
However, at the final stage of the work she learned the hard way how convoluted copyright and public domain could be,
and her frustration led her to the discovery of free culture.


\afterpage{%
\begin{figure}[h]
	\caption{Sita Sings the Blues}
    \includegraphics[width=\textwidth]{BhavanaSitaContaminated} 
	\fnote{Still Frame: Nina Paley, CC0, 2008}
    \label{fig:sita}
\end{figure}
\clearpage
}


She explained to me that in her work she had used samples from records of nineteen twenties American Jazz Age singer Annette Hanshaw,
that Paley thought were in the public domain.
Unfortunately,
copyrights for some technical and compositional elements of the records were still held,
and as an independent film maker she was personally liable for a $220,000 bill,
a mandatory step to settle with the copyright holders before having the work distributed.
Through negotiation and loan,
she was eventually able to license the music for a quarter of the original demand,
but what is important here is that while researching about rights clearance,
she came across the 2005 essay *The Surprising History of Copyright and The Promise of a Post-Copyright World* [@Fogel:surprise],
from American software developer Karl Fogel.
Through the essay,
that lauds without surprise the stereotypical battle of Stallman against closed source and proprietary software,
she was introduced to the general theme of free and open source software struggle.
Moreover,
she told me that she found in this struggle similarities with the problems and frustration she was facing with the final distribution of her work.
As a result,
she would eventually join Fogel in 2009 in the QuestionCopyright.org nonprofit organisation,
an effort to promote public debate around copyright and art.
With this newly acquired affinity with the world of free and open things,
she then naturally chose the copyleft-inspired CC BY-SA license for her animated film,
presenting the free cultural publishing of her work as promotional copies,
so as to avoid extra licensing from copyright holders.
No wonders why she became a poster child for free culture.


The peculiarity of her situation though,
is exemplary of the diversity of opinions that are gathered as free culture.
Indeed,
as I explained in Chapter 2,
under the free culture definition,
both the FSF software freedom of the GPL and the cultural freedom of CC BY-SA sit together in harmony.
But so much for the theory.
In practice,
and as discussed in the previous chapter with Stallman's own resistance to publish his texts under free culture licenses,
the reverse situation also exists:
Paley does not directly use free software in the making of her work.
What is more,
she explained to me that she was still using the now dated 2005 Macromedia Flash 8 software,
^[
A very specific Mac OS version of the multimedia authoring software which at the time of our interview was owned by Adobe and was at version 14.1.
]
even though many new version have been released since then.
Paley said she was sticking with this particular version because of a feature that was removed in later iterations of the software.
As a response,
her position was to resist the idea of upgrading this software for the sake of it,
refusing to buy more expensive versions,
that,
while claiming the usual productivity increase argument to justify bumping a version number up,
whilst actually her oppinion supporting a counter-productive system.
So even though this story is the kind of anecdote on which the FSF and OSI usually build their narrative of software liberation and vendor lock-in
^[
That is to say that the top-down decision to remove the feature is a typical disadvantage of closed source software,
in which little to no control is given to the users.
],
Paley,
instead of jumping in the free and open software bangwagon,
twisted the situation by continuing to use her old proprietary non-free software to create new free culture.


When I asked her why not simply drop closed source and proprietary software and use free and source software instead,
I received in response a rather bleak personal view of the state of free software animation tools.  
Put simply and according to her,
she would not be able to create anything with the same fluency as she had with proprietary tools.
Paley told me that she had purchased in the past a machine dedicated for running and learning how to use a GNU/Linux distribution,
but this did not work out for her.
She was however not ready to give up just yet,
and this is why she joined the 2013 Libre Graphics Meeting (LGM) in Barcelona,
a meeting for developers and users of free and open source software in the realm of type,
graphic design,
illustration,
and also the place where our interview took place.
For her the conference was an opportunity to find renewed motivation,
and to investigate the possibility of contributing financially to the development of specific free animation tools.
However,
at the end of the event,
and after several discussions triggered by the presentation of her work in relation with free software and free culture,
she found herself forced to admit that based on her needs,
it would still take years to be as proficient with free software as she was now with closed source tools.
According to her,
one of the main problem she identifies with current free software animation tools is that there are simply not enough developers,
and not enough moments of development in which users and developers can interact with each other,
not remotely using IRC,
mailing lists,
or web forums, 
but face to face in the same room,
for extensive periods of time such as a month or so,
so as to thoroughly test and implement a particular feature that is relevant for the user.
Sadly,
Paley's frustration with free software is not simply anecdotal.
It remains an unfortunate illustration that switching between tools,
and more particularly substituting known non-free software for their supposedly equivalent free counterparts,
is easier said than done,
particularly for the type of free and open source software that aims at being a near drop-in replacement for known elements of a production pipeline,
that has been for decades the closed and exclusive territory of the software industry
^[
For an overview of the usual challenges and obstacles for the adoption of free and open source software,
as a replacement for existing closed source and proprietary workflows,
see [@AlRoumi:migration].
].
Paley does not take this issue lightly and she told me it was the cause for considerable suffering,
not only because she greatly admires the free software movement,
but mostly because she admits that out of all her supporters,
free software users are the ones who understand the most what she is trying to do with free culture;
in her own words it was "painful in that way to not use free software, but the most important thing is the art."
Unfortunately,
instead of triggering a discussion,
her presentation during LGM essentially attracted critique from the more dogmatic free software supporters and developers,
who could not comprehend why a non-free but expressive tool could be superior to a free but less expressive tool for Paley.


Next to the question of expressibility,
Paley told me that what matters the most for her is access to the work.
She has no doubts that if a work is liked,
it will be copied no matter what,
and instead of controlling this aspect she wants to *concretely* encourage it,
hence the focus on publishing the work with free culture licenses.
She said that letting go of her intellectual property was the best decision of her life,
because in her view copyright is nothing more than a device of censorship and the inhibition of creativity,
which is the same point made in the GNUArt project discussed previously.
In addition,
she argued that intellectual property rights do not only impact artists,
but society as a whole because they encourage ludicrous efforts to keep alive and scale systems of surveillance,
control,
and penalisation needed to enforce them
^[
One notable source of reporting on such issues is the blog TorrentFreak, started in 2005,
and still very active at time of this writing.
[@Renkema:TF].
].
Paley believes this will only lead to increasing civil unrest,
an opinion that overlaps with United Nations (UN) reports from Pakistani sociologist and activist Farida Shaheed,
who has recently articulated several sharp critiques of intellectual property from the perspective of human rights
^[
See [@Shaheed:statement; @Shaheed:report].
].


In that sense,
in the case of Paley,
free culture became a transitional device to help articulate her artistic intention and connect her practice with cultural rights.
I use the term transitional because today the cartoonist and animator does not present herself as being a copyright reformist,
but as a copyright abolitionist.
She explained to me that she also decided in 2013 to re-license *Sita sings the Blues* under the much more permissive CC0 license,
another CC and free culture definition approved license.
CC0 is more of a legal tool than a typical free culture license,
permitting the waiver of as many rights as possible and is currently considered a valid free software,
free culture approved license,
and conformant license for the Open Definition.
It is however not approved by the OSI for the distribution of open source software
^[
The hesitation from the OSI to list CC0 as an acceptable open source license comes from the fact that CC0 essentially works in two steps:
first it tries to waive as many legal rights as possible to reach a near public domain status for the work;
and a second optional step,
because the first step might not be possible in every jurisdiction,
CC0 provides a fall-back open source like license as permissive as possible.
The issue for the OSI is that this fall-back license explicitly convey protection for patent rights that may be linked to the licensed work. [See @OSI:CC0].
].
According to Paley having her work under CC0 is still suboptimal,
in regard to her intention to let go of the entirety of her intellectual property,
but it was the only way to be as close as possible to the public domain.
Going back to this idea of free culture as a transitional device,
what the work and experience of Paley shows is that despite the territoriality of free culture licensing,
that was particularly explicit in the counter-hegemonic position of the FAL,
these forms of distribution and publishing are not necessarily definitive.
Far from the cultural epicentre of these licenses,
artists and their work are able to move from one ideological perspective to another more freely,
making free culture more of a strategy rather than an end in itself.




Stéphanie Villayphiou and Alexandre Leray - Process
---------------------------------------------------


If some practitioners tend to lean towards one particular side of production,
or distribution of their work,
others seek a certain balance in which their engagement and critical response is equally represented in both their tools and what they make.
To be sure,
I am not referring here to some sort of ultra dogmatic position that would seek to impose a free cultural purity in both the production and the product.
As presented earlier,
this section looks into what I described as free-range free culture practices,
outsider free cultural producers,
that is to say participants from the second stage cultural appropriation of the free software template by artists,
and therefore far from the cultural epicentre and control from which these licenses and definitions have been written in the first place.
As a consequence the alignment here of free software tools and free cultural work should not be understood as an extreme form of free cultural practices,
but more as an opportunity to redefine an existing artistic practice or genre [See @deValk:tools],
making in fact free culture an *un-artistic* process 
^[
In reference to American artist Allan Kaprow's concept of the "un-artist".
See [@Kaprow:unartist1; @Kaprow:unartist2].
].


To illustrate this process,
I will now share elements of a discussion with French graphic designers Stéphanie Villayphiou and Alexandre Leray.
^[
Some of the following writing is based on a semi-structured interview with Stéphanie Villayphiou and Alexandre Leray,
on the 5th of March, 2015, Brussels, Belgium.
]
I first met Villayphiou and Leray in 2007 when they were students in the Media Design and Communication Master,
at the Piet Zwart Institute (PZI), 
in Rotterdam,
Netherlands.
After their graduation in 2009 they started the graphic and media design studio *\<stdin\>*,
and joined the Open Source Publishing (OSP) collective between 2009 and 2010.
Since its infancy in 2006 as a branch of the Belgian art and culture organisation Constant
^[
The group was initially formed in Brussels, Belgium,
by a mixed group of graphic designers, artists and software programmers, 
Pierre Huyghebaert,
Harrisson,
Yi Jiang,
Nicolas Malevé,
and Femke Snelting.
],
OSP is a project that reflects on questions of design,
process,
and tools in the realm of graphic and print design using only free and open source software [@Fuller:blog:osp].
The particular focus of OSP,
now an independent group,
was not totally strange for the two designers.
Villayphiou and Leray told me that they had both been attracted to the culture of free software since their teenage years,
but not immediatly in the context of unlearning and relearning graphic design practices,
but rather as a knee-jerk reflex against the dominance of a very few software publishers in the late nineties.
In fact Leray even recalled that as a high school student,
he once stuck anti-Microsoft stickers depicting the Linux kernel penguin mascot,
Tux,
on the Windows-equipped machines of his school computer classroom,
even though at the time he had *never* used free software himself.
Tux,
the content looking penguin,
had reached the same status as other rebellious symbols such as images of Che Guevara,
Bob Marley,
or The Sex Pistols,
thanks to the free software and the GPL romanticised David against Goliath narrative regularly covered in popular tech magazines.
Here,
the abstraction of struggle offered by the free software template would once again become the main grip for its further appropriation by a younger generation with no direct relation with engineering culture or software development.


I do not mean to say that the relation that Villayphiou and Leray had with free software was entirely superficial or naive,
but it was an entry point that would lead to their individuation as graphic designers reinventing their practice.
In concrete terms,
they explained to me that during their formal education,
their particular interest in web design,
in which both mark-up and programming languages are exposed,
was instrumental in making them favour open standards and systems.
From this point,
they were interested in linking programming and generative graphic design,
something they first did during their study at the École supérieure d'art et design de Valence (ESAD Valence) in France,
and then explored further at PZI.
The latter course being particularly both sensible to and critical of free and open source software culture [@CramerMurtaughMansoux:oscon],
they were able to articulate more precisely what they found problematic with proprietary and closed source software,
turning their attraction to the free software abstracted struggle into something meaningful and relevant to their practice.
Villayphiou and Leray told me that one of the most important issues was the normalisation of the skills and workflows in their line of work.
This aspect was particularly important for the two designers,
who had understood the 2006 disappearance of graphics,
web,
and design software publisher Macromedia,
bought by Adobe,
as the rise of a monopoly that would dictate through a single toolchain the means and methods of their professional activity.
If this same incident had handicapped Paley,
and forced her to no longer upgrade her animation software,
this vendor lock-in and top-down standardisation was instead an opportunity for Villayphiou and Leray to reinvent their practice,
and redefine graphic design.


The important distinction to make here is that the notion of freedom and openness is not at the level of the source code,
but at a higher level,
both in term of diversity and interoperability.
In that regard,
Villayphiou and Leray's position is much closer to the core principle of the Unix philosophy and its modular pipeline,
than to the software liberation agenda of the FSF.
Indeed,
Villayphiou and Leray told me that they were not so much interested in using free alternatives to popular raster and vector graphics editors,
such as GIMP for Adobe Photoshop and Inkscape for Illustrator.
They had no particular hatred per se for any proprietary graphic design tools,
and their question of empowerment goes further than investigating the ownership of tools.
They are rather calling attention to the way the software industry increasingly replaces the gesture of the practitioner with frictionless suites and workflows.
From early on,
it has therefore also been important for OSP to perform the practice of design publicly,
metaphorically with their participation in free software communities and also literally with their *Print Party* series of events,
as an attempt to break the illusion of the perfect and idealised motion found in the factory-like choreography between a dehumanised designer and their tools,
as one of the founder of OSP,
Dutch artist and designer Femke Snelting,
once articulated [@Snelting:gesture].
Their aim is to embed the reflection and experimentation with all sorts of free and open source software,
as an indispensable element of design research.
They admitted though that what they described as "ceaseless gymnastics",
was sometimes horrifying in the way it forced them to constantly unlearn and relearn new things.
What might seem borderline masochist and unproductive given the self-imposed difficulties and challenge here,
is in fact typical of the way some artists and designers engage with free software [@deValk:tools].
If there is fetishism,
it operates at the level of a careful and individuating exploration of uncharted territories,
in order to challenge the critical understanding of their craft.


Of course,
such a position seems highly precarious given the competitive labour standards imposed by today's neoliberal economics.
When Paley decided to stick with proprietary software,
next to the question of expressibility of the instrument,
there was also the very pragmatic issue of time management and productivity.
When I asked them about this matter,
they told me that for them this was irrelevant because their main interest was not in streamlining an assembly line,
but of integrating thoughtfully the means of production for every single project (Figure \ref{fig:osp}),
and learning from each other at OSP without any hierarchical construction or roles that are determined by the type of tools they use,
and the type of skills assumed by their profession.
In their own words,
this is not about "emboîter des boîtes à l'usine"
^[
"putting boxes in boxes in the factory".
].
It does not mean that they live disconnected from the reality of the graphic design market,
but it does mean instead that time,
labour,
and energy are planned differently when they are commissioned.
Unlike the other examples of free software appropriation and inspiration seen so far in this chapter,
the link between OSP and free software can be understood as the twenty-first century resurgence of the late ninetieth,
and early twentieth century Arts and Crafts movement.
The way the collective work echoes the critique made by English social thinker John Ruskin,
and by which the movement was inspired,
on the issue of the division of labour brought about by the industrial revolution,
and more particularly how works produced in factories are unfair and dishonest.


\afterpage{%
\begin{figure}[h]
	\caption{Snapshots from OSP design process and tools}
    \includegraphics[width=\textwidth]{osp} 
	\fnote{Photos: OSP, FAL/CC BY-SA, 2014}
    \label{fig:osp}
\end{figure}
\clearpage
}


For Ruskin, 
if the question of tool ownership is important,
a more central aspect is the context in which they are used.
He therefore makes a clear distinction between things produced manually,
and those which come from a factory. 
According to him,
the latter are *lying*,
they pretend to be the result of a thoughtful process,
but unlike the outcome of craftsmanship they do not leave any record of intents,
trials,
successes and failures. [@Ruskin:seven, pp. 43-45]
In that sense,
even though practitioners like Villayphiou and Leray adopt a modernist stance in their desire to reshape graphic design,
there is at the same time in their approach,
an underlying connection to a more historical form of hacking where crafting plays an important role [@Levy:hackers].
So,
while their practice exists within the second stage cultural appropriation of free software,
and well after the code brutalism [@Yuill:brut] of some practices in the proto-free culture era,
the process they articulate is aligned with the notion of "fanatical devotion to beauty" [@Graham:painters, Hackers and Painters, p. 29] in software writing,
the idea that hacking becomes a medium like painting [@Graham:painters],
in which writing beautiful programs is an art form like composing poetry or music [@Knuth:literate, Computer Programming as an Art],
and where pleasure is found in using and writing such tools [@Knuth:literate].
However such art becomes itself linked to the notion of un-art I refered to earlier,
because it creates a situation of  unlearning and relearning relative to the skills assumed to be required in their work,
while simultaneously promoting their practice as a very classical definition of artistry.


As mentioned earlier,
an interesting aspect of OSP is that the works that they are commissioned to design,
and the tools they might create as part of the process,
are all made available under free culture licenses:
the software is usually released under the GPL or the GNU Affero General Public License (AGPL);
their fonts are made available under the SIL Open Font License (OFL);
graphics works use the double licensing FAL and CC BY-SA.
Properly licensing their work is a matter of ethics for Villayphiou and Leray,
who believe that this mode of publication helps get rid of the myths of artistic geniuses and ex nihilo creations.
This point seems paradoxical given that craftsmanship has been anyway historically positively associated in hacker culture with individual recognition and talent [@Levy:hackers, pp. 99-100],
but for them free culture licensing is a statement on copying which,
unlike appropriation art,
is made to respect and honour the notion of authorship.
As they explained to me,
their decision to use copyleft licenses is also a conscious choice to force sharing,
^[
I use the term *force* precisely to highlight the mechanism of copyleft licenses,
where *sharing* of modifications is mandatory when the work is transformed and published.
]
and a way of paying tribute to all the free software tools they rely upon themselves.
Once again,
the plurality of voices in free culture becomes visible.
If free software became a template for cultural appropriation that led to the creation of the FAL,
the FAL itself becomes in turn a new template to be appropriated and re-contextualised in other practices.
That's why for OSP members who are not linked to Copyleft Attitude,
the idea of the FAL as a critique of the visual contemporary art market,
becomes entirely replaced with a critically reinstated testimonial to authorship and craftsmanship.
What is more,
this re-contextualisation gets further fragmented within the designers collective itself,
as Villayphiou and Leray commented on the fact that other participants in the collective are not comfortable with the distribution of their work under the FAL,
which they believe is too niche,
and therefore would "require an extra act of contextualisation and seduction to convince [their] public that is the right instrument for the job." ^[Email from OSP member Eric Schrijver to author, January 3, 2016.]
For Dutch graphic designer and OSP member Eric Schrijver,
there is also the problem that neither CC licenses,
nor the FAL,
reflect properly his philosophical convictions,
and in particular the fact that defining authorship in a work is an arbitrary process prone to be influenced by socio-political bias or economic opportunism.
Schrijver prefers nonetheless to employ CC licenses because of their existing visibility in the cultural field,
and is willing to make a tradeoff between the precision of the intention and the effect his work can have when distributed via more popular free culture licenses. ^[Email to author, January 6, 2016]
This is the reason why OSP decided to dual license some of their assets under the FAL and the more popular CC BY-SA,
even though the two have been recently made compatible in 2014 [@CC:FAL],
hence creating via licensing itself a meta-discourse on top of their own personal interpretation of what such licenses stand for.
Similar to the example of the GCC and Clang compilers discussed in Chapter 1,
and the double inflection of their licensed source code,
the same duality repeats itself here again.
The meaning and the intention of using or making a free cultural work or expression will vary strongly depending on its licensing.
Even though the FAL and the CC BY-SA are already re-contextualised in this second stage appropriation,
that is to say that their purpose in the context of OSP have diverged from their original purpose,
they are still distinct paratextual elements that translate into different meaning.
What is unique though,
is that OSP does not see this as an issue,
and their choice becomes an echo to the plurality of voices that form the collective:
they do not have to settle for one single license but use dual-licensing instead to highlight the cultural diversity within the group,
and put in place concurrent strategies of distribution.


Finally,
if sharing knowledge and learning,
the record of its gesture,
is the most important part for the two graphic designers,
they also recognise that this is also a point of friction in a collective where the use of software constantly challenge this social dimension of their craft.
With the engineering and industrial context of their tools,
appears therefore an overshadowing bargain between the intimate writing of software to be used once for a specific work,
and the maintenance and documentation of more reusable and efficient production frameworks
^[
For an extensive analysis of the relationship between time and expressibility in open source software written in the context of art practices, see [@Magnusson:expression].
].



A Note on the Artistic Appropriation of the Free Cultural Discourse
-------------------------------------------------------------------


With the examples of Mattin,
Paley,
Villayphiou,
and Leray, 
I have shown that the relationship with free and open source software and free cultural licensing,
exists under the form of a well formulated partisan choice,
upon which the accent of their engagement varies according to the needs of their practice,
and according to their habitus,
even if this articulation is not ideologically aligned with the frameworks they are using.
There are however,
other forms of appropriation of the free cultural frameworks which I will address in the last section of this chapter. 
I want to mention the existence of works,
in which the affiliation to free culture and similar ideas is not only paratextual,
but made explicit by the material used for making the work.
The most interesting aspect of such works is how they turn inner and idiosyncratic discussions within the realm of free and open things,
into an artistic medium.
To illustrate this,
I have chosen to discuss three works, 
*Save the GNou!*,
*Fibre Libre*,
and *CC Ironies*,
all of which appropriate the free software principles at different levels,
suggesting that if there is such a thing as free culture aesthetics,
it does not limit itself to the sole question of licensing.


### Save the GNou!


As explained in the first section of this chapter,
nineties new media art,
and net art in particular,
had championed a new form of digital appropriation art where borrowing,
plagiarism,
stealing,
and quoting became both a method and instrumental in the development of network aesthetics operating as a smoke screen for all sorts of intentions [@Bosma:netart, A Deeper View].
Abusing such a smoke screen was notably the principle tactic for irreverent French group pavu.com
^[
An acronym for Popular Arts Value UPgrade,
where *UPgrade* is used to refer to the transforming methodologies used by the appropriation artists,
who described their formation at the time as a *startUPgrade*.
Parts of this section are derived from email exchanges with pavu.com member Jean-Philippe Halgand,
during March 2013, and January 2016.
],
which collection of automated translation assisted *Frenglish* pseudo entrepreneurial and pseudo avant-gardist web sites presented as "territoire libre du Net" [@Rivoire:pasvu, 01/03/02] was once described as the craziest part of the French cyberspace [@Rivoire:pasvu].


Launched in 1999,
pavu.com is presented as an establishment specialised in "arts informatifs",
a concept described at the time by the group as a form of artistic engineering and semantic reframing,
operating on top of information perceived as raw material and that could be extracted outside of its original context [@Saporos:artsinfo].
Shortly after its creation,
pavu.com released its *MilleniumFlower numerical bouquet* (MFL)
^[
A pun referring to both the Mayflower event,
and the marketing term *bouquet numérique* used in France for the offering and combination of several digital broadcasting services and products,
email to author, January 2, 2016.
],
under the form of five events and projects which were announced over five days,
on several net art and culture mailing lists such as rhizome,
syndicate,
nettime,
and 7-11.
The series of works ranging from different forms of artistic appropriation,
commercial exploitation,
and commissioned advertisements for other net artists,
coincided with a time where the question of privatisation of the Internet was becoming more pressing and started to impose limits to the new networked artistic territories claimed by tactical media practices that had escaped the artificiality of the white cube but now found themselves exposed to corporate regimes,
as best exemplified with the 1999 and 2000 domain name *toywar* between the digital art group etoy and the online retailer eToys [@Grether:etoy].


\afterpage{%
\begin{figure}[h]
	\caption{Dépôt marque Française COPYLEFT}
    \includegraphics[width=\textwidth]{copyleftinpi} 
	\fnote{Extract from Bulletin officiel de la propriété intellectuelle, 99/42 NL, VOL.I, 1999}
    \label{fig:copyleftinpi}
\end{figure}
\clearpage
}


If it is well beyond the scope of this text to cover in depth the whole MFL
^[
Or pavu.com for that matter.
For more information,
the following link lists several texts and interviews which should give enough clues to start deciphering,
or not,
the world of this collective. [@pavu:theory].
],
one of its five parts is however connected in a strange twist with the notions of territoriality and copyleft discussed in Chapter 3.
This particular part entitled *Save the GNou! The Copyright The Copyleft World Campaign*,
was created as a sort of pseudo-hacktivist mock-critique proto-free cultural campaign,
using the free and open source software discourse as material and inspired by late nineties email chain letters ^[Email to author, January 2, 2016].
At the core of the propaganda was the call for the protection of a threatened species called *GNou*,
the French word for the Bovidae gnu,
and of course also the mascot since the eighties of the GNU project. 
The protection campaign started notoriously in September 1999 with the trademark (Figure \ref{fig:copyleftinpi}) of the word *copyleft* in France [@ChabrelyHalgandLaporte:copyleft].
Later in 2000,
new variations on the theme were added,
in a form of a GNou Found Lands (GFL) organisation "fighting attempts to destroy Networked GNou free territories",
and "[p]reventing server space from becoming hunting companies' monopoly" [@pavu:gfl].
Eventually,
it was announced that to expand the *GNou Reserve* one could acquire a plot from the GFL
^[
The *GNou Reserve* term itself was linked to yet another Frenglish play on words derived from typical copyright notices:
"Tous contents CopyGNou 2000, pavu.com, All Right ! GNou Reserve".
],
in the form a small graphic file depicting a gnu,
to be inserted and hosted on one's website.



If French semiotician Roland Barthes had visited one of the websites from pavu.com after writing about the TV ads of French pasta brand Panzani [@Barthes:obvie, Rhétorique de l’image],
he may have rephrased the practice of "arts informatif" in simpler terms,
that is to say the art of stripping the symbolism of an iconic message so as to create confusion and deception given the pre-existing connotation system within the appropriated material.
Here,
even though the resulting collage is also a reaction to cultural transformations and privatisations happening at the time,
instead of deriving the free software template for their own purpose,
pavu.com treated the FSF discourse and imagery as raw material to play with.
The members of the group,
who did not know of the existence of GNUArt and who did not read the FAL when it was released in 2000 ^[Email to author, March 11, 2013],
but however had learned about copyleft from Moreau prior to the existence of Copyleft Attitude [@Moreau:phd, p. 614],
obviously operated as pranksters and trolls.
While such manoeuvres were known in the field of tactical media
^[
Known and popular,
however also ambivalent in their potential political impact that could be questionned.
For a short discussion on that matter,
see [@Dery:CAE, pp. 27-29].
],
they did not impress Moreau,
who will eventually qualify the trademarking of copyleft as an example of the artistic stupidity,
in reference to a comment by French artist Marcel Duchamp in relation to un-reflective traditional art making [@Moreau:phd, p.614-615].
Regardless,
looking today at the campaign from pavu.com it is undeniable that it both pre-dates and is announcing a wider and more globalised form of "arts informatifs": Internet memes.
As it turns out,
on popular western imageboards like 4chan's /g/ or 8chan's /tech/,
to name the biggest at the time of writing,
the practice of appropriating and radically transforming the free and open source software discourse happens on a daily basis,
with little regard to factualness or coherence,
except for those practising it and who use it as part of specific visual grammars and symbols,
constantly providing a counterpoint in near real-time to whatever happens in the world.



### Fibre Libre



If the approach of pavu.com to free culture relies on stripping away symbolism from its iconic message,
other artists do the exact opposite by removing the literal elements and make use of the remaining symbolic images.
By working with a material that is only a system of connotation,
and which has lost its literal counterpart,
the idea of free and open source software as well as free culture,
becomes a means to create new works at a metaphorical level,
and inspire methodologies to explore and frame existing practices from a new angle.
To be sure,
this is different from the cultural appropriation of the free software template discussed earlier.
There is no literal functional adaptation but instead an inspiration to understand and observe free software through the lens of an existing practice.


This is the case with *Fibre Libre* (Figure \ref{fig:fibrelibre}),
a 2009 event and artist's book,
that documents the efforts of a group of people learning about free software while also learning how to make paper. [@Elmer:fl]
It was initiated by American bookmaker and letterpress printer Bridget Elmer as part of Open Edition.


> Open Edition was founded in 2009 by Flatbed Splendor to explore the philosophy of FLOSS (Free/Libre/Open Source Software) through the medium of the artist's book.
> In this exploration,
> the artist's book is considered both for its potential as a free information technology and as a free cultural work.
> As such, Open Edition is an attempt to extend the Free Software Foundation's "four essential freedoms" to the users of the artist's book,
> integrating FLOSS with the art of making books by hand.
> Open Edition advocates for the understanding and use of free software,
> particularly in the book arts community,
> by supporting relevant practice,
> scholarship and pedagogy. [@Elmer:fl] 



\afterpage{%
\begin{figure}[h]
	\caption{Fibre Libre}
    \includegraphics[width=\textwidth]{fibrelibre} 
	\fnote{Photos: Bridget Elmer, 2011, FAL}
    \label{fig:fibrelibre}
\end{figure}
\clearpage
}



Elmer initially discovered bookmaking via the zine and self-publishing culture,
and saw a lot of similarities between those writing free software,
and "those printing their books from redistributed lead type on the letterpress [...] or making books of their own creation on a copy machine".
^[Email to author, April 23, 2011.]
*Fibre Libre* is a way for Elmer to work through the similarities and differences of the two cultures,
as a means to understand what free culture is about,
and share this with her friends and the artist's book community.
The resulting book,
which is limited to fifty handmade copies and published under the FAL,
represents a narrative that unfolds both in space and time.
It derives an idealised understanding of the mechanism of collaboration and cooperation found in the production of free software,
to permit another reading of what is constituent of book making that both highlights and illustrates literally such elements.
In this method,
the different contributors to this event and workshop used,
reused,
and transformed each other’s recipes to make paper,
down to the fiber created from the pulp of their own clothing.
Next to that,
they learned the open source programming framework Processing,
as a means to visualise and keep track of the usage and origins of each pulp vat used in the process of making the paper.
The graphics generated are printed on the paper,
as well as the source code,
whether they are instructions to make the paper,
or the code for the generated visuals.
Finally,
this method is used three times and will form the three parts of the final book,
each new iteration builds upon the previous one so as to highlight and encourage the progressive blending of the source code from the different Processing sketches,
and the different instructions and pulps to make the paper sheets.


The result is describe by Elmer in the form of a parallel between what she calls *objects* and their *source*,
and how the latter are produced [@VT:flatbed].
According to her the project has an educational purpose and see her intention as political in the way it tries to address a sense of disconnection in the practice of book making:
^[Email to author, April 23, 2011.] 


> To sum up,
> we'll fight for days on the listservs as to whether or not polymer type prints as well as lead type.
> We'll argue down to the millimetre when it comes to our binding decisions.
> But we'll just fire up the Adobe Creative Suite,
> live trace our drawings and make a negative without ever understanding the code (written by someone else!) that generates our content.
> This seems like a disconnect to me. [@VT:flatbed]


The fact that Elmer's approach to free culture is essentially metaphorical is very useful to understand the process of cultural diffusion in free culture.
*Fibre Libre*,
while not using the free software template to create a functional framework relevant to her practice and community,
demonstrates clearly how this abstraction of the free software struggle can move so easily from one field to another,
and explain why many artists were able to relate to its general idea and see similar patterns in the way the things they make,
highlighting in particular the contradiction between a polymath desire to remain autonomous in one's practice,
yet articulate such independence within broader collaborative and cooperative networks.



### CC Ironies

  
Another form of artistic appropriation of the free and open source discourse is also possible.
For instance, 
artists can turn the question of choice for licenses upside down,
and instead use licensing itself as a way to engage their audience directly with intellectual property issues in the art.
An illustration of this is the 2007 *CC Ironies* series (Figure \ref{fig:ccironies}) by English artist and writer Rob Myers,
a work that takes the form of nested art in which the artist articulates the tensions between authorship,
appropriation,
attribution,
collaboration,
copyright,
and CC licenses at the three levels of icons,
indices,
and symbols.
Here,
the iconic messages of the appropriated images are not stripped of their symbolism,
however,
instead of generating meaning through their discreet and discontinued arrangement,
which would be the way in which such images are usually encountered,
Myers creates a new symbolic interpretation by superposing these images.
To be sure,
the particularity of such collage is that the context and functionality of the images used has been deeply challenged.


\afterpage{%
\begin{figure}[h]
	\caption{CC Ironies (sample from a series of 42)}
    \includegraphics[width=\textwidth]{brilliant} 
	\fnote{Vector graphics: Rob Myers, CC BY-SA, 2007}
    \label{fig:ccironies}
\end{figure}
\clearpage
}



Unlike the FAL---which as I discussed earlier was not understood by its initiators as a work of art,
in the sense that it did not claim any affiliation with artistic practices using the contract as medium---a series like *CC Ironies* is instead a free culture addition to the notion of contract aesthetics,
or to be more precise license aesthetics.
Indeed,
the making of the work was the result of a discussion between Myers,
the Serbian artist duo Marija Vauda and Nikola Pilipovic (MANIK),
and English entrepreneur David Bausola on the topic of copyleft as form,
and that according to Myers,
drew inspiration from Carey Young's use of legal documents as sculpture and installations [@Young:works].
Here,
a particular point of attention was given to the social and legal form that copyleft is intended to be,
and the way it can,
or cannot,
affect the aesthetic form of art
^[Email to author, April 22, 2011.].
If the strategies of pavu.com can be understood politically,
they are not a direct political action.
This is not the case with Myers whose work is more militant than partisan,
or to be more precise,
is inscribed in a copyright reformist trajectory where the choice of a license is not an innocent gesture.
Similar to the point I made earlier on the role of licensing as a form of artist statement,
Myers argues that free culture licensing is a "small political act",
that make artists and their work directly implicated in the *Copyfight*
^[
Copyfight is a blend word between copyright and fight,
often used in free and open source discussion as a general term to describe the struggle over intellectual property.
See [@Seltzer:politics, p. 150].
].
Even though he generally finds the political commitments of artists to be "more often a cause of embarrassment than an interesting component of the work" [@Myers:licensewhy] that causes the impairment of artistic freedom,
he considers Copyfight to be an exception because,
and it is a paradox,
he argues that it is precisely inscribed in an effort to remove limits on artistic freedom [@Myers:licensewhy].


In that sense,
the use of licensing in *CC ironies* is more than an artist statement by Myers on free culture,
it is also an attempt to communicate to his fellow artists this particular reflexivity.
He believes that free cultural licensing makes tangible and visible the underlying legal apparatus of art production and distribution,
and *CC Ironies* is thus a bold attempt to demonstrate this point through practical means.
Once again,
there is with such position an interesting split between a more partisan approach to free culture,
as illustrated by the examples of free-range free culture practices discussed earlier,
and the way artists like Myers understand theses documents beyond their paratextual role.
In fact,
what triggered Myers to first articulate the effect of licensing,
and eventually produce *CC Ironies*
^[
In June 2007,
Myers first wrote about this aspect of licenses,
and then made the work.
The latter was further developed in August of that year following the creation from Bausola of a series of free culture licensed *Free Gift Wrapping Paper* inspired by the notion of the gift economy.
Email to author, January 13, 2011.
],
was a comment on CC licensing from American artist duo M.River & T.Whid Art Associates (MTAA),
who while presenting themselves as CC license users,
also wondered about the artificiality of such licenses which they qualified as a "legal-addon" [@Whidden:addon].
By *artificiality* Tim Whidden from MTAA explains that it is "something added on by outside influence and may or may not have any meaning or value vis-a-vis what the artist was trying to communicate in the art work" [@Whidden:addon],
and as consequence a CC licensed work is not necessarily better than a non CC-licensed work because in his view,
for most viewers the contextual shift provided by the CC license will not be perceived [@Whidden:addon].
According to Whidden,
the value of a work will be appraised based on traditional artistic criteria and not the license used.
Worse,
"drawing a viewer’s attention to the licensing aspect of a work of art may confuse the viewer"[@Whidden:addon].
For Myers,
these points are moot because there is no clear distinction between understanding the work and understanding the artist's intention once there is copyfighting as political commitment.
In his own words,
CC licenses may not "make a work look better [but] it can make the work be seen better and can lead to the creation of better work" [@Myers:licensewhy].
They create a direct implication of the work and the artist with the issue of artistic freedom.
This argument implies however that the artist has been using the license knowingly,
a point I will discuss in the next chapter.

\newpage

