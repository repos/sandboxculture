Free Cultural Misunderstandings
===============================

The Double Misunderstanding with Copyleft
-----------------------------------------

On the 26th of May 2014
Italian noise musician Eleonora Oreggia,
working under the artist name xname,
published via email a call for experimental musical pieces on the theme of lullabies
^[
The following short account was narrated to me during an email exchange with Oreggia in 2015.
].
The selected works were meant to be released by the new net-label *nebularosa*,
run by the artist,
and distributed both as digital downloads and limited edition vinyl.
Being both familiar with,
and supportive of free culture practices [@Oreggia:piksel] and also a free software user for many years,
Oreggia requested the applicants to specify which license they wished their work to be published under.
However after making the final selection of works for the compilation,
a strange pattern became apparent in the licensing choice.
Indeed,
instead of specifying the name of a particular license,
the majority of submitters had simply put "copyleft",
which as discussed earlier in Chapter 1,
is not a license but simply a property of *some* free culture licenses.
But the story does not stop here.
After trying to clarify the situation with the musicians and explaining that a proper license was required,
and that copyleft per se was not a license,
she eventually received the following list of Creative Commons licenses from the artists:
CC BY,
CC BY-NC,
and CC BY-NC-ND.
Perfect,
these were indeed valid licenses, the project could proceed as planned,
except for one small puzzling fact:
*none* of these licenses were copyleft licenses.
^[
As discussed previously,
out of all the Creative Commons licenses,
only the CC BY-SA is close to a copyleft license.
For a more detailed discussion on the difference between copyleft and CC's ShareAlike,
see [@Myers:blog:nc].
]
How did that happen?


A circled backwards letter C,
the vertical mirror of the copyright symbol,
is the graphic representation of copyleft.
It can be found today on T-shirts,
mugs,
and of course on stickers to decorate the mood board that represents the laptop cases of artists,
designers,
musicians,
and writers who want to demonstrate their support for...
Well,
for what precisely?
As explained in the first chapter,
and in the context of free software,
copyleft is a property of a free software license,
to ensure that all the modifications and extensions made to the software must be free as well [@FSF:copyleft],
meaning published and distributed under the same licensing terms.
Copyleft is *not* a synonym of free software.
Non-copyleft licenses,
which can generally be described as permissive licenses,
do not require sharing back changes
^[
I am purposefully simplifying here to make the basic distinction more clear.
In practice however,
depending on the license,
the copyleft principle can either be non-existent,
or weak,
or strong.
A license is said to be permissive,
when the copyleft principal is non existent and the licensed program can be turned into closed source software.
When a license is weakly protective,
then the copyleft principle is said to be weak,
as the program is prevented from becoming closed source,
yet it can become part of a larger closed source system.
Finally,
when a license is strongly protective,
then the copyleft principle is said to be strong,
because the program is strictly prevented to become or be part of a larger closed source system.
For a more complete overview,
See [@Wheeler:slide].
Last but not least,
copyleft does not only apply to software,
CC's ShareAlike is roughly equivalent of copyleft,
and free culture licenses can also be categorised by function of their copyleft weight.
See [@FD:licenses].
].
In fact these permissive licenses are sometimes referred to as copyfree licenses by their supporters,
and the advocates of this term are openly against copyleft,
arguing that unlike copyleft,
copyfree is true software freedom because these licenses do not impose sharing [@Perrin:copyfree].
In practice,
both the FSF and OSI supports and list free software licenses that are copyleft and copyfree,
and open source licenses that are copyleft and copyfree,
which should come as no surprise given the important overlap between the two listings.


According to the FSF,
the purpose of the copyleft mechanism is to prevent *uncooperative* [@FSF:copyleft] people from converting free software into proprietary software:
copyleft is here to avoid a situation in which the freedom granted by the author to the users of their software,
has been stripped away by an intermediary agent.
As a consequence,
in the case of the copyleft license GPL,
it means that any distributed modifications of GPL'ed software must in return also be licensed under the GPL itself,
thereby leading in theory to more free software being written and distributed.
This is why some critics of the free software movement started to use the term *viral licensing* or virus [@Raymond:jargon; @Vixie:email] to describe the possibility of the GPL spreading whenever free software was modified and distributed.
Some even called it the "Borg property" [@Hawkins:borg],
and there is certainly in these analogies a mix of popular sci-fi and posthumanist anxiety towards something inhuman going out of control,
stealing our identities,
and taking over the world.
Here the notion of creativity is understood as a sort of Bergsonian *élan vital* [In reference to @Bergson:evo],
a precious biological reproductive function that needs to be diligently safeguarded from a virus that might lead to involuntary sharing of embodied private property and identity.
The analogy is not exaggerated and it seems these metaphorical strategies come up fairly often during debates around IP,
whether or not specific to copyleft and free software.
For instance in February 2012,
following the peak of online protest against the US bill Stop Online Piracy Act (SOPA),
the American film industry magazine The Hollywood Reporter solicited a branding and advertising expert to draft a purposefully populist campaign targeting piracy [@THR:SOPA, p. 34].
The resulting mockup called DTCs,
for Digitally Transmitted Content,
made a questionable parallel between viral sharing and STDs,
Sexually Transmitted Disease,
using a condom as illustration and on the packaging of which could be read in capital letters "PROTECT YOUR CREATIVITY" [@THR:SOPA, p. 34].



\afterpage{%
\begin{figure}[h]
	\caption{RIP!: A Remix Manifesto}
    \includegraphics[width=\textwidth]{rip} 
	\fnote{Still frame: Brett Gaylor, CC BY-NC 3.0, 2008}
    \label{fig:rip}
\end{figure}
\clearpage
}



To return to the puzzling situation of licensing choice made by the musicians of the *nebularosa* net-label,
a question that I asked myself in relation to this anecdote,
was did the artists misunderstand what copyleft is,
or did I misunderstand what the artists meant by signing their work in such a way?
I have shown that copyleft is indeed a very particular legal mechanism with no possible misunderstanding,
and is emblematic of sharing and co-creative practices.
It is the most popular aspect of Stallman's work, 
and plethora of free cultural copyleft licenses lists can be found on the Internet.
Yet,
the term is regularly misused.
An example of such a confusion can be seen in one of the scenes of the very popular documentary *RiP: A Remix Manifesto*,
in which copyleft is used to visually represents several icons of non-copyleft
Creative Commons licenses (Figure \ref{fig:rip}) such as non-commercial,
sampling,
and even public domain---the latter being the most radically non-copyleft status a work can possibly receive.
Similarly the free software movement is frequently assimilated to the so-called copyleft movement,
and somehow put in relation with art traditions of non- and anti-copyright practices [See @Liang2005, The Black and White (and Grey) of Copyright].
This creates confusion because copyleft relies heavily on copyright as explained several times in this thesis,
and also---as discussed in the previous chapter---when it comes to mapping the different artistic intentions connected to cultural freedom,
there are irreconciable differences within the different communities which animate these fields. 


The reason copyleft is misunderstood is very simple.
The term sits at the cross-road between the cultural field and the legal field.
Copyleft,
an obvious play on the word copyright,
is a way to express a certain form of rebellious and tongue-in-cheek humour which mocks or defies IP laws.
The term predates the FSF,
and so a trivial symbol like a copyleft sticker or the casual use of the term is not the sign of defusion and recuperation of free software by the means of mass producing stereotypes of cultural resistance,
because such a sign occupied the cultural field long before its legal articulation with free software.
In fact,
one day in 1984 Stallman received by mail a programming manual that had been borrowed by American hacker and computer artist Don Hopkins.
On the envelope a stickers reading "Copyleft (L)" was used to seal the small package.
Hopkins had bought a pack of stickers at a science fiction convention,
where hackers,
including Stallman, 
often gathered and where it was common for them to organise and share rooms,
notably for "@" parties in which people with email addresses could meet each other
^[Email to author, February 17, 2015.].
According to Hopkins,
at that time the term copyleft was not part of the hacker culture,
and the stickers had been purchased in the dealer's room of one convention with other comics,
political,
and satirical stickers and buttons.
^[Ibid.]
Knowing Stallman's appreciation for such things,
Hopkins had decorated the letter in a similar spirit.
Little did he know that eventually the sticker and the pseudo-copyright statement he had written as a joke (Figure \ref{fig:copyleftsticker}),
would inspire Stallman to use the word copyleft to describe the properties of the GPL. [@Williams:freeasinfreedom, The GNU General Public License]
This is how copyleft,
the symbol of rebellious cultural practices,
ended up being claimed as a term to describe a particular mechanism of free software licensing.
Regarding the copyleft term that inspired Stallman,
it seems that it kept on being occasionnaly used in the nineties,
with no connection to free software.
For instance,
I found it mentionned with the mark "\<L\>" instead of "(L)" in the lyrics of a filk song
^[
A folk derived participatory music genre linked to science-fiction and fantasy fan communities as briefly discussed in Chapter 3.
]
inspired by the *Dune* science fiction saga by American author Frank Herbert.
The lyrics were signed "\<L\> 1992 by Jeremy Buhler" with a note at the end of the file "PS - \<L\> means copyleft." [@Buhler:dunefilk, dune.txt]
 

\afterpage{%
\begin{figure}[h]
	\caption{Copyleft (L) sticker}
    \includegraphics[width=\textwidth]{copyleft-sticker-back-envelope} 
	\fnote{Envelope scan: Don Hopkins, 1984, CC BY-SA 4.0}
    \label{fig:copyleftsticker}
\end{figure}
\clearpage
}


While Hopkins explained that copyleft was not part of the hacker culture at the time he bought the stickers,
the overlap of different alternative,
countercultural,
niche,
or underground communities was however already visible in the copyright notice of a 1976 implementation of the proto-free software Tiny BASIC,
where could be read on the title screen "\@COPYLEFT ALL WRONGS RESERVED" [@Wang:tinybasic, p. 15].
This particular line of copyleft linked to computational culture also kept on being active in the nineties with no apparent connection to free software.
For instance it can be found in some ezines mentionned as "(CL) Copyleft" [@020-1, 020_1.txt],
or "Copyleft 1992 - All Rites Reversed" [@Zen:church, scsa-ash.txt],
or "(CP) Kopyleft 1999 QNARKK PRODUCTIONS all rites reversed" [@QNARKK4, q04.txt].
The last two are particularly interesting because they suddenly connect to much older publishing practices.
It was relatively common in the late sixties and seventies to spot in underground publication a statement against the publishing industry and intellectual property,
in various forms,
such as the phrase "All Rights Reversed",
spelled or expressed differently like in the "Ⓚ All Rites Reversed – reprint what you like" notice in the 1979 version of the *Principia Discordia* [@Hill:principia, SPECIAL AFTERWORD].
Concerning the term copyleft itself,
it is striking that mail artists such as Ray Johnson also used the term *copy-left* in their work [@Wark:email],
and it was possible on occasions to spot the now very popular copyleft icon,
a vertically mirrored copyright logo,
marking a mail art related publication.
In this context copy-left was more politicised and articulated by those who refused to engage with the art scene of the time,
and who experimented with alternative systems of property by giving their art away,
in an age were different strategies such as the staging of happenings,
were created to resist the commodification of culture.
In particular the use of copy-left was seen by Japanese mail artist Ryosuke Cohen as a symbol of "free-from-copyright relationships" [@Cohen:copyleft] with other artists,
in a way that was "not bound to ideologies" [@Cohen:copyleft].
Here the statement is not just paratextual,
it also refers to a practice and attitude towards particular communities of sharing,
similar to the 1973 "COPY-IT-RIGHT" and "distribution religion" philosophy from American video artist and activist Phil Morton [@Cates:copying],
or the earlier 1970 so-called *Xerox mark*,
a circled *X*,
used in the American video journal *Radical Software*,
as the "antithesis of copyright"[@GershunyKorot:rs1] and to "encourage the dissemination of information"[@GershunyKorot:rs1].
Even though it is out of the scope of this research to map thoroughly other important or forgotten historical examples of copyright inversions,
it should be clear that they have been quite numerous.
The problem with such approaches,
to come back to the topic at hand,
is that their legal validity is at best questionable,
which makes it easy for them to be claimed by the intellectual property framework they criticise.
Unless potential artistic relationships and cooperation are made explicit,
which is what Lithuanian-American artist George Maciunas did with fellow Fluxus artists by using a shared copyright [@StilesSelz:doc, GEORGE MACIUNAS - Letter to Tomas Schmit (1964)],
or unless the estate of an artist or collective is taken over by a caring group or institution willing to document and share the work in the same original spirit,
like The Phil Morton Memorial Research Archive [@Cates:copying],
then the door to contradictions can open at any time.
For instance,
in a very unfortunate and sad twist,
the copy-left free-from-copyright ethos of mail-art echoed years later in some reproductions of Johnson's copy-left works,
which are now stamped "Copyright the estate of Ray Johnson" [@Wark:email].



\afterpage{%
\begin{figure}[h]
	\caption{Cover of 1985 copy-left issue \#3}
    \includegraphics[width=\textwidth]{copy-left} 
	\fnote{Photo: Aymeric Mansoux, 2011, CC0}
    \label{fig:copy-left}
\end{figure}
\clearpage
}


But the copyleft trail does not stop there.
The term copy-left and its iconic representation were introduced onto the mail-art scene by Swiss artist Manfred Vänçi Stirnemann,
after the artist had sent stamps of the copy-left word and logo to Cohen,
who then started to use the latter to imprint copy-left marks as part of his widely distributed stamp sheet editions
^[
This paragraph is based on an email exchange with Stirnemann in March 2015.
].
At the time Stirnemann was not aware of any similar usage of the term,
and admits it is a quite obvious play on the word copyright,
he would not be surprised if other artists with some political inclination had also come up with the same idea.
At first,
Stirnemann was not involved in mail-art,
and used copy-left and its mark for his projects and publications,
such as the 1984 "copy-left" editions.
His work has been inspired by various topics and things,
from the eighteenth century *Encyclopédie* edited by Denis Diderot and Jean le Rond d'Alembert,
early eighteenth and nineteenth century anarchism and socialism,
American poet Gary Snyder and the Beat Generation,
hippies,
McLuhan's global village,
to art brut and the Frankfurt School.
For Stirnemann,
"no copy-right" easily translates into copy-left while making allusion to left wing politics,
it is as simple as that.
Regarding the coining and usage of the term,
Stirnemann cites as first personal influence the Underground Press Syndicate (UPS),
a late sixties born countercultural network of underground newspapers and publishers,
within which community things were shared,
with simple rules of no copyright but the crediting of source and author.
This was actually often made explicit in these publications,
for instance in the colophon of the UPS affiliated underground magazine *HOTCHA!* initiated by Swiss artist and writer Urban Gwerder,
the following statement could be read: "anti-copyright aber quellenangabe und beleg erwünscht",
anti-copyright but please cite the sources and references [@Gweder:hotcha].


Such an approach itself is of course in the trajectory,
of the even more radical pseudo-copyright statement found in the *Internationale Situationniste* publication,
which started with its third issue of 1959 to print the following notice:
"Tous les textes publiés dans 'INTERNATIONALE SITUATIONISTE' peuvent être librement reproduits,
traduits ou adaptés,
même sans indication d'origine." [@Debord:IS3, p. 2]
All the texts published in 'INTERNATIONALE SITUATIONISTE' can be freely copied,
translated or altered,
even without mention of origin.
The link could be further explored to take into accounts the large history of anti-copyright and plagiarist practices in art [@Cramer:anticopy],
but it is not necessary.
The demonstration here,
is to simply show that copyleft licenses are not derived and do not belong to the cultural legacy of anti copyright practices.
They are completely different trajectories.
It would be more correct to say that it just happens that Stallman was exposed unknowingly to the micro-mediatic [In reference to @Thornton:club, "Micro-Media: Flyers, Listings, Fanzines, Pirates"] diffusion of underground art scenes with the copyleft sticker,
and ended up fixating a term outside of its original context.
This of course helped a lot free software to become adopted and appropriated back by artists who thereafter,
with very few exceptions such as Copyleft Attitude,
did not interpret copyleft in its techno-legal context but linked it to an internalised symbolic critique of the culture industry in the past century.


American scholar James O. Young  suggests using the term style appropriation when "artists do not reproduce works produced by another culture,
but still take something from that culture [and] produce works with stylistic elements in common with the works of another culture." [@Young:cultural, p. 6]
In that sense,
the artists contributing to Oreggia's netlabel sampler effectively appropriate the style of free software culture by using the term copyleft in relation to the licensing of their work,
yet picking the apparently wrong non-copyleft licenses.
Similarly,
the *.copyleft!_* notice from Turkish artist İbrahim O. Akıncı,
both refers to the notions of free art,
copyleft attitude,
and free culture,
yet presents itself as a non-license,
a comment on the moral values and ethics of free culture,
as they are perceived by the artist [@Akinci:copyleft].
But Stallman's use of copyleft is *also* a case of style appropriation of underground and countercultural practices,
for which the meaning of copyleft is not universal,
but as I have shown,
points to a collection of intentions and processes that can vary greatly,
from encouraging copying,
but not specifying the possibilities of transformation,
or requesting attribution,
to complete permissiveness and the occasional legal limbo to provoke a challenge to copyright.
They are all unique and specific to the cultural context they stem from.
These practices were in fact not proto-copyleft but similar to the proto-free culture era described in Chapter 2,
where all sorts of exotic licenses were used to publish digital works.
Therefore,
and returning to the netlabel anecdote,
it becomes understandable that when asked to specify a license,
the musicians all come with very different licenses,
each illustrative of a personal understanding of copyleft art that interfaces with common language,
as part of an ongoing dramatisation
^[
In reference to [@Hebdige:subculture, p. 87].
]
of the processes of cultural commodification.
So in the end there are truly two misunderstandings occuring with the use of free software derived copyleft for works of art:
the first is most obviously the failure to properly use free cultural copyleft licenses,
but the second,
more subtle underhand misunderstanding,
and of equal if not more importance,
is the failure to see behind the first one the continuation of poetics and resistance,
as part of a long history of practices critical of intellectual property.


The Enduring Debate over the Commercial Exploitation of Free Culture
--------------------------------------------------------------------


Another frequent source of confusion is the commercial exploitation of free and open things,
and the muddiness surrounding the topic seems to be the most persistent misunderstanding within free culture.
Because of this,
the literature on the topic has yielded in the past,
and is still producing a plethora of contradictory analysis.
For instance open source was presented early on as exemplary of a cyber-communist gift economy and wrongly associated with the shareware and freeware business models [@Barbrook:gift1998; @Barbrook:gift2005, Special Issue Update],
or articulated as anti-commercial effort [@Galloway:protocol, pp. 169-171],
that sometimes was even described as the underlying meaning of copyleft [@HardtNegri:multitude, pp. 301-302].
It is an old confusion and more recent writings have started to look back at the connection between free software and the software industry in a less one-sided way [@Berry:copy, The Commercialisation of FLOSS],
providing in particular a much needed articulation of the relationship between the liberal interpretation of free software and free markets,
and the tension that arises in the symbiosis between capital and community [@Sodeberg:hacking, Business models based on free software].


Still,
even today the relationship between free and open source software,
and its commercial exploitation from large corporations to garage-hacker startup companies,
is the topic of heated debate
^[
For instance on popular tech news posting and discussion forums,
such as *Slashdot*, 
*Hacker News*,
and also some subreddits from *Reddit* and various chan's /g/ and /tech/ boards,
such debates have solicited emblematic knee-jerk reactions from its community of users,
whenever something related to free and open source software and commercial exploitation is discussed.
].
It is true that the link between commercial practices,
software distribution,
and the idea of selling software has always been a complicated construction within free and open source communities.
Swedish scholar Johan Söderberg uses the 1989 slogan from early free software supporting company Cygnus Solutions,
"we make free software affordable" [@Sodeberg:hacking, p. 32],
to sum up the contradictory logic of the first commercial exploitations of free software practices.
But this ambiguity is also mirrored,
early on in the nineties,
with the discourse of the first large non-commercial and not-for-profit free software projects.
For American software engineer Ian Murdock,
founder of the free software Debian project and operating system,
software freedom in relation to commercial exploitation was referred in such a way: 


> The Free Software Foundation plays an extremely important role in the future of Debian.
> By the simple fact that they will be distributing it,
> a message is sent to the world that Linux is not a commercial product and that it never should be,
> but that this does not mean that Linux will never be able to compete commercially.
> For those of you who disagree,
> I challenge you to rationalize the success of GNU Emacs and GCC,
> which are not commercial software but which have had quite an impact on the commercial market regardless of that fact [@Murdock1994].


The idea of something presented as non-commercial,
which nonetheless has the ability to be commercially competitive on a market,
is not trivial to communicate and understand,
but it makes explicit that the resistance towards commercial exploitation is not necessarily an opposition to the principles of free market.
Fast forwarding fifteen years after the release of the Debian manifesto from which the above text was quoted,
this ambiguity has played in favour of developing a large free and open source software supported anti-capitalist network infrastructure
^[
For a list of 32 active,
at the time of writing,
of "[a]nti-capitalist,
anti-hierarchy,
autonomous revolutionary collectives which provide free or mutual aid services to radical and grassroots activists",
see @Riseup:servers.
],
but also fuelled many large scale free and open source software based commercial projects.
The latter is obvious for products relying on permissive licensing,
as often exemplified by the relationship between FreeBSD and Mac OS [@Weber:success, p. 202],
but also for copyleft licensing for which commercial exploitation is possible in spite of the much feared source code closedness.
This strategy was particularly demonstrated with Google's Android mobile operating which Linux source code,
was essentially reduced to an open middleware and thin client,
meant to interface with a corporate controlled closed ecosystem of apps and cloud services [@SpreeuwenbergPoell:android].


As covered in the first chapter,
since its infancy,
the FSF goal was never to promote the distribution of software free of charge,
but instead to liberate the software culture from the closed source and proprietary software model.
Even before the introduction of the term open source,
Stallman was very well aware of the risk of using the adjective *free*:


> The word "free" in our name does not refer to price;
> it refers to freedom.
> First,
> the freedom to copy a program and redistribute it to your neighbours,
> so that they can use it as well as you.
> Second,
> the freedom to change a program,
> so that you can control it instead of it controlling you [@Stallman:definition1986].


However,
this was only the beginning of what would be an unceasing struggle with language.
Not only did the FSF supporters have to liberate software to fit their particular definition of freedom,
now they would also need to do the same for their own vocabulary.
Therefore,
by the end of the nineties,
and shortly before the creation of the OSI,
the FSF started to maintain a collection of "confusing or loaded words and phrases that are worth avoiding" [@FSF:words1998].
This effort is in fact a preemptive lexicon meant to defuse possible current and future weaknesses in the free software discourse.
The evolution of this collection of definitions is literally an ever changing media archaeological artefact that is the witness of Stallman's learning process and own individuation,
which development,
like GNU's source code,
is made public through an iterative and version controlled workflow.


Throughout the years,
the list has kept on growing,
as an attempt to patch any new misunderstanding,
and to remain in control of the GNU language.
Regarding the issue of the commercial exploitation of free and open source software,
this lexicon is therefore helpful in its function of logging Stallman's defusing efforts.
For instance,
the entry "Sell software" added in 1998,
is essentially a response to the threat presented by the creation of the OSI the same year:


> "Sell software"
> 
> The term "sell software" is ambiguous.
> Strictly speaking,
> exchanging a copy of a free program for a sum of money is "selling";
> but people usually associate the term "sell" with proprietary restrictions on the subsequent use of the software.
> You can be more precise, and prevent confusion,
> by saying either "distributing copies of a program for a fee" or "imposing proprietary restrictions on the use of a program,"
> depending on what you mean [@FSF:words1998].


In this quote,
Stallman and the FSF try to balance an ethically driven free software discourse with a touch of openness towards commercial exploitation.
This attempt to connect with past defectors and future OSI supporters is even stronger fours years later,
were the term "commercial" is added in response to the increasing popularity of the term open source in business contexts:


> ``Commercial''
> 
> Please don't use "commercial" as a synonym for "non-free".
> That confuses two entirely different issues.
> 
> A program is commercial if it is developed as a business activity.
> A commercial program can be free or non-free,
> depending on its license.
> Likewise,
> a program developed by a school or an individual can be free or non-free,
> depending on its license.
> The two questions,
> what sort of entity developed the program and what freedom its users have,
> are independent.
> 
> In the first decade of the Free Software Movement,
> free software packages were almost always noncommercial;
> the components of the GNU/Linux operating system were developed by individuals or by non-profit organisations such as the FSF and universities.
> But in the 90s,
> free commercial software started to appear.
> 
> Free commercial software is a contribution to our community,
> so we should encourage it.
> But people who think that "commercial" means "non-free" are likely to assume the idea is self-contradictory,
> and reject it based on a misunderstanding.
> Let's be careful not to use the word "commercial" in that way [@FSF:words2002]


This long quote is particularly striking because it shows two aspects of the free software discourse prototyping.
First,
Stallman starts to reach the limits of its conceptual framework,
and the more he tries to articulate a neutral all encompassing position the more difficult it becomes for the reader.
If the usage of free in free software was already confusing and questionable,
^[
Every now and then,
some debates sprout online which discuss whether or not the term is ambiguous and should be renamed.
Usually the alternatives suggested are so tainted with a personal interpretation of freedom,
that trying to clarify leads to even more problematic alternatives,
for instance "Freedom Software",
or "People's Software",
or "Software for the Masses".
See [@Bhattacharya:problem]
]
the introduction of a term like *free commercial software*,
while perfectly correct and coherent within the GNU language,
does little to help communicate that free software and commercial exploitation are compatible.
Kelty uses the term *recursive public* to describe how the free software community articulates itself via direct engagement and modification [@Kelty:bits, Introduction],
but what the FSF and Stallman's collection of problematic words shows is that the procedure in which such recursion happens,
while being public and informed by public discussions,
is in fact private and authoritative.
It is also more recursive than Kelty may have wished for as it gives little room for a change of direction,
because its self-similar generative process only points to a downward spiral.
The text quoted above is also symptomatic of an information driven culture that constantly rewrites its own history.
In particular,
the entry quoted above significantly alters the commercial origins of free software as it omits the fact that Stallman's efforts to develop the concept of free software was bootstrapped by the selling of his own free software [See @Stallman:gnuandfree, GNU Emacs],
or to be more precise,
by distributing copies of proto-GNU programs for a fee.
Regardless,
this novel practice would indeed prove to be an "innovative business model" [@Salus:daemon, p. 50],
which makes the emergence of open source software a logical next step in the refinement of such commercial practices.


If free software is truly a recursive public,
then its base case is the famous expression "free as in speech, not as in beer"
^[
Originally formulated in 1998 as such:
"'Free software' is a matter of liberty, not price. To understand the concept, you should think of 'free speech', not 'free beer'".
See [@Stallman1998].
The modern version was introduced in 2001.
See [@Stallman2001].
],
which has the specificity to link the free software discourse with broader free cultural issues,
but also doom the latter by transmitting further its ambiguity to non-software free cultural things.
This aspect was notably highlighted with the 2005 *free beer* project [@Superflex:beer].
This brew was initiated by a group of students from the IT-University in Copenhagen and the Danish artist collective SUPERFLEX.
It was first released under the name *Vores Øl*[@ITU:beer],
the open source beer,
and was later modified and developed further by the artist collective as the *Free Beer* project.
What is specific about this beer is that the recipe and the branding are published under a CC BY-SA license that allows anyone to produce the same beverage,
or any other one that would be derived from this freely available recipe.
Similar to the free software copyleft principle,
this is made possible as long as the terms of the CC license are respected.
The conditions boils down to publish the original or modified recipe with the same license and requires credits to the project initiators,
and other contributors if the recipe has already been modified.
As long as this condition is respected, anyone is *free* to make and sell the free beer product and earn money with it,
without having to pay any royalties or licensing fees to the authors of the original recipe,
or to those who modified it further.
But,
next to the playfulness of the work,
what such a project shows is the fragility of the FSF position towards the expression of selling software.
*Free Beer* is a free cultural work,
and more precisely a beer liberated from the closed and sometimes secretive practices of brewers,
but it's also a product of consumption that is sold in different contexts,
and for which it would be rather strange to rephrase the selling of free beers in favour of the distribution of free beers for a fee.


So in practice,
the confusion discussed here,
when transposed at a non-software free cultural level,
has multiple repercussions,
on economic profit,
cultural commentary,
and consumerism.
First it's the open door for crude and direct exploitation because free culture can present itself as a gift economy [@Mauss:gift],
in which money is not the purpose of the exchange of goods or services,
which is more or less implied by Stallman's effort to avoid using the word selling.
The consequence is that for instance when the CC supporter and image hosting website Flickr attempted to monetise the photos of its users [@Nieva:flickr],
it offered a classic licensing model to remunerate the photographers who had chosen to publish their photos under standard copyright protection,
but did not offer any compensation to those who had publish their photos under the CC licenses that were not explicitly non commercial.
Nothing wrong from a legal perspective but a rather painful reality check for the photographers using CC licenses who had not quite understood some of the subtleties of this pseudo-gift economy.
Second,
the confusion exists also on the other side of the free cultural transaction,
which seriously weakens the paratextual message shared by free culture supporters.
For instance free culture supporting scholars such as Cramer,
or animators like Paley,
are almost systematically asked by editors,
publishers and distributors to approve,
license,
authorise,
and make contracts copies of their work for publications or screening
^[
Emails to, 
and in discussion with author,
2013-2014.
],
despite their use of free culture licenses,
thus ignoring and making irrelevent the point they try to make to the very culture industry that remains blissfully unaware or unwilling to engage with such critiques.
And third,
from the perspective of the consumer,
the default interpretation of free in the context of exchange and sharing,
simply means gratis.


Of course,
those expecting free software to be free as in free beer,
or believing that it is the outcome of a spontaneous global cooperative mechanism are very much misinformed about how such software is produced.
The vast majority of Linux kernel developers are employed by tech companies [See @CorbetKroahHartman:linux] which have extended their competition in the writing of source code relevant for their product,
and many important desktop and mobile applications and their components are managed and produced by large corporations,
following a model in which free and open source software is used strategically [@MianTeixeiraKoskivaara:market; @TeixeiraLin:webkit].
Similarly,
emblematic projects like Mozilla Firefox are still alive simply because of external revenue streams and deals
^[
Essentially royalties from deals with search engine companies,
see [@HoodStrong:mozreport].
],
made possible via a construction in which the very communicative nonprofit organisation controls a more discreet revenue-generating entity [See @Mozilla:reorg].
At the opposite end,
small or independent software projects constantly struggle to generate income for its developers,
even if their work is widely used commercially [@Beck:electricity],
sometimes with their economic struggle noticed only once covered in tech news channel [@Angwin:gpg].
Yes,
free software licenses allow for commercial exploitation and most free and open source software source code is nowadays just one click away to download for free,
but abuses from corporations and the reluctance of the FSF to engage seriously with the question of work and labour,
combined with very optimistic views on a fully cooperative society and sharing economy living on thin air,
all this has today severely damaged the cultural diversity within the free and open source software ecosystem.
As a result,
in practice free software is expected to be gratis,
available on-demand,
disposable,
and coming out of nowhere but the cloud.
Worse still,
this aspect is often given as an advantage of free and open source software over closed source and proprietary software.
Any independent developer or small team of programmers trying to make a user pay for their work---or trying to justify the need to make a living---will in the best case provide a minimal income [@Davis:ardour],
or in the worst case be trashed publicly for daring to ask for money [@reddit:elementary].


If the FSF can greatly help with intellectual property issues and abuse regarding free and open source software,
it is neither a union,
nor a cooperative.
Free and open source software programmers are on their own trying to find ways to survive until the day when Stallman's free society comes true,
a society where "nobody will have to work very hard just to make a living" and "[t]here will be no need to be able to make a living from programming" [@Stallman:manifesto].
But that proposition also ends up sabotaging the further development of free and open source software,
and today results in a situation where for some,
public source code has became a way to show off skills,
to present source code as a curriculum vitae to eventually get hired and paid to write software that will unlikely be free software,
a trend accelerated by so-called social coding platforms like GitHub [@Dabbish:github; @Doubrovkine:gitresume],
but also by the same boards,
like the social news website Hacker News
^[
Notably the "Show HN:" threads.
See [@ycomb:showhn].
],
where such practices are discussed and where it is well accepted to show pet projects [@Dabbish:github].
These demos are often personal projects,
highly topical and personal,
or dependant on external services and platforms,
and for which user attraction and software rot is irrelevant because they are software of the moment.
Such software is a disposable material to gain reputation and visibility within the startup software industry.


For non software free cultural works and expressions however,
this translation does not work well,
as---with the exception of performing a work---most artistic income streams rely on making derived objects from the work,
or licensing its access.
^[
It is out of the scope of this thesis to discuss alternative and speculative models for free culture production,
crowd-funding and other patronnage.
For some case studies, see [@Unknown:sustain].
]
What is more,
unlike software tools,
with a few exceptional cases where the tool itself becomes a culturally infused constraint practice associated with a specific community and culture
^[
For instance ASCII and ANSI editors.
],
the value of these works or expressions do not age the same:
by effect of fashion,
discovery,
trends,
inspiration,
these works can become financially relevant at any time.
To distribute them both for free and with a free software licenses is therefore truly radical,
because of the financial suicide it may represents.


As a workaround,
partisan or free culture often adopts a liberal communist discourse in which the role of services is presented as a requirement for sustainability:
the musician does not make money from the music freely licensed but from merchandising,
gigs,
limited physical editions or the free tracks on cassette tapes and vinyls;
the writer derives income from special physical limited editions of an electronic publication;
the artist does not make money from commercial gallery purchases and exhibitions but from public funding,
residencies,
and commissions.
All of these strategies come with strings attached given the mediation created by the production and distribution of these new objects,
in which the free culture freedom of the author is moderated by the editorial freedom of the platforms,
the publishers,
the funding organisations,
and the curators standing at the gates of a liberated culture as service driven economy.
On top of that, novel forms of funding and micro-payments or patronage can be put in place,
but are so far only effective for already established authors,
or very talented marketers and net-workers,
or targeted at mainstream culture consumption.
Finally,
for free culture artists who were not born wealthy,
working today still remains the most straightforward option to liberate a practice and bypass entirely the ambiguity of the commercial exploitation of free culture,
thus coinciding with economic models of anti-professional art production that pre-date free culture [@StilesSelz:doc, GEORGE MACIUNAS - Letter to Tomas Schmit (1964)] and also connect back with strategies to sustain the making of work that resist commodification,
either because of their form or because of the intention of their author.

\newpage


