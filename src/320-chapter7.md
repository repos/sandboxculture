From Techno-Legal Templates to Sandbox Culture
==============================================

Deceptive Participations in a RO/RW Remix Culture
-------------------------------------------------


In the previous chapter I have departed from the argument on innovation that is central in the free culture discourse---made popular by Lessig to explain why he believes free software is a model to develop free culture---because if the analogy of software re-usability seemed to transpose theoretically to culture in relation to productivity,
sharing ideas,
and inspiration,
the same analogy was clearly over stretched when in practice culture had been reduced to file-sharing and the remix of flattened down works.
The latter practice,
which refers to the problem of lack of cultural source discussed in the previous chapter,
demonstrates that all the subtleties,
tensions,
plays,
and conflicts,
found in the way works respond and relate to each other,
as was exemplified, 
for instance,
with the 1973 influence analysis and theory from literary critic Harold Bloom [@Bloom:anxiety],
are at the risk of being streamlined to their weakest form of pleasurable and entertaining collages,
as Benjamin had predicted in his time for photography [@Benjamin:producer].


To be sure,
I am not saying that remix is per se a poor practice that solely exemplifies the failure of artists to change the productive apparatus.
Growing from the *versioning* of Jamaican songs into dub music,
to its systematisation in the nineties music industry,
the remix has been increasingly used to demonstrate the power of combinatorial practices. [See @Navas:remix]
For instance, remix can be used literally as an experimentation,
an appropriation of the medium and its instruments,
as it is in the practice of turntablist Janek Schaeffer [@Schaefer:audioh];
and it can also be instrumental in another way,
as a framework to analyse the semantics of political discourse,
as illustrated in the President George W. Bush's 2002 "Axis of Evil" speech remix from artist Lenka Clayton [@Clayton:qqqqqq].
Regarding the latter approach,
I would go as far as to say that remix as a folk political tool has worked in the past as proto-tactical media.
For example,
in the mid-eighteenth century Paris,
folk songs were spread orally in popular neighbourhoods.
But these songs were not just for entertainment,
they were also used from time to time as a vector to memorise and spread commentary and critiques on public affairs. [@Darnton:police]
The well known melodies of existing songs were used as carriers,
in which the original lyrics were replaced by critical texts and poetry,
which would have been banned and illegal otherwise,
thus turning the process of influence to which I was referring earlier,
not into mere entertaining collages but into a powerful political communication network of remixes,
building upon the effect of the latter as a "subversion of the listener's expectations." [@veal:dub, p. 89]
Furthermore,
the transformative generalisation offered by remixing makes it possible to link it easily to all sorts of practices and theories,
from musique concrète,
to appropriation art,
intertextuality and dialogism,
and more.
With such adaptability in mind,
writer Eduardo Navas describes remix as a "cultural glue" [@Navas:remix, p. 4],
as opposed to a movement or something that can be framed precisely.
The message is strong as it relies on the obvious cultural mechanisms in which any object is a cultural product,
specifically an object deriving from existing ideas and technologies and therefore,
through cultural diffusion,
yes indeed,
of course,
everything can be seen as a remix of something else.



With that said,
if some have made the claim indeed, 
that "everything is a remix"  [@Ferguson:everything],
the remix becomes problematical however when it is used to showcase democratic processes of participation in cultural production.
Before showing why this is an issue,
I must first explain how remix and free culture relate to each other in file-sharing culture.
The instrumentalisation of remix culture to justify the purpose of free culture was in fact articulated by Lessig himself.
More explicitly,
Lessig used terms from the file system permissions read-only (RO) and read-write (RW),
which he called geek-speak metaphors,
in order to illustrate the mechanisms of remix culture [@lessig:remix, p. 28].
This approach to remix and culture,
that holds a privileged position in Lessig's free culture [@Berry:copy, p. 22],
was the obvious next step in a process of cultural rationalisation reduced to file-exchange,
and in which participation thus also became reduced to file permissions.
This geek-speak metaphor is not only used by CC,
but also within some of the groups and networks mentioned in the previous thesis part
^[
See for instance [@Constant:RWworld].
].
Of course,
the advantage of such simplification is that it offers a very strong example,
as it relies upon technological jargon and practices that have been increasingly democratised with the rise of the Internet and P2P file-sharing.
In that sense it also becomes a subversive vector that can be used to accelerate the spread of new ideas,
in a similar way as was the engineering of popular tunes mentioned earlier.
This is why remixing has been frequently used by CC as:
an inspiration
^[
In the early days of CC,
there was even a series of rather confusing Sampling licenses,
that were inspired directly from remix practices.
The licenses were however flawed in several aspects and were eventually retired in favour of more generic licenses.
For more details on retired CC licenses, see [@CC:retired].
],
a handy shortcut to communicate about licensing changes [@CC:bigwin],
and a way to illustrate the potentiality of CC's "pool of content"[@CC:licenses2017, What our licenses do].


It is interesting that this particular understanding of remixing,
as a quantifiable form of commodified reusable artistic elements also leaks into the contemporary art discourse.
For instance,
French art historian and critic Nicolas Bourriaud stated in 2002 that the artistic question is no longer what can we make that is new,
but instead what can we do with what we have. [@Bourriaud:post, Introduction]
And with this point he argued that artists were no longer considering the artistic field as a museum containing works that must be cited or surpassed,
but as so many storehouses filled with tools that should be used,
stockpiles of data with which to manipulate and present.
According to Bourriaud,
artists are remixers and the consumption and production of information are no longer so separate.
He sees artists as,
what he calls,
*semionauts* who can produce endless narratives and journeys within information.
However,
once the art critic gives examples of such an artistic approach and dissolution of the barrier between consumption and production,
a completely different image is painted and which clearly demonstrates the discrepancy between remix as a creative mechanism and remix as a controlled environment.
When Bourriaud quotes French artist Dominique Gonzalez-Foerster,
the pre-condition of this new practice becomes in particular very clear:


> Even if it is illusory and Utopian,
> what matters is introducing a sort of equality,
> assuming the same capacities,
> the possibility of an equal relationship,
> between me
> - at the origins of an arrangement, a system -
> and others,
> allowing them to organize their own story in response to what they have just seen,
> with their own references. [@Bourriaud:post, p. 13]


But,
how can equality exist when such forms of sharing are built on the premises that these systems are,
in fact,
understood as an *origin* to which *responses* are expected?
In this situation,
remixing is limited to a process in which an original becomes the source of an anticipated creative chain reaction.
The spectre of access and potentiality discussed in the previous chapter returns,
from the famous artist waiting for an audience to recombine the elements of their work,
to CC waiting for users to recombine the elements of their digital commons.
Similarly,
free culture supporters have done poorly in making their cause resonate beyond the concerns of very few privileged classes.
According to scholar Laura J. Murray,
the popular documentary *RiP: A Remix Manifesto* is essentially the glorification of a North American white male middle class culture,
in search of some Robin Hood-like thrills by doing something that could be illegal,
and where stereotypes of gender and stardom are carried with absolutely no awareness or reflection. [@Murray:rip]
These disconnections between the way these discourses present themselves and how they materialise are,
however,
not specific to contemporary cultural appropriation of remixing.
In fact this ambivalence was already present in one of remix culture's exemplary cases:
dub music.


In late sixties Jamaica,
dub music was born from a mixing mistake that led to the creation of a dubplate---a non durable acetate disc on which the master recording was cut for testing and demonstration purposes---from which the vocal track was omitted.
The disc was nonetheless kept and played during a sound system gig.
The lack of vocal track was turned into an opportunity for the deejay to improvise over the instrumental music,
to the delight of the crowd.
^[
The origin of the myth and its narrative is slightly controversial,
and there exists different accounts on the birth of this genre. See [@Navas:remix, pp. 37-38].
]
The immediate success of the accidental performance of both the studio mixing and of the deejay,
toasting and chatting over the faulty dubplate,
led to one of the richest musical dialogues of the twentieth century,
where the live performance of sound system deejays inspired producers to make new *versions* of Jamaican songs,
using the sound mixer and effects such as spring reverb and tape delays as improvisational instruments.
The resulting dubplates fed back into the sound system improvisation culture,
and back again into the studios.
Looking at dub music from the sole perspective of the remix as a creative mechanism,
it is possible to draw extensive analysis on its formal aesthetics,
its sonic qualities,
and its semiosis [For more discussion on the study of remix, see @NavasGallagherBurrough:remix].
However,
it is also possible to look at dub music from the perspective of the remix as a controlled environment.
American scholar Michael Veal has written extensively on the history and development of dub music[@veal:dub],
and has most notably analysed the original dub culture beyond its technical and sonic qualities.
From Veal's research,
it is not a big stretch to say that dubplates functioned as an addictive product,
sold to competing sound systems which,
in order to attract an audience and sell alcohol,
constantly needed new stocks---the dubplates were self-deteriorating plates---and new uniquely cut versions of popular tunes.
Sound system sometimes even paid extra for dubplate *specials*,
a particular version of a popular song,
with the lyrics modified to praise the sound system that had effectively paid for the placed advertisement,
but essentially these sound systems were advertisement and sponsoring platforms for the tracks used as source material for the dub versions. [For an overview of the dub music market and relation to sound systems, see @veal:dub, p. 54]
Here remix worked as a sort of twisted reverse crowdfunding scheme,
where the original tracks were never given to the sound systems,
and were only made available to purchase in music stores,
owned by the same studios that produced the dub versions.
On top of that,
at the source of the versioned tracks were studio musicians working under very precarious conditions,
and were often required to come up with finished music for a whole album to be recorded in a day,
or provide a series of reusable beats and melodies with no possibility of claiming copyright.
The riddims---essentially a database of artistic media---from which new tracks and their dub versions could be generated over and over again [See @Taylor:phd, pp. 53-56].
With many musicians in Jamaica,
the competition and pressure to make riddims for studios was very high,
even with terrible working conditions.
The situation was also amplified due to poorly implemented copyright law in Jamaica---initially imposed by the UK in 1911 with little consideration of the local oral culture in folk music---which allowed the rise of a copyright infringing Jamaican music industry, 
which treated music as a public domain resource [@Taylor:phd, pp. 42-59].
This however became an issue once some Jamaican musical genres and artists,
in particular Bob Marley,
started to profit greatly from a globalised music industry,
and led to the ban of all versioned music from radio airing in Jamaica following pressure and lobbying by the musicians' union.
This action was however mostly a symbolic gesture as sound systems were much more powerful advertising platforms than radio [@veal:dub, p. 91].
As Veal concludes,
despite its counter hegemonic nature,
as well as the singing and improvising to an instrumental popular track used as vector to carry new discourse,
there was a another side to such practices,
with dub becoming the sound of "profit consolidation and competition" [@veal:dub, p.90].


With this example,
I am not arguing that a conservative and restrictive approach would be more beneficial than unregulated copyright and public domain derived cooperation.
Instead,
I want to point out that the free circulation and transformation of information cannot be directly linked to an egalitarian participation in a liberated productive apparatus.
This shortcut is indeed problematic because it ignores the aspect of political economy in relation to these practices.
Even if a detailed analysis of such an aspect is out of the scope of this dissertation,
it nevertheless cannot be completely ignored,
discarded,
or rendered moot by considering free cultural practices as existing in a vacuum.
In that sense,
this free circulation of information encourages that new things constantly compete to become new mediating hubs or nodes of capitalisation,
and thereby providing the ground for a near textbook illustration of classical liberalism,
in which coordination by a central agency is replaced with a system of competitive arrangements of information conveying agents [See @Hayek:serfdom, pp. 50-52].
Exploitation that is driven by the division of labour in such a chain of distribution is therefore no more absolute but instead relative,
which explains why the counter-hegemonic power of the Jamaican music industry,
which has also liberated and empowered the sound engineering culture of Jamaica,
comes at the price of reducing the cultural and political power of folk musicians.
In particular,
I can see an inversion of the situation of the subversive use of folk songs in mid-eighteenth century Paris,
because Jamaican lyricists,
but also song writers like Bob Marley,
saw dub as a damaging practice in which political texts were removed,
erased,
and overshadowed by effects and mixing techniques [@veal:dub, p. 78].
According to Veal,
the whole versioning process can therefore be sensed as a direct result of capitalist influence in the making of music,
that turns folkloric practices into a calculated economic strategy based on a complex and possibly endless archaeology. [@veal:dub, p. 89] 
Therefore,
if the remix nature of dub music can be celebrated for being a cultural glue and creative process,
it simultaneously presents itself as the sound of a "society tearing itself apart at the seams" [@veal:dub, p. 206].


In the context of remix culture,
instead of putting into perspective the different levels of empowerment and their cost to the community,
the idea of an evil,
central and easily identifiable source of control---essentially inherited from the post-war era reaction to central planning [@Popper:open1; @Popper:open2]---creates the foundation for a deceptive subcultural heroic discourse.
In the same way that the non-trivial symbiosis between capital and free and open source software communities can be mistaken as a "black and white dramaturgy of profiteering villains" [@Sodeberg:hacking, p. 31],
the copyright infringing remixer is often portrayed as a beloved liberator,
a David fighting an evil Goliath.
Depending on the context,
the evil Goliath becomes a replaceable figure who embodies the record industry,
the film industry,
the publishing industry,
and all sorts of media industries.
Of course,
there is an urgency to address today's folly found in many intellectual property related incidents,
from a media industry lobbying for more punitive actions against the sharing and distribution of copyright material,
to appropriation artists and musicians suing each other ad nauseam over sampled materials.
However,
by articulating these issues in a such a way that those who prevent the free circulation of information are systematically impersonated by evil entities [See @rip:remix],
the free culture discourse struggles to depart from a Nietzschean position of *ressentiment* [In reference to @Nietzsche:genealogy, "Good and Evil", "Good and Bad" (1887)],
and this hostility prevents free culture supporters from fully evaluating the consequences of their propositions.


Finally,
the deception of the remix as a satellite of an original,
lies in the artificial influence of the original,
that is specifically possible because of the locking down of culture by some industries.
It is therefore ironic,
and problematic for free culture,
to use such a practice to make its point,
and therefore suggest an egalitarian cultural landscape,
in which in fact the stardom driven remix aesthetics they use in their narrative could not exist,
and would eventually be replaced by new structures of mediation in which other mechanisms of artificial influence would operate.
The author-centered regime of the information society that Boyle had warned against [@Boyle:shamans, Chapter 11] is therefore not resolved in free culture but only displaced
^[
This problem of artificial versus egalitarian influence is very concrete:
a white label record mashup is more likely to contain a drum sample from Michael Jackson's Thriller than some drumloops produced by the neighbourhood kid and released under a CC license on a sample sharing site such as freesound.
Even the few Internet memes that do not appropriate from pop cultural icons,
end up at the centre of variations that reinforce their central authority,
such as Wojak/Feels Guy/twarz,
the bald man image used in all sorts of situations to express emotional situations with *feels*,
or trollface,
the popularity of which made its author a royalty annuitant. See [@KYM:feels; @Klepek:trollface].
].


Following the discussion of free cultural techno-legal templates that I developed throughout this thesis,
it should become clear that the questions that matter are not about the potentiality of the free circulation of information,
or the novelty of its form,
but rather how such information comes into existence,
what kind of technological,
social,
and political frameworks permit its access,
what networks of software it requires or gives rise to,
its wider aesthetic inherences and affordances.
It should also become clear how such frameworks influence the groups that inhabit the structures formed by these templates.
Lessig's RO versus RW file system approach to remix and free culture therefore needs to be challenged in its own metaphorical domain:
who owns the file?
Where is it located?
Why can it be accessed?
Who benefits from reading from or writing to it?
Full permissions over a small element of a system does not imply complete control over the latter.


The Early Days of Mixes Between Operating Systems and Social Systems
--------------------------------------------------------------------


By using the RO versus RW metaphor,
Lessig may have underestimated how relevant such an analogy was for the so-called geeks he was referring to,
in particular those sensible to cultural environmentalism,
and whose political life "have indeed mixed up operating systems and social systems in ways that are more than metaphorical" [@Kelty:bits, p. 38].
In fact,
the idea of a computer environment mixed up with social organisation could already be found in several early seventies projects.


The 1973 project Community Memory in the San Francisco Bay area provided three public terminals for a common database,
a resource sharing,
in which people could read and add information (Figure \ref{fig:cm}).
The system,
developed by Resource One Inc. a non-profit corporation and one of few public service computer centres,
ended being used beyond its creators expectations:
student tips,
musician and chess players' announcements,
car pool organisation,
restaurant reviews,
as well as poems and graphics. [@ColstadLipkin:cm]
People queued and taught each other to use the computer,
and according to Berkeley Free Speech Movement activist Michael Rossman,
the system was "inescapably political" [@Rossman:cm],
its politics were "concerned with people's power" [@Rossman:cm],
as anyone could access the network that Rossman considered as the ultimate participatory democracy without central authority,
and of public utility.


> [I]n this system no person or group can monopolize or otherwise control people's access to information.
> Information-power is fully decentralized.
> No editing,
> no censoring;
> no central authority to determine who shall know what in what way.
> 
> [...]
>
> [U]sers of the system must take responsibility for their own judgements about its data,
> supported by whatever judgements other people offer to them through the system. [@Rossman:cm]



\afterpage{%
\begin{figure}[h]
	\caption{Community Memory walkthrough}
	\centering
    \includegraphics[width=0.6\textwidth]{cm-walkthru} 
	\fnote{Photo: Mark Szpakowski, 1974, CC BY-SA 2.5}
    \label{fig:cm}
\end{figure}
\clearpage
}



What is interesting here,
is that even though the system had been clearly designed and programmed with no ill-intention,
it was nevertheless a completely centralised time-sharing system,
from which terminals were used to connect and edit content,
a sort of proto-cloud.
The users had to trust that the anonymous access they were given,
was truly anonymous and that no extra time stamps and information about which terminal was used,
were also recorded,
that the content in the central computer was not tempered with,
and that deleted information was not in fact simply kept.
With this example the notion of mixing up becomes indeed very strong,
because the computer system did not merge with a certain type of social organisation to create a third thing,
instead it created a situation in which two realities co-existed.
Said differently,
the purpose of the system and its perceived social dimension made the network topology appear radically different from its physical and technological reality,
and made it look like a decentralised and anti-authoritarian network.
The contemporary difficulty to articulate the relationship between social systems and the Internet are rooted in this conundrum.
In that sense,
and to give a contemporary illustration,
the pseudo-anonymity and pseudo-privacy offered by image boards like 2ch, 4chan, or 8chan to name a few,
is nothing but helplessly echoing these questions.
^[
With this I mean that the belief of anonymity provided by these platforms is illusory.
Regardless of whether one's post is signed *anonymous*,
it does not guarantee that one's IP adress and other uniquely identifiable information is not gathered and tracked from these centralised discussion platforms.
For instance,
even if justified as a way to prevent abuses,
4chan does not allow access to its service with the anonymous communication software Tor. See [@4chan:faqtor].
]


What is more,
this discrepancy,
between how a computer system is perceived and how it effectively operates,
allows for the materialisation of different social systems.
For example,
it is perfectly possible to portray a Unix-like operating system as a top-down authoritative hierarchical organisation,
that "is deeply indebted to culturally determined notions such as private property,
class membership,
and hierarchies of power and effectivity." [@unsworth:livingos, p. 142]
And yet,
the same system has nonetheless spawned a very rich network of machines built and inhabited around principals of egalitarianism,
sharing,
decentralisation and cooperation;
literally turning the capitalist-like file system organisation,
into a constellation of networked communities,
from hackerspaces [@HackerspacesWiki:list],
to artist-run servers [@monoskop:artservers],
as well as mutual help activist infrastructures [@Riseup:servers].


To explain how such contradictions have come about I must once again turn back to the early days of computational culture,
which had yet to be exposed to the division of labour and managements hierarchies [@Berry:copy, p. 106],
simply due to the lack of a clearly defined computer market or business at the time,
as discussed in Chapter 1. 
According to American historian Roy Rosenzweig,
Community Memory merges impulses from the radical sixties with the hacker ethic.
To make his point,
Rosenzweig explains that the founders of Community Memory included Lee Felsenstein,
who made a living as a computer engineer,
whilst being a New Left radical linked with the Free Speech Movement. Felsenstein was also the son of a district organiser of the Philadelphia Communist Party. [See @Rosenzweig:wizards]
Felsenstein,
who was also member of the influential hobbyist Homebrew Computer Club,
from which the idea of the personal computer was first articulated,
was one of the many engineers that developed a common hatred for large centralised proprietary mainframes:
*computer liberationists*,
in Rosenzweig's own words, 
who were interested in the potential of computers as a vector of decentralisation,
democracy,
and freedom.


In parallel of this political impulse,
the larger time-sharing operating systems they opposed also started to provide social software built into the system,
all the while the production and development of software was increasingly reliant on automation,
and had already started to be envisioned as if it was a factory process [@Bemer:machine].
Computer hacker Don Hopkins recalls that many of these social programs were available on the Incompatible Timesharing System (ITS),
used throughout the seventies and eighties,
such as \texttt{:UNTALK}, \texttt{:SEND}, \texttt{:REPLY}, \texttt{:INQUIR}, \texttt{:WHOIS}, \texttt{:FINGER}, \texttt{:USERS}, \texttt{:WHOJ}, \texttt{:PEEK}, and \texttt{:OS},
all providing the software's bricks and mortar needed to build a cohesive social structure inside the machine:
^[Emails to author, February 17, 2015.]


> The MIT-AI lab’s ITS machines had several ways of chatting and socializing through the host.
> You had a lot more awareness of who was on,
> what they were doing,
> and what they were into,
> than most other time-sharing systems of the time.
> Many people would stay logged in all the time,
> just to be social,
> read email,
> send text messages,
> and chat.
^[Ibid.]


The communal sense of these groups was best demonstrated with the software shelter and educational environment,
that they provided for the under-privileged living outside of such academic networked walled gardens.
For instance,
under the name *Tourist Policy*,
the MIT AI Lab allowed people outside of the lab to apply for accounts,
and use the system during off-hours,
in order to learn how to program and access the network
^[Ibid.].
But most importantly,
the documents in these systems were made available with no restrictions whatsoever:


> And another very social feature was that there was not file protection,
> and it was considered perfectly acceptable to learn by reading other user’s files.
> Deleting and vandalizing wasn’t considered socially acceptable of course,
> but since there was no challenge to it (and the user community was so small),
> it wasn’t a big problem.
^[Ibid.]


If the history of computing and its impact on society would have stopped right here,
Lessig's RO versus RW geek-speak metaphor may have been strong enough,
because it essentially depicts a de-contextualised binary situation of file access in the context of communities small enough to have a good understanding and overview of the system they participated in.
However,
as I will now explain,
the exponential growth in usage of these systems and the reality of their control mechanisms greatly negates any positive effects that could have come from using such a simple shortcut to address the free circulation of information.


Policies, Jails, Chroot and Sandboxes
-------------------------------------


Hopkins also wrote to me that a lot of these network developments and usage were not business-related,
and essentially existed in a grey area.
As it turns out,
and as computer networks grew,
such grey areas became difficult to maintain in an ad hoc fashion and started to require policing
^[A copy of the rules can be found at [@ITS:tourist].],
and in some cases more secretive rules.
This was the case with ARPANET,
which subcultural activities could not be communicated to the outside world,
in order to avoid the Defense Advanced Research Projects Agency (DARPA) losing public and political credibility over their military projects.
This led to the infamous 1985 ban of Science Fiction writer Jerry Pournelle from the network who lost his guest/tourist account,
after mentioning inside stories about the network several times in his columns for the microcomputer magazine Byte. [See @Hopkins:pournelle]
In that sense,
if today Usenet,
the Unix powered network that I introduced in the first chapter,
is still remembered rightfully as a unrestricted and poor man's ARPANET alternative [See @Hauben:usenet],
it should not overshadow the fact that the cultures of the two networks were equally busy with human to human network communication and social organisation,
the difference being that one was not explicitly allowed to communicate about any one of them.
Of course ARPANET and Usenet are taken as examples here,
because of their relevance in the context of this thesis,
and rather important visibility in computer history;
however,
it should not be forgotten that the seventies and eighties proto and early Internet days,
saw dozens of important networks initiatives from different origins,
research,
corporate,
cooperative and so-called metanetworks,
which all shared similar social dynamics,
to those exemplified here
^[For an extensive survey of such networks, see [@QuatermanHoskins:networks]].
In the end,
all these systems were eventually adapted to serve more personal goals and social interests [@Markoff:dormouse, pp. 104-106],
showing the democratic transformations that could occur within such platforms,
no matter what were their original purpose.
^[
Here,
I am essentially paraphrasing and referring to the optimistic analysis of such transformation that philosopher Andrew Feenberg made in the context of the French Minitel,
but entirely relevant to the older networks that I just mentioned.
See [@Feenberg:transforming, pp. 118-120].
]
This is also the reason why,
most notably with ARPANET,
the adaptive bottom-up communication systems have often been associated with the counterculture movement,
even if their origins belong to different contexts [@Scaruffi:sv, Chapter 6].


The direct consequence of these transformations became visible in the way the operating systems running these networks were designed.
Here again,
Unix is exemplary to show how the code to organise social activities,
and the code to execute on machines are often intertwined.
In particular,
an important aspect of a Unix-like environment is its organisation as a hierarchical model,
in which everything is represented by files,
^[
Which is to say,
that almost every aspects of the system,
including devices,
are exposed to the file system.
Based on the explanation on pipes and input/output redirection presented in Chapter 1,
because of this approach,
file manipulation tools can also be used to manipulate hardware devices.
For instance the command \texttt{cat /dev/mem > /usr/home/merzbow/mymemory} will dump the memory of the computer to a file named \texttt{mymemory},
but \texttt{cat /dev/mem > /dev/dsp} will dump it instead to the soundcard,
making *audible* the machine's memory.
]
and arranged in a tree of nested directories.
In such a system each file and process has a single owner.
Users belong to different groups which gives them different permissions to navigate in some parts of the tree structure,
as well as read,
write,
and execute files in the system.
This is basically the technical information on which the RO versus RW metaphor can be understood.
However this is not all,
users are also given a home directory in which they can manage their own files.
In multi-user systems,
regardless of whether they are proprietary or not,
Unix-like or not,
the home directory is a personal and privileged place in the file system,
an entry point after a successful log in,
where a user stores personal files and programs,
but also a place to set configurations and preferences for any given software in the system.
Every popular multi-user operating systems has home directories.
The access to the files that can be experienced through many different mediating layers,
from graphical user interfaces (GUI) to command-line interfaces (CLI) [See @Cramer:echo].
Last but not least,
and sitting on top of the mountain,
a superuser,
called the \texttt{root} user,
possesses all the permissions in the machine,
that is full access and control over every single process and file,
including of course the private ones in the users' home.
Even though this is a very quick overview,
it already projects a much richer imaginary than the one found in the RO versus RW comparison.


Depending on the privileges of a user,
one can either be trusted to take care of other people's home and files,
or trust that a more privileged user will take care of one's home and files,
while not abusing such power.
As it turns out,
the nineties success of GNU/Linux is tightly connected to this structure,
as the access to a UNIX operating system running on affordable hardware such as PC-compatible home computers,
gave the availability for every user to become a local \texttt{root} at a time where UNIX systems were running on expensive machines,
to be accessed remotely as a simple user.
The relative nature of exploitation within liberal cultural production that I was discussing with the remix,
is therefore also technologically implemented in these systems where RO and RW permissions---and to be more complete also *execute*---are isolated in nested structures of relative,
but not absolute,
access.
What is more,
this construction might be completely invisible to the user.
It is in this particular situation that the Unix command \texttt{chroot} becomes a much more powerful file system inspired geek-speak metaphor than RO versus RW.


Added in 1979 to the seventh edition of Bell Labs' Unix [@Lewis:unixhistory],
the \texttt{chroot} program manipulates the way the file system is *perceived* from a user or process perspective.
It does so by moving the apparent uppermost directory of the file system to another location,
thus preventing the *chrooted* users and processes from accessing anything outside of this metaphorical *jail*.
Said differently,
the sub-folder one is jailed inside appears as the base and starting point of all the other folders in the system,
while others,
non-chrooted users and processes,
can see one constrained in the space and resources one has been allocated.
More specifically when discussed in the context of security,
the technique is literally called a chroot jail [@wheeler:secure],
and in some operating systems like FreeBSD,
such technique expanding upon the \texttt{chroot} idea,
is simply called a \texttt{jail} [@Riondato:jails].
It's not by accident that the act of getting administrator rights on an iPhone,
which operates a Unix based system,
is called jail-breaking:
the hack is a form of *privilege escalation* that aims at liberating phone users from their jail,
and subsequently giving them access to the full Unix machine hidden behind their golden cage GUI.
Similarly,
in this universe of software class struggle,
on Android phones,
also a Unix-like OS,
the term *rooting* refers to the process in which the phone user can modify the operating system,
so as to gain superuser permissions: that is becoming \texttt{root} by means of a software assisted coup.


Ultimately,
the introduction of the \texttt{chroot} program was a tipping point in which trust and social organisation within operating systems,
could no longer be solved with ad-hoc moral guidelines,
and in that sense predating virtualisation and the cloud,
that further obfuscated and further mediated the relationship between users within these systems.
Yet once chrooted,
a user or process is given the illusion of complete freedom when they are in reality sandboxed.
The term sandbox is in fact frequently used to describe all sorts of testing,
secure containment,
and prototyping practices that require both the experimental potential offered by such an isolated and malleable place and the illusion they provide to the sandboxed users or processes.
This aspect makes such digital sandboxes similar to physical box full of sand in a children's playground,
where things can be invited,
bounced,
created,
abandoned,
contained,
constrained,
interpreted,
experimented,
censored,
populated and grown.
Said differently,
it's a world on its own.
Most importantly,
it implies the existence of a higher level structure,
and therefore context,
which all these actions are ultimately nested within.
The sandbox is within the playground,
that is within the park,
that is within the city,
that is within the state, etc.


So what matters is not so much if access is provided with read or write permissions,
but the conditions and context for such access,
both at a software and legal level.
If RO and RW are used to illustrate a simplistic understanding of cultural processes within free culture,
using chroots to talk about these instead,
forces us to acknowledge the existence of containment in free culture,
a sandbox culture indeed,
a model that highlights the duality of the aesthetic of consumer society,
^[
Here I make reference to American political theorist Frederic Jameson,
who questioned the ability of postmodernism to resist the logic of consumer capitalism.
See [@Jameson:consumer].
]
that I earlier exemplified with dub music,
but that also resonates with the tension between freedom,
constraints,
and creative territoriality,
which I discussed several times so far in relation to the situations created by the use of free cultural techno-legal templates.


Another point that I want to address is that the enclosure of the information commons,
to borrow a term from software developer Dmytri Kleiner [@Kleiner:teleko, p.20],
is not strictly of a capitalist nature.
In this sense I disagree with an historical creation myth of the Internet as a common and shared resource,
as I have shown that even in the early days of proto-Internet infrastructures,
these systems already worked as a series of enclosed commons and shared resources only accessible *within* specific techno-legal walled gardens of varying ideologies.
Capitalism and the growing commercialisation of the Internet might have enclosed,
or more precisely overlaid their own enclosing structure on top of some already existing resources but they are in fact essentially focused on mimicking the successful structures that support these resources,
not quite enclosing existing commons but in fact facilitating the sandboxing of new commons to be capitalised upon.
This means that the notion of cultural environmentalism from Boyle [@boyle:politics], 
which draws an analogy between ecological and cultural issues,
works only so far with the notion of public domain cultural expressions and not so much with new forms of digital commons that are created as part of a logic of cultural sandboxing and user participation,
or as Stalder notes,
associated to a neoliberal downsizing strategy where context and embedding did not emerge from a bottom-up process [See @Stadler:solidarity, pp. 31-36]. 

\newpage
