Paradigm Maintenance and User Freedom
=====================================

Questioning the Revolution
--------------------------


As with many folk tales,
the archetypical free software stories often begin [@stephenson:beginning, p. 34],
with the presentation of its protagonist within the landscape in which their quest will unfold.
Similar to the first narrative function from the Morphology of the Folktale [@Propp:morphology],
such tales start with the *absentation* [@Propp:morphology, p.26] of the hero,
as he leaves the growing proprietary operating systems of an expanding computer industry which is becoming increasingly reliant on intellectual property laws:
from copyrights,
to patents,
trademarks, 
trade secrets,
and industrial design rights.
Very much at the opposite of these *just working* and *plug and play* software environments---championed in the context of such tales by American multinational corporations like Apple and Microsoft---the userland
^[
In reference to the Unix centric term that designates the virtual memory space outside of the kernel,
and generally occupied by user mode programs and libraries running in an operating system.
See [@Raymond:jargon].
]
our hero seeks seem to be less driven by technological consumerism.
Most importantly,
it is articulated around a strange concept:
the freedom of the user.
In fact,
according to the myths,
the claim of our visionary hero is that there exists,
somewhere,
a ground of liberation where people share their work and their tools.
They could be helping each other,
building together creative and productive software frameworks and be active members of many autonomous,
partly-federated and decentralised,
technological user groups that would form one united community and fuel this extraordinary collective effort.
The project's purpose is grandiose and the founder of this new world aims to build a better society,
one Unix command at a time.


This man is American computer programmer Richard M. Stallman.
The call to join his quest,
the 1985 GNU Manifesto [@Stallman:manifesto],
will be read and discussed in many computer journals,
newsgroups and bulletin boards.
The document promises nothing less than a prophetic paradise for computer programmers.
Moreover,
Stallman's writing is a commitment to create a whole new society based on the development of something called free software,
and the society he envisions is defined by post-scarcity economics,
a concept that describes "economic and political systems where goods are freely distributed according to egalitarian principles" [@Peters:abundance, p. 11],
and which historical antecedents can be found in various economic theories of the first half of the twentieth century,
from mutualism to automation and robotics,
and further adopted in both left and right-wing literature [For a broad genealogical overview of the term see @Peters:abundance, p. 11-12].
However,
Stallman's approach is not driven solely by economics or technological positivism,
it is guided by ethical motives:



> I consider that the golden rule requires that if I like a program I must share it with other people who like it.
> Software sellers want to divide the users and conquer them, making each user agree not to share with others.
> I refuse to break solidarity with other users in this way.
> I cannot in good conscience sign a non-disclosure agreement or a software license agreement.
> [...]
> So that I can continue to use computers without dishonour,
> I have decided to put together a sufficient body of free software so that I will be able to get along without any software that is not free.
> [...]
> In the long run,
> making programs free is a step toward the post-scarcity world,
> where nobody will have to work very hard just to make a living.
> People will be free to devote themselves to activities that are fun,
> such as programming,
> after spending the necessary ten hours a week on required tasks such as legislation,
> family counselling,
> robot repair,
> and asteroid prospecting.
> There will be no need to be able to make a living from programming [@Stallman:manifesto]



It is on such premises,
that the birth of the free software movement is often presented.
Beginning from a one person stand,
a singular position which became a universal matter,
as testified by the innumerable free and open *things* that emerged from the mid-nineties---some of which I will discuss in Chapter 2---and remain a given in software production to this day.
This transformation has made American publisher and open source evangelist Tim O'Reilly,
associate this process with American physicist Thomas Kuhn's concept of the paradigm shift [See @OReilly:shift],
where the scientific world view of the many will eventually be changed by the means of gradual conversion [@Kuhn:revolutions, pp. 151-157].
For instance,
in the 2001 documentary *Revolution OS*,
such a shift is exemplified with the tension between Microsoft Windows and the GNU/Linux operating systems,
and how the supporters of the latter are presented as active participants of a revolution that impacts both the software industry and computational culture in general [See @Moore:revolution].

This narration is in fact emblematic of the free and open source stories,
combining the thematic of revolutionary science with a loose interpretation of social revolution,
by the means of a near Hollywoodian variation of the fight between David and Goliath,
which fits particularly well with the hero rhetoric of subcultural theory [@Stahl:renovating],
and provides the reason free and open source software has often been designated as such [See @denBoomenSchafer:revolution].
If such accounts have been instrumental in fuelling the opposition between the cultural diktat of the nineties computer industry,
and the desire for a more diverse and independent computational culture,
and if it is also undeniable that different practices emerged and were inspired by this struggle
^[
This aspect will be one of the central points in the second part of the thesis.
],
it is however a very crude simplification.


The GNU Manifesto and free software have became emblematic and instrumental in the "explosion in variations" [@PomerantzPeek:50shades] of the word *open*,
a model to be followed not just for software but for pretty much everything and in which "openness breeds more openness" [@PomerantzPeek:50shades].
In this case the gradual conversion mechanism of the scientific revolution has been reduced to a fashionable adjective to put next to virtually anything,
and it is worth asking whether or not free and open source software offered a new single world view,
or triggerred instead a plethora of new world views.
This makes general analysis particularly difficult.
For instance,
the marketing of open source by O'Reilly has been recently criticised by Belarusian writer Evgeny Morozov [@morozov:hustler].
However,
by giving too much importance to the publisher in his critique,
Morozov ended up making an approximative generalisation from only one particular aspect of the free and open source software history.
His focus on dismantling what he refers to as the open source *meme-engineering*,
distracts him from seeing that the revolutionary dimension of free software,
a perspective that he supports,
is equally questionable and prone to be dismantled as well.
Even though Morozov succeeds in taking apart the image constructed by O'Reilly,
he leaves Stallman's image in line with that of the origin myth,
in which Stallman is presented as a leader of a radical social movement,
offering an alternative to proprietary Unix and Windows operating systems.


This narrative has been constructed over the years as much by free software supporters as by their opponents,
as I will show on several occasions throughout this thesis.
Free software is in fact not a *creatio ex nihilo*,
and Stallman and historians of computer science have always been very clear about this when explaining that software was always free [@GonzalezPascualRobles:fs, 2.1 Free Software before Free Software],
an aspect often overlooked but crucial in the first part of this thesis,
and this is why I will often use the term *emulation* to describe free software practices.
However, 
before unpacking what I mean by this,
it is necessary to first explain the freedom Stallman is referring to,
as well as the *users' freedom*,
often cited in the free software discourses [@FSF:what].



Source Code and the Individuation of the Programmer
---------------------------------------------------


So,
what exactly is this *users' freedom* about and how does it relate to the production and distribution of software?
To tackle these questions,
I first want to decouple the idea of liberating users from understanding how software is written.
Once some trivial technical knowledge of programming and source code is explained,
free software is in fact a rather simple idea to comprehend.
Source code specifically is an essential aspect of software production.
Understanding how it is written and published allows us to see very concretely how regulating its access can impact users:
from total alienation and control,
to the empowering liberation of Stallman's manifesto
^[
The importance of source code is also a prerequisite to test later on,
in the third part of this thesis,
the limits of a generalised free culture,
especially in cultural expression where source code is either irrelevant,
undefined,
or metaphorical.
].


As a program part,
source code,
and to be more precise the source files,
is the collection of computer instructions written using some human readable computer language,
such as C or Python.
Source files can be compiled,
like C,
which means that the source code is translated by a compiler into machine code,
that can be executed manually to perform some tasks as a standalone program,
or as part of a larger software [See @HarbisonSteel:c, Introduction].
Some files can also be interpreted,
like with Python code,
in which case the source code is both translated and executed on the fly by a piece of software called an *interpreter*,
by alternating reading of the source files and performing the requested computation [See @Downey:python, Chapter 1].
As the name implies,
source code is where everything starts.
As I will trivially demonstrate below,
its role in the production of software is technically essential,
and its access therefore allows unrestricted modification of its function,
whether it is about adding,
removing features,
or simply fixing faults.


To give a concrete example,
let's consider the following piece of plain text written by user Ada,
and stored in the source file \texttt{software.c}:


	#include <stdio.h>
	
	int main(void)
	{
		printf("There is software.\n");
	} 


This text is the source code of a very simple program written in a programming language called C.
It does not do much for now because it is described in this particular human readable language,
and not as machine language.
The latter is the set of instructions that can be executed by a computer's central processing unit (CPU),
the object code,
that can eventually become an executable program.
To translate the text above into such a file,
and as mentioned previously,
Ada needs another program,
a compiler,
such as the GNU Compiler Collection (GCC).
On a Unix-like system,
this operation can be performed by typing the following command on the computer terminal
^[
The terminal is nowadays a software application,
once born as hardware device,
and which provides a means to interact with the operating system in a textual way.
]:



	gcc software.c -o software



Upon pressing the *Enter* key,
the compiler is called and then translates \texttt{software.c} into \texttt{software}.
If there are no errors in the source code,
this command line operation should yield nothing at all,
and there should now be two files in the folder from which it was executed: \texttt{software.c},
the original C source file from Ada,
and a new file named \texttt{software},
the translation of the source code into object code.
This object code is a software itself [For a brief genealogy of the term, see @Fuller:softstudies, Introduction, the Stuff of Software],
in this case an executable computer program that can be run by Ada from the terminal,
like this:



	./software



The result of this execution,
as hinted by the venerable \texttt{printf} C function in the source code, 
is the sudden display on the terminal of the string of text \texttt{There is software}.
At this point,
the source file could very well be discarded or deleted.
It does not matter for the software,
which will still be running as long as the user has the object code file
^[
This is true as long as the software and hardware environments,
and the libraries the software is linked to,
are unchanged.
Of course.
I hope programmers,
and those working on the conservation of computational culture,
will forgive me for an approximation made in order to keep things relatively simple in this example.
].


In a way,
software is born by bringing its source file to the altar of compilation,
and this transitional characteristic is the reason why media theorist Wendy Chun describes source code as a spectral thing,
a re-source that only becomes source through its destruction [@Chun:programmed, pp. 24-25].
However,
limiting source code to a mere re-source is problematic.
Once the dimension of distributed and cooperative writing is taken into account,
this view is in fact incomplete.
Indeed,
the spectrality of source code becomes increasingly questionable once the distribution and publishing of these files comes about.
Going back to the example,
let's assume that Ada's program is so useful,
that she decides to share it with other users on the same platform.
To do so,
Ada has two options.
She can either distribute the source file,
in which case the other users will have to compile it themselves,
or she can just give the executable program,
so that users only have to run the software.


First,
let's see what happens if she distributes the source file \texttt{software.c}.
In our hypothetical userland,
she gives the source code to user Friedrich.
But Friedrich is partly dissatisfied with what he reads in the file and decides to slightly modify it before the compilation:


```
#include <stdio.h>
	
int
main(void)
{
	printf("There is no software.\n");
}
```


In order to create the executable code,
he then runs a similar command to the one typed by Ada,
however this time using a different C compiler:


```
clang software.c -o software
```


The output is like GCC in the sense that two files are now present,
\texttt{software.c} and \texttt{software},
with the difference that if Friedrich runs his software,
it will now print on the terminal display the following text:
\texttt{There is no software}.



Practically speaking,
and simplifying a bit,
we can say that the software that has been created here has now two branches:
Ada's version and Friedrich's version.
Each presents a particular viewpoint,
thinking,
state of development,
implementation,
or feature,
that is meaningful for its user.
Programming is not just problem solving and computer scientists have early on described it as a literary practice [@Knuth:literate, Computer Programming as an Art]
where the thoughts,
algorithmic vision,
and sensibility of the programmer author,
turns source code into a medium to express more than efficient problem solving.
^[
For a more thorough discussion of this matter,
see [@CoxMcLean:speak, pp. 7-11].
]
If,
according to American computer scientist Joseph Weizenbaum,
compulsive programmers are absorbed in a self reflective conversation with their computers,
where they build worlds of their own making and in which the machine challenges their power [@Weizenbaum:computer, p.119],
source code is therefore much more than an undead spectre:
it is the vessel of the computer as a *psychopomp*
^[
I use here the term psychopomp,
both in a mythological and Jungian sense, see [@Jung:aion].
After all,
it is common to anthropomorphise computers, 
or consider them as anima and animus,
which is why I believe they can perfectly fulfil their psychopompus role in the technologically alienating society.
I will explain in Chapter 2 how the process of individuation is particularly visible in the field of open design.
].
Thus compilation is in fact a rite of passage,
that connects the programmer's Ego and their Self,
and which fragments of the latter are now captured as software,
as part of a process of individuation.
^[
In that regard,
today’s concerns about a singular software and algorithmic driven dystopia can be best described as the disruption of the process of individuation of the software user,
by the (en)coded social imaginary of programmers and those employing them.
]

Even though the compilation of executable code as a by-product of compilation and interpretation is a task that may be completed,
the writing of source code files is something that has the potential to never be finished.
In fact,
and from the perspective of semiotics
^[
And more particularly in connection to [@Eco:open].
],
despite the apparent closedness of the deterministic translation layers present in programming,
I can see two fundamental levels of openness in source code as a literary work.
First,
the source code is an open text for the different software readers which will interpret it more or less differently,
leading to the production of different binaries.
Different C compilers,
no matter how simple the source code is,
will indeed produce different object code,
therefore introducing internal changes in the executable program.
As a result,
the programs produced will perform slightly differently depending on the parsing and translation of the source files
^[
A risky but useful analogy nonetheless,
can be made with the translation of an essay,
from one source language to another,
and for which the meaning must be of course preserved.
It is easy to assume that different translators will produce different wording and introduce their own subtleties,
yet hopefully the resulting essays should in theory convey the same meaning to the readers,
and not betray the original intention of the translated author.
Similarly,
different C compilers will translate source code into object code with their own assemblage of instructions,
while still producing,
it is hoped,
the same expected functionality for the user.
The differences between GCC and Clang cited here are obviously both negligible and irrelevant with the source code examples that I give here.
The impact of these deviations belong to discussions on the benchmarking of compilation time and execution performance,
critical for the making of large software suites or cycle intensive computations,
and well outside of the scope of this text.
].
Second,
the source code is an open text for the different human readers who will interpret differently its significance,
value,
aesthetics.


If the immediate consequence of this difference of interpretations between humans and machines,
can contribute in the most trivial way to the manifestation of software malfunctions,
but also allows for source code obfuscation [see @BroukhisCooperNoll:IOCCC] and deceptive executions [see @Craver:underhandedC],
it also shows that source files and their interpretation are central in this process of individuation.
For instance,
for my explanations of compilation I have purposefully used two different C compilers,
the source code of which are in fact available under different free software licenses.
GCC uses a copyleft license and Clang relies on permissive licensing,
that can be on some occasion associated to copyfree or copycenter licensing.
I will not enter into the details of these forms of licensing just yet,
for now I will just mention that these are fundamentally opposed approaches to free and open source software licensing.
When Friedrich selects Clang instead of GCC,
it might not just be for the different interpretation and parsing quality of the compiler,
it could simply be guided by an ethical or economical belief which leads him to avoid copyleft software,
regardless of the license of the software he is compiling and writing.
By picking up a specific software compiler here,
the choice is not based on the compiler executable code,
but on the imaginary provided by the compiler source code and its legal function.
The resulting compiled software can never be disconnected from this process,
it inherits a permanent coloration from both its objective and subjective interpretations
^[
This issue can be quickly exemplified with the case of the FreeBSD operating system deprecating GCC in favour of Clang. [See @FreeBSD:GPL; @Raymond:gcc; @Stallman:gcc].
].




Engineering Freedom and User Groups
-----------------------------------


Previously,
I made the point that free software was presented as the trigger that led towards an explosion of variations of the word open.
However as I have shown with the dual openness of source code and the issue of interpretation,
it is not so much that free software per se is at the origin of such divergences,
but more that it made visible a pre-existing source code *différance* [In reference to @Derrida:marges, La Différance].
Similarly,
and as I will develop in this section,
the much chanted cooperative mechanism of free and open source software [Notably articulated in @Benkler:penguin],
finds its roots in the very birth of the computer industry,
against which free and open source software have been presented as alternatives [@stephenson:beginning].
The idea of an active contribution to a shared body of technical knowledge is indeed nothing new.
It is the direct legacy of academic research and engineering traditions,
where the access and contribution to existing knowledge have to be facilitated in one way or another,
so as to ease innovation and improve efficiency,
a process well known with manufacturing blue prints [@LemleyOBrien:reuse].
In particular,
the second form of openness combined with writing access,
means that source code is constantly *open* for changes.
It has the potential to be a human driven evolutionary assemblage of text
^[
See [@Spinellis:evolution].
],
that can through time be modified and enhanced
^[
I'll purposefully keep the quines, 
the emerging properties of cellular automata,
as well as self-documenting and self-generating software outside of the discussion,
as they are more exceptions than generalised rules on the production of software. 
At least it is still the case today in our humble *pre-singularity* times,
where the evolutionary nature of software still heavily depends on a metaphorical form of commensalism with human beings.
],
to always improve the previous version,
to always aim for something final and superior,
yet always superseded by better solutions and alternatives:
a never ending prototyping quest.


This collaborative and cooperative prototyping culture has in fact been subservient to the development of the computer industry in the middle of the twentieth century.
At the time,
mainframe computer customers needed to develop their own tools in order to write programs.
This naturally led to duplicate efforts as some of the steps and assemblers developed in the process were similar enough across the different customers applications.
The reason why computer hardware manufacturers,
such as IBM,
were able to expand their business in the nineteen fifties,
was specifically connected with the attempt to promote collaboration and cooperation between customers, 
and also with the corporation building the machines [see @Akera:share].


It is of course outside the scope of this research to provide an extensive history of computation.
However,
I must mention that the advance described here has been essentially accelerated by the introduction of general purpose computation and programming
^[
Such reflection on general purpose computation is best illustrated in the 1936 paper on the *Entscheidungsproblem* by British mathematician Alan Turing [(@Turing:ent)],
and followed in 1945 in a draft report from Austrian-Hungarian born American mathematician John von Neumann [(@vonNeumann:firstdraft)].
By introducing the idea of general purpose computation and facilitating the storing of programs running on the former,
computation would no longer be perceived as the result of extensive and dedicated electronic engineering,
but by a combination of two,
possibly three parts: the hardware,
the data,
and the programs,
which would be much later on be referred to as software.
The case of what will be eventually known as the von Neumann architecture is often portrayed popularly as seminal work in the development of the personal computer [(see @Rheingold:tools, Chapter 4)].
However,
at the time of the draft's circulation,
the whole field of computing already pointed towards the design of general purpose computers.
For instance the 1944 relay based IBM Automatic Sequence Controlled Calculator (ASCC),
also known as Mark I,
conceptualised by computer pioneer Howard Aiken,
built by IBM engineers,
and used by von Neumann himself during the Manhattan project,
led to the Harvard architecture that differed from the von Neumann architecture in the way data and instruction did not share the same memory space.
For a detailed historical overview of the now forgotten work of Aiken during the era of the first general purpose computers,
see [@Cohen:aiken].
Today’s computers are effectively variations and mixes of both Harvard and von Neumann architectures.
As for the concept of programming discussed here,
it is essentially the latest iteration of a long legacy of rationalist computational culture that can be traced back to Leibniz's symbolic logic. See [@Cramer:words; @Davis:engines]
].
Unlike the first generation computers which were literally wired to solve one specific set of problems,
with the programming of general purpose computers it also became possible to share these programs with others,
and arrange the programs to work with other programs.
From this point on,
new kinds of software emerged and were named from this growth,
most notably the programs that help manage other programs like the operating system and its utilities.
The so-called user-friendliness of desktop metaphors that is taken for granted today with popular OS such as Windows and MacOS [@Chun:control, pp. 22-23],
as much as the so-called transparency and ubiquity of the user interface (UI) and experience (UX) of tomorrow's latest pervasive and smart calm technology [See @Case:calm; @WeiserBrown:calm],
was in fact non existent in the early days of programming.
An operating system then merely represented the most barebones environment and framework to make more programs.


Now where to find programmers? 
This was the last problem the rising computer industry had to solve in order to facilitate a wider adoption of their prototyping and solutionist culture.
So in practice,
the creation of communities of code sharing computer users provided a very pragmatic answer to the problem faced by this industry,
namely the need to educate as quickly as possible the growing numbers of operators of this early mass-produced novel canvas of computational wonders.
Said differently,
the industry needed its clientèle to become programmers and participate in the shared exploration of software and general computation.
Therefore,
in a time where no customer support existed,
and where university curricula on programming were in their infancy
^[
Professor of the history of science I. Bernard Cohen credits Howard Aiken,
from the Mark I fame,
for being the first to start an academic program about computation at Harvard in 1947,
a "one-year program leading to a Master of Science degree in applied mathematics with special reference to computing machinery".
See [@Cohen:aiken, p. 186]. 
],
the creation of a collaborative learning and semi autonomous problem solving groups was the best way to disseminate,
acquire,
and improve knowledge about general purpose computation,
as shown with the very appropriately named SHARE user group [@Daffara:society, p. 38].
The latter,
created in 1955,
was backed up by IBM [@Akera:share],
which benefited directly from strongly encouraging the customers of its 701 and 704 systems to meet,
share their problems and solutions,
while financially supporting the user group infrastructure [@Salus:daemon, Ancient History].
In parallel with the emergence of these corporate sponsored communities of users,
another important change hit computer science,
and added yet another social dimension to the use and sharing of computer software:
time-sharing.


Introduced in the late nineteen fifties,
time-sharing enables the sharing of computation time amongst several users,
so that one person does not have to wait for someone else's calculation to complete on a mainframe computer before starting their own [@hafner:wizards, p.25].
In the nineteen sixties, 
and combined with the increasing exchange and writing of programs within specialised user groups,
this idea became both central and instrumental in taking computation outside of its academic communities,
aiming to finally make relevant its existence for society as a whole.


In the context of the 1963 Defense Advanced Research Projects Agency (DARPA) funded project on Multiple-Access Computer,
or Machine Aided Cognitions (MAC),
Italian-American computer scientist Robert M. Fano explained the underlying principles of what he would describe as the computer utility approach [See @Fano:mac]:


> The principal aim is to develop and investigate experimentally a new way in which computers can aid more effectively people in their creative work,
> in their thinking,
> in any field,
> from research,
> to engineering design,
> management,
> education,
> and so forth [...].
> 
> [T]he user does not have to instruct the computer on how to do everything that he wants to get done,
> but only the parts that are very special and very new to the problem which is concerned at that time.
> Many facilities are already available and stored within the system,
> so that in a sense each individual has available,
> literally at his finger tips,
> the work of many people that have preceded him.
> In a very real sense,
> the computer system,
> that we are just barely beginning to develop,
> will contain what amounts to a library.
> A library that is available to every user of the system [@Mornski:fano].


The breakthrough of the library was in fact dual.
Firstly,
the libraries of software understood as a collection of executable programs,
and secondly,
the early sixties computer concept that is the software library---a particular software component the functionalities of which can be accessible by executable programs---therefore allowing the development of applications reusing and sharing the same pool of system and user libraries.


If you look again at Ada or Friedrich's source code in section 1.2,
the function \texttt{printf} used to display text on their terminals is not a magical word.
It is part of the core input and output functions of the C standard library,
and the declaration of the former is provided by the \texttt{stdio.h} standard header file,
hence the need for Ada and Friedrich to \texttt{\#include} the latter at the beginning of their source code,
so that the compiler can find it. 
In essence,
the thought of a software library is close to the concept in which users are sharing and recombining several existing software for their own needs
^[
To give an analogy,
with some strings attached given the issue of material goods scarcity,
using several libraries and bits of existing source code to make a new program,
could be seen as similar to using existing techniques,
already available kitchen appliances,
as well as raw and processed ingredients,
in order to make a new apple pie instead of attempting to make one truly from scratch.
But this notion of doing something *from scratch* is of course deceptively relative to the environment in which things are made.
Similar to astrophysicist Carl Sagan's famous tongue-in-cheek comment about the latter---"if you wish to make an apple pie from scratch, you must first invent the universe" (in @Sagan:cosmos, p. 218)---the general purpose computer,
its operating system,
and existing utilities and libraries,
offer a fantastic head start in the making of new programs and the sharing of files.
For instance,
in terms of openness and from the viewpoint of interoperability,
one of the software that I used to write this thesis,
namely \texttt{pandoc},
is able to export my text to several other markup languages and file formats that can be read by other applications closed source or not,
thanks to a family of libraries that are able to implement open standards without the need to share code.
Similarly,
but at another level,
given the plethora of software libraries available today to provide out of the box all sorts of functionalities,
from drawing the Graphical User Interface (GUI) to spell checking,
network file access and file system support across several types of computer architectures,
I could also have programmed my own exporting software *from scratch* and only focused on specific features and interactions,
not already present in the hundreds of software libraries at my disposal on a free software operating system,
or I could have decided to procrastinate the writing of this dissertation even further and instead write the software *from scratch* yet using an already available programming language and its compiler or interpreter.
Etc.
].
The difference here is that modularity is echoed at a much lower level.


These two forms of library are nowadays taken for granted in multi-tasking and multi-users environments,
regardless of the operating system,
closed source or open source.
But in the nineteen seventies,
this transformation contributed to the slow extinction of the large mainframe dinosaurs,
in favour of more versatile,
smaller and generic computers,
for which interoperability and the portability of software were essential survival traits.
In this race,
a clear winner quickly emerged to dominate the late seventies universities,
corporations and government agencies:
Unix [@Moschovitis:internet].



UNIX Connects the Dots, People and Pipelines
--------------------------------------------


Unix was born from the 1964 Multiplexed Information and Computing Service (MULTICS) project,
one of the earliest time-sharing mainframe operating systems.
Put simply,
the aim of the project was to turn computation into a productive and efficient technology.
As American software developer and open source software advocate Eric S. Raymond described some years later,
the purpose of MULTICS was to hide the complexity of the operating system from users and programmers,
"so that more real work could get done" [@Raymond:hackerdom, p. 29].
The project was the outcome of a cooperation between the Massachusetts Institute of Technology (MIT),
General Electric,
Project MAC,
and Bell Telephone Laboratories (Bell Labs).
But in 1969,
Bell Labs stopped its involvement,
dissatisfied with the progress made so far,
leaving its employees involved at loose ends.
Still,
these employees,
computer engineers and programmers Doug McIlroy,
Dennis Ritchie and Ken Thompson,
were eager to explore further some of the MULTICS ideas,
and proceeded to do so within the constraint of a smaller budget,
and as a consequence were compelled to use smaller and simpler computers [@Salus:daemon, UNIX].


So despite the fact that the original name of Unix
^[
Nobody seems to remember who spelled it like this and why it was changed.
See [@Dolya:Kernighan; @Salus:UNIX, p. 9].
]
was the Uniplexed Operating and Computing System (UNICS),
which can easily be read as a mocking reference to the much larger MULTICS operating system [@Raymond:artunix, p. 31],
the 1970 UNICS operating system was in fact not as reactionary as it may seem.
Its ethos as a simplified design mostly came from the more limited technical environment available at Bell Labs,
combined with the desire to build "neat small things,
instead of grandiose ones" [Quote from author of the C programming language, and important UNIX contributor, American computer scientist Dennis Ritchie, in @Salus:daemon, p. 15].
But this trait soon became a major advantage.
The operating system gained popularity for technically implementing the idea of an ever expanding toolbox,
a cornucopia of prototypes,
workflows,
usage,
yet to be discovered in the many extensions and permutations of its original design.
The modular nature of the operating system,
as envisioned with Project MAC and its notions of library and the computer utility,
turned out to be the flagship of Unix which soon became the best Lego bricks set available to lower the accessibility threshold of computer programming
^[
The analogy between Lego bricks and Unix-like systems,
and eventually free and open source software,
is quite popular.
It seems to stem from the early nineteen nineties which saw increasing references of the toy in scientific literature,
to describe modular and reusable technical components.
In the case of Unix, it was used notably in [@Scoville:style].
].
If the idea of software libraries and libraries of software,
that grew from the sudden opportunities provided by time-sharing and the necessary creation of user groups,
is not a Unix invention,
what made it stand out nonetheless was the simple implementation and inter-operability of such concepts.


> [Unix] attack the accidental difficulties that result from using individual programs together,
> by providing integrated libraries,
> unified file formats,
> and pipes and filters.
> As a result,
> conceptual structures that in principle could always call,
> feed, and use one another can indeed easily do so in practice.
> This breakthrough in turn stimulated the development of whole toolbenches,
> since each new tool could be applied to any programs that used the standard formats [@Brooks:silver, p. 15].


What is striking here is how the idea of standing on the shoulders of giants,
is equally present in the way source code can be assembled and modified from different parts,
and the way the resulting binaries can be combined with each other.
This approach eventually sparks what would latter be coined as the Unix philosophy:


> Although that philosophy can't be written down in a single sentence,
> at its heart is the idea that the power of a system comes more from the relationships among programs than from the programs themselves.
> Many UNIX programs do quite trivial things in isolation, but, combined with other programs, become general and useful tools.[@KernighanPike:UNIX, viii]


Central in this philosophy,
the Unix pipe,
represented with the vertical bar glyph \texttt{|},
enables interprocess communication,
so that several programs can be connected to each others to create not just new functionality,
but offer to the user the possibility of coding their own environment [@Howse:UNIX] in a text driven environment of writerly computation [See @Cramer:exe].
For instance:


```
lynx -dump -nolist http://ur1.ca/lzt7 | \
 dadadodo -c 20 - | \
 espeak -s 120 -v mb-en1 --stdin | \
 mbrola -e /usr/share/mbrola/voices/en1 - - | \
 ices2 netradio.xml
```

This particular pipeline is made of five different programs,
that were not created to work together but can nevertheless be combined because they all comply with the Unix pipeline mechanism.
What does it do?
It downloads 
^[
For readability purpose,
the example above uses a shortened URL.
]
a 2010 BBC web article [@Gompertz:40] on the lack of great works of art using the Internet as medium,
and renders the HTML page and its 54 slightly upset comments into plain text (\texttt{lynx}),
then it passes it to a Markov chain based processing software to generate some relatively grammatically correct text cut-ups (\texttt{dadadodo}).
The resulting prose is turned into speech (\texttt{espeak} and \texttt{mbrola}),
and the synthesized voice stream is sent to pre-existing audio broadcasting server (\texttt{ices2}),
creating an instant British Broadcasting Cacophony net radio of sorts,
accessible by anyone on the Internet.

Next to the way programs can be arranged with each other,
the isomorphic relationship between the combination of source code and the combination of the resulting binaries is further echoed in the way programmers and users work with each other with the aid of the very same tools that emerged from early forms of collaborative and cooperative organisation:
from the creation of a library for users to the emergence of networked libraries of librarians.
From the perspective of technological determinism,
Unix itself will be instrumental in shaping these self-similar forms of technological and social organisation.
An illustration of this is the introduction of Unix-to-Unix Copy (UUCP) programs and protocols in the 1979 seventh edition of Unix.
Immediately upon release of this new set of tools,
two graduate students from Duke University,
Tom Truscott and Jim Ellis directly exploited the Lego brick versatility of Unix,
by implementing a news system at the cross road of emails and bulletin boards,
using a combination of pre-existing simple programs
^[
Said differently, it was a shell script.
].
The software was made accessible across a network of Unix computers,
transforming radically communication across Unix users and connecting them to foster the development of software.
By the summer of 1980,
eight nodes were connected: Duke University,
University of North Carolina at Chapel Hill,
Reed College,
University of Oklahoma,
two machines at Bell Labs Murray Hill,
and the University of California at Berkeley.
The network,
named Usenet,
quickly expanded to become an unrestricted and not-for-profit public platform,
reaching six hundred nodes by 1983 [For a more detailed historical account see @Hauben:netizens],
and joining the assemblage of different and not always compatible computer networks that co-existed during the pre-Internet era of computer networked communication [@QuatermanHoskins:networks].
Usenet became the embodiment of a type of informal cooperative software development,
bridging the corporate and university Unix programming communities,
where property rights and restrictions on the reuse of software were mostly seen as irrelevant [@LernerTirole:eco].


\afterpage{%
\begin{figure}[h]
	\caption{UNIX license plate}
    \includegraphics[width=\textwidth]{unix_license_plate} 
	\fnote{Photo: Armando P. Stettner, CC BY-SA 3.0, 2013}
    \label{fig:unixplate}
\end{figure}
\clearpage
}


So far,
while no ideas like free or open source software were expressed,
their practice were nonetheless already embodied in the early days of computation,
and were in fact as I have shown,
instrumental in its further expansion.
According to O'Reilly,
early Usenet was the Napster of shared code [@OReilly:shift].
As a matter of fact,
the birth of the Internet,
more particularly its commercial industry,
was triggered by the need to sustain the infrastructure of the fully collaborative UUCP and Usenet infrastructure [@OReilly:shift].
Today,
the technological legacy of Unix is overwhelming.
It has paved the way for a large family of operating systems found in servers,
network devices,
video game consoles,
desktop and laptop computers,
and of course smart phones,
tablets and all sorts of digital gadgets.
Yet,
what is nonetheless most peculiar about Unix was this idea of programming freedom,
independence and users community.
This aspect was already explicit in the early days of the introduction of the system,
where a conscious community oriented direction was set in motion by its original authors:
"a system around which a fellowship could form" [@Ritchie:evolution, p. 25].
This ethos of fellowship,
is best exemplified with the early eighties vanity license plate (Figure \ref{fig:unixplate}) that was used throughout the history of Unix-like products,
and that combined the United States of America New Hampshire motto with the name of the operating system:
"LIVE FREE OR DIE - UNIX". [@Open:UNIX]





The Growing Unix Fellowship
---------------------------




As with Ada and Friedrich's software,
Unix and most of its programs were eventually written in C
^[
For a more detailed report on the gradual transition from assembly to C, see [@McIlroy:UNIXreader].
],
a general programming language developed by American computer scientist Dennis Ritchie,
and which specificity is in its ability to not be tied to any operating system or machine [@KernighanRitchie:c, Introduction],
thus allowing Unix and other C programs to be easily ported to all sorts of different hardware.
Unlike previous generation mainframe operating systems,
with C,
software becomes more easily uncoupled from the hardware.
If the later was already made possible given the situation created by the 1969 unbundling case with IBM [@Grad:IBM],
in which the computer manufacturer was forced to sell the software part of his product separately to avoid a monopoly position,
this precedent only becomes truly relevant to software development now that software portability was a technical given [@Reinfelds:port].


The adaptability of Unix and the availability of its source code,
turned out to be ideal as a learning environment for graduate and undergraduate students who could start to tinker with and enhance the code [@Weber:success].
The reading of the source code written by other users and system operators,
then became part of the learning process.
Source code was and still is the primary documentation of software production,
and is best exemplified with the Star Wars film inspired abbreviation UTSL "use the source, Luke" [See UTSL entry in @Raymond:jargon296],
which was often expressed in developer user groups whenever someone was trying to figure out something with an unfamiliar software or situation.
But more than a manual,
and connecting back to the process of individuation,
source code is influential in the development of programmer practice.
As computer scientist Donald Knuth simply puts it,
reading source code is the mandatory step towards technological appropriation,
creativity and innovation:
"[t]he more you learn to read other people's stuff,
the more able you are to invent your own in the future." [Quote from interview in @Seibel:coders, p. 601]


But Unix technical prowess aside,
the cooperative productivity and educational value of accessing source files could be said for any software.
So why did Unix end up as an historical landmark of such principles?
As it turned out,
Unix was more than just engineering freedom.
Due to AT\&T's legal monopoly status in running the USA long-distance phone service,
Unix could not be sold as a product,
neither was the corporation allowed to provide support for the software;
however upon a simple request and for a nominal fee,
the source code could be acquired by anyone [@Toomey:unixbirth].


> Under a 1958 consent decree in settlement of an antitrust case,
> AT&T (the parent organization of Bell Labs) had been forbidden from entering the computer business.
> Unix could not,
> therefore,
> be turned into a product;
> indeed, under the terms of the consent decree,
> Bell Labs was required to license its non telephone technology to anyone who asked.
> Ken Thompson quietly began answering requests by shipping out tapes and disk packs — each,
> according to legend,
> with a note signed "love, ken" [@Raymond:artunix].


At a time when the computer industry was still very immature,
the slow democratisation of the computer utility approach,
envisionned by Fano in the sixties,
materialised itself in an experimental way.
In the process,
it is notable that some of these experiments operated along similar dynamics as those of the nineteen sixties counterculture movement,
where the climate of dissent and anti-establishmentism favoured individual expression and independence in the context of communication technology that had the potential to transform cultural production [@Scaruffi:sv, 5. The Hippies (1961-68)].
While not being something that can be generalised and applied to the whole computational culture of the nineteen sixties,
important projects in the field of networking and futurist mind expanding technology overlapped with the counterculture movement.
Other late nineteen fifties and sixties projects like the Augmentation Research Center (ARC),
initiated by American electrical engineer Douglas Engelbart,
have been described as being inhabited with part engineering culture and part counterculture [See @Markoff:dormouse, p.163].
In particular,
in the team of people helping Engelbart produce the canonical 1968 *The Mother of All Demos*---a famous live demonstration and vision of what would characterise todays personal computers and networks---was American writer Stewart Brand,
who was already working on his *Whole Earth Catalog* where concepts from cybernetics,
ecology,
do-it-yourself (DIY),
self-sufficiency,
alternative education and tools,
including computers,
were all mixed in the form of a countercultural product review catalog.
While not specific to Unix,
I find it important to note such a cultural landscape as it is very much in this context of social experimentation,
alternative tools and DIY practices that the development of Unix was facilitated. 
It was in this context that the early dispersing of Unix copies started as an eccentric academic effort,
largely ignored by big computer manufacturers [@Scaruffi:sv, 5. The Hippies (1961-68)],
and the first Unix supporters were "delighted in playing with an operating system that not only offered them fascinating challenges at the leading edge of computer science,
but also subverted all the technical assumptions and business practices that went with Big Computing" [@Raymond:artunix, p. 58].
The 1975 user association Unix Users Group was instrumental in building such a computational counterculture.
There,
members of the group could exchange new software and fixes by sending and receiving back magnetic tapes [@Toomey:unixbirth],
and in that sense also actively encouraged a form of self-sufficiency. 


All these elements greatly contributed to help Unix spread like wildfire,
and at the same time helped strengthen a system ending up modified and maintained by a several user groups.
One of the most notable byproducts was the 1977 Berkeley Software Distribution (BSD),
a Unix derivative put together at the time by Berkeley graduate student Bill Joy [@Mckusick:bsd],
and from which some ideas found their way back into the main official Unix releases.
Following the legacy of sharing operating system and program source code in the early general purpose computer groups like SHARE,
the group dynamics and exchanges occurring within the Unix scene [@Leonard:power] can be best described as being proto free and open source software production.


So,
to return to my doubt as to whether or not the cooperative and collaborative models of free and open source software should be perceived as novel,
it should now become clear that they are not.
In fact the notion of *social software* [@Fuller2003, pp. 24-28],
is not specific to free and open source software,
but deeply rooted in the birth of software making.
All that said,
if the technological aspect of these collaborative and cooperative exchanges were already highly mature,
their legal framework at the opposite end was completely underdeveloped.
What could possibly go wrong in such a situation?
As it turned out it is this particular situation that will come to endanger the proto free and open source software production model,
and as I will explain in the next two sections,
lead finally to the necessity felt by some programmers to define and protect these practices,
to protect such social software.



Controlling Software Development
--------------------------------


Earlier on,
I mentioned that in order to distribute her software,
user Ada had two possibilities: either providing the source files or only ship the compiled program.
Now let's assume that another user of the system,
let's call him Richard,
is interested in using Friedrich's variation of Ada's software.
However this time Friedrich decides to only give Richard the object code.
With this,
Richard is able to run the software but modifying it becomes rather problematic.
If he decides to open it with a text editor he will be surprised to see something like the following:


```
%^L ^@h^@^@^@^@éàÿÿÿÿ%^B^L ^@h^A^@^@^@éÐÿÿÿ1íI<89>Ñ^H<89>âH<83>äðPTI
ÇÀÀ^E@^@HÇÁ0^E@^@HÇÇ^@^E@^@èÇÿÿÿô<90><90>H<83>ì^HH<8b>^E<99>^K ^@H<8
5>Àt^BÿÐH<83>Ä^HÃ<90><90><90><90><90><90><90><90><90><90><90><90><90
>UH<89>åSH<83>ì^H<80>=°^K ^@^@uK»@^N`^@H<8b>^Eª^K ^@H<81>ë8^N`^@HÁû^
CH<83>ë^AH9Øs$f^O^_D^@^@H<83>À^AH<89>^E<85>^K ^@ÿ^TÅ8^N`^@H<8b>^Ew^K
 ^@H9ØrâÆ^Ec^K ^@^AH<83>Ä^H[]Ãfff.^O^_<84>^@^@^@^@^@H<83>=p   ^@^@UH
<89>åt^R¸^@^@^@^@H<85>Àt^H]¿H^N`^@ÿà]Ã<90><90><90><90><90><90><90><9
0><90><90><90><90><90><90>UH<89>åH<83>ì^PH<8d><%^\^F@^@ÇEü^@^@^@^@°^
@èÒþÿÿ<8b>Mü<89>Eø<89>ÈH<83>Ä^P]Ã<90><90><90><90>H<89>l$ØL<89>d$àH<8
d>-ã^H ^@L<8d>%Ü^H ^@L<89>l$èL<89>t$ðL<89>|$øH<89>\$ÐH<83>ì8L)åA<89>
ýI<89>öHÁý^CI<89>×èSþÿÿH<85>ít^\1Û^O^_@^@L<89>úL<89>öD<89>ïAÿ^TÜH<83
>Ã^AH9ëuêH<8b>\$^HH<8b>l$^PL<8b>d$^XL<8b>l$ L<8b>t$(L<8b>|$0H<83>Ä8Ã
^O^_<80>^@^@^@^@óÃ<90><90><90><90><90><90><90><90><90><90><90><90><9
0><90>UH<89>åSH<83>ì^HH<8b>^EH^H ^@H<83>øÿt^Y»(^N`^@^O^_D^@^@H<83>ë^
HÿÐH<8b>^CH<83>øÿuñH<83>Ä^H[]Ã<90><90>H<83>ì^HèOþÿÿH<83>Ä^HÃ^@^@^A^@
^B^@There is no software.
%^@^@^A^[^C;(^@^@^@^D^@^@^@¬ýÿÿD^@^@^@Ìþÿÿl^@^@^@üþÿÿ<8c>^@^@^@<8c>ÿ
ÿÿ´^@^@^@^T^@^@^@^@^@^@^@^AzR^@^Ax^P^A^[^L^G^H<90>^A^@^@$^@^@^@^\^@^
@^@`ýÿÿ0^@^@^@^@^N^PF^N^XJ^O^Kw^H<80>^@?^Z;*3$"^@^@^@^@^\^@^@^@D^@^@
^@Xþÿÿ,^@^@^@^@A^N^P<86>^BC^M^F^@^@^@^@^@^@^@$^@^@^@d^@^@^@hþÿÿ<89>^
@^@^@^@Q<8c>^E<86>^F_^N@<83>^G<8f>^B<8e>^C<8d>^D^BX^N^H^@^@^@^T^@^@^
@<8c>^@^@^@Ðþÿÿ^B^@^@^@^@^@
```


This situation could be roughly described as similar to the transformation of annotated human music notation into a piano roll.
With the sole access to the latter,
it would be rather difficult to recreate the original music sheet,
adding to that the loss of information that was not relevant for the creation of the roll and therefore impossible to access.
So yes,
one can see *there is no software* in this stream of randomly looking characters,
but this is in fact the binary machine code of Friedrich's *software* that Richard's text editor desperately tries to translate into plain text,
as it is the only thing it can interpret.
Maybe another interpreter could help?
To reverse the translation that was done by the compiler,
Richard can use some tools.


For instance a disassembler,
will effectively translate the object code into architecture dependent assembly language.
Unlike C,
assembly language is a human-readable representation of low-level machine instructions,
and while this is one step friendlier than reading binary,
or hexadecimal translated machine code,
and of course much more readable than opening a compiled program with a text editor,
it is still quite far from the comfort provided by C,
which is why the later is called a high-level programming language as it provides an abstraction from computation,
by the means of natural language.
Below an extract from the more than five hundred lines of output produced from the command \texttt{objdump -sd software},
typed by Richard on his terminal:


	[...]
	Contents of section .rodata:
	 400618 01000200 54686572 65206973 206e6f20  ....There is no 
	 400628 736f6674 77617265 2e0a00             software...     
	[...]
	0000000000400500 <main>:
	  400500:       55                      push   %rbp
	  400501:       48 89 e5                mov    %rsp,%rbp
	  400504:       48 83 ec 10             sub    $0x10,%rsp
	  400508:       48 8d 3c 25 1c 06 40    lea    0x40061c,%rdi
	  40050f:       00 
	  400510:       c7 45 fc 00 00 00 00    movl   $0x0,-0x4(%rbp)
	  400517:       b0 00                   mov    $0x0,%al
	  400519:       e8 d2 fe ff ff          callq  4003f0 <printf@plt>
	  40051e:       8b 4d fc                mov    -0x4(%rbp),%ecx
	  400521:       89 45 f8                mov    %eax,-0x8(%rbp)
	  400524:       89 c8                   mov    %ecx,%eax
	  400526:       48 83 c4 10             add    $0x10,%rsp
	  40052a:       5d                      pop    %rbp
	  40052b:       c3                      retq   
	  40052c:       90                      nop
	  40052d:       90                      nop
	  40052e:       90                      nop
	  40052f:       90                      nop


This sample is the most relevant as it displays the main entry point of the program and where to find the string of text that is displayed when the object code is executed.
For such a trivial example,
Richard could easily make a modified version of the software,
by adjusting the content of the executable with a simple hexadecimal editor.
However,
making the program do something else,
like writing the string of text to a file instead of displaying it on the terminal's shell,
would require more extensive modifications of the binary.
It would require to change the program functionality and not just editing a string of characters.
In fact,
such modifications become exponentially difficult,
as the new desired features drift too far from the original software's purpose and features
^[
This issue,
the process of which belongs to the practice of reverse engineering,
is particularly visible in projects that attempt to improve an existing software for which original source code and documentation is not available,
or is purposefully protected or obfuscated.
For instance Vitaly Kiselev's research on the software powering a range of Panasonic digital cameras,
has enabled the improvement in the quality from their recorded images and motion,
but does not add entirely new and distinct features to the camera.
These changes are much more difficult to make without the tools,
files,
and technical documentation to produce the software in the same way as the manufacturer did.
See [@PV:gh2].
].
The question to be asked then,
given the cooperative and technical advantage of distributing the source code of a software,
why would one refuse to do so,
literally deciding to distribute closed source software and therefore limiting the interpretation value of software?


As stated previously,
Unix's popularity and easily obtainable licensed source code allowed for the modification and creation of other Unices.
However the resulting Cambrian explosion of all sorts of Unices,
research Unix,
commercial Unix implementations,
academic Unices and so forth,
eventually led some to wonder who actually controlled and owned this particular source code.
Already in the mid-seventies,
the freeness of this operating system became more and more questionable.
Starting first with the threat from AT\&T to the Unix User Group,
asking the group to stop using the Western Electric owned UNIX trademark,
the community renamed itself USENIX [@Lehey:column].
The same year,
Australian computer scientist John Lions published a commentary on the Version 6 of the UNIX operating system [@Lions:comment],
for use as teaching material.
It quickly became a must-read book for anyone interested in Unix and operating systems in general,
and was eventually distributed by Bell Labs itself.
Even so,
due to Western Electric's desire to limit the distribution of the source because of trade secret status in the kernel[@Raymond:jargon, Lions Book]---combined with the change in the license of 1979 Version 7 of UNIX that started to forbid classroom use---the book became forbidden literature continuing to spread via samizdat [@Salus:daemon, The Users].
The legal layer in which the code was wrapped also made it hard to merge the efforts of the different contributing groups,
with the work from those at Bell Labs.
At this point,
update and fixes were propagating in a very secretive way:


> [...] Lou Katz, the founding president of USENIX,
> received a phone call one day telling him that if he went down to a certain spot on Mountain Avenue (where Bell Labs was located) at 2 p.m.,
> he would find something of interest.
> Sure enough,
> Katz found a magnetic tape with the bug fixes,
> which were rapidly in the hands of countless users [@Toomey:unixbirth].


This particular Unix era is not very well documented,
depending by which Unix historian these anecdotes are told,
the narrative and the myth change slightly.
But understandable romanticism aside,
it does confirm that some unofficial back channels had been purposefully put in place at the time or in an *ad hoc* way,
inside and outside of Bell Labs,
to bypass entirely the legal limitations of Unix and the official corporate channels of distribution.


This tension reached a point of non-return in 1983 with the breakup of the Bell System freed AT\&T from the 1956 anti-trust consent decree,
that had so far forbidden it from using Unix as a commercial product.
Despite coming from the same Bell Labs Research Unix root,
the following licensing of the newly AT\&T UNIX System V released the same year of the divestiture,
accelerated the development of the commercial Unices,
and dramatically increased the cultural gap with university efforts such as BSD
^[
The rest of the so-called Unix wars that follow,
and that will cripple many efforts to develop the operating system throughout the eighties and nineties,
is outside of the scope of this text.
].
In fact,
By the mid-eighties,
dramatic changes had occurred in the fast expanding world of computer business.
The control over the copying,
modification,
and distribution of software became essential to ensure the large scale monetisation and capitalisation of executable code.
This is why commercial closed source proprietary software,
as a valid business model,
grew in parallel with the standardisation and democratisation of computing infrastructures---due to the strong business decoupling between software publishing and hardware manufacturing
^[
A transition point in that historical business decoupling is visible for instance in the way Commodore International,
a famous eighties home computer manufacturer,
licensed from another company,
Microsoft,
its ROM-resident BASIC for several of its machines, including the highly popular Commodore 64. [@Dillon:C64, p. 17].
Of course,
Microsoft was also selling licenses for its BASIC to other home computer manufacturers and end-users.
]---and accelerated by a hobbyist computer scene that was operating outside of the academic walled garden of computer science [see @Driscoll:nothing].


All that said,
it is worth insisting that this was not specific to Unix systems.
The same reversal was visible across the whole computer industry.
And for instance,
the once IBM supported SHARE group who used to work on the source of the tools and operating systems of their machine,
were eventually denied access to the code in 1983 after the introduction of a new Object Code Only (OCO) policy.
According to a SHARE memo celebrating with sarcasm the tenth anniversary of OCO,
at the time of the announcement,
the man who eventually became the Senior Vice President of IBM had "came to SHARE and said that [IBM] no longer needed customer creativity" [@Varian:oco].
Today’s fragile relationship and labour transaction occurring between user generated content and online platforms or service providers [@Scholz:labor] is nothing new,
it is just history repeating itself.



Fratricide Software
-------------------


Stallman,
who joined the  MIT Artificial Intelligence Laboratory in the early seventies,
was a strong believer in what American journalist Steven Levy called the "hacker ethic" [@Levy:hackers, The Hacker Ethic],
which originated from the sixties computer programming scene,
and in which several principles were shared, namely that:
information and technology should be freely accessible and unrestricted;
hackers should be judged solely on their hacking skills,
which is to say the technological elegance and crafting dimension of their work;
and overall,
computers can be used to improve society [@Levy:hackers].
Within this subculture,
and in regard to the brief historical overview of the early days of general purpose computing covered previously,
sharing software was "as old as computers,
just as the sharing of recipes is as old as cooking" [@Stallman:fsfreesociety, p.17].


Stallman was not from the same generation as the initial Unix authors,
and not a Unix user himself,
but he was however fully aware of the habitus of software production at the time,
being himself introduced to it at a relatively young age with the practice of programming interleaved with his studies at Harvard and MIT,
and eventually becoming his main occupation.
And as much as Unix suffered from the intellectual property issues mentioned previously,
it was not the only project to become legally constrained by the novel business model that was closed source software.
This was a global change felt across all the academic research fields for which computers had became tools of the trade.
Software was no longer solely produced within technological limitations,
but also within the legal constraints operating on top of,
and overriding the former.
But if what happens at the MIT AI Lab is also connected to this change in the computer industry landscape,
it is mostly the story of its community of programmers who were torn apart in an epic tragedy that was the catalyst to the first expression of user freedom.


In brief,
two companies,
Symbolics and Lisp Machines Inc.,
emerged from the MIT AI Lab scene and were both centred around the production of proprietary Lisp machines,
which were general purpose computers running the LISP computer programming language.
The two efforts came out from the lab members themselves.
One company,
Symbolics was an archetypical computer business built around outside investment that hired many lab members full-time to work on their product,
while the other,
according to Stallman,
was meant to be a "hacker" company keen to sustain and support the lab hacker culture by hiring the programmers part-time [@Stallman:lisp] and relying solely on economies of scale for the development of its capital and resources.
Eventually both companies hired hackers from the lab,
and the ex-MIT programmers from both systems kept on contributing to improve the AI Lab's own Lisp machines.
However at some point,
in order to get rid of the competition---still according to Stallman---Symbolics demanded that the lab only use their machines in order to keep using Symbolics software,
and Stallman, 
who was at the time pretty much the only one left of this generation of hackers,
and not hired by any of the companies,
recalls:


> This,
> in effect,
> meant that they demanded that we had to choose a side,
> and use either the MIT version of the system or the Symbolics version.
> Whichever choice we made determined which system our improvements went to.
> If we worked on and improved the Symbolics version,
> we would be supporting Symbolics alone.
> If we used and improved the MIT version of the system,
> we would be doing work available to both companies,
> but Symbolics saw that we would be supporting LMI because we would be helping them continue to exist.
> So we were not allowed to be neutral any more [@Stallman:lisp]


The rest is history,
at the beginning of 1982 and until the end of 1983,
Stallman dedicated almost two years of his life replicating the improvements made by Symbolics,
and sharing them with Lisp Machine Inc.
American anthropologist Gabriella Coleman would later qualify Stallman's work during this period,
as that of a "revenge programmer" [@Coleman:freedom, p.68].
But this is not a simple revenge.
Stallman felt that he was forced to take on this role of "punisher" [@Stallman:lisp],
and admits that he did not care so much about the future of Lisp Machines Inc. either [@Stallman:lisp].
So, 
more than just a revenge,
Stallman was essentially trying to maintain the illusion of a community,
not letting go of what used to be the MIT AI lab culture and thereby becoming a human bridge between the trio Symbolics,
Lisp Machines Inc.,
and the MIT AI Lab.
The software he wrote was more an emergency band aid than a revenge.


In fact,
I think that what this episode reveals essentially is the ethical foundation of what will become free software,
a foundation born from this fratricide and proprietary war,
and in which the strategy of not sharing software was *not* the main issue.
It was just one of the weapons used in the attempt to control the MIT AI Lab territory,
a war during which Stallman was only concerned with the collateral damage:
the disappearance of his fellow programmers employed at these computer manufacturers and his increasing loneliness. [@Levy:hackers, p. 448]
There was no *other* evil entity,
but the self-inflicted shredding of a group of people unable to work together and unable to reconcile commercial interests within their academic walled garden.


These aspects however became quickly conflated,
and to be sure,
I mean that it is probably with Stallman that the moral attribute of software production becomes for the first time a cornerstone,
where there was an attempt to make a distinction between two kinds of user groups generalised from the AI Lab event
^[
I say it is an attempt to make a distinction because I also think this is a convenient simplification,
that would obviously permit Stallman to justify his tragic exodus and new mission in the years that followed.
Similarly such a radical distinction between two kinds of groups only works within the rethoric of the free and open source software subcultural hero.
As I will also begin to explain in the following chapters, 
the categorisation of communities is not as simple in reality as it first seems.
]:
the first kind are socially driven groups that emerge in a rather decentralised,
and self-governing way, 
which rely on historical social traditions and legacies of computational culture,
as USENIX was initially;
and the second kind are groups in which users are mere customers consolidating an existing product,
like the early days of the SHARE group
^[
I mention early days,
because throughout its history the SHARE group became more and more independent,
starting most notably with the work on its own operating system,
the SHARE OS (SOS),
See [@Akera:share]. But in the case of SHARE as the sense of community will surpass the role of customer,
SHARE became somehow less and less relevant to IBM's own business agenda,
as mentioned previously, 
with the introduction of the OCO policy.
].



The Need to Define
------------------


Having failed to save the AI Lab of his generation,
Stallman left the conflict of two Lisp machine manufacturers and MIT,
and in 1983 announced, 
on both USENET and ARPANET,
the creation of his Ark,
the GNU project,
a recursive acronym which stands for GNU's not Unix,
a free Unix-like operating system with Lisp implementations as user programs:


> Free Unix!
>
> Starting this Thanksgiving I am going to write a complete
> Unix-compatible software system called GNU (for Gnu's Not Unix),
> and give it away free to everyone who can use it.
> Contributions of time, money, programs and equipment are greatly needed.
>
> [...]
>
> I consider that the golden rule requires that if I like a program I
> must share it with other people who like it. I cannot in good
> conscience sign a non-disclosure agreement or a software license
> agreement.
>
> So that I can continue to use computers without violating my principles,
> I have decided to put together a sufficient body of free software so that
> I will be able to get along without any software that is not free [@Stallman:freeunix].


Finally abstracted from their originating stories,
the notion of user groups and fellowship that were so far overlapping with consumerism and computer clientèle,
with Stallman changed into ideas of self-sufficient user communities,
as well as of tales of a neighbourhood oppressed by a bad *other* seeking to impose its rules upon the group:


> This meant that the first step in using a computer was to promise not to help your neighbour.
> A cooperating community was forbidden.
> The rule made by the owners of proprietary software was,
> "If you share with your neighbour,
> you are a pirate.
> If you want any changes,
> beg us to make them." [@Stallman:fsfreesociety, p.18]


Later,
in March 1985,
Stallman accentuated his call in the now famous GNU manifesto.
In October of the same year,
he founded and registered the Free Software Foundation (FSF) with the following purpose:


> The corporation is formed for literary,
> educational,
> and charitable purposes with the special purposes of
> i) encouraging,
> fostering,
> and promoting the free exchange of computer software and information related to computers and other technology;
> ii) distributing and disseminating software and information related to computers and other technology;
> and iii) increasing the public's access to computers and other high technology devices [@Stallman:manifesto].


The first attempt to define free software naturally also came from Stallman who,
a year after the GNU Manifesto,
published a text about the newly created FSF, 
which contained a brief articulation of what exactly can be considered as free software:


> The word "free" in our name does not refer to price;
> it refers to freedom.
> First, the freedom to copy a program and redistribute it to your neighbours,
> so that they can use it as well as you.
> Second, the freedom to change a program,
> so that you can control it instead of it controlling you;
> for this, the source code must be made available to you [@Stallman:definition1986].


What had been bootstrapped by the FSF and the early concept of free software then gradually became an inspiration for others.
One of the most notable can be found in the creation of the popular Debian free software operating system [@Murdock1994].
At the same time,
in this new wave of free software projects,
it is possible to start to see the effects of cultural diffusion of the core FSF ideas.
Debian for instance,
does not aim to implement the GNU OS or an OS that directly translates the concepts from Stallman,
instead it further articulates the definition of free software,
specifically its social dimension,
that was sketched with Stallman's community etiquette,
subtly shifting from the idea of users to the concept of neighbours.
So with the Debian operating system,
the definition becomes embedded within a social contract.
This contract was drafted and eventually announced by American computer programmer Bruce Perens in 1997,
who was at the time project leader of Debian [@Perens:contract].
The Debian Free Software Guideline part of this social contract is based on 10 sections,
that build upon Stallman's definition published more than a decade earlier:


> 1. Free redistribution.
> 2. Inclusion of source code.
> 3. Allowing for modifications and derived works.
> 4. Integrity of the author's source code (as a compromise).
> 5. No discrimination against persons or groups.
> 6. No discrimination against fields of endeavour, like commercial use.
> 7. The license needs to apply to all to whom the program is redistributed.
> 8. License must not be specific to Debian, basically a reiteration of the previous point.
> 9. License must not contaminate other software.
> 10. The GPL, BSD, and Artistic licenses are examples of licenses considered free.
^[
This is an extract of all the headlines from [@Perens:contract].
]


This guideline is modified a few years later to serve as the Open Source definition [@Perens1999],
for the newly founded Open Source Initiative (OSI),
following the 1998 call to embrace the term *open source* instead of free software [@Raymond1998].
It can be summed up as the following:


> 1. Free Redistribution
> 2. Inclusion of Source Code
> 3. Allowing for modifications and Derived Works
> 4. Integrity of The Author's Source Code
> 5. No Discrimination Against Persons or Groups
> 6. No Discrimination Against Fields of Endeavour, like commercial use
> 7. The license needs to apply to all to whom the program is redistributed.
> 8. License Must Not Be Specific to a Product
> 9. License Must Not Restrict Other Software
> 10. License Must Be Technology-Neutral
^[
This is an extract of all the headlines from [@Perens1999].
]


As with software source code that gets shared and modified to fit one user's specific need or interpretation,
with the open source definition,
it is possible to see that similar exchanges are now happening at the level of the articulation of such practices.
Software freedom is no longer a concept that describes existing practices,
it is also an idea evolving independently within an ever expending territory where different agendas meet.
When comparing the two definitions,
the striking similarity whether in these shortened versions above or with the full texts,
one can certainly wonder about the redundancy of such an effort.
However,
it becomes apparent that each project brings a different facet forward.
Could it be that just like Debian's particular attention to the social context of free software,
some others have found in this idea another trait that needs more promotion?
In fact the change of some words is not innocent.
For instance the clause 9 of both definition has seen a change in the word *contaminate* into *restrict*.
The explanation for this clause remained however unchanged,
and the purpose of this rule is to avoid licenses that would impose constraints on the other software it is bundled with inside the same collection or medium.
Two things can be deducted from such a change:
one,
access to the source code is not enough to be free or open source software
^[
For instance the source code of the email client pine,
released under a legal notice that demands particular commercial and distribution conditions that breaks the clause on contamination/restriction,
is neither free software in the Debian sense,
or open source software.
See [@pine:legal].
];
two,
open source appears to be a tamed re-articulation of free software.


In fact this re-articulation finds its origin in a meeting held in 1998 following the release of the source code from Web browser Netscape Communicator 4.0 [@OSI:history] by Netscape Communications Corporation.
For many,
this event is a sign that open models of production can be relevant to business practices,
and should therefore be advocated,
supported,
and actively promoted, [see @denBoomenSchafer:revolution].
Even so,
for the future founders of the OSI,
words such as free and freedom were too ambiguous to be used effectively in a commercial context,
hence the need to make a discursive move from free software to open source
^[
The term open source was coined by American nanotechnologist Christine Peterson during the 1998 meeting,
as stated in [@OSI:history].
],
in which the questions of hacker ethics and free society are radically reformulated to fit the economic relevance of openness in the context of free market and laissez-faire,
and as a consequence make the *social software* aspect of the FSF secondary to an affiliation with the political philosophy of libertarianism,
occasionaly made explicit by OSI members [@Tiemann:libertarians].
However this did not halt the FSF efforts,
and throughout the years the free software definition evolved and,
again,
as with the writing of software code that is virtually never completed,
new features were added.
Today's version of the definition originates in its major update made in 2000,
which contains four elements best known as the four essential freedoms of programs users:


> The freedom to run the program, for any purpose (freedom 0).\
> The freedom to study how the program works, and adapt it to your needs (freedom 1). Access to the source code is a precondition for this.\
> The freedom to redistribute copies so you can help your neighbour (freedom 2).\
> The freedom to improve the program, and release your improvements to the public, so that the whole community benefits. (freedom 3). Access to the source code is a precondition for this [@Stallman2000].


According to Bruce Perens,
Stallman's update might have been motivated by the creation of the OSI, 
"as an alternative to the Open Source Definition",
and he suggests that they probably existed prior to their online release [@Perens2009].
As a matter of fact they even existed as three freedoms in 1998 [@Stallman1998],
which are now numbered 1,2 and 3.
Freedom 0 was added at a later stage in 1999 [@Stallman1999].
Still,
the difference between the two is more visible in the way they are interpreted by their respective supporters.
With the historical schism,
it is almost as if the merged pragmatic and the social dimension of the Unix user groups legacy was suddenly parted in an irreconcilable mode.


> Nearly all open source software is free software;
> the two terms describe almost the same category of software.
> But they stand for views based on fundamentally different values.
> Open source is a development methodology;
> free software is a social movement.
> For the free software movement,
> free software is an ethical imperative,
> because only free software respects the users' freedom.
> By contrast,
> the philosophy of open source considers issues in terms of how to make software “better”--in a practical sense only.
> It says that non-free software is a suboptimal solution.
> For the free software movement,
> however,
> non-free software is a social problem,
> and moving to free software is the solution. [@Stallman:opensource]


Here,
Stallman reduces open source to a development methodology,
but fails to see he is applying the same approach to free software itself,
by applying the prototyping and engineering culture of software production to the free software discourse,
constantly improving the free software definitions, licenses, and texts as if they were products that improve with every new version.
^[
As an example,
the 2007 article from which this text is quoted,
has been continuously modified throughout the years,
and still is at time of writing.
]
For Raymond open source is essentially free software *without ideology*,
and solely based on "economics and development processes and expected return." [@Leonard:esr]
Being a member of the American Libertarian Party,
the OSI founder is not quite a socialist or Marxist hacker,
a point he often makes explicit in his writing,
whenever he feels open source risks generalisation and reduction to political interpretations for which he has no particular sympathy [see @Raymond:bezroukov].
His critique of free software's *ideology* is nothing but a conservative tactic to frame Stallman as the leader of a fanatical and crazy project.
However,
if I consider ideology in the terms defined by British media theorist Dick Hebdige in *Subculture: The Meaning of Style*
^[
Drawing most notably from Marx, Althusser and Barthes,
Hebdige provides a broader approach to ideologies as the set of means and signs representing the interests of specific groups and classes.
See [@Hebdige:subculture, pp. 11-15].
],
the schism between free software and open source software is in fact nothing more than the manifestation of different ideologies,
whose subjects could no longer live under the same roof,
as it became clearer that their ruling ideas were not quite the same.
Of course,
this process is not necessarily obvious or conscious for all of those who lived through this split,
in the sense that the implicit ideological assumptions of the free and open source software definitions are not immediately visible,
if ever,
leaving some to switch sides several times.
This was for instance the case of Perens,
who,
even having drafted both the Debian and OSI definitions,
would in 1999 leave the open source camp that he helped establish,
precisely because of this increasing drifting away from what he believed were the original intentions of free software [@Perens:time].


This is why I disagree with Kelty,
who after having significantly summed up the difficulty of framing and using the term *movement* in reference to these two groups,
concludes that they "share practice first,
and ideologies second". [@Kelty:bits, p. 113]
But practice and ideology cannot be so easily decoupled.
Of course,
when free and open source software is analysed through the lens of subculture,
it is logical to understand that source code is the commodity through which the subversion takes place in the form of an heroic liberation from its proprietary and restricted context within the increasingly mainstream computer industry.
So in that sense,
it is easy to mix up free software and open source software practices as one shared front.
But this is not how it works,
and what I think is more crucial here is that,
liberated source code itself can also be opened to a double inflection
^[
Double inflection in the case of the historical split,
but also in many more cases, 
as I will develop further in this thesis.
].
I wish to stress that the same piece of source code can be used differently as free software or open source software,
even if the license is the same.
This is made very explicit with the Linux kernel,
licensed under the free software and open source software approved GPLv2, 
the main line of development of which is in the form of an open source project within a well established software industry
^[
At time of writing,
the latest report from the Linux Foundation states that since 2005,
1340 companies have contributed to the software,
and currently 92,3% of contributions are from paid developers.
See [@CorbetKroahHartman:linux].
],
and,
at the same time,
also exists as a distinct free software project,
GNU Linux-libre [@FSFLA:linuxlibre],
which offers an alternative version cleaned from all potential proprietary and closed source compromises made with said software industry,
and is the recommended kernel for FSF approved GNU/Linux distributions
^[
I will return to this example in Chapter 3.
It is also worth noting that even within the same camp several interpretations of a term or practice can co-exist,
for instance with copyleft being perceived as strategy rather than a moral principle by free software supporters.
See [@Kuhn:copyleft].
].
Therefore,
as I explained previously,
what matters ultimately is the human interpretation of these particular instructions:
free and open source software licensed source code is not only an element of resistance against an external hegemonic entity,
it is a place holder for multiple forms of resistance acting against each other's potential rise to dominance,
and in the free versus open source case,
it is the expression of a liberal conflict to decide what should be prioritised: ethics or economics.
This dichotomy is not shared but echoed in their respective practices.


Software Licenses
-----------------


Practically speaking,
these parallel efforts from the FSF,
Debian,
and the OSI,
operate as both a guideline for new,
and a filter for,
existing unilateral permissions specific to intellectual property:
the software licenses.
The emphasis on ethics and economics in free and open source software is more than a collection of beliefs,
it is also a disciplinary and normalising process that needs to be coded and enforced.
The software license works therefore as a permission given to the licensee to distribute the licensor's copyrighted work under specific terms
^[
For the licenses that rely on copyright.
].
To be sure,
a free and open source software license is not a contract.
During the twentieth century,
licensing has been approached through the perspective of an exchange of promises,
such as giving a copy of a copyrighted work in exchange for a fee and the respect of some obligations,
but free and open source software licenses are in fact closer to historical property law licensing:
they are unilateral permissions in which no obligations are reciprocally required by the licensor [@Jones:license].
They rely on copyright law,
not contract law,
so if the licensee does not respect the term of the license,
which are essentially terms of distribution,
they can be expressed by the plaintiff as the redistribution of copyrighted work without permission and not as a breach of contract [@Jones:license].


According to Austrian computer scientist and technology historian Peter H. Salus,
the first software license for Unix was written in 1977 when the operating system was ported to the Model 7/32 and Model 8/32 32-bit minicomputers developed by Interdata, Inc.
Prior to that,
Unix ran solely on the PDP-11/20 16-bit minicomputer sold by Digital Equipment Corporation (DEC).
Mainframe operating systems ran only on the machines with which they were sold and the idea of decoupling hardware from software was far from obvious [@Salus:daemon, p. 102].
Of course as mentioned earlier,
the 1969 IBM unbundling case made necessary the sale of software separately from the hardware.
However,
such necessity was transformed into a possibility for further commercial strategies only after programs became portable.
Software licensing also brought a moment of deceptive recognition that source code was not some magical public property,
but rather an actual privately owned object,
the rules of access and use of which were defined in increasingly constricting terms.
If,
on the one hand,
it is possible to reflect upon the evanescence of software [@Kittler:nosoftware] or its performativity [@Mackenzie:perf],
the software industry eventually responded to the question of software as an immaterial cultural expression in a very crude and direct way.
Under USA jurisdiction,
software can be considered as an original human creation,
which therefore can be copyrighted.
It's as simple as that, 
and to simplify things further,
this applies whether or not the software exists as object code or is available in a more human friendly medium and language.
As a result,
selling software becomes a problem as it could imply a copyright transfer,
which is why as a workaround the computer industry started to license software [@Heffan:copyleft, I. Software License Agreements].


Free from legal restriction to enter the computer market,
AT\&T created the Unix Systems Laboratory and in 1988 a Unix license costed 100,000 USD,
something affordable for corporations such as IBM and DEC,
but not for university researchers and smaller groups that were therefore motivated to switch to BSD Unix.
There was a catch however,
even though BSD Unix was distributed with its own highly permissive licensing,
the fact that it was built upon AT\&T property meant that BSD users were supposed to *also* acquire the costly license from the corporation in order to use the system in complete legitimacy [@Weber:success, p. 39].
As a response to this,
BSD developers came up with the idea of separating their own contributions and modifications from AT\&T's source files,
and releasing the whole lot in 1989,
as *Networking Release 1* under the generous terms of one of the early BSD-style license
^[
Such strategies are still popular today, 
they allow the redistribution of modified copyrighted software.
For instance groups and individuals from the ROM translation scene---communities focussed on making high quality fan translations of non locallised video games---only distribute their work as patches to apply on top of the original software,
so as to limit the visibility of an activity that is already questionable given the derivative nature of the work.
].
For the more affordable fee of 1,000 USD,
one was given a tape with source files,
and the rights to do pretty much anything with it,
including turning the code into closed source proprietary software and copying everything without any royalties due,
as long as credit to Berkeley was clearly given and original copyright notices in the source files remained intact [@Weber:success, p. 40].
From this point on,
the BSD project would progressively attempt to get rid of AT\&T's code,
rewriting what was missing and accepting contributions which would turn BSD into more complete operating systems,
as opposed to its early existence as a patch on top of AT\&T's UNIX.


This led to a new wave of Unix-like systems that were released throughout the early nineties,
with some of these standalone systems distributed as proprietary software
^[
For an in-depth history on these early days of BSD Unix and the legal battle with AT\&T that followed the distribution
of proprietary Unix-like systems, see [@Weber:success, The Early History of Open Source].
].
As mentioned in Section 1.5 of this chapter,
the original BSD development
^[
As well as all its surrounding stories relating to the development of some of its user programs,
such as *vi*. See [@Salus:daemon, A Tale of Two Editors]
],
demonstrates the existence of proto free and open source software practices that combine in one project the traditional hacking context,
the practice of collaborative code development and sharing,
and provide a software licensing model so as to escape vendor control and instead favour freedom for the users.
Given this situation and the apparent overlap in practices,
it might seem strange that Stallman does not join the effort of the proto-free software BSD.
As it turns out,
once again,
sharing practices is not everything.
Next to the entanglement of BSD Unix in several licensing systems [@Stallman:fsfreesociety, Free Software: Freedom and Cooperation],
a crucial problem for Stallman is that since the Symbolics episode he saw a correlation between the free circulation and complete access to eventual source code changes,
with the strength of a hacker community busy working on such source code.
As a result,
he must,
under these circumstances,
create a license which,
unlike the BSD license,
must enforce sharing,
to force the creation of social software at all costs.
This is why for Stallman a specific new license must be created and applied to a new Unix software,
which is detached from its historical codebase.
This is an history he does not seem to feel too attached to however,
given that his interest in Unix was not particularly acute to begin with [@Stallman:manifesto, Why GNU Will Be Compatible with Unix].


From Machine Instructions to Community Rules
--------------------------------------------


If nowadays free and open source software groups are probably,
as Coleman rightly points out, 
"the largest single association of amateur intellectual property and free speech legal scholars ever to have existed" [@Coleman:speech, p. 433],
it was not always the case.
In fact,
this too had to go through some heavy phases of prototyping,
as the pre FSF software freedom that Stallman often refers to,
can also be understood through the fact that hackers like himself did not think much of software as intellectual property,
let alone had they any clue of how it actually worked.
This was particularly visible in the very messy early years of the EMACS family of text editor programs,
the bits and pieces of which were assembled and distributed in different ways.
This situation led to several conflicts and discussions around the intellectual property nature of the software,
and notably the rather casual exchange and appropriation of source code regardless of its copyright,
or copyright laws for that matter [@Kelty:bits, Writing Copyright Licenses].
If in the later years Stallman seems to be quite casual when retelling this episode,
it was in fact a rather unpleasant process,
that once again forced him to take action so that his notion of community could stand on solid legal ground.
When he announced the GPL in 1988,
it was in fact a fusion and generalisation of different individual licenses that he associated with each of the early GNU software components,
a result of experiments with a series of prototypical licenses such as the GCC General Public License,
and the GNU Emacs General Public License.


> The copyleft used by the GNU project is made from a combination of a copyright notice and the GNU General Public License.
> The copyright notice is the usual kind.
> The General Public License is a copying license which basically says that you have the freedoms we want you to have and that you can't take these freedoms away from anyone else.
> (The actual document consists of several pages of rather complicated legalese that our lawyer said we needed.)
> A copy of the complete license is included in all GNU source code distributions and many manuals,
> and we will send you a printed copy on request. [@Stallman:copyleft1988]




It makes sure that software freedom and openness are ensured once the software is published,
and at the same time provides legal mechanisms to discourage a break in the terms of the license.
You may remember user Richard,
from section 1.6 of this chapter,
was bothered and annoyed by the fact that user Friedrich denied him access to the source file of his software---maybe because user Ada had licensed it to him under the permissive terms of the early BSD license---he now decides to rewrite said software from scratch,
and adds a new feature that matters to him and would have been cumbersome to implement via reverse engineering.
His new \texttt{software.c} source file looks like this:


```
/*  software - my free software
 *  (C) Copyright 2017 - Richard
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program. If not,
 *  see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

int main(void)
{
    puts("There is no software.");
    puts("There is only free software.");
}
```

Here Richard avoids potential copyright infrigement by implementing the software function differently from Friedrich's and Ana's original source code.
Alongside this,
he marks his source code with a disclaimer explaining the type of software it is,
and finally,
includes another text file,
the GPL,
which are simply the legal distribution terms that implements the FSF software freedom.
Any user deciding to publish this software in the future,
must comply with its terms [See @Kuhn:compliance],
namely that the distribution of the software must always be done with access to its source code,
including any modifications made to the latter:
the copyleft mechanism is born.
In practice,
with copyleft licenses,
if Friedrich then decides to use Richard's software and modifies it before sharing the new version on his website,
he must also provide the source code and must guarantee the same terms to the users of his version,
unlike with permissive licenses that allow closed source and proprietary integration and distribution.
It is very important to understand that the copyleft principle is absolutely not a mandatory characteristic of free software,
but a property of *some* free software licenses
^[
Similarly,
the FSF distinguishes GPL-compatible and GPL-incompatible licenses,
to distinguish source code that can or cannot be merged with GPL licensed source files,
and vice versa.
To give an example,
the initial 1998 4-clause BSD License is considered by the FSF to be a free software license but it is non-copyleft and incompatible with the GPL.
But more recent evolutions of the BSD license,
eventually became GPL compatible,
like the FreeBSD license,
despite still being a permissive non-copyleft license.
So while the FSF strongly encourages the use of copyleft licenses,
it also recognizes a whole range of licences.
Notable other free software licenses are the Apache License,
the CC0 license,
the FreeBSD license,
the BitTorrent Open Source License,
the Mozilla Public License,
the Microsoft Public License,
the Do What the Fuck You Want To Public License.
See [@FSF:licenses].
The two aims of the definition are thus:
first it establishes what in Stallman's terms software freedom means;
and second,
it is a test to validate whether or not a license can be called a free software license.
].


But most importantly here,
with such a marked file and unlike Chun's investigation of the notion of source code,
where the latter is reduced to an intermediary spectre that only becomes the source of an action once it is turned into an executable [@Chun:programmed, pp. 24-25],
software source code as exemplified with free software source code,
does not even need to be executed to be performative.
Chun admits that the English-based commands found in source code also means that it can be read by other people than programmers.
She takes works from artists such as Mez Breeze and Graham Harwood to illustrate how this can be taken advantage of in an artistic context [See @Chun:programmed, pp. 55-56].
I could not agree more here,
and this cross-readability,
by machine and human parsers,
has been most notably the root of the code poetry genre,
where the principles of poetry and computer code are purposefully conflated [See @Cramer:words, pp. 93-95].
However,
Chun overlooks other aspects of source files and source code,
and more particularly the comments,
as paratextual place holders for other forms of human instructions and manipulation that have the power to change and control the contextual interpretation of software execution well outside the machine itself.
A similar missed opportunity can be found in the writing of American writer and computer programer Alexander R. Galloway,
whose focus on the technological context of code [@Galloway:protocol, p. 171],
overshadows the openness of the paratextual marks carried by the later and fails to notice the dominance of another protocol,
albeit a more historical one:
free software as a social étiquette.
It is expressed here with Stallman's own understanding of the hacker ethics as one of sharing communities,
and such ethical social protocol can now be enforced via the simple act of source code distribution.


So far,
I have purposefully delayed the introduction of the term community.
Nowadays it is taken for granted from popular Internet culture and also academic literature that any social groups operating via computer mediated communication systems can be referred to as a community [See @Jones:cyber].
The term gained notable mainstream popularity with American writer Howard Rheingold's 1993 prophetic work on the concept of virtual communities [@Rheingold:virtual].
From his own participation in several electronic chatting,
interactive,
and discussion platforms
^[
Such as the 1985 forum Whole Earth 'Lectronic Link (The WELL),
and also Internet Relay Chat (IRC) channels,
and Multi-User Dungeons (MUDs).
],
as well as building upon early nineties social science research
^[
In particular from Amy Bruckman,
Marc Smith,
and Elizabeth M. Reid.
For detailed references,
see [@Rheingold:virtual, Bibliography].
],
he draws an analogy between so-called real world communities and the social organisation of individuals who cluster around different technologically driven communication systems.
Referring more particularly to fieldwork in The WELL from American sociologist Marc Smith [@Smith:WELL],
he uses the schema of collective goods to support his comparison.
However,
the idea of linking communication technology, commonness and community is not novel,
and its history,
promises and downfall have been extensively covered by American communication theorist James W. Carey in his 1989 work *Communication as Culture*,
where he traced amongst other things the path that led to the mythos of the electronic revolution in which new forms of community would arise [@Carey:communication, The Mythos of the Electronic Revolution],
and similar connections can be found in BSD history which has been linked with the mid 60s Free Speech Movement [@Leonard:power].


By bridging social organisation with operating systems,
and making the protocols of software access,
compilation,
execution, 
and distribution dual,
free software source code challenges the notion of openness as a purely technological network oriented idea.
Here,
I am not being metaphorical,
free software source code is a mix of human and machine semantics that target more than computer compilers,
it also gives instructions to human interpreters by the means of its legal apparatus,
it becomes a method to directly program the actors of the free society envisioned by Stallman with the help of intellectual property laws.
Therefore Stallman's understanding of community is not only in the lineage of Carey's mythos,
it is also a method by which to thoroughly structure these communities.
Even before the Symbolics instalment,
an important contribution was made by Stallman to the EMACS editor,
next to his central role in its infancy and development [@Williams:freeasinfreedom, Chapter 6: The Emacs Commune],
was the notion of *EMACS Commune* articulated in late seventies documentation of the software,
in which the project was presented as a sharing community operating according to some prototypical copyleft rules [@Levy:hackers, p.417],
however not yet expressed in legal terms.
When free software becomes more precisely formulated,
the combination of a sort of civil rights organisation,
like the FSF,
proposing a software definition and matching licenses,
like the GPL,
is not only a legal framework,
but also a toolkit for community rules,
a sort of cybernetic law model for any virtual community with GNU and the FSF providing an exemplification of such methodology.
So when different voices emerge from the early days of the free software versus open source schism,
the overlap in licensing should not be misunderstood as the growth of a *social movement*.
In effect,
it is the beginning of a process of fragmentation of software freedom,
into all sorts of diverging communities.
This was particularly visible in the results and discussions that followed the first Free/Libre/Open Source Software (FLOSS) survey in 2002 [@GhoshGlot:survey].
However,
at the time,
Stallman disagreed with some of the report findings,
and in particular how it presented the existence of two communities,
whereas he considered free and open source software to be two movements within one single community [@GhoshGlot:survey, The Stallman-Ghosh-Glott mail exchange on the FLOSS survey: Two communities or two movements in one community?].
His request to rephrase and replace community with movement was ultimately declined. [@GhoshGlot:survey]


If I look at free software from the perspective of the *Gemeinschaft* and *Gesellschaft* dichotomy,
developed by German Sociologist Ferdinand Tönnies
^[
In 1887,
Tönnies provides a conceptual framework,
in the form of an idealised generalisation,
in which he suggests that social organisation can be split into two groups:
the *Gemeinschaft*,
which is the community where relationships between individuals are structured by traditional social conventions;
and the *Gesellschaft*,
which is the society where these relationships are instead fabricated to fulfil a particular plan.
See [@Tonnies:community].
],
the sense of fellowship and bounding found in the early computer hacking groups are the essential drive of a *Gemeinschaft*.
But,
the satisfaction of being part of a community,
subjectively connected to the so-called hacker ethic,
disappears with the increasing technical and social alienation brought about by the changes in the computer industry.
From the resulting isolation rises a new strength,
Stallman's will to rally other hackers around a new speculative or utopian agenda,
a *Gesellschaft* in the form of a free society,
the rules of which are put in place to emulate his lost *Gemeinschaft*
^[
This effort was also prototyped with the *Emacs Commune* experiment,
which is how Stallman drafted his first social contract,
by embedding sharing terms in the source code from Emacs.
See [@Williams:freeasinfreedom, Chapter 6: The Emacs Commune; @Kelty:bits, p. 189]
],
and therefore engage with a conflict against the other *Gesellschaften* he claims are responsible for this loss
^[
In some of the next chapters,
I will develop further the relationship between communities,
societies,
and licenses,
and the consequence of such an emulation in the context of dual,
and triple licensing.
].


Even though Stallman once thought of himself in 1983 as being the "last of the true hackers" [@Levy:hackers, p. 415],
with free software he creates much more than a copyright hack [@Kelty:bits, p. 182],
becoming the first hacker to transcend the pejorative description of "computer bums" [@Williams:freeasinfreedom, p. 124],
who in the seventies and in the eyes of Weizenbaum,
were for the most part,
skilful yet aimless technicians,
who could not set long-term goals [@Weizenbaum:computer, p. 118].
Stallman does not set GNU's and the FSF long-term goals as philanthropic activity.
He is forced to.
Something got in the way of his main occupation and he will try to solve it the same way he hacks software.
But in the process,
the system he puts in place to emulate and compensate his loss,
becomes a guideline available for any lost,
endangered,
imaginary,
nostalgic,
novel,
or speculative *Gemeinschaften*.

\newpage
