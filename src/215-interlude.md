Interlude {.unnumbered}
=========

\setcounter{footnote}{0}


So far in this chapter,
I have built upon the argument that free software is essentially the abstraction of a struggle,
in the form of a template that can be culturally appropriated by others,
and therefore also removed from its original context,
creating a divergence of discourse within the extended family of free and open source things.
Most notably in the first section of the chapter,
I have shown that proto-free culture efforts like free art present fundamental differences with free software from which it is derived.
What is more,
yet not so surprising from the template hypothesis,
free art has co-existed historically in the proto-free culture era as two different concepts,
that did not merge,
precisely because they both proposed two different ideas of artistic freedom.
To be sure,
and going back to the link I made in the first chapter in the context of agonistic pluralism,
such differentiation should not be understood as a failure of constructing so called commons or establishing globalised cooperative economics,
but instead as a healthy sign of cultural diversity.


The free software ontology is also echoed in free art in its form,
that is a system in which practices and production gravitate around an authoritative centre that works as both reference and model.
The closer to this centre,
the closer the practice is aligned with,
and becomes a direct manifestation of the ideology at hand.
For instance in free software,
this means that the work is deemed useful enough to be accepted by Stallman to be part of GNU,
while in free art,
this means that an artist is able to engage directly with other artists within Copyleft Attitude.
As one moves further away from the centre of influence,
a more neutral zone is visible where what is produced can be listed somehow officially and based on specific criteria,
as seen with the Free Software Directory (FSD) and the free art Liste des Oeuvres.
Even further away,
in this last remote cultural circle,
different practices co-exist with little to no influence from the centre


I want to be precise that this circular ontology is not specific to free software and free art.
As a matter of fact,
it can also be found in any other proto-free,
pseudo-free,
and open content and free culture projects.
For instance Creative Commons follows the same logic of *différance* [@Derrida:marges, La Différance].
At a *first level*,
the CC website presents works and projects that are selected by the organisation to showcase projects which illustrate perfectly the function and usefulness of their license.
For instance the 2011 CC samples of "cultural creativity in 'the commons'" [@CC:culture2011] showcased a collection of instrumental music tracks by American musician Trent Reznor's industrial rock project Nine Inch Nails,
released under a CC BY-NC-SA license.
It was used to exemplify a CC economic model in which the artist can still benefit from exclusive licensing and the royalties from collecting societies for their work,
yet at the same time allow their audience to freely distribute,
share and remix the work as long as it is not for commercial purpose.
Then at a *second level*,
many web platforms,
software applications,
for individual or collaborative use,
can rely fully or partly on CC licensed work from their users---from YouTube to Wikipedia or Flickr,
to name a very few---yet maintaining a role of moderation to validate or not the contributions,
based on different more or less explicit end-user license agreement (EULA),
terms of service (TOS),
or Code of Conduct (COC).
These terms of various nature may contradict or limit the effectiveness or the license,
for economic purposes as explained in Chapter 2 with Thingiverse's terms of use,
or for ethical reasons,
similar to the way free art is moderated in Copyleft Attitude "Liste de Oeuvres".
And finally at a *third level*,
CC licenses can be used by any groups or individuals who will distribute their work across public online forums,
personal or commercial websites,
and motivated by rather diverse beliefs and intentions as I will highlight in the interviews presented in the coming chapter.


Before looking closer to possible ambiguity and misunderstandings resulting from such dispersion---which I will do in the last chapter of the current Part 2---I will now examine the sort of free-range free culture practices found in the long-tail of this cultural diffusion.
Indeed,
until now I have been mostly focussed on the epicentre and immediate surrounding of these different variants of the free software cultural diffusion.
By doing so,
I have essentially shown that the cultural appropriation of the free software template has permitted the constitution of new communities,
that started to rely and articulate their own interpretation of cultural freedom,
then made explicit by the writing of definitions,
licenses,
as well as publishing and collecting things that reinforce their ideology.
In this first-stage appropriation it became clear that the resulting language-games could not form a coherent whole,
and this is fine,
but perfect coherence could be found nevertheless within and at the proximity of these communities.
However,
it is important to keep in mind that what constitutes these groups is of course public,
and is also actively communicated and propagated as part of a desire for wider ideological adoption and expansion.
As a consequence,
and to give a concrete example,
an artist could stumble upon the FAL and start using it without joining the Copyleft Attitude mailing list,
and without reading anything about the notion of free art,
except maybe for a Wikipedia summary,
an abstract in a licensing guide,
or a link from a list of approved licenses for free culture or open knowledge.
The same goes for a programmer and the GPL,
and this point can be obviously quickly generalised for any individual in regards to any license.
So in this section,
I argue that the consequence of this is the existence of a second-stage cultural appropriation,
in which the ideological constituency of the appropriated free software template,
becomes in turn another template for other interpretations and intentions to be formed by other groups or individuals that were not part of the first-stage epicentres described so far.


This second-stage cultural appropriation developed notably in the early noughties as a quick follow-up to the proto-free culture era.
It declined with the raise of a more defined free culture,
when,
paradoxically,
less defined notions of sharing,
commons,
and open source,
became new curatorial topics for large generalist media art festivals and exhibitions with little to no connections with free and open source practices [@NetArtCommons:OSAH2002],
and to finally find their way,
even further diluted today in the visual contemporary art discourse [@Schiff:opensource],
or technically re-framed as part of a revival of interactive or generative art [@Google:devart].
While it is beyond the scope of this research to map thoroughly this under documented part of art history,
having been involved deeply in some of these groups during the peak of this second-stage cultural appropriation,
as stated in this thesis introduction,
it became progressively clear for me that the artists and hackers involved at the time either individually or as collectives,
had all their own specific understanding of cultural freedom,
that did not necessarily overlap or were compatible.
If one listens to the artists behind the works, performances,
installations,
lectures,
and workshops programmed throughout the noughties in artist-run art festivals that promoted the use of free software and free culture licenses,
such as Piksel in Bergen,
LiWoLi in Linz,
make art in Poitiers,
Junctions in Brussels,
OpenLab in London,
to name a very few,
it is obvious that their intentions and interests varied greatly,
from the technical exploration of versatile free software tools,
to anti-capitalist forms of engagement provided by the idea of community developed tools.
^[
For a good sampler of such broad interests,
I recommend watching the documentaries *FLOSSOFÍA*, about Piksel and make art festivals,
[@flossophia],
and the LiWoLi09 video interviews,
[@CannitoJianBence:liwoli].
Similarly the 2005 *Underneath The Knowledge Commons* edition of Mute magazine,
offers interviews from active artists and hackers, 
that already showcases the plurality of voices present at the time,
[@Iles:free].
More recently,
the book *Conversations*,
present a similar diversity in the form of transcripts from several discussions "between developers and designers involved in the wider ecosystem of Libre Graphics". See [@HaagKleinSnelting:conversations].
Last but not least the round of interviews conducted in 2009 during the *Winter Camp* event,
organized by the Institute of Network Cultures in Amsterdam,
also testify the discrepancy of interests and intentions between those of the invited groups that integrated notions of free culture as part of their networked practices, see [@ColemanLovinkRossiterZehle:wc09].
]
It was also not uncommon for tension and conflict to rise between these groups and individuals,
when it was apparent that there was no particular ideological alignment or shared valued to start with
^[
A good illustration of this was the feud between the openFrameworks toolkit and Piksel workshop communities,
regarding the licensing of the former,
and ultimately what such licensing meant ideologically.
See [@gif:gpl, mailing list thread].
].


All that said,
regardless of the variety of discourse,
and next to the usual ethics versus economics split already present in the free software versus open source discourse,
it was possible to discern in this long-tail three different emphases: production, product, and process.
During the writing of this dissertation I was able to conduct several semi-structured interviews with practitioners involved in the practice of free-range proto-free and free culture,
and it became clear these were indeed cardinal references to navigate within and make sense of the apparent cultural randomness found in these practices.
I decided for this text to focus in particular on three discussions that were the most illustrative and articulate of these aspects,
and each of them will be presented in their own section:
the tools and the means of production for Basque noise and improvisation artist Mattin,
the final outcome and its distribution for American cartoonist and animator Nina Paley,
and the process of making where the distinction between production and product becomes blurred,
with French graphic designers Stéphanie Villayphiou and Alexandre Leray.
Finally in a fourth subsection,
I will discuss a particular case of free cultural production where free culture is not only a vessel,
but also a subject and artistic material.

\newpage
