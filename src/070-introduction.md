Introduction {.unnumbered}
============


What Is Free Culture? {.unnumbered}
---------------------


According to the website *freeculture.org*,
developed by a "non-partisan group of students and young people who are working to get their peers involved in the free culture movement" [@fcstudents:about],
the term free culture was originally coined by American law professor Lawrence Lessig in his 2004 book *Free Culture: How Big Media Uses Technology and the Law to Lock Down Culture and Control Creativity* [@fcstudents:fc].
Lessig's book is an elaborate collection of anecdotes,
that together form a critique of the increasing discrepancy between, on the one hand,
the way people use technology to share,
create,
and transform media,
and on the other hand,
the laws that regulate and control such activities.
He puts an emphasis on digital media files such as music and films,
and notably exemplifies in his analysis the role and context of piracy in the development of the media industry.
Lessig warns the reader of the increasingly negative impact of legal rights such as copyright on culture,
on creativity,
and more precisely: 
on the ability to create and share new productions of different artistic,
musical,
or literary creations.
In particular,
the laws that regulate the intellectual property aspect of material production have became inadequate to the production and consumption infrastructure of the Internet.


As British sociologist Dick Hebdige explained in his seminal 1979 book *Subculture: The Meaning of Style*,
culture is a particularly ambiguous word that has been redefined several times,
sometimes with contradictions,
and that as a whole could be used both to describe processes as well as products,
and relations within the whole way of life as well as standards for excellence [see @Hebdige:subculture, pp. 5-19, From Culture to Hegemony].
However,
the idea of culture that Lessig refers to bares no ambiguity,
and can be best associated to a specific category once defined by Welsh cultural theoretician Raymond Williams,
namely "works and practices of intellectual and especially artistic activity" [@Raymond:keywords, p. 90],
where "culture is music,
literature,
painting and sculpture,
theatre and film" [@Raymond:keywords, p. 90],
albeit taking into account their digital materialisation.
However,
even if Lessig refers to the circulation of digital works and cultural expressions on the Internet,
the free culture he refers to is not a gratis culture.
In his words,
the concept of cultural freedom is tightly linked to liberal traditions,
and in the preface of his essay he connects the notion of free culture with the ideas of free speech,
free markets,
free trade,
free enterprise,
free will,
and free elections [@Lessig:fc, p. XIV],
to both honour the lineage this term aims to be associated with,
and also to note about the linguistic misunderstanding that a term such as *free* could create.
For him,
free has nothing to do with gratuitousness and the lack of property.


> A free culture supports and protects creators and innovators.
> It does this directly by granting intellectual property rights.
> But it does so indirectly by limiting the reach of those rights,
> to guarantee that follow-on creators and innovators remain as free as possible from the control of the past.
> A free culture is not a culture without property, just as a free market is not a market in which everything is free [@Lessig:fc, p. XIV].


After the publication of the book,
those who have embraced the idea of free culture,
like the contributors of the freeculture.org website,
admitted that since then the original notion might have been changed or expanded [@fcstudents:fc].
As a matter of fact,
the website does not offer one unique definition,
but instead points to internal and external resources that could potentially further inform the reader about how free culture could materialise,
what its manifesto could be,
and how difficult it is to define it more clearly [see @fcstudents:looklike; @fcstudents:manifesto; @fcstudents:question].
Free culture could therefore be claimed by potentially anyone sympathetic to what was sketched by Lessig.
In the end,
because of such a loose framework,
free culture came to be understood as a social movement by some while others described it as a subculture [see @fustermorell:gov, p. 27].


In fact,
if Lessig was the first to publish a book on the concept of free culture,
the debate on cultural freedom and cultural activism in the age of networked collaboration and digital works was much older.
Free culture in that sense was the logical next step to what was already anticipated in the late nineties with the early analysis of copyright regulations over the Internet [@boyle:politics],
but was also the first attempt to rationalise an infrastructure in the lineage of the mid-nineties notion of collective intelligence existing over digital networks [@levy:ci; @levy:world].
Indeed,
digital cultural freedom resonates strongly with the idea that access to knowledge and information should be facilitated,
in order to create communal ownership for new idealised networked societies [@Castells:risenet],
in which free culture could be both the mechanical apparatus for the exchange of information,
but also a binding element for different groups interested in these issues.
So even though free culture relates to a narrow definition of culture,
it must also be understood in terms of wider societal concerns such as the "general process of intellectual, spiritual and aesthetic development" [@Raymond:keywords, p. 90].
This allowed free culture to broaden its cultural scope beyond the exchange and transformation of digital works.
By existing at these two levels,
free culture,
in its broad critique of intellectual property laws,
became part,
with varying interpretations,
of the discourse of several social and political activist efforts
^[
This has been visible notably in the rise of the Pirate Party,
[see @burkart:pirate],
and recent discussions on the articulations of political agendas supporting commons-oriented economy and society, [see @KostakisBauwens:collab],
where the question of free culture and the commons have joined other issues and evolved beyond the issue of file sharing.
].
Lessig would eventually describe free cultural efforts as inspired by the notion of *cultural environmentalism* [@lessig:lawcontemp],
a term originally coined by Scottish professor of law James Boyle,
to illustrate how the model of environmentalist movements raising awareness of ecological disasters,
could be transposed to cultural activism's raising awareness of cultural disasters,
and in particular the enclosure of the public domain. [@boyle:politics]


In essence,
what the free culture generalisation implies is that culture is currently not free,
it needs to be liberated from those who use intellectual property laws to control it for their own benefits,
and at the same time limit its circulation as well as transformation.
To be more precise---and this will be more thoroughly explained in this dissertation---regardless of the long-term intention,
this liberation is in practice more of an attempt to balance more fairly the control over the production and publication of cultural products,
rather than oppose entirely copyright and other intellectual property laws.
This balancing is achieved by working around the very intellectual property laws identified as being the source of the problem,
and use them in order to reclaim the way works can be distributed,
used,
published,
and transformed.
Because of the emphasis made by free culture on the legal vessel,
it means that in this context there are no differences made between free cultural works themselves:
a work of art is no different from a beer recipe.
It is up to the practitioner to contextualise their struggle using generic tools,
which are the instructions that I was referring to earlier:
the licenses.
Licenses are legal documents distributed with the free work or cultural expression,
and these licenses specify what can be done and under which conditions.
^[
I will return regularly through the course of this thesis in order to explain what these documents are and how they operate.
]


Free culture offers, 
in effect, 
a rather paradoxical form of cultural freedom:
to develop new constraining techno-legal frameworks so as to liberate cultural production from other constraining techno-legal frameworks.
If this proposal may seems curious at first,
scholar Christine Harold notes however that more radical forms of cultural activism,
such as anti-copyright for instance,
are not necessarily a good thing.
She uses two notable analogies,
the first is from American activist David Bollier who argues that the creative process needs an "open white space" [@Harold:space, p. 154],
and the second is from Canadian composer John Oswald who states that "if creativity is a field, copyright is the fence" [@Harold:space, p. 154].
From this point Christine Harold proceeds to argue that fences are not always strict boundaries,
they can be straddled or crossed,
reconfigured and be transformed as part of a democratic process [@Harold:space, p. 154].
In that sense,
she argues that these fences are not as antithetical to liberated cultural processes as some artists and activists might affirm [@Harold:space, p. 153]. 
Furthermore,
Harold explains that such "blockages and constraints have always been inextricably linked to invention" and that "the pirating strategy of 'theft', however unwittingly, perpetuates the very notion of property that it rejects" [@Harold:space, p. 153]. 
If Harold also adds that efforts like Creative Commons---a key project in free culture that will be discussed several times in this thesis---takes regulations and markets very seriously [@Harold:space, p. 145],
what her fencing counter-argument shows is that licenses can be a strategic social democratic tools to claim back lost,
or protect new,
cultural territories.


In fact,
the free cultural strategy of playing with fences,
was heavily appropriated from free and open source software licensing.
Free and open source software---a collaborative and cooperative mode of software production in which source code is shared---has variously been described as a technological revolution [@OReilly:shift],
and as a paradigm shift
^[
To refer to the concept of scientific revolution,
originally articulated by @Kuhn:revolutions.
].
Its cultural significance beyond the realm of software was noted in 2008 by American scholar Christopher Kelty,
who employed the term *modulation* [@Kelty:bits] to explain how free and open source practices could be transposed to other fields.
However,
signs of such modulation,
or cultural diffusion,
started to be visible and articulated very precisely a decade earlier in the late nineties,
and at a time where the term free culture was also yet to be introduced,
a time I will refer to as the proto-free culture era.
Such early modulations made it so that free culture today is in fact not a speculative mode of production but the tip of an iceberg,
made of all sorts of cultural activism that manifest in the broadest ways possible:
from agriculture [@Aoki:seeds],
to terrorism [@inspire:jihad],
and also physical spiritual practices [@OSYU:what].


Discussions around the influence of free and open source software on art and culture,
and more particularly art and culture that involve the use of technology,
often revolve around the role of the artist in a networked community [@CatlowGarrett:DIWO],
and their relationship with existing free and open source software communities [See @Lee2008].
But other aspects need to be investigated,
from the engineering advantage of free and open source software both as exceptional artistic tools [See @deValk:tools; @Howse:plug],
to the relationship between a proto-free or free cultural license and the work that carries it.
The latter in particular,
has been given a lot of attention in this research,
and connecting the intention of an author with the choice of a license [@Liang2005, p. 57] will help us understand what happens when free and open source strategies are applied to art and culture production.


Research Question {.unnumbered}
-----------------

The impact on cultural production and on practices developed in relation to the ideas of free and open source software has been both influential and broadly applied,
and for this reason such cultural practices operate essentially at the tail of the free and open source cultural diffusion.
The consequence of this is that these different free and open source licensing ideas and their materialisation,
might not share so much with the systems from which they appear to be derived,
let alone the fact that such germinal systems are more complicated than they appear to be.
The choice of a free culture license in particular,
is not straightforward. 
There is clear distinction to be made between practitioners consciously constraining their practice around a novel techno-legal system,
and those who are pressured under the same system,
and that they may have neither chosen to adopt,
or have overlooked or misunderstood.
The strength of the free culture proposal to simplify and generalise cultural mechanisms as a shared techno-legal process may also be its biggest weakness.
Once the illusion of a lingua franca,
that is to say using legal definitions of cultural movements and objects encoded as licenses,
is eroded by a deeper analysis of the intentions of free culture practices,
all sorts of dialects may appear,
for which the free cultural jargon can only approximate some ideas and make compromises.
In that sense free culture may not liberate its practitioners but subjugate them to a particular cultural hegemony,
or at the other end of the spectrum,
open up possibilities for alternative rules and control over cultural production for those who can understand existing and create new techno-legal templates. 

Put simply,
even though all art and cultural activism inspired by free and open source software practices,
can be quickly placed under the umbrella of free culture,
the goal of this research is to demonstrate that art and culture,
connected to the ideas of free and open source software,
has been affected in ways that were both unforeseen and different from what they were believed to do.
By looking at how free and open source ideas have been effectively applied across different groups and contexts,
and how these application overlap or differ,
the question I ask is,
in which practical and theoretical ways has free and open source software licensing provided a model of transformation for art and cultural production?



Methodology {.unnumbered}
-----------


I cannot stress enough the importance of the context and scale in which cultural production occurs in this research,
and why this aspect will be regularly highlighted throughout the whole thesis.
To give a brief example:
contributors to free software can be presented as members of one united front in which its participants are bound together by the same ideology.
^[
I will come back in more detail in Chapter 2 on the usage of this term within free and open source discourse,
and also within this thesis.
]
The very existence of a free software *movement* further reinforces this sense of common direction,
and this approach is useful to introduce such an effort in broad terms.
However this comes with highly problematic strings attached.
The infamous historical schism between free software and open source software,
or the difference between copyleft and copyfree licenses provide some of the many examples,
which examplify that things are not so simple once looked at more closely.
Such details can be easily overshadowed and bring confusion,
when associated with the popularity of an encompassing acronym such as Free and Open Source Software (FOSS),
or Free/Libre and Open Source Software (FLOSS).
While both of these acronyms clearly attempt to go beyond internal conflicts and aim at consolidating the different parties,
so as to engage with greater sets of concerns,
like free versus proprietary and open versus closed,
nonetheless in this simplified view,
the freeness and openness of objects become arguably more vague.
What such simplifications gain in information compression is counter-balanced by a loss of its most significant details.


To be sure,
the discussion on the difference between free software and open source software has been exhausted already,
however in this research I will show that what is often cited as an example of discourse discrepancy [For instance in @rip:remix] is but one of myriad ideological differences that must be addressed.
Things can get particularly murky when the cultural diffusion of such simplified principles then triggers new things,
like the term free culture,
which is both a further generalisation and simplification of previous software-centric ideas of freedom and openness.
As a result,
although the possibility of free and open source appropriation is undeniably a proof of success,
it remains questionable if the resulting different appropriations actually mean the same thing when attached to different disciplines,
or when introduced by different groups.
If I am interested here,
in both culture as communication [See @Hall:silent],
and communication as culture [See @Carey:communication],
what this journey into the open mist of free culture could very well highlight is in fact,
an ode to culture as miscommunication and miscommunication as culture.


Without close reading and comparison between what a practice is believed to do,
how it is articulated ideologically,
how it manifests and materialises itself,
and finally how such manifestation and materialisation is perceived,
there is a risk of providing an incomplete picture.
Because of that,
to investigate the transformative model of free and open source practices as a whole,
one requires a ceaseless analysis of its discourse,
yet one that can only be achieved via an ongoing change of scope,
from the micro scale to the macro scale and back,
so as to limit as much as possible any misinterpretation and inductive generalisation.
This difficulty has so far prevented a comprehensive discussion of the political,
artistic,
and technological aspects of free culture.
Previous efforts to do so have been limited to advocacy [@Lessig:fc; @Benkler:wealth],
discourse analysis disconnected from practice [@Berry:copy],
a focus on free and open source software communities that might not be always representative of their free cultural neighbours [@Coleman:freedom; @Kelty:bits],
or framed from the perspective of a particular ideology [@Moglen:anar; @Terranova:free].
To be sure,
and connecting back to the notions of context and scale introduced in this section,
I am not claiming to provide here a research that is a "total view,
and which is able to move effortlessly between scales" [@McCarthy:scale].
I do however have a particularly involved position of participant observer in this research,
that comes from being closely involved in free and open source inspired art and culture communities for many years,
through the production of works of free software art, 
the curating and organisation of exhibitions,
festivals and conferences,
as well as the development of several free and open source software projects [For a discussion about these works and projects in relation to free culture see @Renno:mansoux],
and co-editor of the first anthology on free software and art practices [@MansouxDeValk:FLOSS+Art].
This position gives me a rare opportunity to try to provide a more holistic approach.
That being said,
this thesis is not practice-based or auto-ethnographic,
and in this text I will only very rarely and only anecdotally refer to projects I have been involved with.
My unusual position helped me however to arrange semi-structured interviews with practitioners whose work and ideas on free culture represented the most exemplary proof of diversity.
These interviews occurred by email and face-to-face encounters,
with discussions sometimes spread over several years.
The semi-structured interviews were used in this thesis either to illustrate or argument an idea,
or as a very specific case-study,
in which case I will dedicate a whole section to them.


In parallel to linking case studies with discourse analysis,
I will be using several theoretical frameworks.
The purpose of doing so is twofold.
First,
this was needed to explore more precisely a particular aspect of what was being discussed at a particular time,
for example,
the work from French semiotician Roland Barthes [@Barthes:obvie; @Barthes:langue] will be useful as a basis to discuss the artistic appropriation of the free cultural discourse,
but not so relevant for other parts of the research.
Second,
some artists interviewed during this research articulated their practice in relation to existing theoretical concepts,
this was the case for instance with Basque noise musician Mattin,
who found inspiration in the writing from German critic Walter Benjamin. [@Benjamin:producer; @Benjamin:workofart]
In that case,
it is useful to partially remain within the same theoretical framework when discussing the artist's work.
Next to that,
I am very much indebted towards the concepts of *Gemeinschaft* and *Gesellschaft*[@Tonnies:community],
the *iron cage* [@Weber:protestant],
from German Sociologists Ferdinand Tönnies and Max Weber, respectively,
that inspired me to explain how free software was essentially a constitutive template to emulate communities,
and propose the term of sandbox culture to describe free cultural mechanisms,
where software and legal code become a dual liberating and constraining constituent device for different communities to experience varying ideologies and practices.
I also hope that the notion of cultural sandboxing can contribute a new way to approach and discuss post-subcultural dynamics,
that cannot be easily analysed with existing static subcultural models [@Stahl:renovating] in the context of groups that mixes operating systems with social systems.
Throughout my writing,
I will also frequently refer to the notions of *radical democracy* [@LaclauMouffe:hegemony] coined by Argentinian and Belgian political theorists Ernesto Laclau and Chantal Mouffe.
In particular,
Mouffe's later re-articulation of radical democracy as *agonistic pluralism* [@Mouffe:agomodel],
will be a crucial theoretical tool used in this thesis to critically analyse liberal democratic dynamics within proto-free and free culture communities.
Last but not least,
in this dissertation the use of the term culture will often vary.
For instance,
*free culture* may be presented as Lessig's general ideas on culture production,
but it can also loosely refer more broadly to art and cultural production that offer alternative to existing copyright frameworks,
or on the contrary,
*free culture* may be very specific to a particular set of licenses.
Finally free culture is not necessarilly a synonym of the free and open source software culture,
or the cultural practices surrounding a particular group.
One of the goals of this dissertation will be to provide a thorough mapping of the different usage of the term and similar ambivalent ones,
the use of the word culture will therefore be always contextualised to clarify its meaning,
and to define the modes it adresses.



Limit of the Research {.unnumbered}
---------------------

The scope of this research is essentially centred on North-American and European art and cultural production,
with some exceptions and relevance notably for Latin American countries.
As a consequence,
reference to intellectual property laws,
practices of sharing and copying,
as well notions of freedom and politics,
must be understood strictly within these boundaries.
If some aspects can be still relevant beyond this scope,
their generalisation might be very risky without considering other factors.
For instance the usage of free and open source technology in Africa, Western and Northern Asia would need to be put in perspective with both postcolonial analysis,
the rise of fab labs,
and the lack of ethnic and culture diversity in hackerspace communities [@GrenzfurthnerSchneider:spaces].
Similarly,
if free and open source software practices found their way to Eastern Asia,
as novel artistic tools [@Matsumura:pd],
or communities [@Huang:openlab],
it would be difficult to treat art and culture activism without first looking at the specific relation between art and technology in this region of the world,
as well as discuss forms of technological openness that are specific to these places,
like the shanzhai *open BOM* culture ,
where the list of materials and component assemblies are shared and improved across different manufacturers,
following word-of-mouth rules that are policed by the manufacturing communities themselves [@Huang:openbom].
Finally,
the relevance of free and open source principles and practices,
as well as their possible transposition to non-software works and cultural expressions,
must always be contextualised in relation to the intellectual property frameworks they attempt to work around.
Their usage and value cannot be decoupled from the way such frameworks are defined and enforced [@Ilyas:maldives].



Structure of the Argument {.unnumbered}
-------------------------


This thesis is constituted of eight chapters that are organised in three different parts,
named after American computer programmer Richard M. Stallman's famous attempt to contextualise software freedom in his own terms
^[
I will come back to this contextualisation,
and explain it in more detail several times in this thesis.
]:
free as in speech [@Stallman2001].
Between each chapters and parts,
small sections presented as *interludes* are provided to give the reader an overview of what was recently discussed,
announce what will come up next,
and reflect as an aside on some of the ideas discussed so far.
Generally speaking these bridging sections will also help situate the progress of the argument,
and how the material analysed relates to the thesis question.

I chose to divide the thesis in three parts in order to address three sub-questions needed to break down the main research question (in which practical and theoretical ways free and open source software licensing has provided a model of transformation for art and cultural production):
what does make free and open source software relevant to cultural production;
what are the relationships between free culture, free art and free software;
and what kind of techno-legal and social systems does free and open source practice create.

Part one,
*Free as in... Culture*,
will answer what makes free and open source software relevant to cultural production,
with the help of the following two chapters:
Chapter 1 *Paradigm Maintenance and User Freedom* that will question what is truly revolutionary about free software practices;
and Chapter 2 *In Search of Pluralism*,
that will explain how the free software techno-legal template contributed to shaping both the proto-free and free culture eras of culture activism.
^[
In this chapter I am proposing the term *proto-free* to describe ideologies and practices related to free culture ideologies and practices before their articulation as such.
]

In part two,
*Free as in... Art*,
I will discuss the relationships between free culture, free art and free software.
This more precise illustration of how cultural appropriation operates within free culture will be addressed in three chapters:
Chapter 3 *Art Libre*,
which as the name indicates will trace the history of Free Art and its license;
Chapter 4 *The Practice of Free-range Free Culture*,
where I will discuss the practice and works of some specific artists and designers;
and Chapter 5 *Free Cultural Misunderstandings*,
in which notorious misunderstandings in the free culture discourse will be discussed,
in particular the term copyleft and the commercial exploitation of free works and cultural expressions.

Finally in part three,
*Free as in... Trapped*,
I will formulate the kind of techno-legal and social systems that free and open source practices create.
I will do so in three chapters:
Chapter 6 *The (Almost) Endless Possibilities of the Free Culture Template*,
that will notably explore the limit of transposing software freedom to cultural freedom;
in Chapter 7 *From Techno-legal Templates to Sandbox Culture*,
I will show the mix between operating systems and social systems is an essential aspect of the free cultural techno-legal template;
in the last chapter,
Chapter 8 *The Mechanics of Sandbox Culture*,
I will develop further the sandbox analogy as an attempt to provide a model that shows how the paradoxes and conflicts found in free cultural discourses are not just misunderstandings,
but are in fact what help sustain these novel forms of production and organisation.

\newpage
