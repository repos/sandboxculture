Interlude {.unnumbered}
=========

\setcounter{footnote}{0}

As shown in the previous chapter,
free software is not so much of a revolutionary thing,
which is to say it is not a paradigm shift in the way software is produced.
Similarly,
this seminal notion that software freedom is essentially the articulation of an opposition towards closed source and proprietary practices is only one side of the story.
To be sure,
it is not my intention to undermine the work of Stallman,
but I do believe that the most interesting aspect of the free software story is that of the creation of a thorough techno-legal structure for virtual communities,
that goes well beyond the question of online platforms as the term often implies.
In that sense free software is the containment and protection of practices idealised by Stallman,
they are emulations,
because they enable genuine modes of organisation and production that are embellished by Stallman's own ethics and ideals,
partly experienced and partly derived from the magical recovery [In reference to @Clarke:magical] of the early computer programming communities,
while being at the same time tightly contained in a construction made of software and legal code.


In the next chapter,
Chapter 2,
I will argue that the perception of free software as such a model,
template even,
is the reason it became widely adopted by groups whose interests could not be formulated and controlled within existing techno-legal frameworks.
This is also why this process of diffusion quickly escaped the realm of software because what is being produced does not matter any more,
as long as it can be expressed with the system definition-licenses inspired by free software.
As a result the notion of culture itself will also become rationalised in the same techno-legal fashion.
However,
I will argue that what first appears to be an interesting model for pluralism in a system that mirrors the dynamics of liberal democracy,
the broad diffusion of this system,
will start to show its limits when its commonness is challenged by the conflict between the different ideologies that have appropriated the free software techno-legal methodology.
In particular,
I will explain that if such pluralism had been founded on a consensus about cultural freedom and openness,
the fact that these were however interprated differently meant that the free and open source family of things was closer to an *imagined community* [@Anderson:imagined] rather than a coherent whole,
or single community,
as Stallman believed so [@GhoshGlot:survey, The Stallman-Ghosh-Glott mail exchange on the FLOSS survey: Two communities or two movements in one community?].


To develop my argument I will first look at the early diffusion of the free software model into non-software things,
and how the process leads to a growing distance from matters that were so far specific to computational culture.
I will frame this active diffusion and proliferation of new definitions and licenses as a positive sign of pluralistic activities,
and make a connection with the concept of radical democracy [See @LaclauMouffe:hegemony].
I will then discuss that even though the cultural diffusion of free software will eventually reach domains that seem unrelated,
the nature of the template,
most notably its origin in prototyping and engineering culture,
will remain consistent and applied to every subject.
Finally I will analyse the moment in which some of these definitions and licenses ended up more thoroughly categorised,
filtered,
aggregated and defined,
and the consequence it has on pluralism and the wide diversity of ideologies that appropriated the free software template,
more particularly looking at the field of open design and the makers movement as an illustration of my argument.


\newpage