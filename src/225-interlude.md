Interlude {.unnumbered}
=========

\setcounter{footnote}{0}


With everything discussed so far in this text and more particularly in this second part,
it should become clear that free culture is symptomatic of a situation in which---in postmodernist terms---the temptation to present it under the form of yet another *grand narrative* [In reference to @Lyotard:condition],
"les grands récits" [@Lyotard:rapport],
is constantly challenged and questioned by its value and function at the level of individual experiences and personal narration.
Similarly,
the dual openness of source code presented in the first chapter,
and now the dual openness of licenses,
leads to a state where the technical,
legal,
and literary interpretation of these documents can never be taken for granted.
To make things even harder to track,
as explained previously,
proto-free and free culture practices have greatly inherited from engineering prototyping culture,
where things are constantly cooperatively rewritten,
revised,
and reiterated.
As for the question of authorship,
a very broad range of opinions can be found in these free cultural processes of participation.
The way they are expressed in practice is directly aligned with the means of reproduction that they offer to their audience:
from traditional verbatim strategies,
so as to leave a thought or an intention pristine and pre-interpreted ready to be consumed by an audience,
to an invariably replayed and materialised death of the author [@Barthes:langue, II. De l'œuvre au texte],
whereby every reader has the permission to become writer.


The direct consequence of this ongoing process is that it becomes questionable to find a foundation of discursivity [@Foucault:auteur] in such mussiness.
If I have indeed shown that several stages of appropriation of free cultural principles imply their organisation around originative ideas, 
I must clarify that this is different from describing a system of influence around a foundational idea.
Instead it was merely used as a simple way of following some of the transformation of the free cultural discourse as it is diffused.
To be sure,
I do not see these transformations as proofs of defusion,
or elements of deviance or divergence,
that would imply the weakening or recuperation of an *authentic* free and open foundational position.
On the contrary,
each of these variations is given the possibility to become the foundation of a new discourse,
and as explained in Chapter 2,
this should not be seen as a problem,
but a healthy sign of hegemonic and counter-hegemonic dialogue in a very diverse cultural landscape [@Mouffe:cultworker, p. 210],
and operating by the means of the expression of power relations via their conscious choice of inclusion and exclusion.
Such practices help establish a certain order in a context of contingency [@Mouffe:political, pp. 16-17].


The consequence again is that the license as the vector of a clearly defined intention and usage simply cannot be trusted.
The risk here is that even if the freedom of interpretation and usage is beneficial for both the cultural and legal fields,
and can attest to the existence,
if not to some clichés,
of a romantic understanding of the needs and apparatus of each side,
this same freedom of interpretation can also quickly turn into misunderstanding.
In the thesis introduction I made reference to Hall and Carey,
to push forward the ideas of culture as communication and communication as culture,
respectively.
With free culture both these approaches seem to constantly work to simultaneously improve and undo the outcome of the other,
to the point where what we are left with could be more accurately defined as culture as miscommunication and miscommunication as culture.


With that said,
if it's not a problem per se,
it does not mean that it is not problematic.
The risk arises when a fixed viewpoint at a particular time,
becomes framed and turned illustrative of the whole machinery by means of hasty generalisations.
Although it is not in the scope of this research to systematically list all of these faulty generalisations,
in the next chapter I intend to provide an analysis of two aspects that are the most prone to misunderstandings:
the meaning of copyleft within and outside free culture;
and the relationship between commercial activity and free culture.
Each point will be addressed with examples in its own section.

\newpage
