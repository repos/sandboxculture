The (Almost) Endless Possibilities of the Free Culture Template
===============================================================


Free Software Art Publishing
----------------------------


Debian,
which has been mentioned several times in this thesis,
is a collection of free and open source software put together to form a complete operating system (OS) that is called a *distro*. 
To be more exact it is a Linux distribution,
or a GNU/Linux distribution when it is desirable to put the emphasis on the fact that many such distros rely at their core on the combination of both the GNU OS---without its kernel called Hurd---and the Linux kernel.
^[
For a more extensive account of the historical relationship between the GNU OS project and the Linux kernel project,
and the controversy around the term GNU/Linux,
see [@Williams:freeasinfreedom, Chapter 10 GNU/Linux].
]
Indeed,
Debian is by no means the only distro available.
According to the webzine LWN,
there are at the time of writing nearly five hundred active distros [@LWN:distros].
However,
looking at the vertiginous *GNU/Linux distribution timeline* [@Lundqvist:timeline],
it is striking to see that the majority of these distros are derived or still directly dependant today,
on only three free software collections that started in the early nineties:
Debian,
Slackware,
and Red Hat.
The free software techno-legal template,
is therefore not limited to the appropriation of licenses,
it also operates at the level of software code,
and in this case,
provides the ability to create different operating systems fine tuned for all sorts of purposes and communities.
But the amount of users is not evenly distributed,
and according to DistroWatch,
a website dedicated since 2001 to tracking the development and releases of free and open source Unix-like OS,
there are,
at the time of writing,
ten "most widely used" Linux powered *major* distros:
Linux Mint,
Ubuntu,
Debian GNU/Linux,
Mageia,
Fedora,
openSUSE,
Arch Linux,
CentOS,
PCLinuxOS,
and Slackware Linux [@distrowatch:major].


Most distros provide the usual graphical user interface (GUI) desktop metaphor similar to Mac OS or Windows,
and a collection of free and open source software for both general and specialised tasks.
But next to a standard default selection,
these operating systems are connected to several repositories of software,
that allow the user of the system to add more software and adapt the OS to their needs and liking.
In this context,
free and open source software customisation can therefore happen at two levels:
first by picking a particular distro whose base content has been curated by others,
and second by adding software packages that are compatible with the distro,
and generally accessible on networked repositories specific to the chosen distro
^[
A third level also exists,
which is the ability for the user to manually compile other software sources and modify the system quite extensively.
This aspect goes beyond the OS ecosystem itself but is interesting to consider given its link with commercial activities relying on copyleft or permissive licensing and which ship products based on existing operating systems,
customised and sometimes integrating closed source software as well.
An example of this would be the operating system running on broadband wireless modems and routers,
that can be based on a Linux operating system and a few more free and open source software projects,
the source code of which must be shared by the manufacturer,
but not the source code of any other software written by the latter and yet bundled in,
and vital to the functionning of the device.
].
The flexibility of these operating systems is such,
that almost any distro,
whether general or specialised with many standard packages provided by default,
can be made minimal and bare bones again by removing packages,
and changed into something radically different at a later stage.
Regardless if the distro provides pre-compiled software or not,
the source packages maintained by distro developers,
maintainers,
and also sometimes less officially by the users themselves,
tend to provide the same things:
the original source code written by the original software author(s),
as well as optional patches to apply on top of it,
and the license(s) under which these files are published;
the metadata of the distributed software,
that is its description,
category,
and a list of author(s) and maintainer(s);
as well as the technical prerequisite of its installation,
that is to say,
a list of other packages needed to be installed before,
and which the package is dependent.
Last but not least,
any changes in these files are logged and stored in the packages themselves or external databases.
These changes combined with the storage of previous versions of the software,
and its source code,
and the ability to access these at any time,
literally turns almost any distro into a vast software archaeological excavation site.
If this transparency and traceability facilitates communication and the efficiency of technical infrastructure needed for making these distros,
it is also a side-effect of the free cultural licensing of the distributed software.


Another consequence of the publishing model enabled by free culture licensing,
is that the packages can be mirrored online by anyone with enough storage space and bandwidth.
From a user perspective,
package managers---administrative software developed by the distro developers---can then be used to install,
remove or upgrade software,
which simplifies greatly the maintenance of one's operating system to one's liking.
^[
In a way these package managers could be perceived as similar to app stores,
that are popular nowadays and found in mobile and desktop operating systems.
However, app stores notably do differ in the way they introduce a hierarchy of usefulness,
where optional applications are given most visibility,
as opposed as traditional package managers where no particular filtering is enforced. 
]
Furthermore,
this process is not unidirectional,
because users are often given the possibility to help and give feedback by writing documentation,
submit bug reports,
write patches for their favourite software,
suggest new software to be packaged,
and even maintain such software themselves by also becoming official maintainers and developers.
They can also simply publish other or slightly different software in unofficial repositories that can be used by other distro users.
What is striking here,
is that these systems are not mere advanced forms of prosumerism,
because their whole infrastructure can be re-appropriated and derived into new projects,
new operating systems and software collections as shown with the overwhelming amount of distros available.
^[
It is out of the scope of this research to dive into the specifics of what precisely constitutes a distro,
in practice there are some significant differences from one distro to another.
For instance some are truly put together from scratch,
while others are customising an existing operating system,
or combining different sources of pre-packaged software.
Some distro also start provide such level of customisation within their own installation process,
such as the Debian Pure Blend project.
See [@Debian:blends].
]


This is why so many distros can be produced and distributed.
These can be of different nature,
as not only technically specialised distros can be released,
for instance focused on network security [@Kali:homepage] and privacy [@Tails:homepage],
scientific computation [@SL:homepage],
or medical applications [@Debian:med],
but also any community has the potential to manifest its interest or ideology under the form of a distro:
enter the stranger than fiction realm of Ubuntu Christian Edition [@Hancock:uce],
the North Korean Red Star OS [@RSOS],
and of course Biebian,
the Justin Bieber Linux distribution [@Bieber:linux].
This level of customisation is such that it has become its own aesthetics,
as software artists Gordan Savičić and Danja Vasiliev illustrated with their 2011 work *The 120 days of \*buntu*,
a collection of 120 modified Ubuntu Operating Systems. [@VasilievSavicic:buntu]

To be sure,
I use Linux distributions as an example here, 
given their popular usage and visibility in mainstream tech media,
but these modular qualities also exist in other free and open source UNIX-like operating systems.
In fact,
as mentioned several times in the first chapter,
these properties were already visible with the birth of the Berkeley Software Distribution (BSD).
In particular the extreme adaptability of Unix was the main drive behind the so-called Unix wars,
and explained the reason why the different Unix-like OS failed to reach standardisation in the late eighties and early nineties [@Kelty:bits, 5. Conceiving Open Systems].
If free and open source BSD-derived operating systems differ structurally from Linux distros--in the sense that BSD OS like FreeBSD,
OpenBSD,
NetBSD,
or DragonFly BSD offer a complete base system that can at a later stage be *optionally* extended with extra software,
as opposed to Linux distros piecemeal-assembly [@DFuller:designbsdlinux]---their flexibility and ability to be transformed is as powerful and was demonstrated in the commercial field due to the permissive licensing of the base system
^[For instance Sony relies extensively on FreeBSD and other free and open source software for its PlayStation 4 video game console. See [@Sony:ps4bsd].
].
Ultimately,
all these Unix-like free and open source operating systems offer an interesting publishing system,
in which archiving,
conservation,
distribution,
and access are merged into one replicable and modifiable structure.


Given this potential and possibility to adapt to any cultural context,
it was to be expected that these infrastructures became at some point,
also considered for the collaborative development and distribution of digital cultural expressions,
using and possibly contributing artistic works to these OS
^[
An idea notably developed in the context of the Debian ecosystem. [See @Candeira2006; @Dekker:2020, p. 5; @Laforet:netart, p. 162].
].
The democratisation of software production and execution in the form of free and open source Unix-like operating systems,
could indeed in theory permit the existence of cooperative forms of publishing for free and open source code poetry,
net art,
generative art and software art,
and also media art, 
which software elements,
free culture supporters have argued [@LaMaVa2010],
could also be released under free and open source licenses and then integrated into distributed infrastructures.
In practice the GNU/Linux distribution Puredyne has distributed works from software artists such as Alex McLean and Martin Howse[@Laforet:netart, p. 162] throughout the mid-noughties.
Similarly,
Debian and FreeBSD have distributed and maintained generative artworks such as Electric Sheep [@Draves2005].
Alongside this,
every now and then it is possible for media artists releasing their work as free and open source software,
to be approached by distribution maintainers to help integrate their piece within free and open source operating systems [See @LaMaVa2010].
It goes without saying that such software must comply with the distribution's guideline,
and its localised understanding of user-friendly applications,
or *usefulness*,
to refer to the FSF free software criteria discussed in Part 2 of this thesis.


However it is at this point that things start to get complicated and the free software template shows some limit.
While there was no trouble for a work like Electric Sheep---that essentially and effectively runs as a screen saver---to be accepted as part of large public repositories from several Linux distros and BSD operating systems,
the same cannot be said,
maybe thankfully,
for other types of digital and media art, 
in particular software art.
If free software contributed to ontological freedom [@Cramer:words, p. 123],
it is not surprising to see that the resulting cultural expansion can no longer be contained by the very structure that gave birth to it.
Said differently,
here free software art not only radically challenges the conservative FSF understanding of software as something *useful* ,
but once distributed within an operating system,
also makes it ambiguous and difficult to separate the OS-as-platform to distribute software art,
from the OS-as-software-art itself.
The adaptability of free and open source operating systems,
and therefore the possibility for such publishing strategies to exist outside of major distributions,
does not help either.
For instance,
Puredyne,
^[
Also known as pure:dyne and which found its root as a heavily modified version of the dyne:bolic or DyneBolic distro,
itself inspired by and originally based on development tools of LoA hacklab's Bolic1 distro.
See [@GOTO10:puredyne; @jaromillobo:dynebolic; @Hadzi:deptford, p. 59].
]
mentioned earlier,
started as a single user operating system containing free software art works from several artists.
However,
Puredyne was also used by its developers to teach workshops and provide a platform to encourage artist to use and write free software.
As a result it became more than a singular collection of software shared within a small community of free software artists,
eventually evolving into an hybrid Debian-Ubuntu distribution,
funded by Arts Council England and used as a media art teaching tool in art organisations,
academies and universities [@BrooksTremblay:divide].
In this educational context with a strict separation between tools and works produced,
software culture became an hostage in a discussion on the pragmatic aspect and usefulness of Puredyne as a whole.


\afterpage{%
\begin{figure}[h]
	\caption{self3[cpu]}
    \includegraphics[width=\textwidth]{self2} 
	\fnote{Screenshot: Martin Howse, 2006}
    \label{fig:self2}
\end{figure}
\clearpage
}


This transformation became conflictual for the Puredyne distro,
now that its new users,
external to the free software art networks from which Puredyne stemmed,
were confronted with a system in which no safeguarding was offered and that was simultaneously a proof of concept free software art distribution system and a fully functional Unix-like OS.
The safeguarding that I am making reference to is dual:
first it assumed that the users would not need to be constantly assissted or prevented from doing foolish things,
such as wiping out all their data;
second,
the artistic computation was not separated or isolated from the rest of the system.
This second point in particular was discussed during the FLOSS+Art panel at the 2007 edition of the Make Art Festival [@GOTO10:makeartFLOSS+Art],
Poitiers, France,
and specifically on the question of what would be the consequence if the Puredyne developers modified the source code of a work (Figure \ref{fig:self2}) from Howse,
so as to provide a version of the software with a more *user friendly* program exit mechanism,
that is to say,
a simple way to quit the application.
This functionality was not present in the original program,
and Puredyne users had complained they did not know what to do once they started the program,
and were forced to reboot their machine to make the software stop.
This posed a particular problem precisely because Howse's work in his collaboration with English performer Jonathan Kemp,
essentially drew inspiration from,
and also used, 
operating system mechanisms,
including the notion of interrupt signal.
The latter mechanism ended up in this case at the conflicting point of being both an artistic material for Howse and Kemp,
and a critical system feature needed for a classic desktop interaction.


Some other works also touch so directly on the underlying mechanics of the operating system,
that they prove very hard to publish in an executable form,
and distributed even in free software art distributions like the first iteration of Puredyne would have permitted.
For instance,
McLean's *ungovernable.patch*,
a 2011 free software licensed modification to the Linux kernel that reverts the standard CPU throttling behaviour,
makes the CPU frequency decrease under load and increase when the machine is idling [@CoxMcLean:speak, p. 57-58],
and would be unlikely to be accepted in any Linux based OS that aimed to be fully functional,
given this very functionality is questioned by this work.
In the end,
even liberated from proprietary and closed systems,
software art remains an aspect of computational culture that resists entirely free cultural infrastructures,
despite an apparent closeness in the way they relate to care of software crafting,
expressibility of programming,
and the sharing of tools.
Even if free software allowed software art to expand itself towards technological layers not accessible in closed source and proprietary systems,
it did not change its nature of being unsustainable by design---therefore useless and problematic computation---making moot the question of normalisation of such practices within large scale techno-legal infrastructures.
As briefly shown with Puredyne,
free software art's viral property does not exist solely at the licensing level,
nor the source code,
but the execution of software,
that can compromise the OS as a whole if not contained or diminished.
Next to that,
the *code brutalism* [@Yuill:brut] of these works clashes with the polished and organised idea of distributed,
cooperative,
and to some extent decentralised approach to software art publishing.
If such systems would be possible,
beyond indexing and classification [Such as @AlexanderGoriunovaMcLeanShulgin:runme],
they would not be able to provide more than tamed software art,
similar to those found in app stores,
and which brutalism becomes emulated or simulated,
as glitchy gimmicks sandboxed in a software white cube,
and isolated from the computational usefulness of the rest of the system.
^[
In this context,
it is interesting to put in parallel projects such as the Satromizer iPhone app and the early performances from group 5VOLTCORE.
While the two relate to glitch aesthetics,
the first is essentially a standalone graphic tool available from the Apple App Store,
whereas the second is about direct and abusive live intervention on computer chips.
See [@Syverson:satromizer; @Andel:5VOLTCORE].
]



The Source of Free Cultural Expressions
---------------------------------------


Another problematic aspect I have yet to discuss is the becoming of source code,
once the latter has been transposed to non-software cultural expressions.
As discussed several times in this thesis,
the importance of source code availability is essential in free and open source software,
and the reason why such availability was fully part of the free software definition and licensing models.
But what about works that are neither code or software based,
which is what non-software free culture is.
How would that work practically?


In Chapter 2,
I provided a general overview of how the free software definition has been slowly transformed,
into a series of definitions that aimed to provide the same freedom and openness for any cultural expressions.
As I demonstrated,
the affiliation of these definitions was both visible in style and content,
and the link with their parent software-centric definitions was also blatant.
In spite of that,
if we take a closer look at the definitions in Chapter 1,
even though the first attempt in porting the software freedom to knowledge---the *four kinds of free knowledge*---took into account the idea of source,
the following proto-free-cultural attempts stopped mentioning it.
The reason for this can be put simply:
while computer software is a cultural expression [See @Fuller2003],
not all cultural expressions are computer software.
Therefore the computer-specific jargon,
which the term source code is,
was eventually lost in translation.


So free cultural definitions are after all not a perfect transposition of free and open source software definitions,
due to the lack of,
or incomplete,
approach to defining what is a source.
This is not specific to one particular approach to free culture,
but all the free,
libre,
and open content,
knowledge,
expression and work ideas that emerged from the proto-free culture era.
If the notion of source code is not easily transposable to non-software free culture,
I will argue that its absence is problematic and needs to be addressed.



\afterpage{%
\begin{figure}[h]
	\caption{A maybe free and highly compressed thumbnail}
    \includegraphics[width=\textwidth]{zkmcat-compressed} 
	\fnote{Image: Anonymous, 2013}
    \label{fig:zkat1}
\end{figure}
\clearpage
}


First of all,
from a simple pragmatic perspective,
the consequence of the absence of source means that it is fine to publish and distribute *any* content.
For instance a low-resolution,
highly compressed,
photo or video can be distributed freely under these licenses (Figure \ref{fig:zkat1}).
But then,
while these files would perfectly qualify as valid work under their respective free and open definitions, 
their value becomes questionable when the high-resolution,
raw,
or less destructively compressed *original* digital file,
can still remain under other copy or licensing rights.
Here the software equivalent of this process would roughly be the so-called shareware,
a freely distributed,
usually closed source,
software distribution mechanism in which the full potential of the software is unlocked only once the user has paid a fee,
which would roughly translate for non-software objects,
in paying licensing rights to acquire such sources,
in the eventuality these would be anyway available under such type of classic licensing.
Here the term licensing can be confusing.
A work can be licensed under a free culture license allowing a usage defined by the terms of the license,
but licensing can also refer to any unilateral permissive process,
and in some case reciprocal contracts,
in which a work can be commercially licensed for a specific use.
For instance a musician can license their music to an advertisement company to be used in a television commercial,
in exchange for a fee.
The two forms of licensing,
classic and free culture,
do not necessarily exclude each others.
For instance the music platform Jamendo,
invites artists to contribute free culture licensed music,
and at the same time provide commercial licensing to businesses and individuals so they can use the music royalty-free.
^[
Jamendo claims that it uses a fair model to redistribute the financial gain to the artists for this commercial licensing,
therefore acting as an automated agent for works that are not commissioned,
but in fact they are essentially crowdsourcing their catalogue for free.
Jamendo also suggests artists to use NC licenses combined with the Jamendo licensing agreement,
to make sure they will be paid for their work,
whereas in fact it is a barely disguised strategy to make sure only Jamendo can exploit commercially their work.See [@Jamendo:license; @Jamendo:FAQ].
]




Another aspect is what the Freedom Defined project calls the "practical modifiability" of a work [@FD:licenses],
which is how in practice a work can be appropriated and modified by someone else.
For instance,
if the licensed work is an image composed of several elements,
its practical modifiability is affected if the author decides to publish such an image exclusively as a flattened down work,
or if instead the author also provides the layers used to make this final image.
To make things more difficult to follow,
there is also an unavoidable recursive mechanism triggered by the existence of such external pseudo source files.
Indeed,
and still using the example of a digital collage,
one can ask what would happen if the layers provided were themselves derived from other originals?
Shouldn't these also be included?
What about the font used for a caption or logo,
what would be the practical modifiability of a rasterised text layer?
Would it make sense to provide the font file?
If someone wants to practically modify the file at a level that is not a mashup or remix using the flatten and merged output---essentially a product of passive consumption---then such elements are in fact very much needed,
and the difficulty of distributing and accessing grows in proportion with the composite depth of the image (Figure \ref{fig:zkat2}).
The same could be said of course of music as free culture licensed mp3, ogg or FLAC digital files,
as opposed to music as free culture licensed score,
separate audio tracks,
OSC and MIDI digital dumps of the parameters for the hardware and software synthesizers,
settings of the sequencing software,
and so forth.
And to make things even more complicated,
if an author is to distribute the source of their work,
this source being a distinct cultural expression itself,
the author is free to distribute the material under separate licenses.
Several questions come to mind.
Is it acceptable then for free content to have its assets under non free culture licenses?
Is it acceptable if these external cultural expressions are freely licensed,
yet using closed standards from proprietary software?
How far can these ideas of free, and open, content or works can be pushed?



\afterpage{%
\begin{figure}[h]
	\caption{How deep is your source?}
    \includegraphics[width=\textwidth]{zkmcat-layer2} 
	\fnote{Image: Anonymous, 2013}
    \label{fig:zkat2}
\end{figure}
\clearpage
}



To address such issues,
Myers,
whose work was introduced in Chapter 4,
offered an idea on what an ideal cultural source could be [See @Myers2007].
He suggested considering five attributes which are:
transparent,
in an easily editable text-based format;
full quality,
in a standard that permits the recreation of the final format;
complete,
so that all the materials needed to produce the distributed work are provided;
unencumbered,
that is free of patents and DRM;
structured,
as in provided in a descriptive format,
such as vector graphics.
This theoretical approach is however not fully translated in practice.
It is worth noting that the FSF,
with the 2000 GNU Free Documentation License (GFDL),
had attempted to tackle this problem already,
and most notably the notion of transparency,
needed for the collaboration on,
and the distribution of free documentation.
^[
The license was notably used by Wikipedia which later in 2009,
demanded the FSF to change the license,
using the infamous "later version" loophole present in most FSF licenses,
to make the content of the online encyclopaedia compatible with the trending CC BY-SA license,
to which it eventually switched without requiring authorisation from the GFDL copyright holders.
For some context see [@FSF:GFDLFAQ; @Slashdot:GFDL].
]
To date,
in the history of proto-free and free culture definitions,
only Freedom Defined tried to address this issue.
According to them,
to truly be a free cultural work,
a work must respect four more conditions,
and one that is specific about the notion of source data:


> Availability of source data:
> 
> Where a final work has been obtained through the compilation or processing of a source file or multiple source files,
> all underlying source data should be available alongside the work itself under the same conditions.
> This can be the score of a musical composition,
> the models used in a 3D scene, the data of a scientific publication,
> the source code of a computer application,
> or any other such information. [@FreedomDefined2007]


There is however an important flaw in this approach:
unlike free software licenses that legally implement the free software definition,
this extra condition of source data availability is not part of any free culture approved license terms.
It is simply part of a guideline to decide whether or not a work could truly be called a free cultural work.
Said differently,
an author does not have to respect this clause when using a free culture license,
because it is not part of the license conditions.
In practice,
it is therefore possible to distribute works that are not truly free with free culture licenses,
literally turning free culture into a messy mix of both free and non-free cultural expressions.
If the different free cultural techno-legal systems were not already confusing or difficult to navigate through,
they are now genuinely Kafkaesque.
Creative Commons even uses the misleading term "approved for free cultural works"
^[
The affiliation is made visible with a graphical badge in the human-readable summaries of their licenses. See [@CC:approvedFC].
],
for its licenses that respect the free culture license definition,
whereas it really should say that such or such licenses are free culture licenses,
no less,
no more.


In practice,
a thorough publication of properly licensed source materials for works of art is rare,
and is usually limited to artists and collectives already close to free and open source software communities,
such as software artists using free software as a framework.
Similar to Vilayphiou and Leray's design practice,
exposed to this particular mode of production and distribution in their daily use of free software tools,
these practitioners eventually applied the same philosophy with their work,
and make many elements of the latter publicly available in repositories,
using different software licenses [See @Lee2008].
This aspect is particularly obvious for artists and designers using free and open source Unix-like operating systems,
and who are therefore exposed to these replicable infrastructures and their modes of distribution which rely on source code.
For instance with Debian,
the connection between source code and freedom is clearly expressed in its own free software guidelines:


> - Source Code
> 
> The program must include source code, and must allow distribution in
> source code as well as compiled form.
> 
> [...]
> 
> - Integrity of The Author's Source Code
> 
> The license may restrict source-code from being distributed in modified
> form underline{only} if the license allows the distribution of ``patch files''
> with the source code for the purpose of modifying the program at build
> time. The license must explicitly permit distribution of software built
> from modified source code. The license may require derived works to
> carry a different name or version number from the original software.
> (This is a compromise. The Debian group encourages all authors to not
> restrict any files, source or binary, from being modified.) [@Perens:contract]


Infused in such a habitus,
these artists adopt them in their own practice,
sometime expressing the moral imperative to share back regardless of the computational usefulness of their work,
simply because the latter would not exist in the first place without the access to such tools.
^[
This explanation comes up fairly often in interviews. See [@flossophia; @CannitoJianBence:liwoli].
]
However,
such an attitude towards the meticulous sharing of source material is unlikely to become popular,
due to the complete or partial disappearance of an articulated concept of source.
In fact,
by cleaning up the computer jargon when software freedom was transposed to culture,
the moral justification of free software which was embedded in this idea of source code availability,
disappeared as well.
In spite of the idea of defined deliberative free culture presented as an ethical counterpart of the aggregative market driven CC licensing that I discussed in Chapter 2,
the ethics of free culture have no means by which to materialise.
As a result,
and in a strange twist,
the imperfect transposition of software freedom to cultural freedom also has a negative impact on free and open source software itself:


> Can I apply a Creative Commons license to software?
> 
> We recommend against using Creative Commons licenses for software.
> [...]
> Unlike software-specific licenses,
> CC licenses do not contain specific terms about the distribution of source code,
> which is often important to ensuring the free reuse and modifiability of software.[@CC:faq2016]


Indeed CC licensed software,
even though as culturally free as free and open source software,
is in fact a pseudo form of free and open source software.
For instance an obfuscated and compressed JavaScript library can easily be distributed with a CC BY-SA license,
or simply a CC BY license,
therefore encouraging the widespread of said library,
yet making it clear that its inner mechanisms are not the concern of anyone but its original authors.
In this case,
free culture in practice seems closer to a gratis sharing consumer culture rather than a liberated and empowered productive apparatus.
It is also significant that in CC's perspective,
as shown in the quote above,
the question of modifiability is only an issue for software.


The problem of source has yet to be solved at the time of writing,
but some efforts to take into account this issue are worth mentioning.
In fact,
as early as 2004,
the Open Art Network started to work on the Open Art license (OAL),
also known as the View Source license,
or simply the Source License [See @OAN2004].
Even though this license would be considered today as non-free because it prohibited commercial use,
it requested that "source file/s for the work must remain accessible to the public".
Unfortunately,
there was no consideration on the nature of the standard used for such source files.
OAL made no difference between free software and proprietary software,
and no difference between open or close file formats and standards.
Another take on the question can be found in the ongoing work from French composer and pianist Valentin Villenave,
on a license that would solve some of the source issues discussed so far,
yet unpublished to this date.
Villenave is an active member of the Copyleft Attitude community from which the free culture FAL was born,
as discussed in Chapter 3.
His idea is to modify the FAL,
in a way that it would require the artist to provide all intermediary source material used during the creation of a work of art.
This would include sketches and research in all versions.
If at any given time a source element is involved,
it must be provided,
so as to avoid a situation,
according to Villenave,
where what is given access to,
is in fact a summary of the work and not the work as a whole [@Villenave2011].
This approach would be,
according to the musician,
a concrete way to resist the passive and commodified consumption of free cultural expressions,
and connect back with the free software engineering freedom,
where re-usability and modularity is necessary for any progress and innovation,
and at the same time preventing free culture from turning into gratis sharing consumer culture or a shareware culture,
to use the analogies I made earlier.
However,
with this extra step,
it seems that our problem is expanding further and further beyond the recursive vertigo triggered by diving into the cultural sources of cultural sources:
it is also reaching the *context* in which these very sources are created.



Sharing Is Caring but How Many Files Are Enough?
------------------------------------------------


The problem with the notion of cultural source is that it is difficult to draw a clear line between a well defined cultural artefact and the context in which the latter has been produced once culture has been reduced to shareable files.
Free culture does not provide a solution,
but instead further stresses this reduction.
What is more,
this situation creates a follow-up in the digital realm to some reflections of twentieth century American philosopher Nelson Goodman,
and more precisely in his 1968 book *Languages of Art*,
in which the distinction is made between *autographic* and *allographic* works of art.
Goodman's interpretation of the art object is of course not developed in the context of artistic cooperation and collaboration,
but it does overlap coincidently with some of the intellectual property issues covered in this thesis as it approaches the concept of authenticity by looking at the difference between originals and copies [@Goodman:languages, III Art and Authenticity].
According to the philosopher's examples,
painting is qualified as *autographic* because a copy of the original work is never authentic,
while music is *allographic*,
because the work of the composer is finished with the writing of a score that can be used for multiple authentic performances;
he also notes that art can be formed of multiple stages,
giving examples with printmaking being both two-stage like and *autographic*,
which helps him clarify that *autographic* art must not necessarily translate into the production of one unique object [@Goodman:languages, pp. 113-115].
These reflections on art and the work of art,
leads Goodman to eventually develop a theory of notation,
where stipulations are made for the creation and use of satisfactory systems of notation [@Goodman:languages, IV The Theory of Notation].
This approach is close to the questions of how to define the source of cultural works and what would be an acceptable medium and protocol to create and distribute these.


However,
the difference with Goodman is that even though free culture seems to employ a rigorous syntactic and semantic system,
its theory of notation---that is built upon software data and licenses---is not fixed;
it evolves constantly.
Consequently,
although it escapes the reductionism employed by Goodman,
it nonetheless fails to capture anything sharply despite a techno-legal apparatus that keeps on expanding.
This is particularly visible for artistic practices that have emerged from this techno-legal changeability,
such as live coding which originally came from the desire to use free software programming as both a performance art medium and approach to improvisation in the context of electronic dance music [@McLean:disco].
This particular practice is exemplary of the appropriation of free and open source in the arts [@Yuill:notation],
but it also shows the limitation of the free culture rational, 
defined,
and quantifiable notation system.
In such a practice,
"the specificity of code is opened towards the indeterminism of improvisation," [@Yuill:notation, p. 69]
however,
its distributivity also make irrelevant the multiple staging analysis of art production within and outside of the scope of its reproduction,
and in turn makes it impossible to determine which of all of its original sources is the most valuable.


Defining an artistic source is as problematic as defining the language of art,
yet the access to increasingly sophisticated legal and technological tools,
which can enforce a fine-grained versioned capture of the artistic creation,
directly fuels an endless quest to capture the "participation mystique" of the poet [In reference to @Jung1933, Psychology and Literature].
What happens is that by being unable to extract universal usefulness from cultural production---as opposed to the slighty more defined usefulness of free software or free art discussed in Chapter 3---a technologically assisted brute force approach to capture everything is set into motion.
The argument from Cramer that without a dump of an artist's storage device no complete works or biography can be written [See @Cramer2003],
shows how the quantification and capture of data footprints has both revitalised discussions on intermediality,
but also demonstrates the infiltration of information technology into the art discourse beyond practical questions of conservation,
archiving,
and documentation.
This strategy of sharing as *dumping* whatever has been digitally captured,
was exemplified early on with the *Praystation Hardrive* [sic] published in 2001.
The later was a CD-ROM containing *raw data* from the hard drive of media artist and Macromedia Flash specialist Joshua Davis [@Davis:praystation].
The shared data was meant to be explored,
studied,
and reused.
Even though the content was far from being a raw bitstream copy of the designer's drive,
it was nonetheless quite an impressive collection of 3637 files of all sorts and spread in a maze of folders.
Some scholars made a parallel between this project and the free and open source ethos [@CuSta2009; @Kirschenbaum2008, p. 54],
but this is a misunderstanding of how free software and open source operate,
because the files were released without any licenses or copyright notices
^[
Email from Joshua Davis to the author, June 8, 2012.
].
Effectively the drive fell instead into the gooey grey swamp that is unspecified public domain and default copyright laws.
Still,
its tremendous positive impact on the Flash user community,
both as an educational and inspiring cultural artefact,
demonstrated the effectiveness of a brute force approach to sharing.
A strategy whose motto could be: if in doubt, share it all.


But this makes me wonder about the process of production.
If the hypothetical aim here is to provide the source code of an artwork,
why not try to capture the creative process as well [See @Dekker2010]?
This situation would share some resemblance with the first attempts of commercial art galleries in the early seventies,
to claim back conceptual art in a commodified form by encouraging the collection of by-products,
artefacts,
and documents,
that could generate commercial interest accentuated by the novelty practices these objects came from [@Taylor:avant, p.34].
It also brings back the possible analogy between artistic use of free cultural licensing with prior attempts to use the contract as a means of institutional critique like *The Artist's Contract*  by Siegelaub,
as briefly discussed in Chapter 3.
However, 
here the emphasis is no longer on aesthetics,
but rather whether or not these practices reinforce or instead liberate the autonomy of the artist,
and how these new methods of documenting,
archiving,
and publishing transform the language of art. 
These issues are important ones to take into account,
in order to evaluate the becoming of the artistic practice in free culture where---as I mentioned earlier---liberation and empowerment also creates a consumer culture of sharing; 
in fact a multi-layered sharing economy.
So without noticing it,
the frustration coming from the lack of definition of artistic sources,
combined with the increasing digital capture of human activity,
is an open door towards a commodified analysis and recording of the artistic practice itself,
where Snelting's awkward gestures [@Snelting:gesture] of free software craftsmanship I discuss in Chapter 4,
could end being misinterpreted as movements waiting to be sampled with all sorts of sensors and captors.
With increasing means by which to sample phenomenons into data sets,
if there is more to these sources than just a flattened object,
nothing prevents the capturing of such *intermediality* by also providing electroencephalographic data,
DNA samples,
cosmological models and more,
thus transforming the capture of pretty much any phenomenon into the source of art as noumenon,
and reduce culture to an ever expanding digital Voyager Golden Record,
constantly challenging Lyotard's hypothesis that knowledge cannot be translated in its entirety by machines [@Lyotard:rapport, pp. 5-7].
If anything at all,
I might well suggest a new free cultural license,
the Borges Public License,
for tomorrow’s librarians of Babel [In reference to @Borges:babel, The Library of Babel (1941)],
and their lawyers.


By only focusing on the techno-legal infrastructure that permits the distribution and the processing of data,
information,
and content,
the value of what is being distributed and processed is however constantly re-contextualised.
Its *raison d’être* becomes more ambivalent.
As I said,
the difficulty of qualifying a universal usefulness to what is shared---essentially the failure to define a universal approach to the digital commons---means that old paradigms such as quantity versus quality have became superseded by *potentiality versus accessibility*.
The nineties debate on the societal benefit of digitally distributed knowledge [@levy:ci; @levy:world],
has thus been transformed since the mid noughties into discussions on culture as a digital commons,
where the latter is assessed on the function of possible opportunistic transformation and instantaneous availability.
Free culture is not responsible for this but is symptomatic of this trend,
and its implementation of a sharing economy does not create an alternative to this situation.
It is yet another variation of an information society built on top of techno-legal pipes,
in which data flows from one processing unit to another,
so as to shape and develop an infinite Lego construction site.
Here I make the analogy with Lego again---after  introducing its connection with engineering culture and free and open source software in Chapter 1---because if the playfulness of the Lego methodology for cultural production is not so far from the *metamechanics* of Swiss sculptor and painter Jean Tinguely,
it also shows that there is a limit to the translation of engineering culture to artistic methodologies.
The result is the risk of building an infrastructure optimised for non-existing practices,
based on shortcuts that simplify cultural production to an equivalent of industrial production,
in which engineering processes and re-usability are essential for innovation.


I refer to non-existing practices because the discussion on sources and context shows that art and design practices do not always rely on existing free cultural works,
and therefore have little use for what the free culture machinery excels at:
the bureaucratic organisation of many digital files.
In fact,
even within dedicated free culture supporters,
the very access to usable sources,
let alone *even* finished works,
from their peers is only anecdotally relevant.
For instance,
according to Vilayphiou and Leray,
but also other graphic designers working with free software and distributing their work under free culture licenses,
such as Ana Isabel Carvalho and Ricardo Lafuente from the Porto based design studio Manufactura Independente
^[
This comment was made to me during an interview with the two graphic designers,
during the 2013 Libre Graphics Meeting (LGM) in Barcelona.
],
not all the material found in free cultural licensed graphic design is useful for other designers.
In particular,
for Carvalho and Lafuente there is a constantly moving frontier made between some low-level components deemed somehow neutral that can be useful,
such as a software tool or a font,
and on the other side an authorship tainted higher level artistic object,
like a finalised poster design or illustration that is judged too contextually specific to be useful.
^[
This is especially visible when comparing a general vector graphics database such as the OpenClipArt library,
and the much more personal vektorDB database from design group LAFKON.
See [@LAFKON:vektorDB].
]
Here again we're confronted with the problem of staging what Goodman faced when working on the question of authenticity,
but then if free culture demonstrates anything,
it is that there cannot be one finite number of stages during the making of art,
and that the art object itself can also move across all these stages depending on the context of its making,
distribution,
performance,
appreciation ... and usefulness for others.


By trying to turn cultural fuzziness into a quasi-industrial and modular composite machine,
free culture falls into the trap emerging from its own attempt to demystify cultural production,
but it also fails to be representative of the cultural workers who produce such free culture.
In particular the question of re-usability shows that appropriation art and remix practices are a very good demonstration of the advantage of free cultural processes over more conservative IP mechanisms,
but it is also an inflated tale that helps argument more easily the question of economic accessibility and potentiality of digital culture. 
To be sure,
I do not mean that there are no such things as remix or appropriation within free culture, 
but that outside specific practices,
such as artistic strategies of citation or appropriation,
or playful collaboration within close collectives and networks,
as discussed in Chapter 2,
or as witnessed in small-scale free software art collaborations [@Dekker:phd, 5. The Value of Openness],
they remain singular and localised processes.
As for the source of a work,
Leray explained to me during our discussion that from the perspective of OSP,
there was possibly more value in sharing the documentation of moments of creation and explaining why these moments matter---what the collective calls recipes---rather than just dumping collections of source files and digital assets under free culture licenses.
In the case of free culture supporters like OSP,
it means that the brute force *if in doubt share it all* dump approach is reaching a new level,
by not just preemptively providing access to the things they are unable to attribute a universal usefulness,
but by also making the considerable effort to provide guidance within the dump and explain why some are useful to them.
With this strategy,
the rationalisation of sharing into a free cultural peer-to-peer file exchange,
becomes once again the basis of a human-to-human relation. 

\newpage
