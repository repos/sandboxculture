\begin{titlepage}
	\begin{center}
	
		\secfont
		\huge
		Sandbox Culture\\
		\large
		A Study of the Application of Free and Open Source Software Licensing Ideas to Art and Cultural Production
		\vspace{1.5cm}

		\normalfont
		\large
		Aymeric Mansoux\\
		\normalsize
		Supervisor: Matthew Fuller

		\vspace{1.5cm}

		\normalsize
        Thesis presented for the degree of\\
        Doctor of Philosophy\\

		\vfill
        
        \normalsize
		Centre for Cultural Studies,\\
		Goldsmiths,\\
		University of London,\\
		February 2017

	\end{center}
\end{titlepage}
