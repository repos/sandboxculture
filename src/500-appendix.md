# Appendix: Selection of Proto-Free Culture Licenses {.unnumbered}

\newpage

This selection is ordered by year,
then alphabetically for each year,
as the exact date of publishing of these documents is not always provided.
It is purposefully limited to licenses that aim to expand the scope of free software to other domains,
and excludes licenses only concerned with software publishing.
The selection stops in 2002,
year of the publishing of the first Creative Commons licenses,
which marks the start of a new era in the history of free culture licenses.
This does not mean new proto-free culture licenses were not created after the introduction of Creative Commons licensing,
but this happened less frequently.

This collection of licenses is by no means exhaustive,
but should give an idea of the broad diversity of contexts and intentions present in the proto-free culture era,
as well as indicate how the texts respond to each other,
borrow and share terms,
introduce changes in meaning,
sometimes with subtlety,
sometimes less so.


\stoptocwriting

\setmonofont[Scale=0.8,StylisticSet=3]{Inconsolatazi4-Regular.otf}
\expandafter\def\expandafter\verbatim\expandafter{\verbatim\small\setstretch{0.4}}

\newpage

OpenContent License (1998) {.unnumbered}
----------------------------------------

```
OpenContent License (OPL)

Version 1.0, July 14, 1998.

This document outlines the principles underlying the OpenContent (OC) movement and
may be redistributed provided it remains unaltered. For legal purposes, this document
is the license under which OpenContent is made available for use.

The original version of this document may be found at
http://www.opencontent.org/opl.shtml

LICENSE

Terms and Conditions for Copying, Distributing, and Modifying

Items other than copying, distributing, and modifying the Content with which this
license was distributed (such as using, etc.) are outside the scope of this license.

1. You may copy and distribute exact replicas of the OpenContent (OC) as you receive
it, in any medium, provided that you conspicuously and appropriately publish on each
copy an appropriate copyright notice and disclaimer of warranty; keep intact all the
notices that refer to this License and to the absence of any warranty; and give any
other recipients of the OC a copy of this License along with the OC. You may at your
option charge a fee for the media and/or handling involved in creating a unique copy
of the OC for use offline, you may at your option offer instructional support for
the OC in exchange for a fee, or you may at your option offer warranty in exchange
for a fee. You may not charge a fee for the OC itself. You may not charge a fee for
the sole service of providing access to and/or use of the OC via a network (e.g. the
Internet), whether it be via the world wide web, FTP, or any other method.

2. You may modify your copy or copies of the OpenContent or any portion of it, thus
forming works based on the Content, and distribute such modifications or work under
the terms of Section 1 above, provided that you also meet all of these conditions:

a) You must cause the modified content to carry prominent notices stating that you
changed it, the exact nature and content of the changes, and the date of any change.

b) You must cause any work that you distribute or publish, that in whole or in part
contains or is derived from the OC or any part thereof, to be licensed as a whole
at no charge to all third parties under the terms of this License, unless otherwise
permitted under applicable Fair Use law.

These requirements apply to the modified work as a whole. If identifiable sections
of that work are not derived from the OC, and can be reasonably considered
independent and separate works in themselves, then this License, and its terms, do
not apply to those sections when you distribute them as separate works. But when you
distribute the same sections as part of a whole which is a work based on the OC, the
distribution of the whole must be on the terms of this License, whose permissions
for other licensees extend to the entire whole, and thus to each and every part
regardless of who wrote it. Exceptions are made to this requirement to release
modified works free of charge under this license only in compliance with Fair Use law
where applicable.

3. You are not required to accept this License, since you have not signed it.
However, nothing else grants you permission to copy, distribute or modify
the OC. These actions are prohibited by law if you do not accept this License.
Therefore, by distributing or translating the OC, or by deriving works herefrom, you
indicate your acceptance of this License to do so, and all its terms and conditions
for copying, distributing or translating the OC.

NO WARRANTY

4. BECAUSE THE OPENCONTENT (OC) IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY FOR
THE OC, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN
WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE OC "AS IS" WITHOUT
WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE
ENTIRE RISK OF USE OF THE OC IS WITH YOU.  SHOULD THE OC PROVE FAULTY, INACCURATE,
OR OTHERWISE UNACCEPTABLE YOU ASSUME THE COST OF ALL NECESSARY REPAIR OR CORRECTION.

5. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY
COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MIRROR AND/OR REDISTRIBUTE THE OC
AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL,
INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE
OC, EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES.

```









\newpage

License Publique Audiovisuelle (1998) {.unnumbered}
---------------------------------------------------

```
License Publique Audiovisuelle

Projet Version 0.2 du vendredi 27 novembre 1998

Introduction

Ce document décrit les droits de diffusion pour des programmes audiovisuels.  Il est
inspiré de la General Public License de la Free Software Foundation applicable sur
les logiciels libres.


Copyright © Association Vidéon
BP 221 F-91133 Ris Orangis Cedex FRANCE

La copie et la distribution de copies exactes de ce document sont autorisées, mais
aucune modification n'est permise.  Préambule

Les licences de droits de diffusion de la plupart des programmes audiovisuels
sont définies pour limiter ou supprimer toute liberté à l'utilisateur ou au
diffuseur. À l'inverse, la Licence Publique Audiovisuelle (Audiovisual Public
License) est destinée à vous garantir la liberté de partager et de diffuser
les programmes audiovisuels, et de s'assurer que ces programmes sont effectivement
accessibles à tout utilisateur.

Cette Licence Publique Audiovisuelle s'applique aux contenus de la Banque de
Programmes libres de droits en ligne mise en place par l'association Vidéon, comme
à tout autre programme dont l'auteur l'aura décidé. Vous pouvez aussi appliquer
les termes de cette Licence à vos propres programmes, si vous le désirez.

Liberté des programmes audiovisuels ne signifie pas que vous pouvez faire ce que
vous voulez des programmes. Notre Licence est conçue pour vous assurer la liberté
de diffuser des copies des programmes dans leur intégralité sans nécessiter de
passer un accord préalable supplémentaire avec le propriétaire des droits.

Afin de garantir ces droits, nous avons dû introduire des restrictions interdisant
à quiconque de vous les refuser ou de vous demander d'y renoncer.  Ces restrictions
vous imposent en retour certaines obligations si vous distribuez ou diffusez des
copies de programmes protégés par la Licence. En d'autre termes, il vous incombera
en ce cas de :

    transmettre aux destinataires tous les droits que vous possédez, leur remettre
    cette Licence afin qu'ils prennent connaissance de leurs droits.

Nous protégeons vos droits de deux façons : d'abord par le copyright du Programme
audiovisuel, ensuite par la remise de cette Licence qui vous autorise légalement à
copier, distribuer, diffuser et/ou traduire le programme audiovisuel.

En outre, pour protéger chaque auteur ainsi que Vidéon, nous affirmons
solennellement que le programme concerné ne fait l'objet d'aucune garantie. Si un
tiers l'inclus dans un autre programme (par exemple une émission) et/ou le traduit
puis le redistribue ou le rediffuse, tous ceux qui le recevront doivent savoir qu'il
s'agit d'un programme inclus et/ou traduit afin que sa distribution ou sa diffusion
n'entache pas la réputation de l'auteur du programme.

Enfin, tout programme libre est sans cesse menacé par la protection des droits
de reproduction et de diffusion. Nous souhaitons à tout prix éviter que des
distributeurs ou diffuseurs puissent protéger les droits de diffusion sur des
programmes libres pour leur propre compte. Pour éviter cela, nous stipulons bien que
toute protection éventuelle d'un programme doit accorder expressément à tout un
chacun le libre usage du produit.

Les dispositions précises et les conditions de copie, de distribution, de diffusion
et de traduction de nos programmes sont les suivantes :

Stipulations et conditions relatives à la copie, la distribution, la diffusion et
la traduction

Article 0.

La présente Licence s'applique à tout Programme Audiovisuel (incluant en
particulier les films, musiques, etc.) où figure une note, placée par le détenteur
des droits, stipulant que le dit Programme peut être distribué ou diffusé
selon les termes de la présente Licence. Le terme Programme désigne aussi bien le
Programme lui-même que tout travail qui en est dérivé selon la loi, c'est-à-dire
tout Programme ou une partie de celui-ci, à l'identique ou bien modifié, et/ou
traduit dans une autre langue. Chaque personne concernée par la Licence Publique
Audiovisuelle sera désignée par le terme Vous.

Les activités autres que copie, distribution, diffusion ou traduction ne sont pas
couvertes par la présente Licence et sortent de son cadre. En particulier, les
droits moraux sont régis par les législations nationales.

Article 1.

Vous pouvez copier, distribuer ou diffuser des copies conformes ou traduites dans
leur intégralité du Programme Audiovisuel, tel que Vous l'avez reçu, sur n'importe
quel support, à condition de placer sur chaque copie ou d'indiquer à la diffusion
un copyright approprié et une restriction de garantie, de ne pas modifier ou omettre
toutes les stipulations se référant à la présente Licence et à la limitation de
garantie, de fournir avec toute copie du Programme un exemplaire de la Licence ou de
faire référence lors d'une diffusion à l'endroit où l'auditeur ou le spectateur
peut en trouver une copie.

Vous ne pouvez demander une éventuelle rétribution financière que pour la
réalisation de la copie ou de la diffusion.

Article 2.

Vous pouvez inclure votre copie du Programme dans un autre programme ou le traduire,
et copier, distribuer ou diffuser ces modifications selon les termes de l'article 1,
à condition de Vous conformer également aux conditions suivantes :

    Ajouter aux programmes traduits l'indication très claire qu'il s'agit
d'une traduction, ainsi que la date de la traduction et la mention du programme
d'origine.

    Ajouter à Vos programmes qui incorporeraient un programme protégé par la
présente Licence l'indication très claire qu'il s'agit d'un programme inclus
protégé par la Licence Publique Audiovisuelle.

    Copier, traduire, distribuer ou diffuser le Programme dans son
intégralité en y incluant les génériques et les mentions de mécénat, sauf dans
le cas du droit d'illustration permis dans le pays de distribution ou de diffusion.

    Faire en sorte, si ce n'est pas déjà le cas, que le programme comporte le
copyright approprié en indiquant clairement la limitation de garantie, qu'il stipule
que toute personne peut librement redistribuer ou rediffuser le Programme selon les
conditions de la Licence Publique Audiovisuelle Vidéon.

Si des éléments inclus dans le programme disposent d'ayant droits différents du
titulaire des droits du programme (par exemple des musiques), il doit être fait
mention des droits appliqués à chacun de ces éléments dans le générique ou
lorsque cela n'est pas possible, sur le support de distribution (jaquette, page Web
de téléchargement …). Cela est vrai quelque soit la licence d'utilisation de ces
éléments, qu'il s'agisse de la Licence Publique Audiovisuelle, d'une autre cession
des droits ou de droits commerciaux classiques. Dans ce dernier cas, la distribution
ou la diffusion du programme peut entraîner la nécessité de payer des droits de
diffusion de ces éléments.

Article 3.

Vous ne pouvez pas copier, traduire, céder, déposer, distribuer ou
diffuser le Programme d'une autre manière que l'autorise la Licence Publique
Audiovisuelle. Toute tentative de ce type annule immédiatement vos droits
d'utilisation du Programme sous cette Licence. Toutefois, les tiers ayant reçu
de Vous des copies du Programme ou le droit d'utiliser ces copies continueront à
bénéficier de leur droit d'utilisation tant qu'ils respecteront pleinement les
conditions de la Licence.

Article 4.

Ne l'ayant pas signée, Vous n'êtes pas obligé d'accepter cette Licence.
Cependant, rien d'autre ne Vous autorise à traduire, distribuer ou diffuser le
Programme : la loi l'interdit tant que Vous n'acceptez pas les termes de cette
Licence. En conséquence, en traduisant, distribuant ou diffusant le Programme, Vous
acceptez implicitement tous les termes et conditions de cette Licence.

Article 5.

La distribution ou diffusion d'un Programme suppose l'indication d'une licence
autorisant la copie, la traduction, la distribution ou la diffusion du Programme,
aux termes et conditions de la Licence. Vous n'avez pas le droit d'imposer de
restrictions supplémentaires aux droits transmis au destinataire.  Vous n'êtes pas
responsable du respect de la Licence par un tiers.

Article 6.

Si, à la suite d'une décision de Justice, d'une plainte en contrefaçon ou pour
toute autre raison (liée ou non à la contrefaçon), des conditions Vous sont
imposées (que ce soit par ordonnance, accord amiable ou autre) qui se révèlent
incompatibles avec les termes de la présente Licence, Vous n'êtes pas pour autant
dégagé des obligations liées à celle-ci : si Vous ne pouvez concilier vos
obligations légales ou autres avec les conditions de cette Licence, Vous ne devez
pas distribuer ou diffuser le Programme.

Si une partie quelconque de cet article est invalidée ou inapplicable pour quelque
raison que ce soit, le reste de l'article continue de s'appliquer et l'intégralité
de l'article s'appliquera en toute autre circonstance.

Le présent article n'a pas pour but de Vous pousser à enfreindre des droits
ou des dispositions légales ni en contester la validité ; son seul objectif
est de protéger l'intégrité du système de distribution du Logiciel Libre.  De
nombreuses personnes ont généreusement contribué à la large gamme de Programmes
Audiovisuels distribuée de cette façon en toute confiance ; il appartient à chaque
auteur/donateur de décider de diffuser ses Programmes selon les critères de son
choix.

Article 7.

Si la distribution et/ou l'utilisation du Programme est limitée dans certains pays
par la législation, le détenteur original des droits qui place le Programme sous la
Licence Publique Audiovisuelle peut ajouter explicitement une clause de limitation
géographique excluant ces pays. Dans ce cas, cette clause devient une partie
intégrante de la Licence.

Article 8.

L'association Vidéon se réserve le droit de publier périodiquement des mises à
jour ou de nouvelles versions de la Licence. Rédigées dans le même esprit que
la présente version, elles seront cependant susceptibles d'en modifier certains
détails à mesure que de nouveaux problèmes se font jour.

Chaque version possède un numéro distinct. Si le Programme précise un numéro
de version de cette Licence et " toute version ultérieure ", Vous avez le choix
de suivre les termes et conditions de cette version ou de toute autre version plus
récente publiée par l'association Vidéon. Si le Programme ne spécifie aucun
numéro de version, Vous pouvez alors choisir l'une quelconque des versions publiées
par la Free Software Foundation.

Limitation de garantie

Article 9.

Parce que l'utilisation de ce Programme est libre et gratuite, aucune garantie n'est
fournie, comme le permet la loi. Sauf mention écrite, les détenteurs du copyright
et/ou les tiers fournissent le Programme en l'état, sans aucune sorte de garantie
explicite ou implicite, y compris les garanties de commercialisation ou d'adaptation
dans un but particulier. En tant que distributeur ou diffuseur Vous assumez tous les
risques quant à la qualité et aux effets du Programme. Si l'utilisation que vous
faites du Programme est illégale par rapport à la législation du ou des pays de
distribution ou de diffusion, Vous assumez les coûts et les dépends consécutifs à
votre utilisation.

Article 10.

Sauf lorsqu'explicitement prévu par la Loi ou accepté par écrit, ni le détenteur
des droits, ni quiconque autorisé à copier, traduire redistribuer et/ou diffuser
le Programme comme il est permis ci-dessus ne pourra être tenu pour responsable de
tout dommage direct, indirect, secondaire ou accessoire découlant de l'utilisation
du Programme ou de l'impossibilité d'utiliser celui-ci.

Fin des termes et conditions

---

Comment appliquer ces directives à vos nouveaux programmes audiovisuels

Si vous réalisez un nouveau programme audiovisuel et désirez en faire bénéficier
tout un chacun, la meilleure méthode est d'en faire un Programme Libre que tout
le monde pourra redistribuer et diffuser selon les termes de la Licence Publique
Audiovisuelle.

Pour cela, insérez les indications suivantes dans votre programme (il est
préférable et plus sûr de les faire figurer dans le générique du programme ou à
défaut sur le support de diffusion du programme : jaquette de disque ou de vidéo,
page Web de téléchargement …) :

    ((une ligne pour donner le nom du programme))

    Copyright (C) 19xx ((nom du détenteur des droits))

    Ce programme est libre. Vous pouvez le traduire, le redistribuer et/ou le
diffuser selon les termes de la Licence Publique Audiovisuelle publiée par
l'association Vidéon (version 1 ou bien toute autre version ultérieure choisie par
vous).

    Ce programme est distribué SANS AUCUNE GARANTIE, ni explicite ni
implicite. Reportez-vous à la Licence Publique Audiovisuelle pour plus de détails.

    Vous pouvez trouver une copie de la Licence Publique Audiovisuelle sur le
Web à l'adresse suivante : http://www.videontv.org/. Si ce n'est pas le cas,
écrivez à l'association Vidéon, BP 221 F-91133 Ris Orangis France.

    ((Ajoutez également votre adresse électronique, le cas échéant ainsi
que votre adresse postale))

    ((indication éventuelle des éléments inclus dans le programme dont les
droits sont détenus par une autre personne ou organisation))

Si vous officiez en tant que réalisateur ou auteur, n'omettez pas de demander à
votre employeur, votre établissement scolaire ou autres de signer une décharge
stipulant leur renoncement aux droits qu'ils pourraient avoir sur le programme :

    ...((employeur, école...)) déclare par la présente ne pas revendiquer de
droits sur le programme " (nom du programme) " réalisé par ...((nom de l'auteur)).

    ((signature du responsable)), ...((date)), ...((nom et qualité du
responsable)).
```








\newpage

License Association des Bibliophiles Universels (1999) {.unnumbered}
------------------------------------------------------

```
License ABU
-=-=-=-=-=-
Version 1.1, Aout 1999

Copyright (C) 1999 Association de Bibliophiles Universels
   http://abu.cnam.fr/
   abu@cnam.fr

La base de textes de l'Association des Bibliophiles Universels (ABU) est une oeuvre
de compilation, elle peut être copiée, diffusée et modifiée dans les conditions
suivantes :

1.  Toute copie à des fins privées, à des fins d'illustration de l'enseignement
    ou de recherche scientifique est autorisée.

2.  Toute diffusion ou inclusion dans une autre oeuvre doit

     a) soit inclure la presente licence s'appliquant a l'ensemble de la
        diffusion ou de l'oeuvre dérivee.

     b) soit permettre aux bénéficiaires de cette diffusion ou de cette
        oeuvre dérivée d'en extraire facilement et gratuitement une version
        numérisée de chaque texte inclu, muni de la présente licence.  Cette
        possibilité doit être mentionnée explicitement et de façon claire, ainsi
        que le fait que la présente notice s'applique aux documents extraits.

     c) permettre aux bénéficiaires de cette diffusion ou de cette
        oeuvre dérivée d'en extraire facilement et gratuitement la version
        numérisée originale, munie le cas échéant des améliorations visées
        au paragraphe 6, si elles sont présentent dans la diffusion ou la nouvelle
        oeuvre. Cette possibilité doit être mentionnée explicitement et de façon
        claire, ainsi que le fait que la présente notice s'applique aux documents
        extraits.

   Dans tous les autres cas, la présente licence sera réputée s'appliquer à
   l'ensemble de la diffusion ou de l'oeuvre dérivée.


3. L'en-tête qui accompagne chaque fichier doit être intégralement
   conservée au sein de la copie.

4. La mention du producteur original doit être conservée, ainsi
   que celle des contributeurs ultérieurs.

5. Toute modification ultérieure, par correction d'erreurs,
   additions de variantes, mise en forme dans un autre format, ou autre, doit être
   indiquée.  L'indication des diverses contributions devra être aussi précise que
   possible, et datée.

6. Ce copyright s'applique obligatoirement à toute amélioration
   par simple correction d'erreurs ou d'oublis mineurs (orthographe, phrase
   manquante, ...), c'est-à-dire ne correspondant pas à l'adjonction d'une autre
   variante connue du texte, qui devra donc comporter la présente notice.
```









\newpage

Comprehensive Open Licence (1999) {.unnumbered}
---------------------------------

```
The Comprehensive Open Licence Draft (COLD)

Draft v0.01, 11/11/1999

I. REQUIREMENTS ON BOTH UNMODIFIED AND MODIFIED VERSIONS

This License applies to any article, documentation or other work which contains a
notice placed by the copyright holder saying it may be distributed under the terms of
this License.

The Licenced works may be reproduced verbatim and distributed in whole or in part,
in any medium physical or electronic, provided that the terms of this license are
adhered to, that an appropriate copyright notice is conspicuously published on each
copy identifying the original author(s) and that this license or an incorporation
of it by reference is displayed in the reproduction. "Due credit" shall include a
reference to the source of the original document.

You may not copy, modify, sublicense, or distribute the Licenced work except as
expressly provided under this License. Any attempt otherwise to copy, modify,
sublicense or distribute the Program is void.

You are not required to accept this License, since you have not signed it. However,
nothing else grants you permission to modify or distribute the Licenced work or
its derivative works. These actions are prohibited by law if you do not accept this
License. Therefore, by modifying or distributing the Licenced work (or any work based
thereon), you indicate your acceptance of this License to do so, and all its terms
and conditions for copying, distributing or modifying the Licenced work or works
based on it.

Each time you redistribute the Licenced work (or any work based on it), the recipient
automatically receives a license from the original licensor to copy, distribute
or modify the Licenced work subject to these terms and conditions. You may not
impose any further restrictions on the recipients' exercise of the rights granted
herein. You are not responsible for enforcing compliance by third parties to this
License.

If you wish to incorporate parts of the Licenced work into another document whose
distribution conditions are different (eg a magazine article), write to the author to
ask for permission.

Writers of derivative works will have the right to commercial exclusivity on their
article for a specified period (6 weeks), after which time such derivative work will
revert to the terms of this Licence, that it too may be freely distributed.

Any publication in standard (paper) book form shall require the citation of the
original publisher and author.

Proper form for an incorporation by reference is as follows:

Copyright © <year> <author's name or designee>.  This material may be distributed
only subject to the terms and conditions set forth in the Comprehensive Open License
(Draft), v0.01 or later.

Revised and/or new versions of the License may from time to time be published. Such
new versions will be similar in spirit to the present version, but may differ in
detail to address new problems or concerns.

Each version is given a distinguishing version number. If the Licenced work specifies
a version number of this License which applies to it and "any later version", you
have the option of following the terms and conditions either of that version or of
any later version published by the Licensor. If the Licenced work does not specify
a version number of this License, you may choose any version ever published by the
Licensor.

II. COPYRIGHT

The copyright to each Licenced publication is owned by its author(s) or designee.

III. SCOPE OF LICENSE

The following license terms apply to all such Licenced works.

Mere aggregation of Licenced works or a portions of such work with other works or
programs on the same media shall not cause this license to apply to those other
works.

The aggregate work shall contain a notice specifying the inclusion of the material
and appropriate copyright notice.

SEVERABILITY. If any part of this License is found to be unenforceable in any
jurisdiction, the remaining portions of the License remain in force.

NO WARRANTY. Any Licenced works are licensed and provided "as is" without warranty
of any kind, express or implied, including, but not limited to, the implied
warranties of merchantability and fitness for a particular purpose or a warranty of
non-infringement.

IV. TERMS AND CONDITIONS FOR MODIFICATION

All modified versions of documents covered by this License, including translations,
anthologies, compilations and partial documents, must meet the following
requirements:

    1. Any modified version of a Licenced document must be labeled as such.

    2. The person making the modifications must be identified and the modifications
    dated.

    3. All substantive modifications (including deletions) must be clearly marked up
    in the document.

    4. Acknowledgement of the original author and publisher if applicable must be
    retained according to normal academic citation practices.

    5. The location of the original unmodified document must be identified.

    6. The original author's or authors' name(s) may not be used to assert or imply
    endorsement of the resulting document without the original author's or authors'
    permission.
```








\newpage

Counter Copyright notice (1999) {.unnumbered}
-------------------------------

```
A B O U T   C O U N T E R - C O P Y R I G H T S   [CC]

As an alternative to the exclusivity of copyright, the counter-copyright invites
others to use and build upon a creative work. By encouraging the widespread
dissemination of such works, the counter-copyright campaign fosters a rich public
domain.

The idea surrounding the counter-copyright campaign is fairly easy to understand. If
you place the [cc] icon at the end of your work, you signal to others that you are
allowing them to use, modify, edit, adapt and redistribute the work that you created.

The counter-copyright is not a replacement for an actual copyright, rather
it is a signal that you as the creator are willing to share your work. The
counter-copyright strips away the exclusivity that a copyright provides and allows
others to use your work as a source or a foundation for their own creative ideas. The
counter-copyright initiative is analogous to the idea of open source in the software
context. For a more thorough explanation of open source see the following site:
http://www.opensource.org/.

Show your support for the public domain by marking your work with a [cc] and a link
to the Copyright's Commons web site.
```






\newpage

Design Science License (1999) {.unnumbered}
-----------------------------

```
DESIGN SCIENCE LICENSE

TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

Copyright © 1999-2000 Michael Stutz <stutz@dsl.org>
Verbatim copying of this document is permitted, in any medium.

0. PREAMBLE.

Copyright law gives certain exclusive rights to the author of a work, including the
rights to copy, modify and distribute the work (the "reproductive," "adaptative," and
"distribution" rights).

The idea of "copyleft" is to willfully revoke the exclusivity of those rights under
certain terms and conditions, so that anyone can copy and distribute the work or
properly attributed derivative works, while all copies remain under the same terms
and conditions as the original.

The intent of this license is to be a general "copyleft" that can be applied to any
kind of work that has protection under copyright. This license states those certain
conditions under which a work published under its terms may be copied, distributed,
and modified.

Whereas "design science" is a strategy for the development of artifacts as a way to
reform the environment (not people) and subsequently improve the universal standard
of living, this Design Science License was written and deployed as a strategy for
promoting the progress of science and art through reform of the environment.

1. DEFINITIONS.

"License" shall mean this Design Science License. The License applies to any work
which contains a notice placed by the work's copyright holder stating that it is
published under the terms of this Design Science License.

"Work" shall mean such an aforementioned work. The License also applies to the output
of the Work, only if said output constitutes a "derivative work" of the licensed Work
as defined by copyright law.

"Object Form" shall mean an executable or performable form of the Work, being an
embodiment of the Work in some tangible medium.

"Source Data" shall mean the origin of the Object Form, being the entire,
machine-readable, preferred form of the Work for copying and for human modification
(usually the language, encoding or format in which composed or recorded by
the Author); plus any accompanying files, scripts or other data necessary for
installation, configuration or compilation of the Work.

(Examples of "Source Data" include, but are not limited to, the following: if the
Work is an image file composed and edited in 'PNG' format, then the original PNG
source file is the Source Data; if the Work is an MPEG 1.0 layer 3 digital audio
recording made from a 'WAV' format audio file recording of an analog source, then
the original WAV file is the Source Data; if the Work was composed as an unformatted
plaintext file, then that file is the the Source Data; if the Work was composed
in LaTeX, the LaTeX file(s) and any image files and/or custom macros necessary for
compilation constitute the Source Data.)

"Author" shall mean the copyright holder(s) of the Work.

The individual licensees are referred to as "you."

2. RIGHTS AND COPYRIGHT.

The Work is copyright the Author.  All rights to the Work are reserved by the
Author, except as specifically described below. This License describes the terms and
conditions under which the Author permits you to copy, distribute and modify copies
of the Work.

In addition, you may refer to the Work, talk about it, and (as dictated by "fair
use") quote from it, just as you would any copyrighted material under copyright law.

Your right to operate, perform, read or otherwise interpret and/or execute the Work
is unrestricted; however, you do so at your own risk, because the Work comes WITHOUT
ANY WARRANTY -- see Section 7 ("NO WARRANTY") below.

3. COPYING AND DISTRIBUTION.

Permission is granted to distribute, publish or otherwise present verbatim copies
of the entire Source Data of the Work, in any medium, provided that full copyright
notice and disclaimer of warranty, where applicable, is conspicuously published on
all copies, and a copy of this License is distributed along with the Work.

Permission is granted to distribute, publish or otherwise present copies of the
Object Form of the Work, in any medium, under the terms for distribution of Source
Data above and also provided that one of the following additional conditions are met:

(a) The Source Data is included in the same distribution, distributed under the terms
of this License; or

(b) A written offer is included with the distribution, valid for at least three
years or for as long as the distribution is in print (whichever is longer), with a
publicly-accessible address (such as a URL on the Internet) where, for a charge not
greater than transportation and media costs, anyone may receive a copy of the Source
Data of the Work distributed according to the section above; or

(c) A third party's written offer for obtaining the Source Data at no cost, as
described in paragraph (b) above, is included with the distribution. This option is
valid only if you are a non-commercial party, and only if you received the Object
Form of the Work along with such an offer.

You may copy and distribute the Work either gratis or for a fee, and if desired, you
may offer warranty protection for the Work.

The aggregation of the Work with other works which are not based on the Work -- such
as but not limited to inclusion in a publication, broadcast, compilation, or other
media -- does not bring the other works in the scope of the License; nor does such
aggregation void the terms of the License for the Work.

4. MODIFICATION.

Permission is granted to modify or sample from a copy of the Work, producing a
derivative work, and to distribute the derivative work under the terms described in
the section for distribution above, provided that the following terms are met:

(a) The new, derivative work is published under the terms of this License.

(b) The derivative work is given a new name, so that its name or title can not be
confused with the Work, or with a version of the Work, in any way.

(c) Appropriate authorship credit is given: for the differences between the Work
and the new derivative work, authorship is attributed to you, while the material
sampled or used from the Work remains attributed to the original Author; appropriate
notice must be included with the new work indicating the nature and the dates of any
modifications of the Work made by you.

5. NO RESTRICTIONS.

You may not impose any further restrictions on the Work or any of its derivative
works beyond those restrictions described in this License.

6. ACCEPTANCE.

Copying, distributing or modifying the Work (including but not limited to sampling
from the Work in a new work) indicates acceptance of these terms. If you do not
follow the terms of this License, any rights granted to you by the License are null
and void. The copying, distribution or modification of the Work outside of the terms
described in this License is expressly prohibited by law.

If for any reason, conditions are imposed on you that forbid you to fulfill the
conditions of this License, you may not copy, distribute or modify the Work at all.

If any part of this License is found to be in conflict with the law, that part shall
be interpreted in its broadest meaning consistent with the law, and no other parts of
the License shall be affected.

7. NO WARRANTY.

THE WORK IS PROVIDED "AS IS," AND COMES WITH ABSOLUTELY NO WARRANTY, EXPRESS OR
IMPLIED, TO THE EXTENT PERMITTED BY APPLICABLE LAW, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.

8. DISCLAIMER OF LIABILITY.

IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
IN ANY WAY OUT OF THE USE OF THIS WORK, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

END OF TERMS AND CONDITIONS
```






\newpage

Free Document Dissemination Licence (1999) {.unnumbered}
-----------------------------------

```
Free Document Dissemination Licence -- FDDL version 1

This document may be freely read, stored, reproduced, disseminated, translated or
quoted by any means and on any medium provided the following conditions are met:

    every reader or user of this document acknowledges that he his aware that
no guarantee is given regarding its contents, on any account, and specifically
concerning veracity, accuracy and fitness for any purpose;

    no modification is made other than cosmetic, change of representation
format, translation, correction of obvious syntactic errors, or as permitted by the
clauses below;

    comments and other additions may be inserted, provided they clearly appear
as such; translations or fragments must clearly refer to an original complete
version, preferably one that is easily accessed whenever possible;

    translations, comments and other additions must be dated and their
author(s) must be identifiable (possibly via an alias);

    this licence is preserved and applies to the whole document with
modifications and additions (except for brief quotes), independently of the
representation format;

    whatever the mode of storage, reproduction or dissemination, anyone able to
access a digitized version of this document must be able to make a digitized copy
in a format directly usable, and if possible editable, according to accepted, and
publicly documented, public standards;

    redistributing this document to a third party requires simultaneous
redistribution of this licence, without modification, and in particular without
any further condition or restriction, expressed or implied, related or not to this
redistribution. In particular, in case of inclusion in a database or collection, the
owner or the manager of the database or the collection renounces any right related to
this inclusion and concerning the possible uses of the document after extraction from
the database or the collection, whether alone or in relation with other documents.

Any incompatibility of the above clauses with legal, contractual or judiciary
decisions or constraints implies a corresponding limitation of reading, usage, or
redistribution rights for this document, verbatim or modified.
```








\newpage

GNU Free Documentation License (1999) {.unnumbered}
------------------------------

```
GNU Free Documentation License Version 0.9
                                DRAFT


0. PREAMBLE

The GNU Free Documentation License is a form of copyleft designed for books, such as
reference manuals and tutorials.  We designed it in order to use it for documentation
about free software, but it can be used regardless of the subject matter.  It can
also apply to textual works that are not released in book form.  It gives users the
right to copy, redistribute and modify the work, just as users have the right to
copy, redistribute and modify free software.


1. APPLICABILITY

This License applies to any manual or other work which contains a notice placed by
the copyright holder saying it can be distributed under the terms of this License.
The "Manual", below, refers to any such manual or work.  Any member of the public is
a licensee, and is addressed as "you".

A "Modified Version" of the Manual means any work containing the Manual or a portion
of it, either copied verbatim or with modifications and/or translated into another
language.

The "Invariant Sections" are certain appendices or front-matter sections of the
Manual, which deal exclusively with nontechnical matters (such as the political
views, histories or legal positions of the authors), and whose titles are listed
as Invariant Sections in the notice saying that the Manual is released under this
license.

The "Front-Cover Texts" are certain short passages of text are listed as Front-Cover
Texts or Back-Cover Texts in the notice saying that the Manual is released under this
license.

A "Transparent" copy of the Manual means a machine-readable copy, represented in
a format whose specification is available to the general public, in which the
text may be viewed straightforwardly with ordinary text editors, and which is
suitable for input to text formatters or for automatic translation to a variety
of formats suitable for input to text formatters.  Examples of suitable formats
for transparent copies include Texinfo input format, LaTeX input format, SGML, and
standard-conforming HTML.  A copy that is not "Transparent" is called "Opaque".

The "Title Page" means, for a printed book, the title page; for works in other
formats where there is no title page as such, it means the text near the most
prominent mention of the work's title, preceding the beginning of the body of the
text.


2. VERBATIM COPYING

You may copy and distribute the Manual, in any medium, either commercially or
noncommercially, provided that this license is reproduced in all copies, and you add
no other conditions whatsoever to those of this license.  You may accept compensation
in exchange for copies, but you may not technically obstruct the reading or further
copying of the copies you make or distribute.

You may also lend copies, under the same conditions stated above, and you may
publicly display copies.

It is requested, but not required, that you give the authors of the Manual thirty
days (or more) advance notice of your plans to redistribute any large number of
copies, to give them a chance to provide you with an updated version of the Manual.


3. COPYING IN QUANTITY

If you publish or distribute printed copies of the Manual numbering more than 100,
and the Manual's license notice requires Cover Texts, you must enclose the copies in
covers that carry, clearly and legibly, all these Cover Texts: Front-Cover Texts on
the front cover, and Back-Cover Texts on the back cover.  If the required texts for
either cover are too voluminous to fit legibly, put the first ones listed (as many as
fit reasonably) on the actual cover, and continue the rest onto adjacent pages.

If you publish or distribute Opaque copies of the Manual numbering more than 100,
you must state in or with each copy a publicly accessible computer network location
containing a Transparent copy of the Manual, no more and no less, which the general
network-using public has access to download at no charge.  You must take reasonably
prudent steps, when you begin distribution of Opaque copies in quantity, to ensure
that this Transparent copy will remain thus accessible at the stated location until
at least six months after you last distribute an Opaque copy.


4. MODIFICATIONS

You may copy and distribute a Modified Version of the Manual under the conditions
of section 2 and 3 above, provided that you release the Modified Version under
precisely this license, with the Modified Version filling the role of the Manual,
thus licensing use of the Modified Version to whoever possesses a copy of it.
In addition, you must do these things in the Modified Version:


A. Mention the Manual's title on the Title Page.
B. Add something to the title, or a subtitle, stating that the version has
   been modified, and distinguishing it from the Manual you started with.
C. Mention on the Title Page at least one name of a person or entity
   responsible for authorship of the modifications in the Modified Version and/or
   publication of the Modified version, and describe that entity's relationship to
   the Modified Version.
D. Retain on the Title Page or its continuation the authors' and
   publishers' names listed on the Manual's Title Page.
E. Preserve all the copyright notices of the Manual.
F. Add an appropriate copyright notice for your own work.
G. Include after them a notice stating giving the public permission to
   use the Modified Version under the terms of this license, in the form shown in the
   Addendum below.
H. Preserve in that notice the full list of Invariant Sections,
   and the full list of required Cover Texts, given in Manual's notice.
I. Include an unaltered copy of this license.
J. Preserve the network location, if any, given in the Manual for
   public access to a Transparent copy of the Manual, and likewise those network
   locations given in the Manual for any earlier versions it was based on.
K. If the Manual has an Acknowledgements and/or Dedications section,
   preserve therein all the substance of each of the contributor acknowledgements
   and/or dedications stated therein.
L. Preserve all the Invariant Sections of the Manual,
   unaltered in text and in their titles.

If the Modified Version includes new front-matter sections (or appendices) which
deal exclusively with nontechnical matters, and contain no material copied from the
Manual, you may at your option add the section titles of any or all of these sections
to the list of Invariant Sections in the Modified Version.

You may add up to five words of Front-Cover Text and up to 25 words of Back-Cover
Text to the end of the list of Cover Texts in the Modified Version.

The author(s) and publisher(s) of the Manual do not by this license give permission
to use their names for publicity or to assert or imply endorsement of any Modified
Version.


5. COMBINING MANUALS

You may combine the Manual with other manuals released under this license, under
the terms of section 3 above as for modified versions, provided that you include
all of the Invariant Sections of all of the original manuals, unmodified, in the
combination, and list them all as Invariant Sections in your combined work.

The combined work need only contain one copy of this license, and multiple identical
Invariant Sections may be replaced with a single copy.  If there are multiple
Invariant Sections with the same name but different contents, make the title of each
such section unique by adding at the end, in parentheses, the name of the original
author or publisher of that section if known, otherwise the name of an author or
publisher of the manual that section came from; and make the same adjustment in the
list of Invariant Sections in the license of the combined work.


6. AGGREGATION WITH INDEPENDENT WORKS

A compilation of the Manual or its derivatives with other separate and independent
documents or works, in or on a volume of a storage or distribution medium, does
not as a whole count as a Modified Version of the Manual, provided no compilation
copyright is claimed for the compilation.  In such a case, this license does not
apply to the other self-contained works thus compiled with the Manual, if they are
not derivative works of the Manual.


7. TRANSLATION

Translation is considered a kind of modification, so you can distribute translations
of the manual under the terms of section 4.  This implies that translation of the
Invariant Sections requires special permission from their copyright holders.  You may
include a translation of this license provided that you also include this license in
the original English version.  In case of a disagreement between the translation and
the English version of this license, the English version will prevail.


8. TERMINATION

You may not copy, modify, sublicense, or distribute the Manual except as expressly
provided under this License.  Any attempt otherwise to copy, modify, sublicense or
distribute the Manual is void, and will automatically terminate your rights under
this License.  However, parties who have received copies, or rights, from you under
this License will not have their licenses terminated so long as such parties remain
in full compliance.


9. ADDENDUM: How to use this license for your manuals

To use this license in a manual you have written, put the following notice on the
page after the title page:

      Copyright (c)  YEAR  YOUR NAME.  Permission is granted to copy, distribute
      and/or modify this manual under the terms of the GNU Free Documentation
      License, Version 1.0 or any later version published by the Free Software
      Foundation, with the Invariant Sections being LIST THEIR TITLES, Front-Cover
      Texts being LIST, and Back-Cover Texts being LIST.  A copy of the license is
      included in the section entitled "GNU Free Documentation License"

If you have no Invariant Sections, write "with no Invariant Sections" instead.  If
you have no Front-Cover Texts, write "no Front-Cover Texts" instead of "Front-Cover
Texts being LIST".  Likewise for Back-Cover Texts.
```









\newpage

IDGB Open Book Open Content License (1999) {.unnumbered}
------------------------------------------

```
Open Content License

IDG BOOKS WORLDWIDE, INC.

OPEN CONTENT LICENSE

v0.1, August 10, 1999

Please read this Open Content License ("License") for our Open Content Work published
by IDG Books Worldwide, Inc. ("IDGB"). You acknowledge that you accept the terms of
our License.

1. License; Requirements for Modified and Unmodified Versions.

    a. Grant. IDGB grants to you a non-exclusive license to reproduce and
distribute, in whole or in part, in print or electronic media, the Open Content Work,
including for commercial redistribution. If you reproduce or distribute the Open
Content Work, you must adhere to the terms of this License and this License or an
incorporation of it by reference as set forth in Section 2 below must be displayed in
any reproduction. IDGB reserves all rights not expressly granted herein.

    b. Attribution. Any publication in standard (paper) book form requires
citation to the original author and IDGB. The original author\u2019s and IDGB\u2019s
names shall appear prominently on the covers and spine of the Open Content Work.

2. Incorporation by Reference. Proper form for an incorporation by reference is as
follows:

This is an Open Content Work published by IDG Books Worldwide, Inc. You
may reproduce and distribute this material provided that you adhere
to the terms and conditions set forth in the IDG Books Worldwide,
Inc. Open Content License (the latest version is presently available at
http://www.linuxworld.com/idgbooks-openbook/lw-oclicense.html).

This reference must be immediately followed with the author(s) and/or IDGB\u2019s
options in Section 7 below displayed.

3. Copyright. Copyright to the Open Content Work is owned by IDGB or the author.

4. License Application and Disclaimer. The following terms apply to the Open Content
Work, unless otherwise expressly stated in this License:

    a. Application. Mere aggregation or compilation of the Open Content Work or
a portion of the Open Content Work with other works or programs on the same
media shall not cause this License to apply to those other works or programs.
The aggregate work or compilation shall contain a notice specifying the inclusion
of the material from the Open Content Work and a copyright notice in the name of its
copyright owner.

    b. NO WARRANTY. THIS OPEN CONTENT WORK IS BEING LICENSED "AS IS" WITHOUT
WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY
WARRANTY OF NON-INFRINGEMENT OR IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE, WITH RESPECT TO (I) THE MATERIAL IN THE OPEN CONTENT WORK,
(II) THE SOFTWARE AND THE SOURCE CODE CONTAINED THEREIN, AND/OR (III) THE TECHNIQUES
DESCRIBED IN THE OPEN CONTENT WORK.

5. Requirements for Modified Works. If you modify this Open Content Work, including
in translations, anthologies, compilations, and partial documents, you must:

    a. Designation. Designate within the modified work that the modification is
a translation, anthology, compilation, portion of, or other modification of the Open
Content Work.

    b. Identity. Identify yourself as the person making the modifications and
date the modification.

    c. Acknowledgment of Original Author and IDGB. Acknowledge the Open Content
Work\u2019s original author and IDGB in accordance with standard academic citation
practices, including a reference to the original ISBN under which the work was
published.

    d. Location of Original Work. Identify the original Open Content
Work's location.

    e. License. Display this License or incorporate it by reference as set
forth in Section 2 above.

    f. No Endorsement. Not use the original author(s)\u2019 name(s) or
IDGB\u2019s name to indicate an endorsement by them of your modifications, without
their prior written permission.

6. Good Practice Recommendations. In addition to the terms and conditions of this
License, the author and IDGB request, and strongly recommend, that redistributors of
the Open Content Work adhere to the following:

    a. E-mail Notification. If you intend to distribute modifications of the
Open Content Work in print media or on CD ROM, you should provide e-mail notification
to the host not later than thirty (30) days prior to your redistribution, in order to
give IDGB or the author(s) sufficient time to provide you with updated material to
the Open Content Work. Your e-mail notification should include descriptions of your
modifications.

    b. Identification of Modifications. You should clearly indicate, by marking
up the document to reference the modifications, the substantive modifications and
deletions you make to the Open Content Work.

    c. Free Copy of Modifications. While not mandatory, you should offer a free
copy of the modifications, whether in print or on CD-ROM, to the original author(s)
and to IDGB.

7. License Options. The following provision(s) are considered part of the
License and must be included when you reproduce the License (or its incorporation by
reference) in modifications of the Open Content Work.

    a. Paper/Book Versions. E-mail notification to the host is required not
less than thirty (30) days prior to distribution of the Open Content Work or a
derivative thereof in any standard (paper) book form.

8. General. This License constitutes the entire understanding between the parties as
to the subject matter hereof. This License shall be governed by California law and
the U.S. Copyright Act of 1976, as amended, and case law thereunder. In the event one
or more provisions contained in this License are held by any court or tribunal to be
illegal or otherwise unenforceable, the remaining provisions of this License shall
remain in full force and effect.

CONTRIBUTORS AGREEMENT

If you desire to contribute to the Open Content Work and the intellectual development
of its subject matter by providing written comments, additions, supplements, fixes,
patches, or other contributions (collectively, "Contributions"), you acknowledge and
agree that:

    a. Your Contribution may be used by IDGB or the author(s) on a
non-exclusive basis, in whole or in part, to reproduce, modify, fix, patch, display,
redistribute, and otherwise use in print or electronic media, in the Open Content
Work, revised editions, and otherwise throughout the world. You also understand
that IDGB and the author(s) cannot guarantee that either of them will use your
Contribution in any manner.

    b. Based on the quality, extent, and use of your Contribution, you will
receive a general or special acknowledgment, and may receive a contributor's
honorarium, from IDGB. In this regard, you hereby give IDGB permission to acknowledge
your Contribution by posting and otherwise reproducing your name and e-mail address.

    c. To your best knowledge, you represent and warrant that (i) your
Contribution does not violate any copyright, trademark, or any other registered
or common law intellectual property or proprietary right of any person or entity,
(ii) all statements in your Contribution are true and accurate and do not violate
the property or privacy rights of any third party, (iii) your Contribution does not
contain information or instructions that could reasonably cause injury to person or
property, and (iv) you have full authority to agree to the terms herein and to make
these representations.

```






\newpage

Licence Publique Multimédia (1999) {.unnumbered}
----------------------------------

```
Licence Publique Multimédia

Version 1.0 du lundi 1er Février 1999

Introduction

Ce document décrit les droits de diffusion pour des contenus multimédia. Il est
inspiré de la General Public License de la Free Software Foundation applicable sur
les logiciels libres.

Copyright © Association Vidéon
BP 221 F-91133 Ris Orangis Cedex FRANCE

La copie et la distribution de copies exactes de ce document sont autorisées, mais
aucune modification n'est permise.

Préambule

Les licences de droits de diffusion de la plupart des contenus multimédia
sont définies pour limiter ou supprimer toute liberté à l'utilisateur ou au
diffuseur. À l'inverse, la Licence Publique Multimédia (Multimédia Public License)
est destinée à vous garantir la liberté de partager et de diffuser les contenus
multimédia, et de s'assurer que ces contenus sont effectivement accessibles à tout
utilisateur.

Cette Licence Publique Multimédia s'applique aux contenus de la Banque de Programmes
Libres de droits sur Internet mise en place par l'association Vidéon, comme à tout
autre contenu dont l'auteur l'aura décidé. Vous pouvez aussi appliquer les termes
de cette Licence à vos propres contenus, si vous le désirez.

Liberté des contenus multimédia ne signifie pas que vous pouvez faire ce que vous
voulez des contenus. Notre Licence est conçue pour vous assurer la liberté de
diffuser des copies des contenus dans leur intégralité sans nécessiter de passer
un accord préalable supplémentaire avec le propriétaire des droits.

Afin de garantir ces droits, nous avons dû introduire des restrictions interdisant
à quiconque de vous les refuser ou de vous demander d'y renoncer.  Ces restrictions
vous imposent en retour certaines obligations si vous distribuez ou diffusez des
copies de contenus protégés par la Licence. En d'autre termes, il vous incombera en
ce cas de :

    transmettre aux destinataires tous les droits que vous possédez, leur remettre
    cette Licence afin qu'ils prennent connaissance de leurs droits.

Nous protégeons vos droits de deux façons : d'abord par la protection du droit de
propriété intellectuel sur le contenu, ensuite par la remise de cette Licence qui
vous autorise légalement à copier, distribuer, diffuser et/ou traduire le contenu
multimédia.

En outre, pour protéger chaque auteur ainsi que Vidéon, nous affirmons
solennellement que le contenu concerné ne fait l'objet d'aucune garantie. Si un
tiers l'inclus dans un autre contenu (par exemple une émission de télévision)
et/ou le traduit puis le redistribue ou le rediffuse, tous ceux qui le recevront
doivent savoir qu'il s'agit d'un contenu inclus et/ou traduit afin que sa
distribution ou sa diffusion n'entache pas la réputation de l'auteur du contenu.

Enfin, tout contenu libre est sans cesse menacé par la protection des droits
de reproduction et de diffusion. Nous souhaitons à tout prix éviter que des
distributeurs ou diffuseurs puissent protéger les droits de diffusion sur des
contenus libres pour leur propre compte. Pour éviter cela, nous stipulons bien que
toute protection éventuelle d'un contenu doit accorder expressément à tout un
chacun le libre usage du produit.

Les dispositions précises et les conditions de copie, de distribution, de diffusion
et de traduction de nos contenus sont les suivantes :

Stipulations et conditions relatives à la copie, la distribution, la diffusion et
la traduction

Article 0. Champ d'application

La présente Licence s'applique à tout contenu multimédia (incluant en particulier
les films, musiques, etc.) où figure une note, placée par le détenteur des droits,
stipulant que le dit contenu peut être distribué ou diffusé selon les termes de
la présente Licence. Le terme contenu désigne aussi bien le contenu lui-même
que tout travail qui en est dérivé selon la loi, c'est-à-dire tout contenu ou
une partie de celui-ci, à l'identique ou bien modifié, et/ou traduit dans une
autre langue. Chaque personne concernée par la Licence Publique Multimédia sera
désignée par le terme Vous.

Les activités autres que copie, distribution, diffusion ou traduction ne sont pas
couvertes par la présente Licence et sortent de son cadre. En particulier, les
droits moraux sont régis par les législations nationales.

Article 1. Copie, distribution, diffusion

Vous pouvez copier, distribuer ou diffuser des copies conformes ou traduites dans
leur intégralité du contenu multimédia, tel que Vous l'avez reçu, sur n'importe
quel support, à condition de placer sur chaque copie ou d'indiquer à la diffusion
un copyright approprié et une restriction de garantie, de ne pas modifier ou omettre
toutes les stipulations se référant à la présente Licence et à la limitation de
garantie, de fournir avec toute copie du contenu un exemplaire de la Licence ou de
faire référence lors d'une diffusion à l'endroit où l'auditeur ou le spectateur
peut en trouver une copie.

Vous ne pouvez demander une éventuelle rétribution financière que pour la
réalisation de la copie ou de la diffusion.

Quelque soit le mode de stockage, copie, distribution ou diffusion, toute personne
ayant accès à une version numérisée du contenu doit pouvoir en faire une copie
numérisée dans un format directement utilisable, suivant les standards publics et
publiquement documentés en usage.

Article 2. Inclusion dans d'autres contenus et traduction

Vous pouvez inclure votre copie du contenu dans un autre contenu ou le traduire, et
copier, distribuer ou diffuser ces modifications selon les termes de l'article 1, à
condition de Vous conformer également aux conditions suivantes :

    Ajouter aux contenus traduits l'indication très claire qu'il s'agit d'une
traduction, ainsi que la date de la traduction et la mention du contenu d'origine.
    Ajouter à Vos contenus qui incorporeraient un contenu protégé par la
présente Licence l'indication très claire qu'il s'agit d'un contenu inclus
protégé par la Licence Publique Multimédia.Si le contenu multimédia qui
inclu le contenu libre n'est pas libre lui même, il doit permettre de délimiter
précisément le contenu libre et permettre sa copie, distribution et diffusion
librement suivant la Licence Publique Multimédia.
    Copier, traduire, distribuer ou diffuser le contenu dans son intégralité
en y incluant les génériques et les mentions de mécénat, sauf dans le cas du
droit d'illustration permis dans le pays de distribution ou de diffusion.
    Faire en sorte, si ce n'est pas déjà le cas, que le contenu comporte le
copyright approprié en indiquant clairement la limitation de garantie, qu'il stipule
que toute personne peut librement redistribuer ou rediffuser le contenu selon les
conditions de la Licence Publique Multimédia de Vidéon.

Si des éléments inclus dans le contenu disposent d'ayant droits différents du
titulaire des droits du contenu (par exemple des musiques), il doit être fait
mention des droits appliqués à chacun de ces éléments dans le générique ou
lorsque cela n'est pas possible, sur le support de distribution (jaquette, page Web
de téléchargement …). Cela est vrai quelque soit la licence d'utilisation de ces
éléments, qu'il s'agisse de la Licence Publique Multimédia, d'une autre cession
des droits ou de droits commerciaux classiques. Dans ce dernier cas, la distribution
ou la diffusion du contenu peut entraîner la nécessité de payer des droits de
diffusion de ces éléments.

Article 3. Utilisations autres que celles décrites

Vous ne pouvez pas copier, traduire, céder, déposer, distribuer ou diffuser le
contenu d'une autre manière que l'autorise la Licence Publique Multimédia.  Toute
tentative de ce type annule immédiatement vos droits d'utilisation du contenu sous
cette Licence. Toutefois, les tiers ayant reçu de Vous des copies du contenu ou le
droit d'utiliser ces copies continueront à bénéficier de leur droit d'utilisation
tant qu'ils respecteront pleinement les conditions de la Licence.

Article 4. Acceptation de la licence

Ne l'ayant pas signée, Vous n'êtes pas obligé d'accepter cette Licence.
Cependant, rien d'autre ne Vous autorise à traduire, distribuer ou diffuser
le contenu : la loi l'interdit tant que Vous n'acceptez pas les termes de cette
Licence. En conséquence, en traduisant, distribuant ou diffusant le contenu, Vous
acceptez implicitement tous les termes et conditions de cette Licence.

Article 5. Transmission des droits

La distribution ou diffusion d'un contenu suppose l'indication d'une licence
autorisant la copie, la traduction, la distribution ou la diffusion du contenu,
aux termes et conditions de la Licence. Vous n'avez pas le droit d'imposer de
restrictions supplémentaires aux droits transmis au destinataire. Vous n'êtes pas
responsable du respect de la Licence par un tiers.

La transmission du contenu libre a un tiers se fait avec transmission de cette
licence, sans modification, et en particulier sans addition de clause ou contrainte
nouvelle, explicite ou implicite liée ou non à cette transmission, autre que
celles autorisées par cette licence. En particulier, en cas d'inclusion dans une
base de données ou une collection, le propriétaire ou l'exploitant de la base ou
de la collection s'interdit tout droit de regard lié à ce stockage et concernant
l'utilisation qui pourrait être faite du contenu après extraction de la base ou de
la collection, seul ou en relation avec d'autres contenus.

Article 6. Incompatibilité avec une décision de justice

Si, à la suite d'une décision de Justice, d'une plainte en contrefaçon ou pour
toute autre raison (liée ou non à la contrefaçon), des conditions Vous sont
imposées (que ce soit par ordonnance, accord amiable ou autre) qui se révèlent
incompatibles avec les termes de la présente Licence, Vous n'êtes pas pour autant
dégagé des obligations liées à celle-ci : si Vous ne pouvez concilier vos
obligations légales ou autres avec les conditions de cette Licence, Vous ne devez
pas distribuer ou diffuser le contenu.

Si une partie quelconque de cet article est invalidée ou inapplicable pour quelque
raison que ce soit, le reste de l'article continue de s'appliquer et l'intégralité
de l'article s'appliquera en toute autre circonstance.

Le présent article n'a pas pour but de Vous pousser à enfreindre des droits
ou des dispositions légales ni en contester la validité ; son seul objectif
est de protéger l'intégrité du système de distribution du Logiciel Libre.
De nombreuses personnes ont généreusement contribué à la large gamme de contenus
multimédia distribuée de cette façon en toute confiance ; il appartient à chaque
auteur/donateur de décider de diffuser ses contenus selon les critères de son
choix.

Article 7 Limitations géographiques.

Si la distribution et/ou l'utilisation du contenu est limitée dans certains pays
par la législation, le détenteur original des droits qui place le contenu sous la
Licence Publique Multimédia peut ajouter explicitement une clause de limitation
géographique excluant ces pays. Dans ce cas, cette clause devient une partie
intégrante de la Licence.

Article 8. Version à utiliser de la Licence

L'association Vidéon se réserve le droit de publier périodiquement des mises à
jour ou de nouvelles versions de la Licence. Rédigées dans le même esprit que
la présente version, elles seront cependant susceptibles d'en modifier certains
détails à mesure que de nouveaux problèmes se font jour.

Chaque version possède un numéro distinct. Si le contenu précise un numéro
de version de cette Licence et " toute version ultérieure ", Vous avez le choix
de suivre les termes et conditions de cette version ou de toute autre version plus
récente publiée par l'association Vidéon. Si le contenu ne spécifie aucun numéro
de version, Vous pouvez alors choisir l'une quelconque des versions publiées par
Vidéon.

Limitation de garantie

Article 9. Risques et Garantie

Parce que l'utilisation de ce contenu est libre et gratuite, aucune garantie n'est
fournie, comme le permet la loi. Sauf mention écrite, les détenteurs des droits
sur le contenu et/ou les tiers fournissent le contenu en l'état, sans aucune sorte
de garantie explicite ou implicite, y compris les garanties de commercialisation
ou d'adaptation dans un but particulier. En tant que distributeur ou diffuseur
Vous assumez tous les risques quant à la qualité et aux effets du contenu. Si
l'utilisation que vous faites du contenu est illégale par rapport à la législation
du ou des pays de distribution ou de diffusion, Vous assumez les coûts et les
dépends consécutifs à votre utilisation.

Article 10. Responsabilité

Sauf lorsqu'explicitement prévu par la Loi ou accepté par écrit, ni le détenteur
des droits, ni quiconque autorisé à copier, traduire redistribuer et/ou diffuser le
contenu comme il est permis ci-dessus ne pourra être tenu pour responsable de tout
dommage direct, indirect, secondaire ou accessoire découlant de l'utilisation du
contenu ou de l'impossibilité d'utiliser celui-ci.

Fin des termes et conditions

Comment appliquer ces directives à vos nouveaux contenus multimédia

Si vous réalisez un nouveau contenu multimédia et désirez en faire bénéficier
tout un chacun, la meilleure méthode est d'en faire un contenu Libre que tout
le monde pourra redistribuer et diffuser selon les termes de la Licence Publique
Multimédia.

Pour cela, insérez les indications suivantes dans votre contenu (il est préférable
et plus sûr de les faire figurer dans le générique du contenu ou à défaut sur
le support de diffusion du contenu : jaquette de disque ou de vidéo, page Web de
téléchargement …) :

    ((nom du contenu, lorsqu'il n'est pas inclus par ailleurs))

    Copyright (C) ((nom du détenteur des droits)) ((année)) ((Ajoutez
également votre adresse électronique, le cas échéant ainsi que votre adresse
postale))

    Ce film (ou cette musique ou...) est libre de droits de diffusion Il est destiné
    aux télévisions de proximité et à favoriser l'échange
entre associations et/ou particuliers.

    Vous pouvez le traduire, le redistribuer et/ou le diffuser suivant les
termes de la Licence Publique Multimédia publiée par l'association Vidéon (version
1 ou bien toute autre version ultérieure choisie par vous).

    Ce programme est distribué en l'état sans aucune garantie ni explicite,
ni implicite.

    Vous pouvez en obtenir une copie de la Licence : http://www.videontv.org/
    Association Vidéon, BP 221 F-91133 Ris Orangis cedex France.

    ((indication éventuelle des éléments inclus dans le contenu dont les
droits sont détenus par une autre personne ou organisation))

Si vous officiez en tant que réalisateur ou auteur, n'omettez pas de demander à
votre employeur, votre établissement scolaire ou autres de signer une décharge
stipulant leur renoncement aux droits qu'ils pourraient avoir sur le contenu :

    ...((employeur, école...)) déclare par la présente ne pas revendiquer de
droits sur le contenu " (nom du contenu) " réalisé par ...((nom de l'auteur)).

    ((signature du responsable)), ...((date)), ...((nom et qualité du
responsable)).

```






\newpage

Linux Documentation Project Copying License (1999) {.unnumbered}
--------------------------------------------------

```
Linux Documentation Project Copying License
Last Revision: 16 September 1999

Please read the license carefully---it is somewhat like the GNU General Public
License, but there are several conditions in it that differ from what you may be used
to. If you have any questions, please email the LDP coordinator, Guylhem Aznar.

Note: All Linux Documentation Project manuals are copyrighted by their respective
authors. THEY ARE NOT IN THE PUBLIC DOMAIN.

The Linux Documentation Project manuals (guides) may be reproduced and distributed in
whole or in part, subject to the following conditions:

    The copyright notice above and this permission notice must be preserved complete
    on all complete or partial copies.

    Any translation or derivative work of Linux Installation and Getting Started must
    be approved by the author in writing before distribution.

    If you distribute Linux Installation and Getting Started in part, instructions
    for obtaining the complete version of this manual must be included, and a means
    for obtaining a complete version provided.

    Small portions may be reproduced as illustrations for reviews or quotes in other
    works without this permission notice if proper citation is given.

    The GNU General Public License referenced below may be reproduced under the
    conditions given within it.

Exceptions to these rules may be granted for academic purposes: write to the author
and ask. These restrictions are here to protect us as authors, not to restrict you as
educators and learners. All source code in Linux Installation and Getting Started is
placed under the GNU General Public License, available via anonymous FTP from the GNU
archive site.

```






\newpage

Open Publication License (1999) {.unnumbered}
-------------------------------

```
 Open Publication License
v1.0, 8 June 1999


I. REQUIREMENTS ON BOTH UNMODIFIED AND MODIFIED VERSIONS

The Open Publication works may be reproduced and distributed in whole or in part,
in any medium physical or electronic, provided that the terms of this license are
adhered to, and that this license or an incorporation of it by reference (with any
options elected by the author(s) and/or publisher) is displayed in the reproduction.

Proper form for an incorporation by reference is as follows:

    Copyright (c) <year> by <author's name or designee>. This material may be
distributed only subject to the terms and conditions set forth in the Open
Publication License, vX.Y or later (the latest version is presently available at
http://www.opencontent.org/openpub/).

The reference must be immediately followed with any options elected by the author(s)
and/or publisher of the document (see section VI).

Commercial redistribution of Open Publication-licensed material is permitted.

Any publication in standard (paper) book form shall require the citation of the
original publisher and author. The publisher and author's names shall appear on
all outer surfaces of the book. On all outer surfaces of the book the original
publisher's name shall be as large as the title of the work and cited as possessive
with respect to the title.


II. COPYRIGHT

The copyright to each Open Publication is owned by its author(s) or designee.


III. SCOPE OF LICENSE

The following license terms apply to all Open Publication works, unless otherwise
explicitly stated in the document.

Mere aggregation of Open Publication works or a portion of an Open Publication
work with other works or programs on the same media shall not cause this license to
apply to those other works. The aggregate work shall contain a notice specifying the
inclusion of the Open Publication material and appropriate copyright notice.

SEVERABILITY. If any part of this license is found to be unenforceable in any
jurisdiction, the remaining portions of the license remain in force.

NO WARRANTY. Open Publication works are licensed and provided "as is" without
warranty of any kind, express or implied, including, but not limited to, the implied
warranties of merchantability and fitness for a particular purpose or a warranty of
non-infringement.


IV. REQUIREMENTS ON MODIFIED WORKS

All modified versions of documents covered by this license, including translations,
anthologies, compilations and partial documents, must meet the following
requirements:

    The modified version must be labeled as such.
    The person making the modifications must be identified and the
modifications dated.
    Acknowledgement of the original author and publisher if applicable must be
retained according to normal academic citation practices.
    The location of the original unmodified document must be identified.
    The original author's (or authors') name(s) may not be used to assert or
imply endorsement of the resulting document without the original author's (or
authors') permission.


V. GOOD-PRACTICE RECOMMENDATIONS

In addition to the requirements of this license, it is requested from and 
strongly recommended of redistributors that:

    If you are distributing Open Publication works on hardcopy or CD-ROM, you
provide email notification to the authors of your intent to redistribute at least
thirty days before your manuscript or media freeze, to give the authors time to
provide updated documents. This notification should describe modifications, if any,
made to the document.
    All substantive modifications (including deletions) be either clearly
marked up in the document or else described in an attachment to the document.
    Finally, while it is not mandatory under this license, it is considered
good form to offer a free copy of any hardcopy and CD-ROM expression of an Open
Publication-licensed work to its author(s).


VI. LICENSE OPTIONS

The author(s) and/or publisher of an Open Publication-licensed document may
elect certain options by appending language to the reference to or copy of the
license. These options are considered part of the license instance and must be
included with the license (or its incorporation by reference) in derived works.

A. To prohibit distribution of substantively modified versions without the explicit
permission of the author(s). "Substantive modification" is defined as a change
to the semantic content of the document, and excludes mere changes in format or
typographical corrections.

To accomplish this, add the phrase `Distribution of substantively modified versions
of this document is prohibited without the explicit permission of the copyright
holder.' to the license reference or copy.

B. To prohibit any publication of this work or derivative works in whole or in part
in standard (paper) book form for commercial purposes is prohibited unless prior
permission is obtained from the copyright holder.

To accomplish this, add the phrase 'Distribution of the work or derivative of the
work in any standard (paper) book form is prohibited unless prior permission is
obtained from the copyright holder.' to the license reference or copy.

```





\newpage

Open Directory License (1999) {.unnumbered}
-----------------------------

```
Open Directory License

The Open Directory is a compilation of many different editors' contributions.
Netscape Communications Corporation (`Netscape') owns the copyright to the
compilation of the different contributions, and makes the Open Directory available
to you to use under the following license agreement terms and conditions (`Open
Directory License').  For purposes of this Open Directory License, `Open Directory'
means only the Open Directory Project currently hosted at http://dmoz.org (or at
another site as may be designated by Netscape in the future), and does not include
any other versions of directories, even if referred to as an `Open Directory,' that
may be hosted by Netscape on other web pages (e.g., Netscape Netcenter).

1. Basic License.  Netscape grants you a non-exclusive, royalty-free license to use,
reproduce, modify and create derivative works from, and distribute and publish the
Open Directory and your derivative works thereof, subject to all of the terms and
conditions of this Open Directory License.  You may authorize others to exercise
the foregoing rights; provided, however, that you must have an agreement with your
sublicensees that passes on the requirements and obligations of Sections 2 and 4
below and which must include a limitation of liability provision no less protective
of Netscape than Section 6 below.

Due to the nature of the content of the Open Directory, many third parties' trade
names and trademarks will be identified within the content of the Open Directory
(e.g., as part of URLs  and description of link).  Except for the limited license to
use the Netscape attribution in Section 2 below, nothing herein shall be deemed to
grant you any license to use any Netscape or third party trademark or tradename.

2. Attribution Requirement.  As a material condition of this Open Directory License,
you must provide the below applicable attribution statements on (1) all copies
of the Open Directory, in whole or in part, and derivative works thereof which
are either distributed (internally or otherwise) or published (made available on
the Internet and/or internally over any internal network/intranet or otherwise),
whether distributed or published electronically, on hard copy media or by any
other means, and (2) on any program/web page from which you directly link to/access
any information contained within the Open Directory, in whole or in part, or any
derivative work thereof:

(a) If the Open Directory in whole or in part, or any derivative work thereof, is
made available via the Internet or internal network/intranet and/or information
contained therein is directly accessed or linked via the Internet or internal
network/intranet then you must provide the Netscape attribution statement as
described in the page(s) at the URL http://dmoz.org/become_an_editor.

(b) If the Open Directory in whole or in part, or any derivative work thereof, is
made available on any hard copy media (e.g., CD-ROM, diskette), you must place on
the packaging a notice providing Netscape attribution as described in the page(s) at
the URL http://dmoz.org/become_an_editor.  If there is no `packaging', the previous
attribution notice should be placed conspicuously such that it would be reasonably
viewed by the recipient of the Open Directory.

(c) If you are using or distributing the Open Directory in modified form (i.e.,
with additions or deletions), you must include a statement indicating you have made
modifications to it.  Such statement should be placed with the attribution notices
required by Sections 2(a) and 2(b) above.

Netscape grants you the non-exclusive, royalty-free license to use the above
identified Netscape attribution statements solely for the purpose of the above
attribution requirements, and such use must be in accordance with the usage
guidelines that may be published by Netscape from time to time as part of the above
URLs.

3. Right To Identify Licensee.  You agree that Netscape has the right to publicly
identify you as a user/licensee of the Open Directory.

4. Errors and Changes.  From time to time Netscape may elect to post on the page(s)
at the URL http://dmoz.org/become_an_editor certain specific changes to the Open
Directory and/or above attribution statements, which changes may be to correct errors
and/or remove content alleged to be improperly in the Open Directory.  So long as you
are exercising the license to Open Directory hereunder, you agree to use commercially
reasonable efforts to check the page(s) at the URL http://dmoz.org/become_an_editor
from time to time, and to use commercially reasonable efforts to make the
changes/corrections/deletion of content from the Open Directory and/or attribution
statements as may be indicated at such URL.  Any changes to the Open Directory
content posted at the page(s) at the URL http://dmoz.org/become_an_editor are part of
Open Directory.

5. No Warranty/Use At Your Risk. THE OPEN DIRECTORY AND ANY NETSCAPE TRADEMARKS AND
LOGOS CONTAINED WITH THE REQUIRED ATTRIBUTION STATEMENTS ARE MADE AVAILABLE UNDER
THIS OPEN DIRECTORY LICENSE AT NO CHARGE.  ACCORDINGLY, THE OPEN DIRECTORY AND THE
NETSCAPE TRADEMARKS AND LOGOS ARE PROVIDED `AS IS,' WITHOUT WARRANTY OF ANY KIND,
INCLUDING WITHOUT LIMITATION THE WARRANTIES THAT THEY ARE MERCHANTABLE, FIT FOR
A PARTICULAR PURPOSE OR NON-INFRINGING.  YOU ARE SOLELY RESPONSIBLE FOR YOUR USE,
DISTRIBUTION, MODIFICATION, REPRODUCTION AND PUBLICATION OF THE OPEN DIRECTORY AND
ANY DERIVATIVE WORKS THEREOF BY YOU AND ANY OF YOUR SUBLICENSEES (COLLECTIVELY,
`YOUR OPEN DIRECTORY USE'). THE ENTIRE RISK AS TO YOUR OPEN DIRECTORY USE IS BORNE
BY YOU. YOU AGREE TO INDEMNIFY AND HOLD NETSCAPE, ITS SUBSIDIARIES AND AFFILIATES
HARMLESS FROM ANY CLAIMS ARISING FROM OR RELATING TO YOUR OPEN DIRECTORY USE.

6. Limitation of Liability.  IN NO EVENT SHALL NETSCAPE, ITS SUBSIDIARIES OR
AFFILIATES, OR THE OPEN DIRECTORY CONTRIBUTING EDITORS, BE LIABLE FOR ANY INDIRECT,
SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, INCLUDING, WITHOUT LIMITATION, DAMAGES
FOR LOSS OF GOODWILL OR ANY AND ALL OTHER COMMERCIAL DAMAGES OR LOSSES, EVEN IF
ADVISED OF THE POSSIBILITY THEREOF, AND REGARDLESS OF WHETHER ANY CLAIM IS BASED UPON
ANY CONTRACT, TORT OR OTHER LEGAL OR EQUITABLE THEORY, RELATING OR ARISING FROM THE
OPEN DIRECTORY, YOUR OPEN DIRECTORY USE OR THIS OPEN DIRECTORY LICENSE AGREEMENT.

7. California Law.  This Open Directory License will be governed by the laws of the
State of California, excluding its conflict of laws provisions.

Rev01-13-99

```






\newpage

Open Resources Magazine License (1999) {.unnumbered}
--------------------------------------

```
This document may be freely read, stored, reproduced, disseminated, translated or
quoted by any means and on any medium provided the following conditions are met:

1. Every reader or user of this document acknowledges that is aware that no guarantee
is given regarding its contents, on any account, and specifically concerning
veracity, accuracy and fitness for any purpose.

2. No modification is made other than cosmetic, change of representation format,
translation, correction of obvious syntactic errors, or as permitted by the clauses
below.

3. Comments and other additions may be inserted, provided they clearly appear as
such; translations or fragments must clearly refer to an original complete version,
preferably one that is easily accessed whenever possible.

4. Translations, comments and other additions or modifications must be dated and
their author(s) must be identifiable (possibly via an alias).

5. This licence is preserved and applies to the whole document with modifications and
additions (except for brief quotes), independently of the representation format.

6. Any reference to the ``official version'', ``original version'' or ``how to obtain
original versions'' of the document is preserved verbatim. Any copyright notice in
the document is preserved verbatim. Also, the title and author(s) of the original
document should be clearly mentioned as such.

7. In the case of translations, verbatim sentences mentioned in (6.) are preserved
in the language of the original document accompanied by verbatim translations to the
language of the traslated document. All translations state clearly that the author
is not responsible for the translated work. This license is included, at least in the
language in which it is referenced in the original version.

8. Whatever the mode of storage, reproduction or dissemination, anyone able to access
a digitized version of this document must be able to make a digitized copy in a
format directly usable, and if possible editable, according to accepted, and publicly
documented, public standards.

9. Redistributing this document to a third party requires simultaneous
redistribution of this licence, without modification, and in particular without
any further condition or restriction, expressed or implied, related or not to this
redistribution. In particular, in case of inclusion in a database or collection, the
owner or the manager of the database or the collection renounces any right related to
this inclusion and concerning the possible uses of the document after extraction from
the database or the collection, whether alone or in relation with other documents.

Any incompatibility of the above clauses with legal, contractual or judiciary
decisions or constraints implies a corresponding limitation of reading, usage, or
redistribution rights for this document, verbatim or modified.

```




\newpage

W3C Document Notice (1999) {.unnumbered}
--------------------------

```
DOCUMENT NOTICE
Copyright © 1994-2000 World Wide Web Consortium, (Massachusetts Institute of
Technology, Institut National de Recherche en Informatique et en Automatique, Keio
University). All Rights Reserved.  http://www.w3.org/Consortium/Legal/

Public documents on the W3C site are provided by the copyright holders under the
following license. The software or Document Type Definitions (DTDs) associated with
W3C specifications are governed by the Software Notice. By using and/or copying this
document, or the W3C document from which this statement is linked, you (the licensee)
agree that you have read, understood, and will comply with the following terms and
conditions:

Permission to use, copy, and distribute the contents of this document, or the W3C
document from which this statement is linked, in any medium for any purpose and
without fee or royalty is hereby granted, provided that you include the following on
ALL copies of the document, or portions thereof, that you use:

    A link or URL to the original W3C document.
    The pre-existing copyright notice of the original author, or if it doesn't
exist, a notice of the form: "Copyright © [$date-of-document] World Wide
Web Consortium, (Massachusetts Institute of Technology, Institut National
de Recherche en Informatique et en Automatique, Keio University). All Rights
Reserved. http://www.w3.org/Consortium/Legal/" (Hypertext is preferred, but a textual
representation is permitted.)
    If it exists, the STATUS of the W3C document.

When space permits, inclusion of the full text of this NOTICE should be provided. We
request that authorship attribution be provided in any software, documents, or other
items or products that you create pursuant to the implementation of the contents of
this document, or any portion thereof.

No right to create modifications or derivatives of W3C documents is granted pursuant
to this license. However, if additional requirements (documented in the Copyright
FAQ) are satisfied, the right to create modifications or derivatives is sometimes
granted by the W3C to individuals complying with those requirements.

THIS DOCUMENT IS PROVIDED "AS IS," AND COPYRIGHT HOLDERS MAKE NO REPRESENTATIONS
OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, OR TITLE;
THAT THE CONTENTS OF THE DOCUMENT ARE SUITABLE FOR ANY PURPOSE; NOR THAT THE
IMPLEMENTATION OF SUCH CONTENTS WILL NOT INFRINGE ANY THIRD PARTY PATENTS,
COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.

COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL OR
CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE DOCUMENT OR THE PERFORMANCE OR
IMPLEMENTATION OF THE CONTENTS THEREOF.

The name and trademarks of copyright holders may NOT be used in advertising or
publicity pertaining to this document or its contents without specific, written
prior permission. Title to copyright in this document will at all times remain with
copyright holders.

------------------------------------------------------------------------------------

This formulation of W3C's notice and license became active on April 05 1999 so as to
account for the treatment of DTDs, schema's and bindings. See the older formulation
for the policy prior to this date. Please see our Copyright FAQ for common questions
about using materials from our site, including specific terms and conditions for
packages like libwww, Amaya, and Jigsaw. Other questions about this notice can be
directed to site-policy@w3.org.

```






\newpage

Ethymonics Free Music Licence (2000) {.unnumbered}
------------------------------------

```
ETHYMONICS FREE MUSIC LICENSE

Version 1, August 2000

Copyright (C) 2000, Ethymonics Limited

Everyone is permitted to copy and distribute verbatim copies of this license
document, but changing it is not allowed.

Preamble

The licenses for most musical works are designed to take away your freedom to share
the music. By contrast, this Free Music License is intended to guarantee your
freedom to make copies of a piece of music, and charge for this service if you
wish. Recipients of those copies have the same freedom. The word "Free" in "Free
Music License" means the freedom to make copies. It does not mean that those copies
cannot then be sold.

This license is designed to protect and pass on the right to make copies to whoever
receives a copy. This encourages wide distribution on the artist's behalf. You can
apply this license to your own music too.

To protect the right to copy the music, it is necessary to pass on certain
requirements that must be followed when the music is copied or distributed. For
example, if you distribute a piece of music subject to this license, even if this is
done for a fee, you must give the recipients all the rights that you have. You must
show them these terms so that they know their rights.

The freedom to copy is protected by two things: (1) Copyright of the music, and (2)
This license that provides legal permission to copy and distribute the music.

The precise terms and conditions for copying, distribution and performance follow.

FREE MUSIC LICENSE

TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND PERFORMANCE

1. This License applies to any musical work or other type of work which has a notice
placed by the copyright holder saying that it may be distributed under the terms
of this Free Music License. The "Music", below, refers to any such music or work,
whether in a recording, performance or other form of musical representation, or
any derivative work under copyright law: that is to say, a work, in any medium,
containing the Music or a portion of it, either unaltered, modified and or
re-performed.

You may play or perform the Music publicly, for example in a broadcast, provided that
you make available to listeners the title of the work and the name of the Artist. A
recording made as a result of the Music being played or performed is covered by this
License when its contents constitute a work based on the Music. If a listener wishes
to make copies of such a recording, the terms and conditions of this License must
be made available to them, along with any other information required to make and
distribute copies according to the terms and conditions of this License.

2. You may copy and distribute copies of the Music as you receive it, in any medium,
provided that you:

(a) conspicuously and appropriately publish on each copy an appropriate copyright
notice;

(b) keep intact all the notices that refer to this License;

(c) supply, with each copy of the Music, all significant information about the Music,
including the title of the work, the name of the artist, and the names and roles of
all credited personnel;

(d) supply, to each recipient of the Music, along with the Music, either a copy of
this License or a clearly visible URL that lets the recipient know where to find a
copy of this License on the Internet.

If the information required by (a) to (d) above is not available, for example
when the Music has been received by making a recording of a performance, then this
information must be obtained independently and no copies can be made or distributed
without this information being included with each copy of the Music.

You may, at your option, charge a fee for the act of supplying a copy of the Music.

3. You may not copy, distribute, perform or sub-license the Music except as expressly
provided under this License. Any attempt to otherwise do so is void, and will
automatically terminate your rights under this License. However, parties who have
received copies, or rights, from you under this License will not have their Licenses
terminated as long as such parties remain in full compliance.

4. You are not required to accept this License, since you have not signed it.
However, nothing else grants you permission to copy, distribute, play or perform the
Music or any of its derivative works. Therefore, by copying distributing, playing or
performing the Music, or any of its derivative works, you indicate your acceptance of
this License to do so, and all of its terms and conditions.

5. Each time you redistribute the Music the recipient automatically receives a
license from the original licensor to copy, distribute, play and or perform the Music
subject to the terms and conditions of this License. You may not impose any further
restrictions on the recipient's exercise of the rights granted herein. You are not
responsible for enforcing compliance by third parties to this License.

6. If, as a consequence of a court judgment or allegation of copyright infringement
or for any reason (not limited to copyright issues), conditions are imposed on you
(whether by court order, agreement or otherwise) that contradict the conditions of
this License, they do not excuse you from the conditions of this License. If you
cannot satisfy simultaneously your obligations under this License and any other
pertinent obligations, then your rights under this License are terminated. For
example, if an existing agreement would not permit royalty-free redistribution of
the Music by all those who receive copies directly or indirectly through you, then
the only way to satisfy both it and this License would be to refrain entirely from
redistributing the Music.

If any portion of this section is held invalid or unenforceable under any particular
circumstance, the balance of the section is intended to apply and the section as a
whole is intended to apply in other circumstances.

It is not the purpose of this section to induce you to infringe any copyright or
other property right claims or to contest validity of any such claims; this section
has the sole purpose of protecting the integrity of a music distribution system,
which is implemented by copyright and license practices.

7. If an activity permitted by this License is restricted in certain countries either
for copyright or other reasons, the original copyright holder who places the Music
under this License may add an explicit geographical distribution limitation excluding
those countries, so that distribution is permitted only in or among countries not
thus excluded. In such cases, this License incorporates the limitation as if written
in the body of this License.

8. Ethymonics may publish revised and or new versions of the Free Music License from
time to time. Such new versions will be similar in spirit to the present version, but
may differ in detail to address new problems as they arise.

Each License is given a distinguishing version number. If the Music specifies a
version number of this License which applies to it, and "any later version", you have
the option of following the terms and conditions either of that version or any later
version published by Ethymonics. If the Music does not specify a version number of
this License, you may choose any version ever published by Ethymonics.

9. If you wish to incorporate parts of the Music into another work whose distribution
conditions are different, write to the composer to ask for permission.

END OF FREE MUSIC LICENSE

```





\newpage

Free Art License (2000) {.unnumbered}
-----------------------

```
Free Art License
[ Copyleft Attitude ]
version 1.1

Preamble:

With this Free Art license, you are given the right to freely copy, distribute and
transform the artworks in the respect of the rights of the author.

Far from ignoring the rights of the author, this license recognizes them and protects
them. It reformulates their principle while making it possible for the public to make
a creative use of the works of art.  Whereas the use that is being made of the right
to litterary and artistic property resulted in a restriction of the public's access
to the works of art, the goal of the Free Art license is to support it.

The intention is to open the access and to authorize the use of the resources of an
artwork by the greatest number of people. To make it available in order to multiply
the pleasures, to create new conditions of creation to amplify the possibilities of
creation. In the respect of the authors with the recognition and the defense of their
moral rights.

Indeed, with the arrival of digital creation, the invention of the Internet and of
free software, a new approach of creation and of production appeared. It is also the
amplification of what has been tested by many contemporary artists.

Knowledge and creation are resources which must remain free to be still truly
knowledge and creation. I.e. to remain a fundamental quest which is not directly
related to a concrete application. Creating is discovering the unknown, it is
inventing reality without any preoccupation about realism.

Thus, the object of art is not equivalent to the artistic object, finite and
defined as such.  This is the essential goal of this Free Art license: promoting and
protecting artistic practices freed from the rules of the market economy.

--------

DEFINITIONS

- Artwork:
It is a common artwork which includes the initial artwork as well as all posterior
contributions (consequent originals and copies). It is created at the initiative of
the initial author who, by this license, defines the conditions according to which
the contributions are made.

- Initial artwork:
It is the artwork created by the initiator of the common artwork, which copies will
be modified by whoever wishes.


- Consequent artworks:
These are the proposals of the authors who contribute to the formation of the common
artwork by making use of the reproduction rights, of distribution and of modification
that the license confers to them.

- Original (source or resource of work):
Dated specimen of the artwork, of its definition, of its partition or of its
program which the author present like the reference for all further updatings,
interpretations, copies or reproductions.

- Copy:
Any conforming reproduction of an original as defined by this license.

- Author of the initial artwork:
It is the person who created the artwork at the origin of the tree structure of this
modified artwork. By this license, the author determines the conditions under which
this work is done.

- Contributor:
Any person who contributes to the creation of the artwork. He is the author of an
original work resulting from the modification of a copy of the initial artwork or the
modification of a copy of a consequent artwork.


1. OBJECT

The object of this license is to define the conditions according to which you can
enjoy this work freely.


2. EXTENT OF THE USAGE

This artwork is subject to copyright, and the author, by this license, specifies the
extent to wich you can copy it, distribute it and modify it:


2.1 FREEDOM TO COPY (OR OF REPRODUCTION)

You have the right to copy this artwork for a personal use, for your friends, or for
any other person and employing whatever technique.


2.2 FREEDOM TO DISTRIBUTE, TO INTERPRET (OR OF REPRESENTATION)

You can freely distribute the copies of these artworks, modified or not, whatever the
support, whatever the place, for a fee or for free if you observe all the following
conditions:
- attach this license to the copies, in its entirety, or indicate precisely where
the license can be found,
- specify to the recipient the name of the author of the originals,
- specify to the recipient where he will be able to access the originals (initial
and/or consequent). The author of the original will be able, if he wishes to, to give
you the right to diffuse the original under the same conditions as the copies.


2.3 FREEDOM TO MODIFY

You have the right to modify the copies of the originals (initial and consequent),
which can be partial or not, in the respect of the conditions set in article 2.2 in
the event of distribution (or representation) of the modified copy. The author of the
original will be able, if he wishes to, to give you the right to modify the original
under the same conditions as the copies.


3. INCORPORATION OF ARTWORK

All the elements of this artwork must remain free, this is why you are not allowed to
integrate the originals in another work which would not be subject to this license.


4. YOUR AUTHOR'S RIGHTS

The object of this license is not to deny your author's rights on your
contribution. By choosing to contribute to the evolution of this artwork, you only
agree to give to others the same rights on your contribution as those which were
granted to you by this license.


5. DURATION OF THE license

This license takes effect as of your acceptance of its provisions. The fact
of copying, of distributing, or of modifying the artwork constitutes a tacit
agreement. The duration of this license is the duration of the author's rights
attached to the artwork. If you do not respect the terms of this license, you
automatically lose the rights that it confers to you. If the legal status to which
you are subject does not make it possible for you to respect the terms of this
license, you cannot make use of the rights which it confers.


6. VARIOUS VERSIONS OF THE license

This license could be modified regularly, for its improvement, by its authors (the
actors of the movement copyleft_attitude) by way of new numbered versions.

You have always the choice between being satisfied with the provisions contained
in the version under which the copy was communicated to you or else, to use the
provisions of one of the subsequent versions.


7. SUB-license

Sub-licenses are not authorized by the present license. Any person who wishes to make
use of the rights that it confers will be directly bound to the author of the initial
work.


8. THE LAW THAT IS APPLICABLE TO THE CONTRACT

This license is subject to the French law.


--------

INSTRUCTIONS:

- How to use the Free Art license?

To benefit from the Free Art license, it is enough to specify the following on your
artwork:

[some lines to indicate the name of the artwork and to give a possible idea of what
it is.]

[some lines to give, if necessary, a description of the modified artwork and the name
of the author.]

Copyright © [the date] [name of the author] (if it is the case, specify the names of
the previous authors) Copyleft: this artwork is free, you can redistribute it and/or
modify it according to terms of the Free Art license.

You will find a specimen of this license on the site Copyleft Attitude
http://copyleft.tsx.org as well as on other sites.


- Why use the Free Art license?

1 / to give access to your artwork to the greatest number of people.

2 / to let it be freely distributed.

3 / to allow it to evolve by allowing its transformation by others.

4 / to be able yourself to use the resources of an artwork when it is under Free Art
license: to copy, distribute it freely or transform it.

5 / This is not all.
Because the use of the Free Art license is also a good way to take liberties with
the system of goods generated by the dominant economy. The Free Art license offers an
interesting legal framework to prevent any abusive appropriation. It is not possible
any more to seize your work to short-circuit its creation and to make an exclusive
profit from it. It is not allowed to take advantage of the collective work being
done, not allowed to monopolize the resources of moving creation for the only benefit
of some.

The Free license Art fights for an economy suitable for art, based on sharing,
exchange and the merry work. What counts in art is also and mostly what is not
counted.


- When to use the Free Art license ?

It is not the goal of the Free license Art to eliminate the copyright or the author's
rights. Quite the opposite, it is about reformulating its relevance by taking into
account the contemporary environment. It is about allowing the right to freedom of
movement, to free copy and to free transformation of artworks. Allowing the right to
the freedom of work for art and artists.

1 / A each time you want to use or enable this right, use the Free Art license.

2 / A each time you want to create artworks so that they evolve and are freely
copied, freely distributable and freely transformable: use the Free Art license.

3 / A each time you want to have the possibility of copying, of distributing or of
transforming a work: check that it is under Free Art license. If it is not, you are
likely to be outlaw.


- To which types of artworks can the Free Art license be applied?

This license can be applied to digital artworks as well as to non digital ones. It
was born out of the observation of the world of the free software and the Internet,
but its applicability is not limited to the digital supports. You can put a painting,
a novel, a sculpture, a drawing, a music, a poem, an installation, a video, a film,
a cooking recipy, a CD-Rom, a Web site, a performance, in short all creations which
can be claimed of a certain art.

- This license has a history: it was born at the meeting " Copyleft Attitude "
http://copyleft.tsx.org which took place at "Accès Local" and "Public" in Paris at
the beginning of the year 2000. For the first time, it gathered together software
specialists and actors of free software with contemporary artists and people from the
art world.
```







\newpage

Freedom CPU Charter (2000) {.unnumbered}
--------------------------

```
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~HEADER~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CHARTER.txt (C) Yann GUIDON 2000 for the F-CPU project.

file created : nov. 23, 2000 by YG
current version : dec. 30, 2000 by YG
$add revisions date/names here.$

Like everything in the F-CPU project, it is a basis and subject for constructive
discussions and it should not be considered as definitive. Everybody is asked to
contribute to this decisive, non-technical side of the project. I have cut&pasted
some parts of the previous "F-CPU licence proposal". It is still incomplete.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~INTRODUCTION~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Freedom CPU project (F-CPU for short) is the only fully parametised 64-bit SIMD
CPU core available today in source code form.  Not only it is designed to be able to
replace (one day) the best existing RISC processors in workstations, but it is being
developped in a net-community environment by students as well as professionals as a
hobby. Because their work is performed for free, they want it to remain free, just
like Linux or the GNU project.

The purpose of this file is to introduce newcomers to the F-CPU design philosophy and
basic rules. More about this can be read on the F-CPU mailing list(s) and manual.

Because the GNU Public Licence covers most of the needs of the project, it is
the only licence that you have to comply with. It determines the rights and duties
concerning the distribution, modification, compilation etc.  of the "source code" of
the processor (that is : the VHDL sources contained in this tarball).

However, the GPL doesn't apply to the "electronic" world and the implementors are
completely free to do whatever they want with the derived physical devices.

You don't have to read the rest of this file if you only want to use the files
without modification. For example, it is not revelant if you just install the bundle
and try to compile the files. However, if you modify a file, add a new file, adapt
a file for compilation with a tool, build the circuit or add features, you should
carefully read this text.


This charter is intended to provide developpers with guidelines, "do and don't" rules
that should be followed to keep the project up and running. If a chip is built from
the F-CPU sources then distributed, the respect of these rules will determine if the
chip can be labelled as "a F-CPU".  The use of the F-CPU source files is completely
free under the terms of the GPL, the implementation is not bound in any way, but the
F-CPU development team follows these basic rules :


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GUIDELINES~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

o  "The name of the game is freedom". It is forbidden to forbid others. "One's
freedom ends where other's freedom starts". These three well-known basic rules favor
reciprocal respect and positive ununcumbered work.

o  We promote collaborative work, free communication and unconstrained sharing of
knowledge and know-how. This project is not a way to earn money quickly and easily,
but a mean to learn technics in a community, with the goal of redistributing the
knowledge evenly.

o  The distribution, modification and knowledge of the sources (non physical forms of
the design, as opposed to the "physical implementation" of this design) must not be
bound or restricted in ANY way.

o  In particular, you need not be a customer of a F-CPU vendor in order to access the
sources of any F-CPU version or derived work.

o Similarly, in-progress works must be available upon a single request. An attempt to
over-delay the transmission of the requested files can be interpreted as a "guilty"
behaviour.

o  The reason for this break from the GPL principle is simple : the F-CPU is not
the property of an individual or a company, but belongs to everybody. Anybody must
be able to examine, use or modify any version of any document because it is not the
exclusive property of a single person. If you have your kid in a kindergarten, you
think it is normal to visit the location and see if your kid is safe or if nothing
wrong can happen.  Same goes with software that we write in community.

o  Do not promote secrecy. Just as the sources came to you openly, you should not
promote secrets or hidden features.  It is forbidden to patent existing features used
in the F-CPU.  The F-CPU forums and mailing lists provide you with different ways to
share your remarks, additions, propositions, etc.  Secrecy has no advantage in the
F-CPU community and corresponds to a self-exclusion from the group.

o  Do not bind the files to a proprietary software or obscure file format. Anybody
should be able to reuse your work without being forced to acquire a specific
software. Standard formats are highly recommended (ISO, ANSI etc), GNU software is
preferred, freeware or public domain is ok, too. If you use a "specific" software,
you are asked to add the required scripts or configuration files that interface the
F-CPU source with said software, and publish them under the GPL.

o  When a source file is modified, the developer should update the comments and
indicate his name, the date, and a short description of the modifications. It is the
easiest way to keep track of the project's evolution. More about this can be read in
the QUALITY.TXT file. Compliance with the quality guidelines can influence whether a
file or directory can be officially part of the F-CPU package.

o  Please : document and comment your modifications or additions, because you can
read and understand the existing sources. The lack of decent documentation, just like
obfuscated source code, slows down the development team's work.

o  When a source file is added to the F-CPU file pool, it must be distributed with
the GPL.

o  Whenever a file is created or modified, the developper has to include his personal
copyright notice. It is a crucial legal protection mechanism because different
copyrights get thus inter-mixed. This strengthens the relations and dependcies
between the developpers. If a legal problem arises, a single developper can not be
attacked alone.

o  All documentations written about the F-CPU and the associated software must
be distributed under the terms of the GFDL (GNU Free Documentation Licence). This
applies to manuals, technical books, drafts or requests for comments (RFCs).

o  Personal opinions, articles or other individual expressions about the F-CPU are
well covered by the copyright laws (that means that an article or conference doesn't
need to be bound by the GFDL).

o  Even though the GLP allows to sell physical media containing GPL'd files, the
present guidelines only allow it if the same files are available for free on the
Internet with the conditions described here. This is consistent with the fact that
the packaging of the files on a physical medium is a service only, it is not an
exception to the present guidelines.

o  The modification of the F-CPU design is allowed under the sole condition that you
agree to and respect these guidelines.  You do not have to register yourself in a
database, you do not need any authorization of any kind and you can do whatever you
want with the F-CPU design, except : changing the copyright notices, altering these
guidelines or use them against their spirit.

o  Unlike some "Open" standards and initiatives, you do not need to fill in a form,
pay a fee or a licence to use the F-CPU design. In return, you may not restrict the
direct access to the design that you have modified, even for the sake of collecting
statistics or polling (or, in general, collecting individual/personal data or going
through advertising pages).


o  Other guidelines will be added in the future.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

These recommendations can be enforced through the Copyright laws and are added to the
terms of the GPL. This document is protected by the Copyright laws.

```






\newpage

Licence ludique générale (2000) {.unnumbered}
-------------------------------

```
Licence ludique générale
__________


Version 1 – mars 2000
Copyright © Olivier Fontan
La copie et la distribution de copies exactes de ce document sont autorisées, mais
aucune modification n'est permise.

PRÉAMBULE

Les modalités de publication de la plupart des jeux de simulation (jeux de rôle,
jeu de plateau, wargames) mettent les joueurs à la merci d’une politique
éditoriale d’une maison d’édition qui pratique des prix disproportionnés,
multiplie les «suites» indispensables à la bonne pratique du jeu, ou abandonne une
série pour cause de non-rentabilité sans mener à bien sa démarche. A l’opposé,
la licence ludique générale, directement inspirée des licences qui garantissent
la publication, la copie et la diffusion des contenus libres et ouverts (logiciels,
textes, œuvres, etc.)  vise à vous garantir de partager et de modifier les jeux
librement accessibles, et ainsi de s’assurer que ces jeux sont accessibles sans
frais pour tous leurs utilisateurs. Les modèles de licences libres garantissent
juridiquement de manière satisfaisante le droit d’auteur et la liberté des
utilisateurs.

Cette licence est conçue pour s’assurer que vous avez la liberté de copier et
de distribuer des jeux, gratuitement ou non, et que vous recevez ou pouvez obtenir
le «noyau du jeu», que vous pouvez modifier le jeu ou en utiliser des parties dans
d’autres jeux libres.

Afin de protéger vos droits, cette licence doit faire des restrictions qui
interdisent à quiconque de vous refuser ces droits ou de vous demander d’y
renoncer. Ces restrictions vous imposent par conséquent certaines responsabilités
si vous distribuez des copies des jeux protégés par la Licence ludique générale
ou si vous les modifiez.  Par exemple, si vous distribuez des copies d’un tel jeu,
gratuitement ou non, vous devez transmettre aux utilisateurs tous les droits que vous
possédez.  Vous devez donc vous assurez qu’ils reçoivent ou qu’ils peuvent se
procurer le noyau du jeu. Vous devez leur montrer cette licence afin qu’ils soient
eux aussi au courant de leurs droits.

Enfin, tout jeu libre est sans cesse menacé par des dépôts de droits
d’auteur. Nous voulons à tout prix éviter que des distributeurs puissent
individuellement déposer la licence des jeux libres, pour leur propre compte.
Pour éviter cela, il est bien stipulé qu’un éventuel dépôt de licence doit
prévoir un usage libre pour tous. Aucun jeu diffusé sous forme libre et ouverte ne
peut ainsi devenir propriétaire.

Les termes précis et les conditions pour la copie, la distribution et la
modification sont les suivants.


TERMES ET CONDITIONS DE COPIE, DISTRIBUTION ET MODIFICATION

    1. – Cette licence s’applique à tout jeu ou tout autre travail
contenant une notice placée par le possesseur du copyright précisant qu’il peut
être distribué selon les termes de cette licence ludique générale. Le « jeu »
désigne soit le noyau du jeu en lui-même, soit n’importe quel travail qui en est
dérivé selon la loi. : c’est-à-dire, un ouvrage comprenant le jeu ou une partie
de celui-ci, que ce soit à l’identique ou avec des modifications, et/ou traduit
dans un autre langage (à partir de maintenant, il sera considéré que le terme
« modification » inclut également la « traduction »). Chaque personne pour qui
s’applique cette licence sera désignée par « vous ».

    On entend par « noyau du jeu » la liste exhaustive de ce que le créateur
du jeu considère comme faisant partie de l’exemplaire original : règles, annexes,
appendices, pions, marqueurs, matériel divers. La composition de ce noyau peut
varier au gré du créateur. La description de ce noyau doit figurer dans chacun des
jeux auxquels s’applique la licence ludique générale.

    Les activités autres que la copie, la distribution et la modification
sortent du cadre de cette licence et ne sont pas couvertes.

    2. – Vous pouvez copier et distribuer des copies conformes du jeu, tel
que vous l’avez reçu, sur n’importe quel support, à condition de placer sur
chaque copie un copyright approprié, la composition du noyau du jeu, et de garder
intactes toutes les parties se référant à cette licence, et de fournir avec toutes
les copies du programme un exemplaire de cette licence ludique générale.

    Vous pouvez demander une rétribution financière pour l’acte physique de
réalisation de la copie.

    3. – Vous pouvez publier en grand nombre, sous forme imprimée ou
électronique, un jeu libre et le diffuser, gratuitement ou non. Toute publication
doit comporter les éléments suivants :

    - a : toute version modifiée doit être présentée comme telle, et
l’auteur et la date des modifications clairement identifiés.
    - b : les auteurs et éventuels éditeurs des versions précédentes
doivent être clairement indiqués, et en premier lieu l'auteur de la version
originale. En couverture doivent être mentionnés au moins les cinq principaux
auteurs ; en pages intérieures, un historique complet des versions successives doit
figurer.
    - c : la localisation de l’édition originale doit toujours être
clairement mentionnée.
    - d : toutes les obligations imposées par cette licence ludique générale
doivent être respectées, et notamment l’obligation d’insertion du texte
intégral de cette licence.

    4. Il serait souhaitable que les auteurs de distributions importantes en nombre
respectent les conditions suivantes :

    - a : si vous souhaitez publier, à titre gratuit ou non, un jeu libre,
prévenez-en l’auteur avec suffisamment de délais pour qu’il puise vous faire
bénéficier des dernières modifications.
    - b : indiquez toutes les modifications substantielles, soit clairement
dans le texte, soit dans un document annexé à votre version du jeu.
    - c : par courtoisie, envoyez un exemplaire de votre édition aux auteurs
du jeu.
    

    5. Vous pouvez modifier votre copie ou vos copies du jeu ou toute portion
de celui-ci, ou travail basé sur ce jeu, et copier et distribuer ces modifications
ou votre travail selon les termes de la section 2 ci-dessus à condition que vous
vous conformiez également aux conditions suivantes :

    - a : vous devez rajouter aux parties modifiées une indication très
claire que vous avez effectué des modifications, et indiquer la date de chaque
changement.

    - b : vous devez distribuer sous les termes de la licence ludique
générale, l’ensemble de toute réalisation contenant tout ou partie du jeu, avec
ou sans modifications.

    Ces conditions s’appliquent à l’ensemble des jeux dérivés. Si des
sections identifiables de ce travail ne sont pas dérivées du jeu, et peuvent
être considérées raisonnablement comme indépendantes, alors cette licence ne
s’applique pas à ces sections, lorsque vous les distribuez seules. Mais lorsque
vous distribuez ces mêmes sections comme parties d’un ensemble cohérent dont
le reste est basé sur un jeu soumis à cette licence, alors elles sont soumises
également à la licence ludique générale, qui s’étend ainsi à l’ensemble du
produit, quel qu’en fut l’auteur.

    Il n’est pas question dans cette section de s’approprier ou de
contester vos droits sur un travail totalement écrit par vous, son but est plutôt
de s’accorder le droit de contrôler la libre distribution de tout travail dérivé
ou tout travail collectif basé sur le jeu concerné.

    6. – Vous pouvez copier et distribuer le jeu (ou tout travail dérivé 
selon la section 3), selon les termes des sections 2 et 3 ci-dessus, à 
condition de respecter les conditions suivantes :

    - a : accompagner la distribution du noyau complet du jeu ; ou,
    - b : que la distribution comprenne une offre écrite, valable pendant au
moins les trois prochaines années, de donner à toute tierce partie qui en fera la
demande, une copie du noyau du jeu pour un tarif qui ne devra pas être supérieur à
ce que vous coûte la copie, selon les termes des sections 2 et 3 ci-dessus ; ou,
    - c : que la distribution soit accompagnée des informations sur
l’endroit ou le noyau du jeu peut être obtenu (cette alternative n’est
autorisée que dans le cadre d’une distribution non-commerciale, et uniquement
si vous avez reçu un noyau de jeu modifié, en accord avec la sous-section b
précédente).

    Si la distribution du jeu consiste à offrir un accès permettant de copier
le jeu depuis un endroit particulier, alors l’offre d’un accès équivalent pour
se procurer le noyau du jeu au même endroit compte comme une distribution de ce
noyau du jeu, même si l’utilisateur décide de ne pas se profiter de cette offre.
 
    7. Vous ne pouvez pas copier, modifier, sous-licencier ou distribuer le jeu
d’une autre manière que l’autorise la licence ludique générale. Toute
tentative de copier, modifier, sous-licencier ou distribuer le jeu différemment
annulera immédiatement vos droits d’utiliser le jeu sous cette licence. Toutefois,
les tierces parties ayant reçu de vous des copies du jeu ou le droit d’utiliser
ces copies, continueront à bénéficier de leur droit d’utilisation tant
qu’elles respecteront pleinement les conditions de la licence ludique générale.

    8. Vous n’êtes pas obligés d’accepter cette licence, puisque vous ne
l’avez pas signée. Cependant, rien d’autre ne vous autorise à modifier ou
distribuer le jeu ou les travaux en étant dérivés. Ces faits sont interdits par
la loi, tant que vous n’acceptez pas cette licence. Par conséquent, en modifiant
ou en distribuant le jeu, ou tout travail basé dessus, vous indiquez implicitement
votre acceptation des termes et conditions de cette licence.

    9. Chaque fois que vous redistribuez le jeu (ou tout travail en étant
dérivé), le récipiendaire reçoit une licence du détenteur original
autorisant la copie, la distribution ou la modification du jeu, selon ces termes et
conditions. Vous n’avez pas le droit d’imposer de restriction supplémentaire sur
les droits transmis au récipiendaire. Vous n’êtes pas responsable du respect de
cette licence par les tierces parties.

    10. Si, à la suite d’une décision de justice, il vous est imposé
d’aller à l’encontre des conditions de cette licence, cela ne vous dégage pas
pour autant des obligations liées à cette licence. Si vous ne pouvez pas concilier
vos obligations légales ou toute autre obligation avec les conditions requises par
cette licence, alors vous ne devez pas distribuer le jeu.

    Si une portion quelconque de cette section est rendue non valide ou non
applicable dans des circonstances particulières, le reste de la section continue
à s’appliquer et la totalité de la section s’appliquera dans les autres
circonstances.

    Cette section n’a pas pour but de vous poussez à enfreindre quelque
droit ou propriété légale ou de contester leur validité, elle n’est là que
pour protéger l’intégrité du système de distribution du jeu libre.

    11. Si la distribution et/ou l’utilisation du jeu est limitée, dans
certains pays, le propriétaire original des droits qui place le jeu et son
noyau sous la licence ludique générale peut ajouter explicitement une clause de
limitation géographique excluant ces pays particuliers. Dans un tel cas, cette
clause devient une partie intégrante de cette licence.
 
   12. Si l’auteur d’un jeu diffusé selon les termes et conditions de
cette licence décide de ne plus y soumettre son jeu, la licence ludique générale
continue de s’appliquer aux versions du noyau du jeu précédemment diffusés.

    13. La Société des jeux libres peut publier des mises à jour ou de
nouvelles versions de la licence ludique générale de temps à autres. Elles seront
dans le même esprit que la précédente version, mais pourront différer dans
certains détails destinés à clarifier de nouveaux problèmes pouvant survenir.

    Chaque version possède un numéro de version bien distinct. Si le jeu
précise un numéro de version de cette licence et « toute version antérieure »,
vous avez le choix de suivre les termes et conditions de cette version et toute
autre version plus récente publiée par la Société des jeux libres. Si le jeu
ne spécifie aucun numéro de cession de cette licence, vous pouvez alors choisir
d’utiliser n’importe quelle licence publiée par la Société des jeux libres.

    14. Si vous désirez incorporez des parties du jeu dans d’autres jeux
libres dont les conditions de distribution diffèrent, écrivez à l’auteur pour
lui en demander la permission./.

```






\newpage

Licence pour Documents Libres (2000) {.unnumbered}
------------------------------------

```
License pour Documents Libres

Version 1.1
Copyright (©) 2000 Guilde des Doctorants.

Voici le texte de la Licence pour Documents Libres proposéee par la Guilde des
Doctorants. Ce document est disponible sous forme transparente en LateX et HTML,
et sous forme opaque en Postscript et PDF. La source est contituéee par le fichier
LateX.

Introduction

Le but de cette licence, rédigée par la Guilde des Doctorants, est de rendre libres
les documents écrits auxquels elle s'applique. Un document est dit libre au sens où
chacun peut le recopier et le distribuer, avec ou sans modification par tous moyens
possibles et imaginables. Cette licence préserve la propriété intellectuelle de
l'auteur et de l'éditeur du document. Elle est aussi conçue pour éviter toute
récupération commerciale d'un travail bénévole sans le consentement express
de ses auteurs. D'autres licences rendant libres des documents, des contenus,
des publications existent comme la Free Documentation Licence de la Free Software
Foundation1 et l'initiative OpenContent2. Elles sont rédigées en langue anglaise
et font parfois référence à des notions de droit américain (notion de fair
use). Nous avons très largement repris certains des points de ces licences et nous
en avons laissé certains de coté. Néanmoins ces licences comme la LDL partagent
une même vocation. La Licence pour Documents Libres (LDL), rédigée en français
est sujette à améliorations qui sont les bienvenues (voir section 7).

Cette licence a été conçue pour s'appliquer à divers types de documents quel que
soit leur support : documents techniques, notes de cours, documentation logicielle ou
encore \oe uvres de fictions. Elle inclut également un "copyleft" pour reprendre la
terminologie du GNU : tous les documents dérivés du document original héritent de
cette licence.

Définitions et domaine d'application

Cette licence s'applique à tout document, quelle que soit sa forme, comprenant la
notice standard associée à cette licence. La "notification de licence" désigne
la section du texte où sont mentionnés le fait que le document est soumis à la
présente licence ainsi que le copyright du document. Un document est produit par un
ou plusieurs auteurs. Un document peut être constitué de plusieurs contributions
mises en commun. Dans ce cas, l'éditeur désigne la personne morale ou physique
qui assure la mise à disposition du document sous diverses formes. S'il n'y a
qu'un auteur, il est son propre éditeur.  Conformément au code de la propriété
intellectuelle, chaque auteur conserve la propriété intellectuelle sur sa
contribution. Toutefois, l'éditeur a toute liberté pour faire évoluer le document
dans le respect des dispositions de la présente licence. La notice de copyright
mentionne l'éditeur si il existe ainsi que les noms des auteurs.

On appellera "version dérivée" tout document comprenant le document de départ
partiellement ou dans son intégralité. Une traduction est aussi considérée comme
une version dérivée.

La "section historique" du document désigne une partie du document comprenant un
historique de sa genèse et de son évolution. Dans le cas d'un document derivé,
la section historique contient le descriptif du ou des documents originaux et
des modifications qui y ont été apportées. Elle peut également contenir des
éléments non factuels comme des considérations éthiques ou politiques concernant
l'historique et les motivations du document. La section historique peut donc n'avoir
aucun rapport avec l'objet principal du document.

La "Page de titre" désigne pour un document imprimé la page de titre au sens usuel
plus les pages de couverture. Dans le cas de documents pour lesquels ces notions
ne s'appliquent pas, c'est le texte proche de l'endroit où apparaît le titre du
document et la fin du document. La notification de licence sera placée dans la page
de titre. La notice standard minimale aura la forme suivante :

    "Copyright (c) ANNEE, EDITEUR et AUTEURS. Le contenu de ce document peut
être redistribué sous les conditions énoncées dans la Licence pour Documents
Libres version x.y ou utlérieure."

Un document peut contenir des "sections invariantes". La liste des titres de sections
invariantes est précisée dans la notice spécifiant que le document est soumis à
la présente licence. Par exemple :

    "Le présent document contient un certain nombre de sections invariantes
qui devront figurer sans modification dans toutes les modifications qui seront
apportées au document. Leur titres sont: TITRES DES SECTIONS INVARIANTES".

Nous distinguons trois modes de diffusion de documents électroniques:

    une forme "transparente", ce qui désigne tout format électronique dont
les spécifications sont disponibles dans le domaine public ou qui peut être édité
par des programmes éventuellement commerciaux largement disponibles à la date de
publication du document. Exemples: ASCII, Texinfo, TeX et LaTeX, RTF, HTML, XML, SGML
avec une DTD publique.

    une forme "opaque", ce qui désigne tout format électronique permettant
l'affichage ou l'impression du document de manière simple au moyen de logiciels
du domaine public ou du moins très largement disponibles à la date de publication
du document. Un tel format ne permet pas la modification dudit document de manière
simple. Exemples: Postscript, PDF, tout format de traitement de texte nécessitant un
logiciel propriétaire, ou tout déclinaison de SGML dont la DTD n'est pas publique.

    une forme "cryptée" ce qui désigne le cas où une partie (ou la totalité) du
document est sous un format électronique qui n'est pas lisible sans la possession
d'une clef logicielle ou matérielle qui en permet le décodage. La forme cryptée
peut être utilisée à des fins d'authentification. Une fois décodé le document
peut être sous une forme transparente ou opaque. Dans le cas d'une forme cryptée,
si l'éditeur n'est plus en mesure de fournir la clef de décodage, il doit rendre
disponible le document sous une forme transparente ou opaque non cryptée.

Conditions communes aux versions modifiées et intégrales

La distribution de versions intégrales et modifiées du présent document sous
n'importe quelle forme est autorisée aux conditions suivantes :

    Les notices de copyright spécifiant que le document est soumis à la
présente licence ainsi que les sections invariantes doivent être préservées.
    Les sections invariantes ne peuvent être altérées.
    Les sections historiques ne peuvent être que complétées.
    La localisation3 du document original doit être mentionnée dans la page
de titre et de manière visible.
    Un document disponible sous forme transparente ne peut être rediffusé
sous forme opaque seulement.
    Vous ne devez mettre en place aucun dispositif visant à restreindre
l'accès au document ou la possibilité de reproduire les copies que vous
distribuez. En particulier vous ne pouvez rendre disponible tout ou partie du
document sous forme cryptée si l'auteur l'a expressément interdit.
    La redistribution dans un cadre commercial ne peut être effectuée sans
l'accord préalable des auteurs et de l'éditeur du document original.
    Si la distribution de copies du document entraîne des frais de
reproduction (photocopies, impressions, pressage de médias), vous pouvez néanmoins
les imputer au lecteur. Mais vous ne pouvez pas percevoir de droits d'exploitation
liés au contenu, ni à l'utilisation du document.

Si vous effectuez une diffusion en nombre du document (diffusion sur le WEB, par
FTP, ou à plus de 99 exemplaires imprimés, par courrier électronique, sur CDROM,
ou sur d'autres supports magnétiques ou optiques), vous devez inclure une copie de
la présente licence. Dans ce cas, vous devez également prévenir les éditeurs
du document original afin de définir avec eux comment veiller à la diffusion de
versions à jour du document.

Modifications
   
Dispositions générales

Vous pouvez utiliser une partie du présent document, en la modifiant
éventuellement, pour produire un nouveau document. Les dispositions de la section
3 s'appliquent.

Vous devez en plus :

    lister un ou plusieurs auteurs ou entités responsables des modifications
apportées au document.
    mentionner sur la page de titre que le document dérivé est une
modification d'un ou plusieurs documents originaux. Vous devez préciser leurs titres
ainsi que l'entité éditrice ou les principaux auteurs.
    préciser sur la page de titre que vous êtes l'éditeur de la version
dérivée.

Vous ne devez en aucun cas :

    altérer une mention d'un nom d'auteur présent dans le document original
et concernant une partie que vous avez réutilisé.
    donner au document dérivé le même titre que le document original sans
autorisation de l'éditeur de celui-ci.

Nous recommandons de plus que les titres de sections ne soient altérés qu'en cas de
changement du plan du document rendues nécessaire par les modifications apportées.

Fusion ou combinaisons de documents Dans le cas où le document que vous produisez
est issu de la combinaison de plusieurs documents (après modifications éventuelles)
soumis eux aussi à la présente licence, vous pouvez remplacer les diverses
notifications de licence par une seule.

De même, vous pouvez regrouper les diverses sections historiques pour n'en faire
qu'une seule. Elle doit mentionner explicitement les documents originaux auxquels
vous avez fait appel et indiquer leur localisation.

Si un des documents utilisé est disponible sous une forme transparente alors la
totalité du document dérivé doit aussi l'être.

Vous devez respecter les points listés en sections 3 et 4.1.

Inclusion dans d'autres travaux

Si le Document ou une de ses versions dérivées est agrégé avec des travaux
indépendants, de sorte que plus de 50 % du document final ainsi produit ne soit pas
soumis à la présente licence, le document final n'est pas considéré comme une
version dérivée soumise dans son ensemble à la présente licence. Néanmoins,
la ou les portions du document final qui sont issues d'un document soumis à la
présente licence restent soumis à cette licence. Les recommandations des sections 3
et 4 s'appliquent.

Traduction

Une traduction d'un document est considérée comme une version dérivée. Dans ce
cas, vous pouvez néanmoins traduire les sections invariantes. Si vous ne laissez pas
les versions originales de ces sections, vous devez prendre contact avec l'éditeur
de la version originale afin d'obtenir son accord pour les traductions de ces
sections.

La Guilde proposera un certain nombre de traductions de cette licence. Si il en
existe une pour la langue cible de la traduction du document, c'est celle-ci qui
s'applique. Dans le cas contraire vous ètes invité à proposer une traduction
de la licence à la Guilde. Si vous ne le faites pas, ou si la Guilde refuse cette
traduction, c'est la version originale qui s'applique.

Dispositions concernant la garantie

Cette licence ne définit que les droits de reproduction modification et diffusion
du document. Elle n'y associe aucune garantie : sauf mention expresse du contraire,
qui n'engagerait alors que l'éditeur du document, et dans la mesure où le contenu
est en conformité avec la législation française, il est entendu que ni l'éditeur
ni les auteurs du document ne sauraient être tenus pour responsables des éventuels
dommages et préjudices que l'utilisation du document aurait entraîné. Ces
dispositions s'appliquent même s'il s'avère que le document contient naturellement
ou par obsolescence une inexactitude, une imprécision ou une ambiguïté.

Tout auteur ou éditeur souhaitant doter un document soumis à la présente licence
de dispositions de garantie doit joindre à chaque copie distribuée du document
un certificat de garantie précisant exactement les dispositions de garantie et
mentionnant explicitement les noms des personnes morales ou physique assumant les
responsabilités de la garantie. Les dispositions de libre copie de la présente
licence restent valable pour un document avec garantie mais toute rediffusion par une
autre personne que l'auteur du certificat de garantie se fait sans garantie.

Cessation de la licence

Vous ne pouvez redistribuer le présent document ou une de ses versions dérivées
sous une licence différente. Cela entraînerait l'annulation des droits de copie,
modification et distribution du document.

En soumettant un document à la présente licence, vous conservez les droits de
propriété intellectuelle liés à votre qualité d'auteur et vous acceptez que les
droits de reproduction, diffusion et modification du document soient régis par la
présente licence.

Évolution de la licence

La Guilde des Doctorants se réserve le droit de faire évoluer la présente
licence. Elle s'assurera de la compatibilité ascendante des différentes
versions. Chaque version de la licence est numérotée.

Sauf mention explicite, il est sous entendu que si le document précise un numéro de
licence, cela signifie que toute version ultérieure convient. Si aucun numéro n'est
précisé, cela signifie que toute version de la licence convient.

La présente licence s'applique à elle-même. Vous pouvez donc la modifier,
à condition d'en changer le titre et de préserver comme section historique
l'introduction. Vous devez également préserver le plan du présent document.

```






\newpage

Licence Publique de Traduction (2000) {.unnumbered}
-------------------------------------

```
Licence Publique de Traduction
(LPT-FR version 1, révision 2)
(c) 2000 I.Robredo, (irm@in3activa.org)

Art.1: Résumé

    La Licence publique de traduction reconnaît dans l'usage de la langue
propre la manifestation d'un droit fondamental de l'individu et contemple Internet
comme un espace libre pour l'échange des idées. Cette licence autorise la
traduction, la modification et l'adaptation en vue de sa publication sur Internet
de l'œuvre qu'elle protège, sous réserve que la version traduite ou dérivée
soit accompagnée de la même licence que la version originale. Sans préjudice de
la tutelle de l'œuvre, l'auteur reconnaît aux bénéficiaires de cette licence des
droits égaux aux siens, selon leurs versions respectives.

Art.2: Définitions

    1. La Licence publique de traduction (LPT) a pour but de promouvoir sur 
Internet la libre circulation en libre accès des œuvres traduisibles qu'elle 
protège.

    2. Conformément à son titre, cette licence protège des œuvres 
traduisibles, c'est à dire des œuvres dont la portée universelle dépend 
nécessairement de leur traduction dans d'autres langues. Au-delà de son 
titre, cette licence admet toutes les nuances de la transformation d'une œuvre 
traduisible et autorise la traduction, la modification et l'adaptation de 
l'œuvre en vue de sa publication sur Internet.

    3. Le propriétaire des droits originaux protégés par cette licence est 
dit « l'auteur ». Le bénéficiaire des droits reconnus par cette licence est 
dit « le bénéficiaire », à la seconde personne (« Vous »). « Internet 
» désigne l'ensemble des réseaux interconnectés et par extension, tout 
logiciel, support ou équipement permettant de consulter l'œuvre publiée sur 
Internet.

    4. Dans ce qui suit, et sauf précision contraire, le terme « œuvre » 
désigne sans distinction la « version originale » et la « version 
transformée » de l'œuvre, c'est à dire, traduite, modifiée ou adaptée en 
vue de sa publication pour l'Internet. La « version originale » désigne la 
version où le nom de l'auteur et du bénéficiaire est le même et la « 
version traduite » ou « version transformée » celle où les noms de 
l'auteur y du bénéficiaire sont différents.

    5. En conformité avec les lois et accords internationaux sur les droits
d'auteur, l'auteur et le bénéficiaire de cette licence déclarent  :

        a. La tutelle de l'auteur sur l'œuvre protégée est de nature
personnelle et unitaire.
        b. La tutelle du bénéficiaire sur sa propre version ne porte pas
préjudice à la tutelle de l'auteur sur son œuvre.
        c. Les droits d'exploitation de l'auteur et du bénéficiaire sur leurs
versions respectives sont les mêmes.

    6. La LPT s'applique à toute œuvre traduisible publiée sur Internet qui
mentionne :

        a. L'identification de l'œuvre et le nom de l'auteur.
        b. Le nom du bénéficiaire, lorsqu'il est différent de celui de
l'auteur.
        c. La référence à la version et au texte intégral de cette licence.
        Par exemple  :
        « LPT-FR version 1 (http://www.in3activa.org/doc/fr/LPT-FR.html) »

Art.3: Licence

    1. D'un commun accord, l'auteur et le bénéficiaire de cette licence
déclarent  :

        a. La publication de l'œuvre sur Internet détermine l'attribution des
droits reconnus par cette licence.
        b. Une nouvelle version de l'œuvre n'annule pas les droits reconnus
par la licence aux versions précédentes de l'œuvre.

    2. Vous êtes libre de traduire, de modifier et de publier l'œuvre sur
Internet à partir de n'importe quelle version originale ou dérivée de l'œuvre, y
compris la version originale de l'auteur, sous réserve que la version publiée soit
accompagnée de la même licence.

    3. Vous êtes libre de traduire ou réviser l'œuvre vers la même ou vers
une autre langue, et d'ajouter votre nom à la suite du nom de l'auteur. Au regard de
cette licence, toute version transformée de l'œuvre est entendue publiée à partir
de la version originale. Le nom de l'auteur ne peut être modifié d'une version à
l'autre.

    4. Vous n'êtes pas libre de publier sur Internet des parties isolées de
l'œuvre. Les versions partiellement traduites ou modifiées mentionneront clairement
ce fait, à la suite du nom du bénéficiaire. Les parties traduites ou modifiées
figureront en lieu et place du texte original dans l'œuvre intégrale.

    5. Vous êtes libre de percevoir une rémunération pour la traduction, la
modification ou la publication de cette œuvre sur Internet, sous réserve que :

        a. l'œuvre soit publiée sur Internet sous les mêmes conditions de
licence ;
        b. le libre accès a la consultation de l'œuvre sur Internet ne soit
pas soumis à l'identification préalable ou au paiement de quantités par le
bénéficiaire.

    6. L'auteur de l'œuvre se réserve à son seul profit et à celui des
bénéficiaires de cette licence, l'intégralité des droits commerciaux de
l'œuvre, toutes versions confondues, pour tous les pays, pour toutes les langues,
et ce, quelle qu'en soit la forme, le support ou la présentation physique ou même
juridique qui n'aurait pas comme finalité dernière la publication de l'œuvre sur
Internet.

    7. En l'absence d'un cadre de procédures auquel adhérer, l'auteur se
réserve le droit d'autoriser selon son seul critère la version ou les versions
commercialisées dans les langues et les pays qu'il décidera, ainsi que celui
de désigner les bénéficiaires qui partageront avec lui les droits commerciaux
dérivés de l'œuvre, selon des conditions établies d'un commun accord.

Art.4: Restrictions

    1. Vous n'êtes pas libre de transformer, c'est à dire, de traduire, de
modifier ou d'adapter cette œuvre en vue de sa publication, quels qu'en soient le
support, la présentation ou format, par aucun moyen existant ou futur, SAUF DANS
LE RESPECT DES DISPOSITIONS de cette licence. Plus particulièrement, vous n'êtes
pas libre, sans le consentement du propriétaire des droits originaux de l'œuvre
et dans les limites prévues par la Loi de votre pays, de traduire, de modifier ou
de publier cette œuvre, de la fixer sur n'importe quel support, de la diffuser par
aucun moyen ou procédé de reproduction mécanique, hertzien ou numérique. Toute
autre tentative de transformation de l'œuvre en dehors des conditions prévues par
cette licence EST INTERDITE et signifie la cessation immédiate des droits qui vous
sont reconnus.
    2. Toute traduction, modification ou adaptation de l'œuvre en vue de sa
publication sur Internet signifie une cession automatique au profit des
bénéficiaires d'une licence émise directement par le titulaire des droits
originaux de l'œuvre. Il ne vous est pas permis d'appliquer des restrictions
ou d'altérer en aucune façon les droits que cette licence reconnaît à ses
bénéficiaires.
    3. Votre acceptation de cette licence n'est pas requise et il n'est donc
pas nécessaire d'y apposer votre signature. Néanmoins, cette licence est la
seule qui vous autorise à transformer, c'est à dire, à traduire, à modifier
et à adapter l'œuvre en vue de sa publication pour l'Internet. Ces actions
sont interdites par les lois nationales et internationales de propriété
intellectuelle. Par conséquent, le fait de traduire, de modifier ou d'adapter
l'œuvre en vue de sa publication sur Internet signifie votre acceptation de toutes
et de chacune des dispositions de cette licence.
    4. Vous ne pourrez en aucun cas passer outre les termes et conditions de
cette licence si, par suite d'une sentence d'un tribunal ou d'une dénonciation
pour infraction aux lois de votre pays, vous vous trouvez dans l'obligation (par
consentement ou pour toute autre raison) d'accepter des dispositions contrevenant
les conditions de cette licence. Si au moment de diffuser l'œuvre, impossibilité
vous est faite de respecter toutes et chacune des conditions de cette licence, la
conséquence serait l'interdiction absolue de traduire, de modifier ou d'adapter
cette œuvre sans le consentement du propriétaire des droits originaux cédés par
la licence.
    5. Dans le cas où les termes et conditions de cette licence seraient
soumis à des restrictions dans certains pays, en raison de dispositions non prévues
par cette licence ou par des lois ou traités internationaux en vigueur dans votre
pays, le propriétaire des droits originaux de la licence pourra publier la liste des
pays exclus par cette licence. Si ce cas vous est applicable, alors la liste de ces
pays sera présumée incorporée au texte de cette licence.
    6. Cette licence prévoit la possibilité d'en réviser ou d'en compléter
le contenu. Les licences futures respecteront l'esprit de la version présente,
mais pourront s'en éloigner sur le détail afin de répondre à des questions ou à
des problèmes nouveaux. Chacune des versions de la licence sera accompagnée d'un
numéro permettant de la distinguer des autres. Si l'œuvre en votre possession fait
référence à une version particulière de cette licence, accompagnée de la mention
« et suivantes » ou « toute version ultérieure », vous pourrez choisir entre les
conditions décrites par celle-ci ou par toute autre version ultérieure. Si l'œuvre
ne fait référence à aucun numéro de version, vous pourrez opter pour n'importe
laquelle des versions publiées de cette licence.
    7. Les versions traduites de cette licence utiliseront comme référence la
version française publiée par le propriétaire des droits originaux de la
licence. Les versions originale et traduites de la licence seront identifiées par
un code de langue, choisit conformément à la nomenclature internationale, suivi du
numéro de version.
    6. Si vous souhaitez incorporer cette œuvre au sein d'autres œuvres
libres dont les conditions seraient différentes, veuillez d'abord contacter par
écrit l'auteur de la licence. La décision sera prise dans le double objectif de
promouvoir cette licence, et de garantir le libre accès et la libre circulation des
idées, dans le respect du droit de tout individu à s'exprimer et à communiquer
dans sa propre langue.

Art.5: Limitation de responsabilité

    1. DANS LES LIMITES PRÉVUES PAR LA LOI, CETTE œUVRE VOUS EST OFFERTE SANS
AUCUNE GARANTIE. SAUF INDICATION CONTRAIRE DE L'AUTEUR, L'œUVRE EST LIVRÉE
« EN L'ÉTAT », SANS GARANTIE D'AUCUNE SORTE, EXPLICITE OU IMPLICITE. TOUTE
GARANTIE COMMERCIALE IMPLICITE OU D'ADAPTATION AUX OBJECTIFS RECHERCHÉS EST
EXCLUE. VOUS ACCEPTEZ TOUTE LA RESPONSABILITÉ LIÉE À LA QUALITÉ OU LA FINALITÉ
DE L'œUVRE. VOUS ACCEPTEZ À VOS DÉPENDS TOUS LES FRAIS POUVANT DÉRIVER D'UN
QUELCONQUE PRÉJUDICE OU CONSÉQUENCE NON SOUHAITÉS INDUITS PAR L'œUVRE.
    2. DANS LES LIMITES PRÉVUES PAR LA LOI OU ACCEPTEES PAR ÉCRIT PAR
L'AUTEUR, EN AUCUN CAS LE PROPIÉTAIRE OU LES BÉNÉFICIAIRES DE CETTE LICENCE NE
SERONT TENUS POUR RESPONSABLES DES DOMMAGES QU'ILS SOIENT GÉNÉRAUX, PARTICULIERS,
ACCIDENTELS OU INDUITS, QUI POURRAIENT DÉRIVER DE L'USAGE, DE LA TRADUCTION OU DE LA
DIFUSION DE CETTE œUVRE ET CELA, MÊME SI CE PROPRIÉTAIRE OU BÉNÉFICIAIRES VOUS
ONT INFORMÉ DE LA POSSIBILITÉ DE CES DOMMAGES.

```





\newpage

Open Game License (2000) {.unnumbered}
------------------------

```
OPEN GAME LICENSE Version 1.0a

The following text is the property of Wizards of the Coast, Inc. and is Copyright
2000 Wizards of the Coast, Inc ("Wizards"). All Rights Reserved.

1. Definitions: (a)"Contributors" means the copyright and/or trademark owners who
have contributed Open Game Content; (b)"Derivative Material" means copyrighted
material including derivative works and translations (including into other computer
languages), potation, modification, correction, addition, extension, upgrade,
improvement, compilation, abridgment or other form in which an existing work may
be recast, transformed or adapted; (c) "Distribute" means to reproduce, license,
rent, lease, sell, broadcast, publicly display, transmit or otherwise distribute;
(d)"Open Game Content" means the game mechanic and includes the methods, procedures,
processes and routines to the extent such content does not embody the Product
Identity and is an enhancement over the prior art and any additional content clearly
identified as Open Game Content by the Contributor, and means any work covered
by this License, including translations and derivative works under copyright
law, but specifically excludes Product Identity. (e) "Product Identity" means
product and product line names, logos and identifying marks including trade dress;
artifacts; creatures characters; stories, storylines, plots, thematic elements,
dialogue, incidents, language, artwork, symbols, designs, depictions, likenesses,
formats, poses, concepts, themes and graphic, photographic and other visual or
audio representations; names and descriptions of characters, spells, enchantments,
personalities, teams, personas, likenesses and special abilities; places, locations,
environments, creatures, equipment, magical or supernatural abilities or effects,
logos, symbols, or graphic designs; and any other trademark or registered trademark
clearly identified as Product identity by the owner of the Product Identity, and
which specifically excludes the Open Game Content; (f) "Trademark" means the logos,
names, mark, sign, motto, designs that are used by a Contributor to identify itself
or its products or the associated products contributed to the Open Game License by
the Contributor (g) "Use", "Used" or "Using" means to use, Distribute, copy, edit,
format, modify, translate and otherwise create Derivative Material of Open Game
Content. (h) "You" or "Your" means the licensee in terms of this agreement.

2. The License: This License applies to any Open Game Content that contains a notice
indicating that the Open Game Content may only be Used under and in terms of this
License. You must affix such a notice to any Open Game Content that you Use. No
terms may be added to or subtracted from this License except as described by the
License itself. No other terms or conditions may be applied to any Open Game Content
distributed using this License.

3.Offer and Acceptance: By Using the Open Game Content You indicate Your acceptance
of the terms of this License.

4. Grant and Consideration: In consideration for agreeing to use this License, the
Contributors grant You a perpetual, worldwide, royalty-free, non-exclusive license
with the exact terms of this License to Use, the Open Game Content.

5.Representation of Authority to Contribute: If You are contributing original
material as Open Game Content, You represent that Your Contributions are Your
original creation and/or You have sufficient rights to grant the rights conveyed by
this License.

6.Notice of License Copyright: You must update the COPYRIGHT NOTICE portion of this
License to include the exact text of the COPYRIGHT NOTICE of any Open Game Content
You are copying, modifying or distributing, and You must add the title, the copyright
date, and the copyright holder's name to the COPYRIGHT NOTICE of any original Open
Game Content you Distribute.

7. Use of Product Identity: You agree not to Use any Product Identity, including
as an indication as to compatibility, except as expressly licensed in another,
independent Agreement with the owner of each element of that Product Identity. You
agree not to indicate compatibility or co-adaptability with any Trademark or
Registered Trademark in conjunction with a work containing Open Game Content
except as expressly licensed in another, independent Agreement with the owner of
such Trademark or Registered Trademark. The use of any Product Identity in Open
Game Content does not constitute a challenge to the ownership of that Product
Identity. The owner of any Product Identity used in Open Game Content shall retain
all rights, title and interest in and to that Product Identity.

8. Identification: If you distribute Open Game Content You must clearly indicate
which portions of the work that you are distributing are Open Game Content.

9. Updating the License: Wizards or its designated Agents may publish updated
versions of this License. You may use any authorized version of this License to copy,
modify and distribute any Open Game Content originally distributed under any version
of this License.

10 Copy of this License: You MUST include a copy of this License with every copy of
the Open Game Content You Distribute.

11. Use of Contributor Credits: You may not market or advertise the Open Game
Content using the name of any Contributor unless You have written permission from the
Contributor to do so.

12 Inability to Comply: If it is impossible for You to comply with any of the terms
of this License with respect to some or all of the Open Game Content due to statute,
judicial order, or governmental regulation then You may not Use any Open Game
Material so affected.

13 Termination: This License will terminate automatically if You fail to comply with
all terms herein and fail to cure such breach within 30 days of becoming aware of the
breach. All sublicenses shall survive the termination of this License.

14 Reformation: If any provision of this License is held to be unenforceable, such
provision shall be reformed only to the extent necessary to make it enforceable.

15 COPYRIGHT NOTICE

Open Game License v 1.0 Copyright 2000, Wizards of the Coast, Inc.
```







\newpage

Trackers Public License (2000) {.unnumbered}
------------------------------

```
Trackers Public License (TPL) Version 0.02 by Sven Windisch, Germany
windischs@gmx.de

-= Preface =-

Any public license, given for Software, has its biggest interest on showing,
that the Software, distributed under this license, is given with absolutely no
warranty. Rather than this, a public license for music should define Copyrights for
the musician, who composes a tracked piece of music, the musician, who wants to remix
that piece and for the audience, which wants to hear it and maybe wants to copy it
for its own use or for friends or as a gift.

We have to distinguish between the samples and the work, with which the samples
were put together. A note about the origin of the samples is indisputable, so every
tracker has to give the audience knowledge about the origin of the samples they are
hearing.

The tracking-process itself is not so complicated, because it's clear, that this
comes from the author of the piece of music.

If you want to know more about the Copyright around tracked music look at:
http://www.united-trackers.org/resources/copyright/

I hope that this TPL will show, that the right for free information research, claimed
in every democracy of the world, is worth to be realized.


20th August 2000,

Sven Windisch


-= Chapter I =-

Downloading, Saving, Hearing

The Download of this piece oft tracked music is free. This means, that you can
download it without any fear of violating copyrights. You can save it on your
Harddisk or on any other place you want to save it. BUT ONLY FOR YOUR OWN USE ! (For
public use see Ch.  III) And of course you, and only you, can hear it as often as
you want.

-= Chapter II =-

Changing, Remixing

You may change or remix the Track as you want, as long as you make a note of the Name
of the Author of the original and of the changes you made.You have to redistribute
your remix under the TPL (See HowTo). Otherway it's not allowed to change or remix
it.

-= Chapter III =-

Public performance

You are allowed to perform this piece of music to the public, as long as you remark
the Author. If you want to perform any remix or sth. that relates to the original you
have to remark the Name of the Author of the original and the Name of the piece of
music you want to perform.

-= HowTo =-

What to do, if I want to distribute my tracked music under the TPL?

First of all, it's necessary, that this is really YOUR piece of tracked music.
If there's any violation of any law, it's not allowed to distribute this music under
the TPL.  Then you have to give a remark of the origin of the samples you used, along
with the track. You have to mark your track with your name (artist name is enough)
and an adress, where the audience can reach you (e-mail adress is recommended). A
very important point is, to make clear, that this piece of tracked music is
distributed under the TPL, so you should remark it on a place, it could be seen. Then
you have to distribute it on a place where mostly everyone interested could get it
for free. (Internet should be the first place)
```







\newpage

Common Documentation License (2001) {.unnumbered}
-----------------------------------

```
Common Documentation License

Version 1.0 - February 16, 2001

Copyright © 2001 Apple Computer, Inc.

Permission is granted to copy and distribute verbatim copies of this License, but
changing or adding to it in any way is not permitted.

Please read this License carefully before downloading or using this material. By
downloading or using this material, you are agreeing to be bound by the terms of this
License. If you do not or cannot agree to the terms of this License, please do not
download or use this material.

0. Preamble. The Common Documentation License (CDL) provides a very simple and
consistent license that allows relatively unrestricted use and redistribution
of documents while still maintaining the author's credit and intent. To preserve
simplicity, the License does not specify in detail how (e.g. font size) or where
(e.g. title page, etc.) the author should be credited. To preserve consistency,
changes to the CDL are not allowed and all derivatives of CDL documents are required
to remain under the CDL. Together, these constraints enable third parties to easily
and safely reuse CDL documents, making the CDL ideal for authors who desire a wide
distribution of their work. However, this means the CDL does not allow authors to
restrict precisely how their work is used or represented, making it inappropriate for
those desiring more finely-grained control.

1. General; Definitions. This License applies to any documentation, manual or other
work that contains a notice placed by the Copyright Holder stating that it is subject
to the terms of this Common Documentation License version 1.0 (or subsequent version
thereof) ("License"). As used in this License:

1.1 "Copyright Holder" means the original author(s) of the Document or other owner(s)
of the copyright in the Document.

1.2 "Document(s)" means any documentation, manual or other work that has been
identified as being subject to the terms of this License.

1.3 "Derivative Work" means a work which is based upon a pre-existing Document,
such as a revision, modification, translation, abridgment, condensation, expansion,
or any other form in which such pre-existing Document may be recast, transformed,
or adapted.

1.4 "You" or "Your" means an individual or a legal entity exercising rights under
this License.

2. Basic License. Subject to all the terms and conditions of this License, You
may use, copy, modify, publicly display, distribute and publish the Document and
your Derivative Works thereof, in any medium physical or electronic, commercially
or non-commercially; provided that: (a) all copyright notices in the Document are
preserved; (b) a copy of this License, or an incorporation of it by reference in
proper form as indicated in Exhibit A below, is included in a conspicuous location in
all copies such that it would be reasonably viewed by the recipient of the Document;
and (c) You add no other terms or conditions to those of this License.

3. Derivative Works. All Derivative Works are subject to the terms of this
License. You may copy and distribute a Derivative Work of the Document under the
conditions of Section 2 above, provided that You release the Derivative Work under
the exact, verbatim terms of this License (i.e., the Derivative Work is licensed
as a "Document" under the terms of this License). In addition, Derivative Works of
Documents must meet the following requirements:

    (a) All copyright and license notices in the original Document must be preserved.

    (b) An appropriate copyright notice for your Derivative Work must be added
    adjacent to the other copyright notices.

    (c) A statement briefly summarizing how your Derivative Work is different from
    the original Document must be included in the same place as your copyright
    notice.

    (d) If it is not reasonably evident to a recipient of your Derivative Work
    that the Derivative Work is subject to the terms of this License, a statement
    indicating such fact must be included in the same place as your copyright notice.

4. Compilation with Independent Works. You may compile or combine a Document or its
Derivative Works with other separate and independent documents or works to create
a compilation work ("Compilation"). If included in a Compilation, the Document or
Derivative Work thereof must still be provided under the terms of this License, and
the Compilation shall contain (a) a notice specifying the inclusion of the Document
and/or Derivative Work and the fact that it is subject to the terms of this License,
and (b) either a copy of the License or an incorporation by reference in proper form
(as indicated in Exhibit A). Mere aggregation of a Document or Derivative Work with
other documents or works on the same storage or distribution medium (e.g. a CD-ROM)
will not cause this License to apply to those other works.

5. NO WARRANTY. THE DOCUMENT IS PROVIDED 'AS IS' BASIS, WITHOUT WARRANTY OF ANY
KIND, AND THE COPYRIGHT HOLDER EXPRESSLY DISCLAIMS ALL WARRANTIES AND/OR CONDITIONS
WITH RESPECT TO THE DOCUMENT, EITHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING,
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES AND/OR CONDITIONS OF MERCHANTABILITY,
OF SATISFACTORY QUALITY, OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY, OF QUIET
ENJOYMENT, AND OF NONINFRINGEMENT OF THIRD PARTY RIGHTS.

6. LIMITATION OF LIABILITY. UNDER NO CIRCUMSTANCES SHALL THE COPYRIGHT HOLDER BE
LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES ARISING OUT OF
OR RELATING TO THIS LICENSE OR YOUR USE, REPRODUCTION, MODIFICATION, DISTRIBUTION
AND/OR PUBLICATION OF THE DOCUMENT, OR ANY PORTION THEREOF, WHETHER UNDER A THEORY
OF CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE,
EVEN IF THE COPYRIGHT HOLDER HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES AND
NOTWITHSTANDING THE FAILURE OF ESSENTIAL PURPOSE OF ANY REMEDY.

7. Trademarks. This License does not grant any rights to use any names, trademarks,
service marks or logos of the Copyright Holder (collectively "Marks") and no such
Marks may be used to endorse or promote works or products derived from the Document
without the prior written permission of the Copyright Holder.

8. Versions of the License. Apple Computer, Inc. ("Apple") may publish revised
and/or new versions of this License from time to time. Each version will be given a
distinguishing version number. Once a Document has been published under a particular
version of this License, You may continue to use it under the terms of that
version. You may also choose to use such Document under the terms of any subsequent
version of this License published by Apple.  No one other than Apple has the right to
modify the terms applicable to Documents created under this License.

9. Termination. This License and the rights granted hereunder will terminate
automatically if You fail to comply with any of its terms. Upon termination,
You must immediately stop any further reproduction, modification, public display,
distr ibution and publication of the Document and Derivative Works. However, all
sublicenses to the Document and Derivative Works which have been properly granted
prior to termination shall survive any termination of this
 License. Provisions which, by their nat ure, must remain in effect beyond the
 termination of
this License shall survive, including but not limited to Sections 5, 6, 7, 9 and 10.

10. Waiver; Severability; Governing Law. Failure by the Copyright Holder to enforce
any provision of this License will not be deemed a waiver of future enforcement of
that or any other provision. If for any reason a court of competent jurisdiction
finds any provision of this License, or portion thereof, to be unenforceable, that
provision of the License will be enforced to the maximum extent permissible so as
to effect the economic benefits and intent of the parties, and the remainder of this
License will continue in full force and effect. This License shall be governed by the
laws of the United States and the State of California, except that body of California
law concerning conflicts of law.

EXHIBIT A

The proper form for an incorporation of this License by reference is as follows:

"Copyright (c) [year] by [Copyright Holder's name]. This material has been released
under and is subject to the terms of the Common Documentation License, v.1.0,
the terms of which are hereby incorporated by reference. Please obtain a copy of
the License at http://www.opensource.apple.com/cdl/ and read it before using this
material. Your use of this material signifies your agreement to the terms of the
License."

```






\newpage

EFF Open Audio License (2001) {.unnumbered}
-----------------------------

```
EFF Open Audio License: Version 1.0

    I. Preamble II. Terms of Use III. How to Use this License

I. PREAMBLE Principles

Digital technology and the Internet can empower artists to reach a worldwide audience
and to build upon each other's ideas and imagination with extremely low production
and distribution costs. Many software developers, through both the open source
software initiative and the free software movement, have long taken advantage of
these facts to create a vibrant community of shared software that benefits creators
and the public.

EFF's Open Audio License provides a legal tool that borrows from both movements
providing freedom and openness to use music and other expressive works in new
ways. It allows artists to grant the public permission to copy, distribute, adapt,
and publicly perform their works royalty-free as long as credit is given to the
creator as the Original Author.

As in the software communities, this license is intended to help foster a community
of creators and performers who are free to share and build on each others' work
while freeing their audience to share works that they enjoy with others, all for the
purpose of creating a rich and vibrant public commons.

More specifically, this license is designed to serve as a tool of freedom for
artists who wish to reach one another and new fans with their original works. It
allows musicians to collaborate in creating a pool of "open audio" that can be
freely modified, exchanged, and utilized in new ways. Artists can use this license
to promote themselves and take advantage of the new possibilities for empowerment
and independence that technology provides. It also allows the public to experience
new music, and connect directly with artists, as well as enable "super distribution"
where the public is encouraged to copy and distribute a work, adding value to the
artist's reputation while experiencing a world of new music never before available.
Why is the EFF advocating a license?

Because, despite the fact that we are uneasy with the licensing, as opposed to sale,
of both music and software, we see this particular license as a tool of freedom. Our
goal is to use the tools of copyright to free artists and audiences from the portion
of current copyright law that seems, to us, to be getting in the way of copyright's
original purpose -- the creation of a vibrant public commons of music that we all can
enjoy and that artists can build upon. As part of it, we hope to demonstrate some
of what we believe should be the best practices in licenses, including respect for
the rights and limitations of copyright law including fair use, first sale rights,
as well as consumer protection laws and of course freedom of speech. The aim of
this license is to use copyright tools to achieve copyright's stated objectives of
spreading knowledge and culture while preserving incentives for the author.

For legal purposes, this document is the official license under which Open Audio is
made available for public use. The original version of this document may be found at:
 http://www.eff.org/IP/Open_licenses/eff_oal.html

Specific terms and conditions for accessing, copying, distribution, adaptation,
public performance, and attribution follow.  II. TERMS AND CONDITIONS FOR USE:
Access, Copying, Distribution, Public Performance, Adaptation, and Attribution

This license applies to any work offered by the Original Author(s) with a notice
indicating that it is released under the terms of the EFF Open Audio License,
"(O)". If used in conjunction with a sound recording (whether in digital or analog
form), this license encompasses the copyright in both the sound recording (the
"master" rights) and the underlying musical composition (the "songwriter" rights).

The Original Author retains the copyrights to works released under this license,
but grants the worldwide public permission to use the work in the ways authorized
herein. Activities other than those specifically addressed below are outside the
scope of this license.

    1. Access, Reproduction, Distribution, Modification, and Performance
    Rights. Subject to the terms and conditions of this license, the Original Author
    irrevocably and perpetually grants to the public authorization to freely access,
    copy, distribute, modify, create derivative works from, and publicly perform the
    work released under this license in any medium or format, provided that Original
    Author attribution be included with any copies distributed or public performances
    of the work, as well as any derivative works based on the work, as further
    described below.

    2. Original Author Attribution Requirement. Original Author attribution is
    generally defined as a method in the regular course of dealing that reasonably
    conveys to the recipient of a copy or performance the following information:
    (1) The notice "(O)" that indicates the work is released under the EFF Open Audio
    license; (2) the identity of the Original Author; (3) the title of the work (at
    Original Author's option); and (4) how the first listed Original Author may be
    contacted (at Original Author's option).

    Where a common, widely-adopted method for attribution is available (such as
    ID3 tagging for MP3 files), Original Author attribution should be implemented
    using the common, widely-adopted method. In other circumstances, Original Author
    attribution may be implemented in any reasonable fashion, such as by including
    attribution in the public performance, or affixing it to the physical media,
    or embedding it in the digital file. See the Suggested Guidelines for general
    attribution requirements for giving proper credit to the work's Original Author
    in differing circumstances.

    3. Agree Not to Limit Others' Use. Any new work that in whole or in part contains
    or is derived from a work (or part thereof) made available under this license,
    must itself be licensed as a whole under the terms of this license.

    Notwithstanding the foregoing, mere aggregation on a volume of a storage or
    distribution medium of an independently created work with one that is made
    available under this license does not bring the other work under the scope of
    this license. It is not the intent of this section to contest the rights of
    others in works created entirely by them; rather, the intent is to exercise the
    right to control the distribution of derivative or collective works based upon a
    work subject to this license.

    4. Acceptance of Terms. Because you have not signed this license, you cannot be
    required to accept it. But nothing besides this license grants you authorization
    to copy, distribute, adapt, or publicly perform royalty-free the copyrighted
    works released under it. These activities are prohibited by law without a license
    or other contractual right granted by the copyright owner. By exercising one of
    the rights granted herein you indicate your acceptance of this license and agree
    to be bound by all its terms and conditions.

    5. License Version. This license is Version 1.0. New versions of this license
    will be published from time to time at: Anyone who releases a work under the
    license without specifying a version number allows the recipient to use the work
    subject to the then-current version of this license

    6. Civil Liberties Unrestricted. Nothing in this license is intended to reduce,
    limit, or restrict any fair use, the first sale doctrine, or the public side
    of the copyright bargain under copyright law, or to in any other way limit any
    rights bestowed under consumer protection or other applicable laws.

    7. Warranty. By offering an original work for public release under this license,
    the Original Author warrants that (i) s/he has the power and authority to grant
    the rights conveyed herein, and (ii) use of the work within the scope of this
    license will not infringe the copyright of any third party.

III. HOW TO USE THIS LICENSE

If you are a musician, band, or other artist and you want your creative works to be
experienced by the widest audience possible and touch the hearts and minds of the
greatest number of people around the world, the EFF Open Audio License allows your
fans and supporters to market and distribute your work through viral marketing that
creates attention and adds value to your identity. You can also help build a common
pool of creative expression that can be accessed and improved upon by all of society.

To do so, convey or affix the following information to or about the copy or
performance of the work:

    The designation "(O)", representing "open" which indicates that the Original
    Author(s) have released the work subject to the terms and conditions of this
    public license;

    Name of work's Original Author(s) (both the performer and the song writer);

    Name or title of work (at option of author);

    First Original Author's specified contact means usually an email or Internet
    address (at option of author);

    notice, year created; and license version number.

Examples:

    (O) Future Tribe "Gaian Smile" www.VirtualRecordings.com 2001 V.1.0

or

    (O) Future Tribe "Imitatio Mundi" future@virtualrecordings.com 2001 V.1.0

This license is designed to provide artists with a mechanism to promote their
creative talents and identity to millions of people through releasing certain
recordings to the public. It is also designed to serve as a tool to allow musicians
to experiment with new business models that do

not depend solely on a payment of fee-per-copy. Changing times require artists
be creative in devising new business models for assuring payment and adequate
compensation for their important contributions to society.
```







\newpage

HyperNietzsche Licenses (2001) {.unnumbered}
------------------------------

```
1. La Licence HyperNietzsche
----------------------------

Article 1 : Parties

La présente licence est concédée par ...............  demeurant à
..................  ci-dessous désigné l’ « auteur », à l’association
HyperNietzsche ...............  ci-dessous désigné l’ « HyperNietzsche »,

Article 2 : Objet

1. La présente licence a pour objet l’œuvre suivante : ................

2. Par la présente licence, l’auteur permet à l’association HyperNietzsche
de publier l’œuvre désignée ci-dessus sur son site Internet. Cette œuvre,
sur le site HyperNietzsche, sera soumise à la licence d’utilisation suivante :
........................

3. À ce titre, l’auteur, par la présente licence, transmet à l’HyperNietzsche,
à titre non exclusif, et pour la durée prévue à l’article 5 ci-dessous,
ses droits de reproduction et de représentation sur son œuvre, sur tout support
numérique, et notamment le réseau Internet.

4. L’auteur transmet également son droit de traduction sur l’œuvre. Si
l’œuvre est traduite, l’HyperNietzsche s’engage à fournir gratuitement à
l’auteur un exemplaire numérique de la traduction, et à lui transmettre les
droits d’exploitation sur cette même traduction.

5. La présente licence est à titre gratuit.

Article 4 : Obligations de l’HyperNietzsche

1. L’association HyperNietzsche reconnaît que la présente licence est conclue à
des seules fins d’enseignement et de recherche, et qu’elle ne peut donc céder
les droits d’exploitation transmis temporairement par l’auteur, à titre gratuit
ou onéreux, et pour une autre finalité.

2. L’HyperNietzsche s’engage à faire figurer en toute occasion le nom de
l’auteur sur les exemplaires de l’œuvre.

3. De façon plus générale, l’HyperNietzsche s’engage à respecter le droit
moral de l’auteur, et notamment son droit au respect à l’intégrité de
l’œuvre.

4. L’HyperNietzsche s’engage à mettre en œuvre sur son site le choix éditorial
opéré par l’auteur sur l’œuvre objet de la présente licence, et signalé
ci-dessus à l’article 2 . 2.

Article 5 : Durée

1. La durée de la présente licence est fixée à dix ans, à compter de la date de
sa formation.

2. La présente licence pourra être reconduite après le terme de dix ans. Si
l’auteur ne signale pas expressément à l’HyperNietzsche, par lettre, courrier
électronique ou autres, qu’il entend retirer son œuvre du site, la reconduction
pour dix ans supplémentaires sera alors tacitement confirmée.

Article 6 : Loi applicable

1. Tout différend pouvant naître à l’occasion de la présente licence sera
soumis à une conciliation préalablement à tout recours devant les tribunaux.

2. La loi française est la seule loi compétente pour la présente licence, sans
préjudice de l’éventuelle application des conventions internationales relatives
au droit d’auteur.

2. La Licence Free Knowledge
----------------------------

Article 1 : Parties

La présente licence est concédée par ..............., dans les conditions
décrites ci-dessous, à tout utilisateur du site HyperNietzsche qui reproduira le
texte objet de la licence en dehors des exceptions au droit d’auteur prévues par
le Code de la propriété intellectuelle.

Article 2 : Formation

1. La présente licence se forme par voie électronique : en télédéchargeant
l’œuvre qui en est l’objet, l’utilisateur l’accepte tacitement.

2. La date de formation de la présente licence est la date du télédéchargement de
l’œuvre sur le site HyperNietzsche.

Article 3 : Objet

1. La présente licence a pour objet l’œuvre suivante : ................

2. L’auteur cède à l’utilisateur, à titre non-exclusif, et pour la durée de
la présente licence, ses droits de reproduction, de représentation et de traduction
sur son œuvre, sur tout support, y compris numérique, et dans tout pays.

3. La présente licence est à titre gratuit.

Article 4 : Obligations de l’utilisateur-cessionnaire des droits

1. L’utilisateur s’engage à faire figurer en toute occasion le nom de
l’auteur, et son adresse de courrier électronique, sur les exemplaires de
l’œuvre.

2. De façon plus générale, l’utilisateur s’engage à respecter le droit moral
de l’auteur, et notamment son droit au respect à l’intégrité de l’œuvre.

Article 5 : Durée

1. La durée de la présente licence est fixée à dix ans, à compter de la date de
sa formation.

2. La présente licence pourra être reconduite après ce premier terme de dix ans :
l’accord exprès de l’auteur est pour cela exigé, et pourra lui être demandé
par courrier électronique.

Article 6 : Loi applicable

1. Tout différend pouvant naître à l’occasion du présent contrat sera soumis à
une conciliation préalablement à tout recours devant les tribunaux.

2. La loi française est la seule loi compétente pour la présente licence, sans
préjudice de l’éventuelle application des conventions internationales relatives
au droit d’auteur.


3. La Licence Open Knowledge
----------------------------

Article 1 : Parties

La présente licence est concédée par ..............., dans les conditions
décrites ci-dessous, à tout utilisateur du site HyperNietzsche qui reproduira le
texte objet de la licence en dehors des exceptions au droit d’auteur prévues par
le Code de la propriété intellectuelle.

Article 2 : Formation

1. La présente licence se forme par voie électronique : en télédéchargeant
l’œuvre qui en est l’objet, l’utilisateur l’accepte tacitement.

2. La date de formation de la présente licence est la date du télédéchargement de
l’œuvre sur le site HyperNietzsche.

Article 3 : Objet

1. La présente licence a pour objet l’œuvre suivante : ................

2. Si l’utilisation de l’œuvre est à des fins d’enseignement et de recherche,
l’auteur cède à l’utilisateur, à titre non exclusif, et pour la durée de la
présente licence, ses droits de reproduction et de représentation sur son œuvre,
sur tout support, y compris numérique. La présente licence est alors à titre
gratuit.

3. L’auteur se réserve le droit de céder ses droits de reproduction et de
représentation sur son œuvre, dans tous les autres cas. La cession pourra être à
titre gratuit ou onéreux : l’accord exprès de l’auteur doit pour cela lui être
demandé.

4. Dans ce dernier cas, les modalités de la cession à titre onéreux seront
précisées par l’auteur sur le site HyperNietzsche.

Article 4 : Obligations de l’utilisateur-cessionnaire des droits

1. L’utilisateur reconnaît que la présente licence est à des seules fins
d’enseignement et de recherche : il ne peut céder les droits reçus temporairement
à titre onéreux pour une autre finalité.

2. L’utilisateur s’engage à faire figurer en toute occasion le nom de
l’auteur, et son adresse de courrier électronique, sur les exemplaires de
l’œuvre, ainsi que la mention « usage à des fins d’enseignement et de
recherche ».

3. De façon plus générale, l’utilisateur s’engage à respecter le droit moral
de l’auteur, et notamment son droit au respect à l’intégrité de l’œuvre.

Article 5 : Droits de l’utilisateur

1. Dans tous les cas, l’utilisateur a notamment le droit de :

a) procéder à une copie, partielle ou intégrale, pour un usage strictement privé
;

b) procéder à des courtes citations de la présente œuvre ;

c) procéder à son analyse.

2. Dans tous les cas, l’auteur a droit au respect de son droit moral, et notamment
:

d) de son droit au nom ;

e) de son droit au respect à l’intégrité de l’œuvre.

Article 6 : Durée

1. La durée de la présente licence est fixée à dix ans, à compter de la date de
sa formation.

2. La présente licence pourra être reconduite après ce premier terme de dix ans :
l’accord exprès de l’auteur est pour cela exigé.

Article 7 : Loi applicable

1. Tout différend pouvant naître à l’occasion du présent contrat sera soumis à
une conciliation préalablement à tout recours devant les tribunaux.

2. La loi française est la seule loi compétente pour la présente licence, sans
préjudice de l’éventuelle application des conventions internationales relatives
au droit d’auteur.

4. La licence Limited Knowledge
-------------------------------

Article 1 : Parties

La présente licence est concédée par ..............., dans les conditions
décrites ci-dessous, à tout utilisateur du site HyperNietzsche qui reproduira le
texte objet de la licence dans le cadre des exceptions au droit d’auteur prévues
par le Code de la propriété intellectuelle.

Article 2 : Formation

1. La présente licence se forme par voie électronique : en télédéchargeant
l’œuvre qui en est l’objet, l’utilisateur l’accepte tacitement.

2. La date de formation de la présente licence est la date du télédéchargement de
l’œuvre sur le site HyperNietzsche.

Article 3 : Objet

1. La présente licence a pour objet l’œuvre suivante : ................

2. La présente licence n’opère pas cession des droits d’auteur. L’utilisateur
détient uniquement les droits consentis au titre des exceptions légales posées
par l’article L. 122-3 du Code de la propriété intellectuelle, et présentés à
l’article 5 ci-dessous.

3. Toute mise en réseau et toute rediffusion, sous quelque forme du présent texte,
sont donc interdites, sauf autorisation expresse de l’auteur.

4. L’auteur se réserve le droit de céder ses droits de reproduction, de
traduction et de représentation sur son œuvre, à titre gratuit ou onéreux. Les
modalités de la cession des droits d’exploitation sur l’œuvre seront
précisées par l’auteur sur le site HyperNietzsche.

Article 4 : Obligations de l’utilisateur-cessionnaire des droits

1. L’utilisateur s’engage à faire figurer en toute occasion le nom de
l’auteur, et son adresse de courrier électronique, sur les exemplaires de
l’œuvre.

2. De façon plus générale, l’utilisateur s’engage à respecter le droit moral
de l’auteur, et notamment son droit au respect à l’intégrité de l’œuvre.

Article 5 : Droits de l’utilisateur

1. Dans tous les cas, l’utilisateur a notamment le droit de :

a) procéder à une copie, partielle ou intégrale, pour un usage strictement privé
;

b) procéder à des courtes citations de la présente œuvre ;

c) procéder à son analyse.

2. Dans tous les cas, l’auteur a droit au respect de son droit moral, et notamment
:

d) de son droit au nom ;

e) de son droit au respect à l’intégrité de l’œuvre.

Article 6 : Loi applicable

1. Tout différend pouvant naître à l’occasion du présent contrat sera soumis à
une conciliation préalablement à tout recours devant les tribunaux.

2. La loi française est la seule loi compétente pour la présente licence, sans
préjudice de l’éventuelle application des conventions internationales relatives
au droit d’auteur.


5. La licence Système Informatique HyperNietzsche
-------------------------------------------------

Article 1 : Parties

La présente licence est concédée par l’association HyperNietzsche, dans les
conditions décrites ci-dessous, à tout utilisateur qui entend reprendre le code
source (de type HTML, Javascript, Java, PHP, SQL ou autre) de son site Internet pour
toute création seconde.

Article 2 : Formation

La présente licence se forme par voie électronique : en reproduisant et en
utilisant le code source du site HyperNietzsche, hors des exceptions légales
prévues par le Code de la propriété intellectuelle, l’utilisateur l’accepte
tacitement.  La date de formation de la présente licence est la date de la
reproduction du code source depuis le site HyperNietzsche.

Article 3 : Objet

1. La présente licence a pour objet les codes sources, tels que publiés sur le
présent site, qui constituent la partie logicielle de l’HyperNietzsche. Ces
codes sont une œuvre originale, protégée par le Code français de la propriété
intellectuelle. Ils sont la propriété exclusive de l’association HyperNietzsche,
personne morale, titulaire originaire des droits d’exploitation.

2. Si l’utilisation envisagée des codes sources objets de la présente licence
est à des fins d’enseignement et de recherche, l’HyperNietzsche cède à
l’utilisateur, à titre non-exclusif, et pour la durée prévue à l’article 6,
ses droits d’exploitation, permettant de les réutiliser, les modifier, et de
créer ainsi une œuvre seconde. La présente licence est alors à titre gratuit.

3. L’auteur se réserve le droit de céder son droit d’utilisation, dans tous
les autres cas, et notamment lorsque l’utilisation des codes sources objets
de la présente licence, et propriété de l’HyperNietzsche, a une finalité
commerciale. La cession sera alors à titre onéreux : les modalités de cette
cession sont précisées sur le site HyperNietzsche.

Article 4 : Obligations de l’utilisateur-cessionnaire des droits

L’utilisateur reconnaît que la présente licence est à des seules fins
d’enseignement et de recherche : il ne peut céder les droits, sur les codes
sources de l’HyperNietzsche, reçus temporairement, à titre onéreux, pour une
autre finalité.

Article 6 : Durée

1. La durée de la présente licence est fixée à dix ans, à compter de la date de
sa formation.

2. La présente licence pourra être reconduite après ce premier terme de dix ans :
l’accord exprès de l’association HyperNietzsche, personne morale, est pour cela
exigé.

Article 7 : Loi applicable

1. Tout différend pouvant naître à l’occasion du présent contrat sera soumis à
une conciliation préalablement à tout recours devant les tribunaux.

2. La loi française est la seule loi compétente pour la présente licence, sans
préjudice de l’éventuelle application des conventions internationales relatives
au droit d’auteur.
```







\newpage

Open Music Licenses (2001) {.unnumbered}
--------------------------

```
LinuxTag Green OpenMusic License
Draft v1.1, 22 April 2001

I. REQUIREMENTS ON BOTH UNMODIFIED AND MODIFIED VERSIONS

The OpenMusic works may be reproduced and distributed in whole or in part, in any
medium physical or electronic, provided that the terms of this license are adhered
to, and that this license or an incorporation of it by reference (with any options
elected by the author(s) and/or publisher) is displayed in the reproduction.

Proper form for an incorporation by reference is as follows: Copyright (c) <year>
by <author's name or designee>. This material may be distributed only subject to
the terms and conditions set forth in the Green OpenMusic License, vX.Y or later
(the latest version is presently available at http://openmusic.linuxtag.org/). The
reference must be immediately followed with any options elected by the author(s)
and/or publisher of the work (see section VI). The refence must be incoporated in any
publication of the work, either physical or electronic. If the work is broadcasted,
or the reference can not be incorporated by physical means, the reference can be
ommitted with prior permission obtained from the copyright holder.

Commercial redistribution of OpenMusic-licensed material is permitted.

Any publication in physical form (like a CD) shall require the citation of the
original publisher and author. The publisher and author's names shall appear on all
outer surfaces of the product. On all outer surfaces of the product the original
publisher's name shall be as large as the title of the work and cited as possessive
with respect to the title.

II. COPYRIGHT

The copyright to each OpenMusic is owned by its author(s) or designee. If the work
covered by this license is bound to a specific transport medium (see section VI), it
is possible for the author(s) or designee to publish the work on a different medium
covered by a different license.

III. SCOPE OF LICENSE

The following license terms apply to all OpenMusic works, unless otherwise explicitly
stated.

Mere aggregation of OpenMusic works or a portion of an OpenMusic work with other
works on the same media shall not cause this license to apply to those other
works. The aggregate work shall contain a notice specifying the inclusion of the
OpenMusic material and appropriate copyright notice.

SEVERABILITY. If any part of this license is found to be unenforceable in any
jurisdiction, the remaining portions of the license remain in force.

NO WARRANTY. OpenMusic works are licensed and provided "as is" without warranty
of any kind, express or implied, including, but not limited to, the implied
warranties of merchantability and fitness for a particular purpose or a warranty of
non-infringement.

IV. REQUIREMENTS ON MODIFIED WORKS

All modified versions of works covered by this license, including partial works
incorporated into new works, must meet the following requirements:

    The modified version must be labeled as such.

    The person making the modifications must be identified and the modifications
    dated.

    The location of the original unmodified work must be identified.

    The original author's (or authors') name(s) may not be used to assert or imply
    endorsement of the resulting work without the original author's (or authors')
    permission.

    The new work has to be released under precisely this License, with the
    modified version filling the role of the work, thus licensing distribution and
    modification of the modified version to whoever possesses a copy of it.

V. GOOD-PRACTICE RECOMMENDATIONS

In addition to the requirements of this license, it is requested from and strongly
recommended of redistributors that: If you are distributing OpenMusic works in
physical form, you provide email notification to the authors of your intent to
redistribute at least thirty days before your media freeze, to give the authors time
to provide updated works. This notification should describe modifications, if any,
made to the work. All substantive modifications (including deletions) be described
in an attachment to the work. Finally, while it is not mandatory under this license,
it is considered good form to offer a free copy of any physical form expression of an
OpenMusic-licensed work to its author(s).

VI. MEDIA LOCKING OPTION

The author(s) and/or publisher of an OpenMusic-licensed document may bind any
publication of this work or derivative works in whole or in part on a specific
transport medium (for example "digital distribution via the Internet"). Exceptions
can be made with prior permission from the copyright holder.

This option is selected by appending language to the reference to or copy of the
license. This option is considered part of the license instance and must be included
with the license (or its incorporation by reference) in derived works.

To accomplish this, add the phrase "Distribution of the work or derivative of the
work is restricted to <insert medium here> unless prior permission is obtained from
the copyright holder." to the license reference or copy.

OPEN MUSIC POLICY APPENDIX:

(This is not considered part of the license.)

OpenMusic works are available in source format via the OpenMusic home page at
http://openmusic.linuxtag.org/.

OpenMusic artists who want to include their own license on OpenMusic works may do so,
as long as their terms are not more restrictive than the OpenMusic license.

If you have questions about the OpenMusic license, please contact LinuxTag e.V.,
and/or the OpenMusic Artists' List at openartists@linuxtag.org, via email.

To subscribe to the OpenMusic Artists' List: Send E-mail to
openartists-request@linuxtag.org.

To post to the Open Publication Authors' List: Send E-mail to
openartists@linuxtag.org or simply reply to a previous post.

To unsubscribe from the OpenMusic Artists' List: Send E-mail to
majordomo@linuxtag.org with the words "unsubscribe openartists" in the body of the
mail.

---

LinuxTag Yellow OpenMusic License
Draft v1.1, 22 April 2001

I. REQUIREMENTS ON BOTH UNMODIFIED AND MODIFIED VERSIONS

The OpenMusic works may be reproduced and distributed in whole or in part, in any
medium physical or electronic, provided that the terms of this license are adhered
to, and that this license or an incorporation of it by reference (with any options
elected by the author(s) and/or publisher) is displayed in the reproduction.

Proper form for an incorporation by reference is as follows: Copyright (c) <year>
by <author's name or designee>. This material may be distributed only subject to
the terms and conditions set forth in the Yellow OpenMusic License, vX.Y or later
(the latest version is presently available at http://openmusic.linuxtag.org/). The
reference must be immediately followed with any options elected by the author(s)
and/or publisher of the work (see section VII). The refence must be incoporated
in any publication of the work, either physical or electronic. If the work is
broadcasted, or the reference can not be incorporated by physical means, the
reference can be ommitted with prior permission obtained from the copyright holder.

Any publication in physical form (like a CD) shall require the citation of the
original publisher and author. The publisher and author's names shall appear on all
outer surfaces of the product. On all outer surfaces of the product the original
publisher's name shall be as large as the title of the work and cited as possessive
with respect to the title.

II. COPYRIGHT

The copyright to each OpenMusic is owned by its author(s) or designee. If the work
covered by this license is bound to a specific transport medium (see section VII), it
is possible for the author(s) or designee to publish the work on a different medium
covered by a different license.

III. SCOPE OF LICENSE

The following license terms apply to all YELLOW OpenMusic works, unless otherwise
explicitly stated.

Mere aggregation of OpenMusic works or a portion of an OpenMusic work with other
works on the same media shall not cause this license to apply to those other
works. The aggregate work shall contain a notice specifying the inclusion of the
OpenMusic material and appropriate copyright notice.

SEVERABILITY. If any part of this license is found to be unenforceable in any
jurisdiction, the remaining portions of the license remain in force.

NO WARRANTY. OpenMusic works are licensed and provided "as is" without warranty
of any kind, express or implied, including, but not limited to, the implied
warranties of merchantability and fitness for a particular purpose or a warranty of
non-infringement.

IV. REQUIREMENTS ON MODIFIED WORKS

All modified versions of works covered by this license, including partial works
incorporated into new works, must meet the following requirements:

    The modified version must be labeled as such.

    The person making the modifications must be identified and the modifications
    dated.

    The location of the original unmodified work must be identified.

    The original author's (or authors') name(s) may not be used to assert or imply
    endorsement of the resulting work without the original author's (or authors')
    permission.

    The new work has to be released under precisely this License, with the
    modified version filling the role of the work, thus licensing distribution and
    modification of the modified version to whoever possesses a copy of it.

V. GOOD-PRACTICE RECOMMENDATIONS

In addition to the requirements of this license, it is requested from and strongly
recommended of redistributors that: If you are distributing OpenMusic works in
physical form, you provide email notification to the authors of your intent to
redistribute at least thirty days before your media freeze, to give the authors time
to provide updated works. This notification should describe modifications, if any,
made to the work. All substantive modifications (including deletions) be described
in an attachment to the work. Finally, while it is not mandatory under this license,
it is considered good form to offer a free copy of any physical form expression of an
OpenMusic-licensed work to its author(s).

VI. COMMERCIAL USAGE

The publication of this work or derivative works in whole or in part in standard
(physical) form for commercial purposes is prohibited unless prior permission is
obtained from the copyright holder. "Commercial purposes" include any broadcasting
via commercial networks, commercial hiring, commercial copying and lending and
commercial public performance.

VII. MEDIA LOCKING OPTION

The author(s) and/or publisher of an OpenMusic-licensed document may bind any
publication of this work or derivative works in whole or in part on a specific
transport medium (for example "digital distribution via the Internet"). Exceptions
can be made with prior permission from the copyright holder.

This option is selected by appending language to the reference to or copy of the
license. This option is considered part of the license instance and must be included
with the license (or its incorporation by reference) in derived works.

To accomplish this, add the phrase "Distribution of the work or derivative of the
work is restricted to <insert medium here> unless prior permission is obtained from
the copyright holder." to the license reference or copy.

OPEN MUSIC POLICY APPENDIX:

(This is not considered part of the license.)

OpenMusic works are available in source format via the OpenMusic home page at
http://openmusic.linuxtag.org/.

OpenMusic artists who want to include their own license on OpenMusic works may do so,
as long as their terms are not more restrictive than the OpenMusic license.

If you have questions about the OpenMusic license, please contact LinuxTag e.V.,
and/or the OpenMusic Artists' List at openartists@linuxtag.org, via email.

To subscribe to the OpenMusic Artists' List: Send E-mail to
openartists-request@linuxtag.org.

To post to the Open Publication Authors' List: Send E-mail to
openartists@linuxtag.org or simply reply to a previous post.

To unsubscribe from the OpenMusic Artists' List: Send E-mail to
majordomo@linuxtag.org with the words "unsubscribe openartists" in the body of the
mail.
```






\newpage

Simputer General Public License (2001) {.unnumbered}
--------------------------------------

```
SIMPUTERTM GENERAL PUBLIC LICENSE

Version 1.3        7 May, 2001

Copyright © The Simputer Trust

The SimputerTM General Public License (the SGPL) is based on the GNU General Public
License but, due to the essential dissimilarities between the types of intellectual
property being distributed, is significantly different. Everyone is permitted to
copy and distribute verbatim copies of this license, but no-one may change it or
distribute changed versions under the same name.

Preamble:

The SGPL is meant to aid in the proliferation of SimputersTM and its innovative
extensions. The SGPL is designed to make sure that you have the right to use, modify
and extend the specifications necessary to make a SimputerTM , and to manufacture and
sell SimputersTM , and that you receive the Specifications or can get it if you want
it and that you know you can do these things.

To protect your rights, we need to make restrictions that makes it illegal for anyone
to deny you these rights or to ask you to surrender these rights. These restrictions
translate to certain responsibilities for you if you make and sell SimputersTM , or
if you modify the specifications used to make SimputersTM .

For example, if you make and sell SimputersTM , you must make sure that all
purchasers or recipients of the SimputersTM manufactured by you, receive or can get
the specifications as to how their SimputerTM was made and how it functions. If you
have made any SimputersTM based on modifications to the specifications which you
received under the SGPL, you must ensure that those modifications are eventually
published so that all purchasers and recipients thereof or other future developers
of SimputersTM may benefit from the modifications you made to the SimputerTM
specifications. You must also show all purchasers and recipients of devices
manufactured by you based on these specifications, these terms so they know their
rights.

The Simputer Trust protects your rights with three steps: (1) it protects the
specifications used to manufacture SimputersTM under appropriate intellectual
property law (to the extent that existing intellectual property laws are not, in the
opinion of the SimputerTM Trust, adequate to offer the degree of protection necessary
to effectively achieve the objects of the Simputer Trust, it may seek to achieve
equivalent protection by using principles of contract or other applicable law);
(2) it legally permits you, under the terms of this SGPL, to use the SimputerTM
specifications for the purpose of deriving modifications to the SimputerTM
specifications; and (3) it trademarks the SimputerTM brand name and allow only
those devices which have been made under and in accordance with this SGPL to be
manufactured, distributed or sold using the SimputerTM brand name. The SimputerTM
Trust recognizes that you may develop devices that are similar to the SimputerTM and
that utilize some of the features of the SimputerTM specifications, but which do not
achieve all such specifications. In these circumstances the Simputer Trust will not
license the Simputer trademark in respect of these devices but would require you to
recognize the input of the SimputerTM specifications in the creation of these devices
by calling such devices SimputerisedTM devices.

Also, for each SimputerTM manufacturer's protection, the Simputer Trust wants to
make certain that everyone understands that there is no warranty for the SimputerTM
specification. The Simputer Trust also requires that every SimputerTM be manufactured
with reference to two identities: (a) the name SimputerTM and (b) the name of the
manufacturer of that particular version of the SimputerTM . The end user should know
that, (i) the product has been manufactured in accordance with the terms of this SGPL
but also that (ii) the SimputerTM specification was converted into a physical product
by an identified third party so that any problems particular to that product will not
reflect on the SimputerTM platform's reputation.

Finally, any hardware specification that is distributed freely, such as is proposed
under this SGPL, will be constantly threatened by recipients of the specification
who may take out patents in respect of devices created or derived from these
specifications. We wish to prevent manufacturers of SimputersTM from individually
obtaining patents in respect of devices based on the SimputerTM specifications or any
modification thereof as such patents will, in effect, make the hardware proprietary
and prevent proliferation of the SimputerTM platform as envisaged. It is therefore
a condition under this SGPL that no-one shall be permitted to register a patent in
respect of any device derived from or based upon the SimputerTM specifications or any
modifications thereof.

I.     Scope, Applicability and Definitions.

1.      This SGPL applies to the hardware specifications, printed circuit board
designs, or other works necessary for the creation of a SimputerTM and which are
distributed under the terms of this SGPL. The term Specifications when used in this
SGPL, refers to any such specifications pertaining to the hardware design and the
printed circuit board layout, as released by the Simputer Trust and published on its
website, from time to time. A Device means any device constructed or fabricated using
the Specifications or any portion, modification or derivation thereof. The terms Core
SimputerTM Specifications or CSS when used in this SGPL refers to the minimal core
features that any Device must necessarily display in order that such device may be
called or referred to as a SimputerTM . The Simputer Trust shall from time to time,
publish the CSS on its website with clear version numbering. The term Functional
Tests when used in this SGPL shall refer to the set of non-invasive tests which if
applied to any Device will indicate whether or not such Device satisfies the CSS. The
Simputer Trust shall from time to time, publish the Functional Tests on its website
with clear version numbering. A SimputerisedTM Device means any device which utilizes
any part, but not the whole of the Specifications or modifications thereof or which
meets or achieves only a portion of the Core SimputerTM Specifications but not all
the Core SimputerTM Specifications. Each licensee is addressed as you.

II.     Terms and Conditions for Copying, Distribution and Modification of the
Specifications:

2.      You may copy and distribute verbatim copies of the Specifications as you
receive it, in any medium, provided that

a)      you conspicuously and appropriately publish along with each copy of the
Specifications, this SGPL and the disclaimer of warranty;

b)      you keep intact all the notices that refer to this SGPL and to the absence of
any warranty;

c)      you give any and all recipients of the Specifications, a copy of this SGPL
along with the Specifications; and

d)      you ensure that no third party can receive or read the Specifications from
you without first having read and agreed to the terms of this SGPL.

3.      You may develop devices based on modifications of the Specifications or on
modifications of any portion of the Specifications. You are not required, under
the terms of this SGPL, to distribute any modifications to the Specifications if
you have not commercially distributed any devices created based on the modified
Specifications. However, if and when you do distribute modified Specifications or if
you manufacture or distribute any devices based on the Specifications, you shall only
do so subject to the terms and conditions of Section 2 above as well as each of the
following conditions:

a)      you must cause the modified Specifications to carry prominent notices stating
that you have changed the Specifications, as well as full details about yourself,
including your name, permanent physical address, email address and other contact
details.

b)      you must cause to be included along with the modified Specifications, the
date and details of the change you have introduced to the Specifications, including
details of the version of the Specifications from which the changes were made.

c)      you must allow any modified Specifications that you distribute or publish,
that in whole or in part contains or is derived from the Specifications or any part,
modification or derivation thereof, to be copied, distributed or modified as a whole
at no charge by all third parties under the terms of this SGPL and to allow all such
third parties who access these modified Specifications to create devices based upon
the modified Specifications under and in accordance with the terms of this SGPL.

These requirements apply to the Specifications as a whole. If you have developed
devices that can function independently and are capable of being connected to or
externally used in conjunction with SimputersTM , then this SGPL, and its terms,
do not apply to the specifications for the manufacture of those devices. However, if
you have modified the Specifications to incorporate any device or devices within the
body of the SimputerTM , or if you have developed specifications for devices designed
to be incorporated within the body of the SimputerTM , all such specifications,
modified Specifications and devices created based thereon, shall be governed by
this SGPL. When you distribute these specifications or modified Specifications,
the permissions granted to other recipients under such SGPL shall extend to the
entire whole, and thus to each and every part regardless of who wrote it. The mere
aggregation of specifications for the manufacture of other devices not based on the
Specifications with the Specifications (or with a modification of the Specifications)
does not bring such other specifications under the scope of this SGPL.

At all times, the Specifications, the modified Specifications and all other
intellectual property distributed under the terms of this SGPL shall constitute the
valuable intellectual property of the Simputer Trust notwithstanding the rights of
the author or creator of such intellectual property therein. You shall not register
a patent or other intellectual property right in respect of the Specifications or
any modifications or derivations thereof, nor shall you register any intellectual
property right in respect of any Devices built using or relying upon the
Specification or any modifications or derivations thereof. To the extent required,
the authors of such intellectual property shall, through an appropriate deed of
assignment or other such document, transfer and assign the intellectual property to
and in favour of the Simputer Trust at the time of putting such intellectual property
in the public domain.

III.     Development and Design of Devices

4.      You may, subject to the conditions of this SGPL, design and build a Device
so long as you provide each person to whom such Device is given, with a copy of this
SGPL as well as with a copy of the Specifications.

5.      Notwithstanding anything to the contrary contained herein, you shall
not commence any commercial activity in relation to any Device, whether it be
manufacture, distribution, sale, or any other such activity, without first obtaining
from the Simputer Trust, a license in accordance with the provisions of Section 7 of
this SGPL.

6.      You may design and build a Device based on any modifications to the
Specifications provided that in the event you commence any commercial activity in
relation to the Device you shall, no later than twelve months from the date on which
the first sale of such Device is completed,

a)      notwithstanding anything contained in Section 3, publish or cause to be
published a copy of the modified Specifications based upon which the Device was
built; and

b)      deliver a copy of the modified Specifications to the Simputer Trust.

During the twelve month period from the date on which you complete the first sale of
a Device till the date on which you are required, under the terms of this Section 6,
to publish the modifications of the Specifications, you shall, subject to the terms
of Part II hereunder, be entitled to exclusively manufacture and sell Devices based
on the modified Specifications. In the event any dispute arises in respect of the
actual date of the first commercial sale of the Device, the decision of the Simputer
Trust in respect thereof shall be final and binding.

IV.     Terms and Conditions for Distribution and Manufacture of SimputersTM or
SimputerisedTM Devices:

7.      If you have developed a prototype of a Device that satisfies the CSS,
you must approach the Simputer Trust for a license to distribute and manufacture
SimputersTM before you complete the commercial sale of any such Device. If you have
developed a prototype of a SimputerisedTM Device that does not satisfy the CSS,
you must approach the Simputer Trust for a license to distribute and manufacture
SimputerisedTM Devices before you complete the first commercial sale of any such
Device. You will not be entitled to distribute or manufacture any Device under the
SimputerTM brand name or any modification or colourable imitation thereof or to
call such Device a SimputerTM or a SimputerisedTM Device unless you have received a
license from the Simputer Trust for such manufacture and distribution.

8.      In order to obtain a license for the manufacture of SimputersTM , you must
provide the Simputer Trust with a fully functional prototype Device. The Simputer
Trust shall perform the Functional Tests on such Device and, if, in the opinion of
the Simputer Trust, the Device satisfies the CSS, the Simputer Trust will grant you a
license to manufacture and distribute SimputersTM and to use the SimputerTM trademark
in association with such Device, subject and in accordance with the terms of this
SGPL. The Simputer Trust may, at its discretion, charge you a one-time lump-sum
license fee in respect of such license to manufacture and distribute and for the
use of the SimputerTM trademark. In order to obtain a license for the manufacture
of a SimputerisedTM Device, the Simputer Trust may, at its discretion, charge you a
one-time license fee payable in respect of such license to manufacture and distribute
such SimputerisedTM Device and for the use of the term SimputerisedTM as a prefix to
the brand name, in relation thereto.

9.      Any license that may be granted to you by the Simputer Trust in accordance
with the terms of Section 8, shall be so granted on the following conditions:

a)      All SimputersTM manufactured or distributed under the SimputerTM trade mark
shall fulfill each and every one of the conditions set out in the most recent version
of the CSS as has been published on the date of manufacture of such Device. This
obligation shall not apply to SimputerisedTM Devices.

b)      The SimputerTM trade mark as well as the logo shall be displayed on all
SimputersTM as well as in all promotional material, documentation, brochures,
notices, etc. relating thereto, strictly in accordance with the instructions
published by the Simputer Trust in this regard from time to time. All SimputerisedTM
Devices shall utilise a brand name distinct from the SimputerTM trade mark, but shall
prefix such brand name with the term SimputerisedTM both on the Devices as well as in
all promotional material, documentation, brochures, notices, etc. relating thereto,
strictly in accordance with the instructions published by the Simputer Trust in this
regard from time to time. All SimputersTM and SimputerisedTM Devices shall also bear,
in a prominent place on the front panel of the Device, details of the name of the
manufacturer thereof.

c)      You shall not be entitled to sub-license the rights under this SGPL with
regard to manufacture and distribution of SimputersTM or SimputerisedTM Devices or
the use of the SimputerTM trade mark, or any modifications or colourable imitations
thereof without the express written consent of the Simputer Trust. You shall not
be allowed to utilise the SimputerTM trade mark or any adaptations or colourable
imitations thereof in respect of any Device in respect of which a manufacturing
and distribution license has not been granted by the SimputerTM Trust. Nothing
contained herein shall limit your right to appoint tooling, manufacturing and
fabrication agents as well as marketing agents and distributors in respect of the
Devices provided that you retain ultimate control over the actual manufacture of the
Devices and that all such agents and distributors agree to be bound by the terms and
conditions of the SGPL.

d)      You shall be obliged to abide by and adhere to all the terms of this SGPL. In
addition a breach of any of the terms of the SGPL by any person who has received,
from you, a copy of the Specifications without the SGPL shall be deemed to be a
breach of the terms of this SGPL.

e)      The license of the SimputerTM trade mark or the right to utilise the
SimputerisedTM prefix shall not be limited in point in time and shall not be
terminated except in the event of a breach of any of the terms of the license of this
SGPL.

f)     You shall ensure that the manufacture or distribution of the Device does not
infringe or violate any existing intellectual property rights of any third party. To
the extent that the Device incorporates any third party intellectual property,
the Specifications provided along with the Device should name the licensor of such
intellectual property and list the specifications thereof in sufficient detail as
would be necessary for any subsequent licensee of the Specifications to be able to
obtain a license for such intellectual property from that licensor.

In the event you are found, at any point in time, to be in breach of any of the
terms and conditions set out in this Clause 9, the Simputer Trust shall be entitled
to terminate, with immediate effect, the manufacturing and distribution license as
well as the trade mark license under the terms of which you have been permitted to
distribute and manufacture SimputersTM or SimputerisedTM Devices. From that date
onwards, you shall not be permitted to legally denote any Device manufactured or
distributed by you, as being a SimputerTM or a SimputerisedTM Device or to represent,
whether expressly or through reasonable implication, that any such Devices are
derived from, or similar to, SimputersTM . You hereby authorize the Simputer Trust,
in the event of such termination of the manufacturing and distribution license or the
SimputerTM trade mark license, to publish, disclose or otherwise generally make known
the fact that your license has been terminated and that any Device manufactured by
you commencing from the date of such termination, are not SimputersTM as defined and
certified by the Simputer Trust.

V.     General

10.     You must accept this SGPL before reading or using the Specifications. You
are prohibited under law from using, modifying or distributing the Specifications
or from manufacturing or distributing any Devices based upon the Specifications or
based upon any modifications thereof. Therefore, by using, modifying or distributing
the Specifications or manufacturing or distributing any Device based on the
Specifications, you indicate by your actions, your acceptance of the terms of this
SGPL to do so as well as all its terms and conditions for copying, distributing or
modifying the Specifications or manufacturing Devices based on the Specifications or
modifications of the Specifications.

11.     Each time you redistribute the Specifications, distribute any modifications
of the Specifications, or sell any Devices based on the Specifications, the recipient
or purchaser, as the case may be, automatically receives a license from the Simputer
Trust to copy, distribute or modify the Specifications provided that such copying,
distribution or modification is carried out subject to the terms and conditions of
this SGPL. You may not impose any further restrictions on the recipients' exercise
of the rights granted herein. You are not responsible for enforcing compliance by
third parties to this SGPL. However you shall be liable for all direct or indirect
consequences arising from a failure on your part to distribute the Specifications
along with a copy of this SGPL and under the terms of the SGPL.

12.     If, as a consequence of a court judgment or allegation of patent infringement
or for any other reason (not limited to patent issues), conditions are imposed on you
(whether by court order, agreement or otherwise) that contradict the conditions of
this SGPL, they do not excuse you from the conditions of this License. If you cannot
simultaneously satisfy your obligations under this SGPL and any other pertinent
obligations, then as a consequence you may not carry out any of the conflicting
obligations at all. For example, if you are subject to an obligation under a decree
of court that would prevent you from providing purchasers of any Device manufactured
by you with a copy of the Specifications, then the only way you could satisfy both
that obligation as well as the terms of this SGPL would be to refrain entirely from
selling the Device.

If any portion of this section is held invalid or unenforceable under any particular
circumstance, the balance of the section is intended to apply and the section as a
whole is intended to apply in other circumstances.

It is not the purpose of this section to induce you to infringe any patents or other
property right claims or to contest the validity of any such claims; this section has
the sole purpose of protecting the integrity of the free distribution system proposed
under this SGPL, which is implemented by public license practices. Many people
have made generous contributions to the wide range of intellectual property that is
currently being distributed through this system in reliance on consistent application
of the system; it is up to the creator of such intellectual property to decide if he
or she is willing to distribute the intellectual property through any other system
and a licensee cannot impose that choice.

13.     If the full scope of the applicability of this SGPL is restricted in certain
countries either by patents or by the operation of any other law, the Simputer Trust
may, at its discretion, add an explicit geographical limitation excluding those
countries, so that distribution of the Specifications, modified Specifications or
Devices manufactured using the Specifications or modified Specifications is permitted
only in or among countries not thus excluded. In such case, this SGPL incorporates
the limitation as if written in the body of this SGPL.

14.     The Simputer Trust may publish revised and/or new versions of the SGPL from
time to time. Such new versions will be similar in spirit to the present version,
but may differ in detail to address new problems or concerns. Each version is given
a distinguishing version number. Regardless of what version of the SGPL your copy
of the Specifications was distributed under, you must ensure that you use the most
recent version published by the Simputer Trust to govern your distribution or use of
the Specifications or modified Specifications or Devices.

V.     Disclaimer of Warranty:

15.     THE SPECIFICATIONS HAVE BEEN PROVIDED "AS IS". THE Simputer Trust DOES
NOT PROVIDE ANY WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT
NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE IN RESPECT OF THE SPECIFICATIONS OR ANY DEVICES THAT MAY BE MANUFACTURED
BASED ON THE SPECIFICATIONS OR THE MODIFIED SPECIFICATIONS. THE ENTIRE RISK AS TO THE
QUALITY AND PERFORMANCE OF DEVICES MANUFACTURED UNDER THE TERMS OF THE SPECIFICATIONS
SHALL VEST WITH YOU. SHOULD THE SPECIFICATION PROVE DEFECTIVE, YOU SHALL ASSUME THE
COST OF ALL NECESSARY REPAIR OR CORRECTION OF THE DEVICE.

16.     IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL THE Simputer Trust OR ANY OTHER PARTY WHO MAY MODIFY AND/OR REDISTRIBUTE
THE SPECIFICATIONS AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING
ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE
OR INABILITY TO USE THE SPECIFICATIONS IN ORDER TO DEVELOP OR MANUFACTURE DEVICES,
EVEN IF THE Simputer Trust OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES.

VI.     Acceptance:

17.     By clicking on the I Agree button below, you affirm that you agree to be
bound by the terms of this SGPL and that you will abide by and adhere to the duties
and obligations contained in this Agreement. Your electronic acceptance shall operate
as a valid binding contract for the purpose of all applicable laws in this regard.
```






\newpage

Academic Free License (2002) {.unnumbered}
----------------------------

```
Academic Free License

Version 1.2

This Academic Free License applies to any original work of authorship (the "Original
Work") whose owner (the "Licensor") has placed the following notice immediately
following the copyright notice for the Original Work:

Licensed under the Academic Free License version 1.2

Grant of License.

Licensor hereby grants to any person obtaining a copy of the Original Work ("You")
a world-wide, royalty-free, non-exclusive, perpetual, non-sublicenseable license (1)
to use, copy, modify, merge, publish, perform, distribute and/or sell copies of the
Original Work and derivative works thereof, and (2) under patent claims owned or
controlled by the Licensor that are embodied in the Original Work as furnished by
the Licensor, to make, use, sell and offer for sale the Original Work and derivative
works thereof, subject to the following conditions.

Attribution Rights.

You must retain, in the Source Code of any Derivative Works that You create, all
copyright, patent or trademark notices from the Source Code of the Original Work, as
well as any notices of licensing and any descriptive text identified therein as an
"Attribution Notice." You must cause the Source Code for any Derivative Works that
You create to carry a prominent Attribution Notice reasonably calculated to inform
recipients that You have modified the Original Work.

Exclusions from License Grant.

Neither the names of Licensor, nor the names of any contributors to the Original
Work, nor any of their trademarks or service marks, may be used to endorse or promote
products derived from this Original Work without express prior written permission of
the Licensor.

Warranty and Disclaimer of Warranty.

Licensor warrants that the copyright in and to the Original Work is owned by the
Licensor or that the Original Work is distributed by Licensor under a valid current
license from the copyright owner. Except as expressly stated in the immediately
proceeding sentence, the Original Work is provided under this License on an "AS IS"
BASIS and WITHOUT WARRANTY, either express or implied, including, without limitation,
the warranties of NON-INFRINGEMENT, MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. THE ENTIRE RISK AS TO THE QUALITY OF THE ORIGINAL WORK IS WITH YOU. This
DISCLAIMER OF WARRANTY constitutes an essential part of this License. No license to
Original Work is granted hereunder except under this disclaimer.

Limitation of Liability.

Under no circumstances and under no legal theory, whether in tort (including
negligence), contract, or otherwise, shall the Licensor be liable to any person for
any direct, indirect, special, incidental, or consequential damages of any character
arising as a result of this License or the use of the Original Work including,
without limitation, damages for loss of goodwill, work stoppage, computer failure or
malfunction, or any and all other commercial damages or losses. This limitation of
liability shall not apply to liability for death or personal injury resulting from
Licensor's negligence to the extent applicable law prohibits such limitation. Some
jurisdictions do not allow the exclusion or limitation of incidental or consequential
damages, so this exclusion and limitation may not apply to You.

License to Source Code.

The term "Source Code" means the preferred form of the Original Work for making
modifications to it and all available documentation describing how to modify the
Original Work. Licensor hereby agrees to provide a machine-readable copy of the
Source Code of the Original Work along with each copy of the Original Work that
Licensor distributes. Licensor reserves the right to satisfy this obligation by
placing a machine-readable copy of the Source Code in an information repository
reasonably calculated to permit inexpensive and convenient access by You for as long
as Licensor continues to distribute the Original Work, and by publishing the address
of that information repository in a notice immediately following the copyright notice
that applies to the Original Work.

Mutual Termination for Patent Action.

This License shall terminate automatically and You may no longer exercise any of the
rights granted to You by this License if You file a lawsuit in any court alleging
that any OSI Certified open source software that is licensed under any license
containing this "Mutual Termination for Patent Action" clause infringes any patent
claims that are essential to use that software.

Right to Use.

You may use the Original Work in all ways not otherwise restricted or conditioned by
this License or by law, and Licensor promises not to interfere with or be responsible
for such uses by You.

This license is Copyright (C) 2002 Lawrence E. Rosen. All rights reserved.
Permission is hereby granted to copy and distribute this license without
modification. This license may not be modified without the express written permission
of its copyright owner.
```






\newpage

CopID notice (2002) {.unnumbered}
-------------------

```
Voici un système complémentaire du copyright et du copyleft, qui a pour but de
stimuler l'échange libre et gratuit des idées et des savoirs : La mention CopID.

Il suffit d'indiquer "CopID" sur l'oeuvre initiale, au bas d'un texte comme celui-ci
par exemple.

Si une oeuvre comporte une mention CopID initiale ou ultérieure, sa copie ou sa
modification sont totalement libres, sans mention du nom de l'auteur ni de la source.

Pour favoriser la circulation de l'oeuvre, il est recommandé mais pas obligatoire de
faire figurer la mention CopID sur toute reproduction d'une oeuvre possédant déjà
une mention CopID. Un autre nom d'auteur peut même figurer sur la copie dans la
mesure où le nom de l'auteur initial ou du copieur précédent est déclaré comme
secondaire depuis son origine.

La mention CopID exprime la volonté d'un auteur de mettre son oeuvre dans le domaine
public non marchand. Elle autorise chacun à s'inspirer librement, à copier ou à
recopier totalement ou partiellement, ou à modifier une oeuvre et elle atteste le
refus de la part de l'auteur d'en faire ou d'en autoriser un usage lucratif.

Un auteur qui déclare l'une de ses oeuvres sous CopID renonce partiellement à
sa propriété intellectuelle personnelle sur celle-ci, mais n'autorise pas une
usurpation de cette propriété par une autre personne à des fins lucratives. Dans
cette éventualité, la propriété intellectuelle d'une oeuvre resterait protégée
légalement par la première divulgation de l'oeuvre faite par son auteur ou son
créateur.

CopID s'applique en premier lieu aux textes écrits sur internet, et dont l'auteur
souhaite une large diffusion par copies faites librement, donc sans référence ni à
une source ni à un auteur.

Application aux textes sur internet

Le texte initial proposé par un auteur comporte la mention CopID. Si le site
internet accepte cette mention lors de la publication du texte, il renonce à son
droit d'être cité en tant que source. Ce texte peut être recopié et modifié
librement sur tout autre site internet, y compris avec un nom d'auteur différent. Il
est alors simplement recommandé de faire figurer au bas de la copie la mention
CopID. La recopie de la mention CopID n'est pas une obligation, seule cette mention
lors de la première mise en ligne d'un texte détermine le statut définitif de ce
texte dans sa forme initiale.

(My name is nobody, think to my idea that's all ! Texte publié sur ICI.ICI.free.fr
le 14 août 2002, mais vous pouvez effacer cette phrase)

Ce texte est CopID (copie et modifiaction 100% libre non marchande).
```













\newpage

Mnémosyne Free Dissemination License (2002) {.unnumbered}
-------------------------------------------

```
Mnemosyne free dissemination licence -- mfdl version 1

This document may be freely read, stored, reproduced, disseminated or quoted by any
means on any medium provided the following conditions are met :

1. every reader or user of this document acknowledges that he his aware that
no guarantee is given regarding its contents, on nay account, and specifically
concerning veracity, accuracy and fitness for any purpose ;

2. no modification is made other than change of representation format, correction of
obvious syntactic errors ;

3. fragments must clearly refer to the original complete version, and to one copy
that is easily accessed whenever possible ;

4. this licence applies to the whole document (except for brief quotes),
independently of the representation format ;

5. whatever the mode of storage, reproduction or dissemination, anyone able to
access a digitized version of this document as the right to make a digitized copy ;
the person must proceed with a format directly usable, and if possible editable,
according to accepted, and publicly documented, public standards ;

6. redistributing this document to a third party requires simultaneous redistribution
of this licence, without modification, and in particular without further condition
or restriction, expressed or implied, related or not to this redistribution. in
particular, in case of an authorized inclusion in a database or collection, the owner
or the manager of the database or the collection renounces any right related to this
inclusion and concerning the possible uses of the document after extraction from the
database or the collection, whether alone or in relation with other documents.

Any opposition or incompatibility of the above licence terms with legal or
contractual decisions or constraints are solved in favour of the legal or contractual
disposition.

Cette licence est une version modifiée par Michaël Thévenet de la Licence de Libre
Diffusion des Documents de Bernard Lang

```

\resumetocwriting