Prologue {.unnumbered}
========


It's just past midnight.
It's cold.
The air is humid and heavy and smells like stale beer,
in an old World War II bunker built by Russian prisoners during the German occupation in Bergen, Norway.
On a stage,
in one of the biggest rooms of the underground concrete structure,
two men,
one bald and another wearing beard and hat,
ignore the small audience that has gathered with hesitation around the mess of vinyl,
electronics,
cables and computers,
all of which form an altar at which the two masters of ceremonies are occupied.
Behind them,
a video image is projected:
green text scrolls on a black background,
the visual layout reminiscent of the popular depiction of computer hacking in nineties films.
Slightly farther away from this enigmatic scene,
other human forms gather in small groups,
around dark and sticky wooden tables,
or sit alone on the dark and sticky floor,
or on the chairs that just happen to have been scattered randomly in the space.
Everything is quiet.
The surrounding humanoids do not look at the stage.
Their attention is focussed on their laptop screens.
The machines,
some of which have seen better days,
are covered with myriad stickers,
with each stratum testifying of a particular era and its associated style,
together forming a constantly changing peacock tail for the otherwise dark clothed men and women attending.
And in the darkness of the bunker,
their already pale looking faces illuminated by the cold and blueish light of the computer displays,
forms a council of floating head spectres,
a perculiar gathering of radically isolated yet fully connected beings,
who only look at each other,
when their chat software stops working.
All of a sudden,
a sharp and intense sound,
or was it a punch,
freezes the room.
In the tiny moment of sonic vaccum that follows,
looks of disbelief,
confusion and fear create a brief moment of communion orchestrated by the two noise performers.
Adreline and cortisol is released into the systems of all assembled:
fight or flight?
No time to think,
their fully alerted animal instinct predicts that another bursting charge is about to be delivered,
and,
like threatened rodents,
the fastest promptly take off to the closest holes formed in the dimly lit corridors of the bunker,
quickly shouting possible future rallying points to each other---apparently the hotel lobby near the harbour has free WiFi---leaving behind only the afterimage of the quickly vacillating tail of a laptop power chord,
abruptly pulled from the wall sockets.
The very few left behind,
are holding strong to their beer bottles,
now absorbing one sound shock after the other,
standing firmly in front of this messy Unix command line noise mass,
hypnotised by the mixed analogue and digital system peacefuly and quietly operated by the two men on stage,
and whose soft,
almost unnoticeable manipulations are contrasted with the brutally tangible manifestation of this human-machine dialogue,
thanks to a merciless amplification.


Meanwhile,
on the other side of the globe,
in San Francisco,
California,
USA,
a smaller group are quietly gathered in a room.
The walls are painted matte white with a few salmon pink horizontal stripes at the bottom,
some wavy turquoise patterns rise towards the ceiling,
in an attempt to add a Mediterranean tone to the already quite warm studio,
artificially heated to forty degrees celcius,
precisely.
Another particular artificially generated feature is the humidity level,
which is also precisely set at forty percent,
thus making the atmosphere resonate with some mystical depth and potential secret meaning,
that such a peculiar configuration would imply.
The more prosaic consequences of this apparatus are however of a lesser spiritual nature:
it is just annoyingly hot and humid in there.
Fighting against the stuffiness of such a surreal setup,
and the very real odors from the still but sweating bodies,
a few sticks of incense bought at the local Asian supermarket stand proud,
slowly burning,
and adding an extra thickness to the already dense misty atmosphere.
On a small bench,
a brandless music and radio player combo rests on top of a piece of cloth,
decorated with generic embroidery and beads.
Both were bought at the same Asian supermarket.
Next to these items,
there is a well aligned pile of compact discs,
the covers of which,
if they would be hung up on a wall,
would form a series of suspiciously happy looking portraits of cheerful people posing with their favourite exotic instruments,
interrupted every now and then,
by the odd photography of equally suspiciously beautiful landscapes from improbable holiday destinations.
The instructors,
a woman and a man,
both middle aged,
looking fit and tanned,
wait for their cue,
the end of one of their favourite tracks in this carefully crafted playlist:
Track 7 *The Elder Connections of Mindful Tibet*,
performed by a Canadian New Age artist on a synthesizer that sounds something like a Peruvian pan flute,
but not quite.
When the piece finally ends with a surprisingly long reverberation mixed with samples of tinkling bells,
it is the signal that they need to make their students aware of the change of pose,
the final in a series of twenty six,
a closing stance called *Kapalbhati in Vajrasana*,
which is the Sanskrit name for blowing in firm posture.
The moves they just finished are in fact one of the asana sequences that Indian yoga teacher Bikram Choudhury had attempted to copyright earlier this year and forced several yoga teacher groups to defend in court their now rogue practice as *open source yoga*.
While quietly whispering the words of a language that they do not speak or understand,
and as the next music track slowly fades in---this time a multi-layered composition of mostly monotonal instruments---the group starts an unsynchronised choregraphy,
with each one of the perspiring participants slowly moving from the one position to the next.
In this last effort of communion and selfless gratitude,
signs of relief can be seen on the face of many,
now rewarded by the physical effort and the subsequent release of β-Endorphin.
The motion is made more colorful by the visual patchwork constituted by all the fancy patterns of the leggings,
bought at online eco-friendly and fair-trade yoga shops,
forming another multitude of peacock tails in front of equally vivid non-slip mats,
also purchased from ethical and fair retailers.


At this exact moment,
a few hundreds people marching with petits fours and sparkling wine in hand, 
enter the underbelly of the Museum of London,
United Kinddom,
booked for the night to provide a space for an important evening dinner.
Guided by several well-mannered helpers,
the chatty column of people moves from one room to another,
passing archaeological artefacts,
interactive and multimedia science related installations,
and several large panels with big titles,
photos,
and much smaller texts.
These were created and strategically placed to provide some contextual references and landmarks,
so as to testify,
in the form of an entertaining and educational tour,
of the epic greatness of human civilisation.
Tonight,
the visiting sample of such greatness look particularly chic and smart,
with properly selected apperal,
accessories and perfume.
Most of them are holding a little piece of glossy cardboard,
on which is printed their seating position and table number.
Indeed,
if having the priviledge to attend a dinner in some of the most impressive rooms of the museum might sound like an eccentric delight,
the true motive of this exclusive gala is business networking.
In the same way that the informational posters have been so particularly placed in the venue,
its attendants have also been carefully placed in advance,
strategically grouped around a multitude of small round tables,
organised by themes,
sectors,
or affinities,
in the hope to foster exchange and provide a catalyst for fruitful synergy,
the outcomes of which,
who knows,
might lead to the need to produce more museum signage and vitrines in the future.
Some of the early birds from this défilé are quick to take a seat and very keen to start a more active participation in this event.
Indeed,
while part of those coming to the gala dinner tonight had either paid a generous sum to be present or had been invited specifically to support the cause,
for the rest of the group however,
this peculiar social situation is the conclusion of a long day of presentations and panels.
During the morning and afternoon conference,
they were exposed to other kinds of peacock tails,
in the form of colourful charts,
of all different shapes and forms,
and employed to represent and visualise pretty much anything possible.
One of the main narratives of the day was how transparency is a way to create trust in business,
and how it can also be used to make many things more efficient.
Furthermore,
the makers of such charts were excited to share how the capturing, calculated publishing and processing of information,
digital data to be more concise,
was a paradigm shift that would affect every possible field from economics to politics,
and, of course,
the arts,
which are represented here by a few selected works and artists illustrating the coming age of a new data-driven open culture.
The head still buzzing with new visions from an efficient democracy in which tomorrow's leaders will have access to the pulse of nations,
markets,
and literally anything that can be captured and sampled by machines,
they were now all ready to make the useful connections to turn this dream into reality.
With introductions tuned and optimised for maximum productivity,
the clever signposting of intention,
and cunning use of specific keywords,
the members of this elite of data openness,
representatives of private and public sector, NGOs,
universities, as well as various organisations,
would have been ready to start their hunt for new business cards,
if it were not for the sudden interruption from a man on a podium,
who was about to deliver a speech.
The man in his early sixties,
can hardly contain his excitment about what happened today,
what is happening now,
and what he is sure will definitely happen tomorrow.
All around him,
several LED displays are blasting numbers:
red,
yellow,
and blue percentages,
statistics about births,
death,
stock markets,
social networks usage,
weather reports,
and others that are barely readable due to the speed of the scrolling.
When his address is over,
the crowd responds with a standing ovation.
The starters are served,
finally.

\newpage
\setcounter{page}{1}
\renewcommand{\thepage}{\arabic{page}}
