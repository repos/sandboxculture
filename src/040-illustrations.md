\addcontentsline{toc}{chapter}{List of Illustrations}
\listoffigures

\pagenumbering{roman}
\setcounter{page}{9}

\newpage

\vspace*{8cm}
\begin{center}
\emph{In memory of Christine Mansoux}
\end{center}
\pagenumbering{gobble}

\newpage
\pagenumbering{roman}
\setcounter{page}{11}
