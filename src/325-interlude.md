Interlude {.unnumbered}
=========

\setcounter{footnote}{0}

On several occasions in this thesis,
I have shown that whilst it might be instrumental for some to describe free and open source software,
as well as free culture,
as movements,
it is more precise to decouple the constitutive techno-legal templates that they offer---and that are all derived from the original free software template---from their usage.
By doing so,
it allowed me to first highlight the cultural diversity present in the proto-free culture era,
and show how such pluralism decayed when aggregative models of free culture relying on economics, 
such as CC,
and a deliberative models of free culture relying on ethics,
such as Freedom Defined,
came into existence.
This decoupling also allowed me to demonstrate,
with art in particular and cultural production in general,
that even within the contemporary reduction and rationalisation of the free cultural framework,
as soon as such templates were used,
appropriated,
or transformed by practitioners,
they all materialised differently and were driven by radically different intentions and purposes.


One consequence of these findings seems to present a new challenge however.
Namely,
that such miscommunication and divergence within the free and open *things* discourse does not lead to a collapse of these efforts,
but instead strengthens it by constant renewal and adaptation to its environment.
Kelty's intuitive description of free software that is "constantly becoming" [@Kelty:nofs] comes to mind,
yet this does not fully explain this apparent contradiction.
This is why in the third part I have momentarily set aside the discussion on licensing,
and discussed instead the copyright respectful free cultural proposal where culture is essentially produced through remix and file sharing,
to show that in this liberal model of free circulation of information,
decentralisation favours the creation of opportunistic territories and agents where such access and circulation of information can be profitable.
This was my main motivation,
to first show that the democratic and egalitarian narrative of the RO versus RW metaphor can be deceptive,
as they need to be put in the perspective of broader consequences from liberal models and commodified approach to culture.
There is of course nothing new in this critique of digital labour [@Terranova:free],
however critiques of exploitation within free and open source projects have never managed to articulate why such systems are nevertheless so durable and passionately defended by their users.
As it turns out,
more recent analysis of participatory platforms populated with user generated content has started to offer other ways to think about such systems,
showing in particular that economic transactions are not the only form of exchange occurring in exploitative digital environments [@Lasse:google].
Similarly,
the question of participation in free and open source projects cannot be answered absolutely,
because of this *constant becoming*,
which can be illustrated, 
for instance, 
with changes in the work dynamics within the Linux Kernel which is now essentially done by employees of companies whose products or production infrastructure depends on the free software kernel [@CorbetKroahHartman:linux].
Essentially the political economy of free and open source software is affected by both the scope and scale of the communities revolving around such software,
and how many active developers are providing the majority of work.
In that sense,
it is not because a barely sustainable one-person project is licensed in the same way as a project that involves hundreds of "eyeballs" [In reference to @Raymond:bazaar],
that their mode of production is the same.
It seems obvious phrased like this,
but it is rarely taken into account when discourse around free and open source software become overly generalised to form a universal narrative.
What can be extrapolated from these remarks is that commodity fetishism and user manipulation,
are not enough to explain globalised arrangements of cultural production in which a few are able to economically benefit from the work of others.


Similarly,
awareness of exploitation cannot be taken for granted in environments that have mixed up operating systems with social systems.
Unlike the Jamaican musicians union standing up,
even if only symbolically,
against version music,
it has become increasingly difficult to position and locate oneself in liberal cybernetic constructions of nested home-factory hybrids,
in which one activity may or may not be unknowingly exploited at a different level.
I have argued that this aspect is particularly visible when the context in which data,
information,
and content are produced,
accessed,
and transformed,
is taken into account.
This is why in the previous chapter I used the Unix \texttt{chroot} command,
to push to its limits the RO versus RW file permissions metaphor,
precisely in order to start articulating a sandbox model.
This allows me to explain both how free culture is constantly becoming,
and why free culture is not an alternative to dominant means of cultural production,
but instead exemplary of the latter.
The somehow neutral sandbox model is also useful to break the discussion about the relationship between artefacts and politics [In reference to @Winner:limits, Do Artifacts have Politics?],
or to be more precise,
the mixed open and closed model of the sandbox allows me to avoid choosing between a model of control or a model of contingency,
to describe a networked culture in which both are nested within each other,
in their most extreme forms and without political agnosticism,
albeit sandboxed.


Linking back to free and open source software and free culture in general,
the sandbox culture I describe is therefore yet another consequence of the dual openness of code,
legal and software,
which I discussed in the first part of the thesis.
Effectively,
sandbox culture manifests itself by the nesting of certain practices through a techno-legal apparatus that,
first of all,
might not be immediately visible to its participants,
and second will be interpreted differently no matter how explicitly these apparatuses present themselves.
However,
because of the ambivalent liberal framework in which such sandboxes exist,
the free circulation of information is still possible inside and outside of these sandboxes,
and benefits greatly from public and private commitments given the wide range of possible interpretations,
from startup commercial exploitation to anti-capitalist artworks.


In this final chapter,
I will illustrate such a sandbox effect within the Pure Data community,
and also explain the role of conflict in these systems.
I will argue that at a lower level their defusing is a threat to cultural diversity and pluralism,
but at the same time,
when conflict occurs it leads not to the collapse of the sandbox but allows for its extension into other territories.
To show this,
I will revisit the notion of forking,
of both code and licenses,
and finally,
explain how the model of sandbox culture can help us think about free and open source dynamics differently,
introducing,
notably,
the concept of software exile.

\newpage
