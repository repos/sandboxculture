The Mechanics of Sandbox Culture
================================

A Day in the Sandbox Life
-------------------------

Pure Data (Pd) is a popular [See @flossophia] cross-platform visual programming language used by artists,
musicians,
and designers to create *patches*.
These are graphical representations of the real-time multimedia processes used for live performances,
installations,
audiovisual creations,
and more [@barknecht:echo].
The software was originally written by US mathematician Miller Puckette,
who had been involved in numerous projects related to electronic music.
One of the initial motivations to start working on Pd,
back in the mid nineties,
was to depart from the frustrations he had with his former employer,
the Institut de Recherche et Coordination Acoustique/Musique (IRCAM),
which at the time made it very hard for him to disseminate his research on the software Max which he wrote whilst employed by the institute.
Therefore one of the other meaning of the Pd acronym is a purposeful synonym of liberation from the IRCAM intellectual property sandbox:
Public Domain. [See @puckette:own]
So when Pd is announced in 1996,
it is in this spirit of dissemination of knowledge that Puckette concludes his paper,
musing and wondering about the future of Pd,
acknowledging the community aspect being as important as the software itself [@puckette:pd].
As it turned out the community that emerged around the software took Pd far beyond its author's "wildest dreams".[@puckette:own, p. 201]
Pd itself was however not distributed as public domain but was released under a modified BSD license,
a permissive,
copyfree license that permitted the use of Pd source code within closed source software,
to provide for instance some building blocks of the real time audio synthesis objects of the software Max/MSP [@cycling74:faq, Where did Max/MSP come from?],
or for the sound engine in the Electronics Arts video game Spore[@danks:spore].


I use Pd as a case-study in this chapter,
because the sandbox it managed to create has attracted a broad range of people,
from academic researchers to musicians,
computer scientists and artists,
all very much alert to the subject of free software and free culture,
due to the openness of Pd's source code and its tight links with several free and open source operating systems.
It provides, 
therefore, 
a very good multicultural context and meeting point for the different and sometimes incompatible interpretations of the free software techno-legal template discussed in the second part of the thesis.
In particular,
I will look at two events that occurred within the Pd user community which demonstrate what happens in these sandboxes when such incompatibilities become tangible.



### RjDj


RjDj was an iPhone app released in 2008 [@rjdj:ann] that promised to change the way music was consumed on mobile devices,
by bringing to the masses a generative and interactive sonic experience,
optionally taking advantages of the different sensors present on the Apple phone.
It was originally presented as a platform,
a new type of music label to some extent,
for composers to contribute and distribute such pieces 
^[Email from English computer scientist and sound designer Andy Farnell to author, May 28, 2013. Farnell was closely involved in the project.].
RjDj was however not developed from scratch,
its core component was Pd.


When the RjDj concept was originally introduced to Pd users in July 2008,
it was done through one of the founders of the Pd community,
in the form of an invitation to join intensive week-end working sessions,
where selected Pd users would be flown over,
fed,
accommodated,
though unpaid,
to contribute hacking and reflection on a new form of interactive music for mobile devices,
thought to be the next generation of Walkman or MP3 player,
in which the consumption of such algorithmic music would,
according to the project description,
result in effects similar to that of taking drugs [@rjdj:sprint].


From the perspective of existing Pd users,
RjDj was essentially a mobile Pd patch player.
For the Pd developers,
and without entering into technical details that are beyond the scope of this thesis,
RjDj was an inspiration and a new impulse to revisit an old desire [See @Lee2008],
that was the better decoupling of the *engine* part of Pure Data from its user interface,
thus creating a software library that could be used by other applications such as games,
embedded systems,
and other audiovisual frameworks.
At the time of the first announcement,
it was stated that the project would be built using several open source components,
and that most parts of the project would be released as open source software in return.
It was only a few months later,
and after few more week-end working sessions around Europe,
that the definitive form of the project became clear.


RjDj was not yet another artistic use of Pd that emerged from the Pd community,
it was the product of a technology startup (Figure \ref{fig:rjdj}),
Reality Jockey, Ltd.,
founded by Austrian entrepreneur Michael Breidenbruecker,
who had gained visibility in the early noughties as a co-founder of the music listening tracking service Last.fm,
eventually sold to CBS Interactive for $280 million in 2007 [@BBC:last.fm],
during the web 2.0 renaissance of Internet economic bubbles. 
So while the project got very good mainstream media attention,
it left some members of the Pd community perplexed,
seeing their favourite creative sandbox suddenly exposed in mainstream technology scenes and the tech startup field,
while still puzzling about how exactly these exciting developments and playful hacking sessions around Pd led,
within just three months,
to the appearance of a company planning to sell tens of thousands of $0.99 Pd-derived apps for its sole profit,
while not sharing back much of the iPhone specific source code developed for the project.



\afterpage{%
\begin{figure}[h]
	\caption{RjDj promotion}
    \includegraphics[width=\textwidth]{artists_rjdj} 
	\fnote{Screenshot: Reality Jockey Ltd., 2010}
    \label{fig:rjdj}
\end{figure}
\clearpage
}



To be sure,
from a legal perspective the actions of Reality Jockey, Ltd. were perfectly fine.
As stated in the second part of the thesis,
commercial exploitation of free software has always been central in the history of the movement,
and a copyfree license makes the release of modified source code completely optional.
This might have come as a surprise for some [See @looney:gpl],
but what created the impression of deception was predominantly the sudden realisation that even a rather niche free cultural practice can be influenced by global free market dynamics.
Said differently,
the enthusiasm to build something together created a positive feedback loop of generosity and mutual help within the Pd community---a form of collectivism similar to that experienced during the early experimentation of time-sharing systems---which made some forget to read or ignore the very rules of these sandboxes,
literally putting individual and personal trust and ethics before the way these were encoded at a techno-legal level.
This lack of understanding or interpretation of what constituted the sandbox was without much consequences,
as long as the community was partly isolated from the rest of the world.
With RjDj,
the free cultural idea of digital commons became fragmented once what was believed to be shared was questioned during public discussion where different motivations and interpretations of licensing were confronted [See @humphrey:nudge; See @makeart:rjdj].
Pd,
even if popular in the world of art and electronic music,
is neither part of GNU,
nor it is a project of the same scale as the Linux kernel.
As I explained in Chapter 7,
using the remix as support for the argument,
free culture promotes above all a liberal framework of free circulation of information,
in which transformation of information,
competition,
and opportunism are intertwined.
It is a perfect ground for entrepreneurial developments.


What is more,
the situation was further complicated by the mix and interplay of several licenses.
As it turned out,
due to conflict between the Apple developer's agreement and the GPL [@FSF:Appstoregpl],
it was not possible to distribute copyleft free software such as GPL software in the iPhone App Store,
which meant that the developers of RjDj made explicit that such GPL licensed software---this is the case for some popular externals distributed as part or next to Pd---should therefore be either avoided or re-licensed by their respective authors.
Practically speaking,
those willing to provide copyleft GPL work instead,
would have to assign the copyright of these files to Reality Jockey.
In layman's terms,
it means that the authors willing to participate in the development of some core components of the app,
must give up their software ownership to the company,
who can then re-license the outcome of such participation the way they want to incorporate them into an iPhone app.
^[
It is an interesting twist on the GPL copyright assignment strategy championed by the FSF,
and recommended to GNU developers who by giving up their source code ownership to the FSF,
also simplify the protection and enforcement of GPL'ed property by the foundation itself.
It would be unimaginable that the FSF would use this position of power to then dual license the source code,
or make it available under other conditions.
But technically copyright owners are able to do virtually anything they want,
regardless of who actually wrote the code.
]
Regarding the App Store's issue with GPL code,
this situation had already prevented multi-author free software from entering the publishing platform [@pidgin:ios],
as every individually copyrighted contributions is in fact a right to veto the re-licensing of software.
To avoid such a blocking situation,
RjDj defused potential conflicts by imposing a condition upon the acceptance of source code to its project,
which in this case is the choice between:
a copyfree'd contribution with original copyright,
or a copyleft'ed contribution with copyright transfer. 
It is this particular trick that triggered most criticism outside of the Pd user community,
namely how,
it was argued,
the GPL was used as "a firewall to protect commercial interests on a closed platform,
while exploiting the work of a free software community."[@mclean:toilet]


Such tension was also palpable on the side of those who had joined the RjDj project.
Some active Pd developers and contributors who were essentially doing voluntary work within the Pd community,
or integrating it as part of diverse university research and academic positions,
had been offered paid work via other developers already involved,
thereby also helping legitimise the product within the Pd community.
Australian freelance software developer Chris McCormick recalls:
"the RjDj thing seemed like it would be pretty wild and the pay was good so I went with that."
^[Email to author, May 31, 2013.]
But as the product was further developed,
the company's agenda seemed to contradict its originally advertised openness and respect towards the Pd community.
One of the RjDj developers,
French music signal processing specialist and Debian maintainer Paul Brossier,
who,
at the time had contributed foundation work on the audio engine and GUI,
told me that the requests from Breidenbruecker to remove the license and copyright notice of Pure Data to further close the project was "one drop too much"
^[Email to author, June 4, 2013.].
As a result of this conflict the developer left the project.
Another developer,
who wished to remain anonymous,
also shared with me how he felt at the time:


> I was very ambivalent.
> Always cheerful and enthusiastic about the team,
> the technical aspects of the project and its potential,
> but always on edge and suspicious.
> [...] There are last-minute meetings that some people do not get to hear about because they "are only technicians".
> Suits begin to appear that nobody knows.
> We hear about "great opportunities" which fortunately were raised,
> but how "compromises will have to be made."
> And the good people who believe in something more than money,
> smell the wind and start leaving.
> This was turning point for me because in it I saw a lot of my fears about the driving of a tech startup come true.
> The crisis and growing-up,
> for me,
> was to see that these things are how it works for this kind of people and are not reflections of my own cynicism or paranoia.
> Many companies use the idealism of young people as a weakness.
> Interest in openness,
> software freedom,
> innovation,
> passionate creativity exist as long as they are useful to recruit and train the team.
> Once the company starts making business transactions all that goes out the window.
^[Email to author, June 1, 2013.]


However,
as I suggested in the introduction,
I do not want to fall into a one-dimensional interpretation of such events.
There is not just one system of transaction in place and they are not all related to the questions of capital or ethics.
For example,
returning to McCormick's experience,
the interpretation of the process is much more nuanced and opens up new possibilities of further emancipation:


> [...] I think I always knew it was a proprietary company and I was paid to do a job within the confines of that.
> I am not even sure it's accurate to say that it was any less transparent than other proprietary companies,
> especially startup companies which are notoriously secretive.
> In the end I was happy that we got some things released under Free Software licenses let alone the whole stack.
> Actually I think if you look at other startups in the music space,
> we were releasing a lot more stuff as Free Software than others did,
> so I feel good about that.
> Some of it is even in use today and it really inspired some cool projects that wouldn't have happened without RjDj.
> 
> If I had have felt like we were actively violating any Free Software licenses that would have been a different thing,
> but I felt like myself and a couple of other people in the company worked hard to make sure that wasn't happening[.]
^[Email to author, May 31, 2013.]


What is more,
the sandboxing effect also happened at a level that was invisible and not suspected inside the Pd sandbox,
that is to say,
RjDj was also established within Apple's sandbox.
The latter's growing visibility experienced by the startup,
and its influence on indirectly dictating the technical and legal form and conditions linking with the Pd community,
eventually dragged the remaining energy out of the project.
^[Email to author from the developer wishing to remain anonymous, June 1, 2013.]
And finally, 
in October 2012, 
the RjDj app was removed from the App Store.
But as McCormick hinted,
as much the whole RjDj development was perceived as a deceptive process by some,
the free circulation of information across sandboxes also led to new opportunities that arose during the development of the app.
After the combination of several efforts,
most notably on the Android platform,
the project libpd---a library based on Pd's source code and which allows the development of standalone applications using Pd as sound engine---was released towards the end of 2010, 
and had also found its way into the iPhone tool-kit of the RjDj developers when the project was still running [@brinkmann:libpd].
In February 2012,
the website libpd.cc was launched next to a book on the topic [@brinkmann:making].
A link to a web forum,
distinct from the existing Pd mailing list and bulletin boards community,
was provided early on for discussions regarding the use of this library.
A new sandbox was born,
which tried to present itself in the least conflicting way possible:


> libpd is Pure Data.
> It is not a fork of Pure Data,
> not a different flavour of Pure Data.
> It is simply a way of using Pd in a new way that can be more convenient and allows compatibility with mobile app development,
> game development,
> embedding into sophisticated 3D visualization tools,
> and lots of other applications.
> As such,
> it adds to Pd,
> without taking away anything from Pd Vanilla’s DSP core.
> It has the same license as Pd,
> too.
> It is every bit as free and open source as Pd.
> As such,
> the project is hugely indebted to the entire Pd community,
> and to Pd’s original creator,
> Miller Puckette.
> Those of us working with libpd have done so because we’re excited to see Pd patches running in more places than ever before,
> doing things they’ve never done before,
> and we trust you’re just getting started. [@kirn:libpdcc]



From a software perspective,
the RjDj project is far from being simply anecdotal,
as it provided new perspectives and horizons for Pd users and developers,
and finally unified efforts around the creation of one library that can expand Pd's territory to other sandboxes.
Alongside this,
in the recent years the project inspired new derivative works,
frameworks,
players,
apps,
whether closed or open,
or in some sort of legal limbo.
It also provided new commercial opportunities for Reality Jockey Ltd.,
that,
after retiring the RjDj app,
kept on using parts of the RjDj software in other commercial apps
^[Email to author from Farnell, May 28, 2013.],
a practice that was already notably experimented with by the company in 2011 with the release of a promotional app for the 2010 science fiction film *Inception*.
In sum,
the whole story arguably mainly had an effect on the people who witnessed and participated in this process,
and it is unclear how this will transform the social dynamics around Pd in the long term,
now that the sandbox participants have seen the cracks in the wall,
and witnessed how the source code of an artistic iPhone app can be turned into a hub for different opinions,
ideologies,
philosophies,
economic imperatives,
and practices to collide,
not always in a pleasant way.



### Fuck the System



In Chapter 7 I argued that the mix up between social systems and operating systems was not just an arbitrary overlap or accident.
Thus behind the RO versus RW cultural dichotomy of Lessig,
using file system permissions as an analogy to describe cultural processes,
I have briefly explained that computer operating systems and their networking can provide different models of social organisation,
with different levels of transparency and policing,
from small-scale emulations of property-less pseudo-secret societies to panopticonesque chroot jails.
This led me to use the term sandbox to refer to these different architectures that have increasingly relied on techno-legal templates,
and most notably in the context of this text,
those derived from free and open source software licensing.
If this approach allowed me to create a counter argument---by simply looking at the ways cultural expressions are produced and not just accessed---to the trivial pro free culture binary RO versus RW,
I now want to discuss the refusal to engage with these sandboxes and their techno-legal fabric,
when they create a conflict of belief,
values,
or ideas.


To do so,
in this section I will examine the work from French noise and experimental musician and computer programmer Yves Degoyon
^[
The text from this subsection is based on a semi-structured email interview with Degoyon,
that took place during April 2015 and March 2016.
].
If some are busy pondering about file permissions,
Degoyon is more in favour of simply getting rid of the files and the whole OS at the same time.
This is the basis for his performance *rm -rf /\* :: f\*\*\* the system*---or */bin/rm -rf /\* :: f\*\*\* the system*---in which the musician performs using an audiovisual noise generating Pd patch,
while at the same time opening up a terminal on his computer and runs the command \texttt{/bin/rm -rvf /{*}},
that will in effect recursively force-remove  every file and directory under the root file system,
while the names and paths of said files and directories are printed on the terminal of his GNU/Linux distro.
Eventually with the file system emptied and only a handful of programs and data left in the RAM of the machine,
the computer crashes,
sometimes with unexpected behaviour,
and with it ends the performance.


Degoyon told me that the work is mainly an experiment in chaos and the instability of computer systems.
However he also admits that the title hints obviously towards a double meaning,
and the action that needs to be taken to get rid of a system before it alienates you.
Degoyon grew up listening to post-punk bands such as Wire,
Gang of Four,
and This Heat,
which while having widened the cultural scope of punk,
have done so, 
according to Degoyon, 
notably through the generalisation of punk's DIY spirit.
Here the punk connection can be deceptive,
because the title of the performance is to be understood differently from the way English punk singer Johnny Rotten claimed to have fucked up the system,
when he was part, 
with other proto punks and early punks, 
of what has been described as a working class Bohemia [@Frith:effects, p. 266].
Instead,
a more abiding connection would be the 1967 pamphlet *Fuck the System* by American political and social activist Abbie Hoffman,
a text filled with tips and advice to organise and survive in the "city jungle" and the development of a "freer more humanistic" society.
^[
The text also paved the way for a better known work by Hoffman,
the 1971 *Steal this Book*,
which I mentioned in Chapter 2 in connection to open design and DIY.
Of course Hoffman is not the only connection to be made here.
Sixties anarchist guerrilla street theatre group the Diggers were early explorers of ideas of anonymity,
freedom of association,
and societies free from private property,
using a wide range of practices from direct action and art happenings,
to the publication of leaflets and manifestos. [See @Grogan:ringo].
]
So it should not be surprising that in his approach,
Degoyon feels more connected to the early days of British collective and arnarcho-punk band Crass,
which he often quoted and referred to during heated mailing list discussions,
where the link to avant-garde art and anarchist political movements was not a trivial appropriation as it was in other early punk bands,
but was more seriously explored via direct action and zine publishing,
so as to advocate animal rights,
anti-war,
anti-consumerism,
vegetarianism,
environmentalism and feminism.
[See @Kugelberg:crass]


When Degoyon started to use and write free software,
it is through this art punk anarchist inspiration that he engaged with this particular digital form of knowledge sharing.
During our exchange,
he refereed to the Spanish video collective R23 [@r23:2005],
founded by artist and computer scientist Lluis Gomez i Bigorda,
as an example of introducing such elements into media art practices.
Degoyon contributed to R23 DIY streaming media projects and network mapping in the early noughties,
and admitted to enjoying the perturbation generated with the introduction of "some spirit of activism in the polished world of media art"
^[Email to author, April 8, 2015.],
at a time where the mix of free software and art offered a self-organised and decentralised alternative to artistic media labs [See @DekkerPlohmanFoldenyi:nop].
However,
what was first perceived as an ideological alignment between Degoyon's beliefs and the technological environment he was contributing to,
unfortunately quickly turned into something very illustrative of the alienation expressed in his performance.


One of the software project actively developed by Degoyon at the time was PiDiP [See @Degoyon:pidip],
which stands for PiDiP is Definitively into Pieces,
a BSD-style licensed Pd external that brings extra video processing capabilities and builds upon the GPL'ed Pure Data Packets (PDP) Pd video processing objects by Belgian software and hardware developer Tom Schouten [@Schouten:pdp],
and also sharing some code with GPL'ed real-time video effect software EffecTV,
originally developed by Japanese programmer Kentaro Fukuchi.
But two events made Degoyon question the relationship between his political views and free and open source software production.
He explained to me that the first event was a conversation with a CCTV company in 2004,
that was present in an international meeting of activists in Switzerland,
and that was interested in using free software technology for motion detection.
The second event occurred at a free software meeting in Brazil in 2005,
where representatives from the army were assessing the viability of using free software in their surveillance systems.
Degoyon told me that he obviously could not accept that,
and was the reason he first first decided to add a clause to his BSD-style license "NOT FOR MILITARY OR REPRESSIVE USE !!!",
and later on take a more radical step by releasing PiDiP under his own license in 2010:


> to cut with all legal blah-blah,
> this license will be made short.
> 
> the code published here can be studied,
> modified, used by anyone that
> provides all the original credits
> and sources in derivative projects.
> 
> there are restrictions on its use,
> it cannot be used for :
> 
> * military amd/or repressive use
> * commercial installations and products
> * any project that promotes : racism, nationalism, xenophobia, sexism, 
> homophobia, religious hatred or missionarism .. ( expandable list)
> 
> this is not a standard license.
> 
> sevy & authors.
^[
LICENSE.txt file from the PiDiP CVS repository,
revision 1.1.1.1,
commitid: MR5avkuVSyEPgbZ,
2010-12-06 06:31:45.
The typo will be fixed with commit aOzDtQZu7yTgVL9v,
in February 2011 for version 1.2.
]


These two changes in PiDiP's licensing terms are an interesting case of fucking up the sandboxing system.
Degoyon,
who told me he had originally chosen a copyfree
^[
For an an explanation on copyfree licensing see Chapter 5, The Double Misunderstanding with Copyleft.
]
BSD style license because it was like Pd's own license,
was in fact releasing a software containing an assortment of code from copyleft'ed EffecTV,
bits and bytes from other sources and collaborations,
and also his own code written from scratch.
By initially releasing PiDiP with a non-copyleft non-GPL compatible license and yet using some copyleft’ed parts,
he was breaking the GPL and misusing the copyright of others.
A GPL-respectful way to publish PiDiP should have been for instance either under the GPL,
or as two collections of source files,
the GPL modified ones under the GPL and the others under the BSD style license or else,
assuming Degoyon did not use other chunks of source code licensed differently,
in which case further fragmentation of the software would have been necessary in case of license incompatibilities.
But Degoyon cared little about that fact and in 2006 stated on the Pd mailing list,
in a very art punk anarchist way,
that people should not forget that PiDiP contributors like to "confuse lawyers and boring people first". [@Degoyon:effectv]
Funnily enough,
the original mis-licensing---when PiDiP was distributed as BSD yet including GPL code from EffecTV---did not prevent the software to be successfully validated by FSF employees and listed in 2003 by the FSF directory with other *useful* free sofware---as I discussed earlier in Part 2---which shows that traceability and transparency in free and open source software has its limits.


Regardless of Degoyon's little interest in respecting licensing terms---a situation which shows some ressemblance with Stallman's early EMACS days where code circulation was more important for the hacker than diligent respect of copyright laws^[See Part 1.]---was an important figure of the Pd community,
whose software was used by several artists and packaged or distributed by other developers.
However,
this started to change in 2005 when the licensing issue was brought up in the Pd mailing lists.
The issue dragged on for years with extremely heated discussions on the user and developer lists of the software.
Degoyon's contributions to the debate tended to add oil to the fire as he explicitly grounded his refusal to change his license based on political motivations---with even more oil poured when he started to change the BSD license into a non-copyfree non-military license---whereas those asking him to conform acted as a sort of neighbourhood watch system,
trying to enforce the cyber constitution of the Pd sandbox.
I use the words neighbourhood watch here,
because in fact *only* Kentaro Fukushi,
and possibly other contributors of EffecTV,
were the ones who could require their licensing to be enforced.
As it turned out,
Degoyon and Fukushi had already met on several occasions previously,
and the EffecTV author knew of PiDiP and appreciated the fact that his work had been ported to Pd.
His silence on the mis-licensing matter may have seemed to indicate he cared little about the potential licensing problem with PiDiP.
However,
as Degoyon was further pushed in to a corner within the Pd community,
which in turn led to the radicalisation of his licensing strategy,
PiDiP started to break more constitutive mechanisms of other sandboxes,
such as operating systems like Debian,
or free and open source software hosts like SourceForge.
Simply put,
by means of TOS, 
social contracts,
or other usage agreements,
these platforms and operating systems can implement their own definition of software freedom,
which help decide which licenses they allow,
ultimately shaping the software culture they distribute.
PiDiP's new license was incompatible with many of such definitions.
Eventually PiDiP became,
in 2010,
a *software non grata* removed from the Pd repositories and distributions
^[
This removal was effective with commit r14502 from the Pure Data SVN code repository,
which motivated Degoyon to start hosting his own public code repository on giss.tv and change the license even more,
as discussed previously.
].
At time of writing,
PiDiP,
the impossible *copypunk* source code,
only exists in a limbo of various repositories outside and disconnected from the Pd community,
but it is still listed in the EffecTV project links as well as in the FSF free software directory.


Within the free cultural techno-legal template,
the practice and intention that led to the creation of PiDiP,
a software that grew organically from the encounter of the author with other artists and developers---and the source code they wrote, notably within the projects of the R23 collective---became incompatible with its technical and legal framework.
It challenged the definition of freedom carried by the sandbox it was born within,
and illustrated the non-trivial interaction between the changes through the years of an author's thoughts,
the fluidity of the digital medium his creation was written in,
and the rigidity of its legal framework.
In such a situation,
PiDiP,
published by a rather proud outlaw [In reference to @Degoyon:illegal],
nonetheless found a deadlock and the execution of its *legal* instructions became eventually incompatible within the system it was developing,
as opposed to its perfectly running *software* instructions.
This example shows once again the strength of the techno-legal template,
and its dual level of interpretation by machines,
and humans,
initially discussed in Chapter 1.
To be sure,
Degoyon's stand should not be marginalised or neglected because it was the response of an artist in the context of a niche software community.
In fact,
similar responses and critiques towards free and open source projects have also been articulated notably by Felix von Leitner,
a German IT security expert and ex-member of the Chaos Computer Club:


> This is what we get when our free software licenses lack a 'not for military purposes' clause:
> DARPA presents a weapon control system on the basis of Android tablets <http://www.darpa.mil/NewsEvents/Releases/2015/04/06.aspx>.
> Linux is now killing people.
^['Das haben wir jetzt davon, dass wir in unseren freie-Software-Lizenzen keine "nicht für militärische Anwendung"-Klausel haben: DARPA präsentiert ein Waffensteuerung auf Basis von Android-Tablets. Linux tötet jetzt Menschen.' Translation Florian Cramer. [@vonLeitner:DARPA].]


More recently, 
in 2015,
one of von Leitner's own GPL licensed free software projects,
dietlibc,
a popular lightweight C standard library [@vonLeitner:dietlibc],
was shown to have been used in products sold by Hacking Team,
the Italian Information Technology company specialised in providing corporations and governments with intrusion and surveillance technology.
Next to the breach of the GPL copyleft,
this situation further prompted Leitner to call for a NOMIL/NOINTL license,
and started to put in motion a modification of the AGPLv3 as a foundation for such a license [@vonLeitner:NONMIL].
von Leitner's effort is not singular,
and there has been in the past several projects that became non-free and non-open source software,
in spite of the availability of the source code,
simply because they used statements [See @Green:NONMIL],
or licensing techniques that exclude military usage like the Peaceful Open Source License [@Diwan:POSL].



PiDiP,
whose name indeed announced its demise,
precisely shows what happens when the license as community law take over the values it was thought to be defending.
Accepting to use a specific license against one's own beliefs brings the risk of creating cognitive dissonance,
and Degoyon avoided this by putting his beliefs before the sandbox's rules when he noticed the contradiction created by the situation.
But even though passion and affects are crucial in creating allegiance to democratic values [@Mouffe:agomodel, pp. 199-200],
they must be removed from the rationalised model of free culture for the latter to operate smoothly,
and could explain why some participants of free and open source projects present their work detached from political intentions [@Coleman:agno].
This is not just an issue of social dynamics within small communities,
but it is also visible in the way the infrastructures that support free culture operate.
To give a short example,
in 2009,
the jsmin-php software was banned from Google Code because the software had inherited the license of jsmin.c it was based on,
a license that was a modified version of the free and open source software MIT license.
The modification was one line stating "The Software shall be used for Good, not Evil",
which made the software non-free and gave the "Don't be evil" company a reason to exclude the code from its free and open source software hosting platform [See @Grove:jsmin].
Interestingly enough,
and linking back to my earlier neighbourhood watch analogy,
Google did not scan their repository for non-compliant licenses,
they were simply informed by another user in the main discussion forum of the Google Code virtual community [@Goode:jsmin].


As shown with these examples,
there is only a thin balance between the free software *Gemeinschaft* emulation,
and the implementation of a cyber disciplinary society.
Free culture in this context is far from being the liberating and pluralistic tool it seemed to be,
or to be more precise and to refer to the first part of this thesis,
I have shown with this example that the aggregative and deliberative democratic models of free culture,
have risen at the cost of antagonism and radicalisation of cultural practices,
by limiting rapid cycles of hegemonic and counter-hegemonic efforts,
that used to be more prominent during the chaotic era of proto-free culture.
As a result,
free culture sandboxes become absolute democracies in which not only artists such as Degoyon,
but any participant in fact,
are effectively forbidden "to engage with a multiplicity of agonistic democratic struggles to transform the existing hegemonic order" [@Mouffe:cultworker, p. 215],
because their software becomes a threat to a public space that according to the defined free culture can only exist as a consensual thing,
and that is defined by certain parameters that rely on the exclusion of others.



Fork the System
---------------


Next to complete obedience or complete resistance,
one particular side-effect of a free cultural mechanism that promotes the circulation of information over the context of its production and usage,
allows a third approach to engage with sandbox dynamics:
forking.


Forking can be described as the process by which the source code of a piece of software can be modified,
so as to make,
for instance,
new software integrating modifications,
minor or major,
that would not have been accepted by the author(s) and community from which the fork stemmed,
or simply to explore transformations unforeseen by the original authors of a work
^[For a general explanation regarding forking in free and open source software culture,
some historical references,
and a case study with the Debian and Ubuntu operating systems,
see [@Hill:fork].
].
The divergence of source code and the proliferation of concurrent versions of the same software is not specific to free and open source software and became an important aspect of source code sharing in the early days of UNIX ,
as it was discussed in Chapter 1.
It has also been argued that copyleft development could either deter forking motivated by competition,
and allow merging back at a later stage if forking occurs [@StLaurent:understanding, pp. 171-173].
However,
the rationalisation of source code sharing with the creation of free and open source software licenses,
can also be interpreted as taking a radical path towards divergence,
a "right to fork" [@Weber:success, p. 159],
regardless if open forms of developments are made mandatory as with copyleft licenses.
In that sense license-assisted forking can be seen more as a liberal remix-culture-oriented free culture approach,
than a community-binding copyleft mechanism.
Both are in fact different materialisations of the rules of software freedom.
Due to the difference of context in which such materialisation occurs---acquiring existing work versus contributing to existing work---forking originally had as a result,
a very bad reputation.
Yet,
it has risen today to become a very important mechanism central in the writing of free and open source software,
in the age of connected machines and users,
and an important component in sandbox dynamics and underlying mechanics of the constant becoming in free and open source software communities.


Before elaborating on the details of such a mechanism---notably with the software \texttt{git} that I will introduce later in this section---I must first briefly explain how forking has co-evolved with the different generations of tools which have facilitated the writing of software.
What is interesting in this co-evolution is the apparent contradiction between the desire to develop a very liberal approach to producing and distributing software,
but done so through the very techno-legal means and methods that will later be feared by those defending such a liberal system.
In particular,
libertarian computer programmer Eric S. Raymond,
who famously articulated the negative consequences of forking:


> Nothing prevents half a dozen different people from taking any given open-source product
> (such as, say the Free Software Foundations's GCC C compiler),
> duplicating the sources,
> running off with them in different evolutionary directions, but all claiming to be the product.
> 
> This kind of divergence is called a fork.
> The most important characteristic of a fork is that it spawns competing projects that cannot later exchange code,
> splitting the potential developer community. [@Raymond:homesteading]


Here it becomes clear that the fork is more than a threat to these communities,
it is a threat to the mechanism of reciprocity which is central to the gift economy [@Mauss:gift],
and which inspired Raymond to describe free and open source software community as gift culture [@Raymond:homesteading]. 
Of course,
as I explained previously in this third part,
and regardless of the desires and mechanisms of reciprocity put in place,
it is to be expected that a system deeply inspired by classic liberal dynamics will create competition between different actors trying to maximise profit,
whatever this profit is,
either financial or based on the free circulating information they can access to.
In that sense,
forking can become a tool to accelerate competition.
Raymond however seems to preemptively defuse the problem by arguing that there is a discrepancy between what he calls *the yield* implied by free and open source licenses,
which according to him is only *use*,
and the yield of participation in the production of free and open source software that is "peer repute in the gift culture of hackers,
with all the secondary gains and side-effects that implies" [@Raymond:homesteading].


In this context indeed,
forks are therefore negative for the community as they "tend to be accompanied by a great deal of strife and acrimony between the successor groups over issues of legitimacy,
succession, and design direction." [@Raymond:jargon422, *forked* entry]
The fork here is seen as a form of failure to reach consensus around a common techno-legal authority,
that should in theory satisfy all the inhabitants of the sandbox.
But given its political power,
the threat of forking could also work as part of a strategy to influence the direction of a project,
and has been described as similar to a "'vote of no confidence' in a parliament" [@Wheeler:why],
a convenient way to work around the effective vote-less rough consensus found in some of these communities [@Stadler:solidarity, p. 39]. 
Therefore in the early days of free and open source software development,
the fear of forking may have worked as a glue to assemble and maintain large software community sandboxes,
where the desire for liberal and libertarian structures was nuanced by the necessity to maintain cohesion in these world of techno-legal social systems,
leading to a sort of macro liberalism.
Another account is to note that in certain cases,
the trademarking and other means of protecting the name of a project has helped discourage the creation of competing projects [@StLaurent:understanding, p. 173].
Lastly,
it was argued that the trading aspect of free and open source software development shared resemblance with iterated games around reputation,
and thus the fear of forking introduces a reputation risk [@Weber:success, p. 159].
Said differently,
it may have not been the threat of schism, name protection, or reputation,
that limited the proliferation of radical software freedom,
that is forking,
but simply that the act of forking took significantly more effort than solving issues within an existing community.
However,
another explanation could simply be that the development platforms available at the time were simply not flexible enough to facilitate forking,
therefore prevented a more radical take on software freedom and the free circulation of information.


In the history of software engineering,
tools such as version control systems (VCS),
also known as revision control and source control,
have allowed developers to keep track of changes in software.
When Marc J. Rochkind started research on VCS in 1972 at Bell Labs with the project Source Code Control System (SCCS) [@Rochkind:sccs, p. 369],
running both on IBM 370 OS and PDP 11 UNIX,
the idea to approach software development to reflect on the continuous and concurrent nature of software engineering was deemed radical [@Rochkind:sccs, p. 368],
but it was not entirely new,
because IBM had already been working on a way to facilitate and control software engineering with their 1968 CLEAR-CASTER system---the combination of the Controlled Library Environment and Resources (CLEAR) and the Computer Assisted System for Total Effort Reduction (CASTER)---so as to provide a unified programming development support system and batch processing system.
In the CLEAR-CASTER system,
changes to source were notably detached from the actual source text to facilitate the keeping track of changes as well as providing contextual documentation for the software [@BuxtonRandell:NATO, 5.3 Support Software for Large Systems].
These VCS and others from the first generation,
to borrow from Raymond's classification of such tools [@Raymond:vcs],
worked by sharing the same file system,
but with the rise of computer networks and remote access to computational facilities,
VCS eventually evolved to adopt a client-server model.
This shift occured with the Unix tool Revision Control System (RCS) created in 1982 by German computer scientist Walter F. Tichy [@Tichy:rcs],
first following a local data model,
the functionality of which was enhanced in 1985 by Dutch computer scientist Dick Grune [@Grune:CVS] so as to facilitate collaboration across several users.
Grune's work eventually led to the creation of the Concurrent Versions System (CVS),
that existed,
not without some irony,
as two concurrent projects [@Grune:CVS].


As part of a client-server VCS like CVS,
or its successor subversion (SVN) introduced in 2000 to improve some of the flaws of CVS [@PilatoCollinsSussmanFitzpatrick:svn, pp. xiii-xiv],
the code repository is commonly served from a single machine,
the server,
that keeps track of all the changes in the source code.
For instance,
a programmer can use a VCS client software to retrieve changes made by other programmers and which are stored remotely on a machine running the VCS server software that serves and tracks changes in the central repository.
The programmer can then make further modifications locally on their personal machine,
and eventually commit changes to the central repository,
granted they are allowed to do so by the server.
It is not difficult to see that there is a lack of balance in this control structure because developers can be denied access to the central repository.
But it also means,
that getting access to the whole database,
the history of the project,
is not trivial because all of this is handled on the server side.
On the other hand,
because of this gated and centralised architecture,
requesting access to a project VCS,
to be trusted with such access,
can only be done by socially interacting with the community or group working on the software.
Changes to the system are therefore also scrutinised and discussed within these same groups and communities,
as access to the main VCS repository of a project does not imply anything can be committed.
But it is important to note that once again,
those in charge of writing software within such environments are not necessarily those able to change and modify such software environments,
and the writing of software can be done following many different participatory and managerial models,
often referred to as *governance models* within free and open source software management discussions [@GardlerHanganu:governance].


In 2005 Scottish artist,
writer,
and programmer Simon Yuill introduced the concept and framework of Social Versioning Systems (SVS),
used in his social simulation game spring_alpha [@Yuill:SVS],
where players are invited to take part in an uprising to form an alternative society to that of the capitalist,
normalising and disciplinary world they've lived in so far.
Next to traditional game mechanics derived from interactive fiction and open-ended world simulation,
the novelty of spring_alpha is that players were able to re-write the code that runs the simulated world [@Yuill:SVS],
a process both facilitated and tracked by SVS.
SVS and spring_apha are both inspired by,
and illustrate well,
the constitutive and social dimension of the free software techno-legal templates that lead to the creation of sandboxes,
whereby rules can be theoretically challenged and modified following different models of participation.
One aspect of SVS in particular was prompted at the time by the growing availability of tools to monitor,
visualise and further track changes within version control code repositories,
as well as quantify and contextualise them.
Looking back today at the way the tracking tool provided by SVS pushed the idea of VCS as a glue to bridge social systems with their techno-legal frameworks,
it is striking to see how some of the principles provided by this critical art and research project announced,
coincidentally,
an age in which VCS are nowadays combined and interleaved with discretised and "computable orderings" [@DuPontTakhteyev:space],
not however to reprogram the social systems they're used in---and this is the key difference---but rather to further order and control software work and dominant modes of production,
as best exemplified with the social-coding platform GitHub [@DuPontTakhteyev:space].


Indeed,
if Yuill's ideas were rooted in the understanding that the moral and social aspects of work were not solely determined by technology,
as Coleman explained with her work on free software communities as high-tech guilds [@Coleman:guilds],
and therefore whose dynamics had the potential to be internally contested and challenged with very rare occasions of forking, 
this was not without counting on two aspects.
The first is as I described earlier with the two Pd examples,
which showed that the immutability of the legal fabric of these sandboxes in practice greatly limits counter-hegemonic efforts.
But most importantly here,
the second aspect is that such analysis and work were highly dependent on the state of all these software frameworks that helped manage and control software production.
If client-sever models of version control,
for instance,
introduced a great change and reinforced the role of governance models---a sort of golden age for systems based on Raymond's description of bazaar versus cathedral and benevolent dictatorship versus meritocracy [@Raymond:bazaar]---the third change in the history of such tools,
which I will now introduce,
is without question the one that will exacerbate the tension between the two approaches to software freedom that I have introduced in this section,
and as a consequence the tipping point that will change the way forking was perceived thus far.


This third alteration is the replacement of client-server architecture with that of distributed version control system (DVCS).
With DVCS,
there is no more central repository,
and no more fixed topology for the networked organisation of software production.
Because each DVCS is both client and server,
every copy of the project *is* a fork and the programmer works first on their local copy before deciding to push which part of their changes and to which other repository.
At first this model seems to suggest a less rigid relation between the embedding of moral and social aspects of work in technology,
because indeed in the case of DVCS it is up to social conventions to shape the network topology of software production,
and this with extremely great flexibility,
with the possibility of breaking free from the more traditional models of governance.
However when several DVCS implementations---such as arch, bazaar, codeville, darcs, git, mercurial---started to gain popularity in the mid-noughties they were not perceived positively [@CollinsSussman:risks],
precisely because "the very conveniences [DVCS] provides also promote fragmentary social behaviours that aren’t healthy for [free and] open source communities" [@CollinsSussman:risks].
It is a threat because the historical situation becomes suddenly inverted:
forking takes less work and effort than interacting with an existing community.
Sending changes back to other code repositories becomes optional,
and depends on the willingness to interact with other developers,
and of course the willingness of these to accept changes.
Above all,
DVCS shows that the old assumption where "it will almost always be more economical for a potential forker to try to get the technical changes he wants incorporated into the existing code base [...],
rather than to split off and try to create a new community" [@Weber:success, p. 160],
might have been wishful thinking,
or at least needs serious revision.


However,
in the same way the success story of the Linux kernel project helped construct the nineties free and open source software narrative of many programmers collaborating and working together,
and became a poster child for the bazaar and benevolent dictator model of free software governance,
the same project is at the centre of a shift in mentality regarding forking.
As mentioned in the previous part of this thesis,
Android,
Google's mobile operating system,
relies on the Linux kernel,
but due to several issues that are not so relevant here [@VaughanNichols:android],
Google's work on the kernel was essentially done on a branch which grows further away from its original source,
with little to no possibility of merging back changes and additions.
In turn,
the initial contributions,
then abandoned,
from Google to the mainline source code repository were removed.
The conflict was initially framed as a stereotypical situation were communication is difficult but forking is easier,
but what was new here,
is that next to the usual knee-jerk response of forking as a threat to communities and the reciprocal blaming for which party was at the source of the situation,
there was a subtle shift in the perception of forking.
Chris DiBona,
American software engineer and director of Open Source and Science Outreach at Google,
posted during the tense exchanges of 2010:


> [...] this whole thing stinks of people not liking Forking.
> Forking is important and not a bad thing at all.
> From my perspective,
> forking is why the Linux kernel is as good as it is. [@DiBona:stink]


The rise of DVCS put in motion a process in which forking transformed from vice to virtue.
because in effect it offered a way for sandboxed communities to go forth and multiply by following this radical materialisation of deregulated software freedom,
and expanding the development of the *metacommunities*,
"sparsely or thickly connected populations of objects, users, producers"[@FullerGoffeyMackenzieMillsSharples:bigdiff, p. 89],
that surround code repositories.
But this new approach also launched into fame a web platform such as GitHub,
in leading the self-coined trend of social coding,
that sits at the cross-roads of social networks,
project managements tools,
and revision control [@FullerGoffeyMackenzieMillsSharples:bigdiff].
On GitHub,
anyone is able to have several public \texttt{git} repositories,
a popular revision control system,
and is given the ability to fork any other repository by clicking on a button,
simply called *Fork*.
The button is enhanced with a counter that reveals how many forks have been made of the given repository,
making explicit,
within this platform,
how forking ends up as a popularity contest.
Users of the platform are also able to contribute back changes they make to their fork,
to the parent repository,
and employ a specific property of \texttt{git},
which allows them to cherry-pick changes made in other forks.
These basic operations represent the so-called "social life" of code sharing on GitHub [See @Mackenzie:175].
They can also simply ignore the parent repository and give a new context to their fork.
In fact other features offered by both the \texttt{git} software and GitHub itself,
and the ability to track all these,
could have the potential to provide a rich "account of how people move through code" [@Mackenzie:traffic, p. 86],
and generally speaking the reason that leads scholar Adrian Mackenzie to argue that "software today is less like a machine,
a system or even an assemblage,
and more like a crowd." [@Mackenzie:traffic, p. 87]
But given everything discussed so far in this thesis---from the proto free and open source era of computational culture,
its strange modes of organisation and the UNIX fellowship,
and of course the Cambrian explosion of free and open *things* triggered by free culture---this analogy to the crowd could easily apply since the early days of code sharing.
In fact,
private forks and exotic code-hosting platforms are nothing new,
but GitHub contributes an authoritative centralisation and forced visibility of such practices.
The shift is not so much from machine to crowd,
but---and expanding on Mackenzie's urban metaphors---it is the transition from rural coding communities to the coding city crowd,
through the means of the *Gemeinschaft* emulation originally triggered by the use of free and open source techno-legal templates. 
But more importantly here,
this crowd is in fact trapped.
While GitHub provides very effective,
and easy to use,
tools to facilitate the self-organisation of communities around one single repository,
there is a catch.
To permit the construction of extra systems on top of the \texttt{git} DVCS the repositories are forked *within* the GitHub platform,
thus revealing the irony of centralising a completely distributed system into one giant... sandbox,
where almost one half of the repositories are forks from other repositories [@Mackenzie:large].


While networked decentralisation has been perceived as an empowering instrument,
as best exemplified with Dmytri Kleiner's Peer-to-Peer Communism vs Client-Server Capitalist State [@Kleiner:teleko],
the techno-legal mechanisms that permit such decentralisation have been greatly overlooked.
In retrospect,
it is clear that when P2P rose to popularity,
it first appeared to provide a lightweight,
democratic,
and nomadic alternative to the client-server models of transactions and capitalisation,
but that was however not counting without how this new model could also be embedded into other systems of different nature.
This is once again very well illustrated with GitHub and shows that no matter what is the topology of network labour,
there will always be opportunities to create overlapping structures to control and capitalise it.
In the case of GitHub,
this capitalisation is moved to another level.
What has escaped from the control of macro-liberal/micro-communal groups is now collected by this platform,
a new form of browser-assisted massive local file system source control à la CLEAR-CASTER,
a shared and collaborative file-sharing app for programmers in the age of Internet turned into an Operating System [In reference to @OReilly:internetos],
worse,
a download site [@Mackenzie:175].
Similarly,
it is possible to witness how the *yielding* effect suggested by Raymond,
can be captured by a platform like GitHub.
It does not matter what the yield is and it certainly is a variable element,
but while the *use* of software can escape GitHub as easily as with a clone command,
its context cannot be extracted from the different additional social and technological features that GitHub has built around the popular DVCS.
In the process the licenses are replaced with Terms of Services,
^[
A recent study in 2013,
even if it was essentially simple data scraping,
showed that out of nearly 1.7 million code repositories on GitHub,
less than 15% had a license.
See [@McAllister:study].
]
and the employees and founders of the platform,
whose core components are strategically closed source [@PrestonWerner:almost],
are the ones to decide what projects and behaviours are acceptable.
They establish a nearly feudal meta-model of governance on top of the communities and groups they host,
occasionally taking advantage of their overarching landlord position,
thanks to the newly-acquired virtuous property of forking,
to directly tap for their own benefit into the gigantic pool of disposable code they host,
regardless of the damage this creates to independent programmers turned sharecroppers [For an example of such abuse see @Mansoux:fworkers].


On Forking Homes, Sandboxes and Software Exile
----------------------------------------------


The idea of a sandbox culture is not to be understood solely metaphorically.
The way cultural processes and transformations take place in free culture are always contained within strict boundaries,
whether it is from the software structures that support human communication,
the policies that shape them,
the source code and other digital cultural expressions produced,
and finally the licenses and other legal and pseudo-legal mechanisms that regulate cooperation.
At each level the effect of sandboxing can be perceived,
yet these sandboxes not only do not necessarily align in terms of ideologies,
they also exist as multiple concurrent systems of belief,
for social groups that are thus not federated around a belief,
but around techno-legal systems that work as a mediation between style and beliefs,
thereby preventing direct confrontation and providing an illusion of common and shared intent.


This creates a situation where the more context is given to analyse what happens in a techno-legal sandbox,
the more difficult it is to rationalise it,
always switching from one state to another.
In Evil Media,
British critical theorists Matthew Fuller and Andrew Goffey proposes a way to avoid such deadlock,
and explore instead the colourful corpuscles that constitute greyness.


> [I]n a period in which it is difficult to trace patterns of conflict and the emergence of antagonism back to a single binary opposition with any degree of plausibility,
> the gray zones of gray media call for new forms of investigation and a nuanced approach to the kinds of tensions and patterns of interference that arise. [@FullerGoffey:evil, p. 31]


The approach of technology as either black and white boxes does not work any better than its underlying dichotomous narrative.
We are all working in white or black boxes nested inside black or white boxes,
themselves components of many other black or white boxes,
like a Kafkaesque Matryoshka doll.
In this situation,
if there is control and exploitation over work,
they are increasingly exercised at a different level,
which is not perceived by participants who are still able to develop a local culture,
cooperative models,
and experiment with all sorts of techno-legal frameworks,
therefore allowing opposing values,
ethics,
and politics to co-exist and grow stronger.
Rather,
the greyness of the systemic ambiguity found in free culture so far is always encoded as discreet binaries,
and it is the dithering between these that give the illusion of different shades of grey,
because the ambiguity raised by the dithering of black and white binaries is in reality razor sharp,
well defined,
and coherent *within* in its own local logic.
There is indeed little fuzziness left when the rules in place are interpreted strictly with the matching apparatus:
source code is compiled successfully or not,
licenses are compatible or not,
definitions help filter licenses into precise groups,
access is granted or revoked based on policies,
and so forth.


If greyness there is,
it is a cultural illusion,
or more precisely the by-product of a constant motion from one sandbox to another.
These sandboxes,
whether they manifest themselves as a license,
a software community, 
a tool,
or even a file system,
are not perfect.
There is always a moment of tension,
conflict,
a form of ugliness in their idealistic interfaces.
It is during these moments of conflict that some of its inhabitants are given a small window of opportunity,
out of which they see through the flawlessness of its rules,
and might understand why some had described the perfection of these simple systems as illusions. [@Timberg:Lanier]
These are simple because,
to borrow from Mouffe's critique of rationalist and individualist forms of liberalism [@Mouffe:political, pp. 10-11],
such sandboxes are archetypical of a form of liberalism that is unable to hold the pluralistic nature of the social world,
in which conflict cannot be solved rationally and there cannot be a fully inclusive *rational* consensus;
such conflicts are thus ignored and pluralism within sandboxes must be presented as harmonious and non-conflictual assemblages of different perspectives and values.


Simple yes,
but not merely illusions.
To be sure,
these sandboxes create a cybernetic illusion in which society can be described in terms of simple operations,
but the framework in place,
the infrastructural implementations of such illusion,
is *not* illusive and provides a very tangible means of production and social organisation. 
But when the wall of a sandbox cracks,
its participants are projected right into a classical Heideggerian situation,
namely that even though these sandboxes are very much handy and at our disposal, *zuhandenheit*[@Heidegger:being]---like a tool required to achieve a particular action---when conflicts emerge from what appeared to be a trivial trigger---such as in the examples cited earlier,
or with a failed software installation,
or the commercial appropriation of commercially prohibited licensed material---then these sandboxes suddenly become visible,
objectively present, *vorhandenheit* [@Heidegger:being],
leaving the demystified sandbox participants confronted with their diversity of illusions.
It is a painful wake up call,
a democratic failure that shows the limit of consensus,
and the price to pay for a free culture that has ignored the proliferation of its language games,
instead benefiting,
regardless ot its intention,
from miscommunication and misunderstandings in an attempt to control and normalise these practices,
wrongly equating differences under common terms and definitions.
As it turns out,
without means and channels for pluralism and cultural diversity to take place within free culture,
the attempt to get rid of the proto-free-culture agonistic situation does not manage to eradicate conflicts,
instead it pressure-cooks them.


At this point,
something interesting happens which deviates slightly from the usual process of revelation.
Any new knowledge acquired by the conflict will unlikely result in a revolution,
even a mere evolution of the sandbox dogma that was just dispelled.
When the fog lifts or the storm passes,
for many the only way out will be denial,
in fact a way in,
further down the sandbox.
There is simply too much at stake,
too much has been invested in and built around these things already for the one-dimensional sandbox inhabitant [In reference to @Marcuse:one].
But in the sandbox,
denial and commitment are not necessarily proof of an uncritical dogmatism
^[
Here I am referring to Jaron Lanier's criticism of open source.
See [@Lanier:gadget, p. 126].
],
or complete subjugation:
it is a survival mechanism to overcome and ignore the *unhandiness* of these sandboxes,
in order to sustain the rational-legal authority of the techno-legal structure that provides a sense of belonging within the sandbox.


I would also add that this sense of belonging creates a sentiment,
a master position within one's own belief,
which is demoted to a slave *ressentiment* once the formal abstraction of the sandbox appears [@Nietzsche:genealogy, "Good and Evil", "Good and Bad" (1887)],
and that can only be suppressed indeed with denial in the hope to recreate the illusion of the open ended,
boundary-less progressive development of such platforms,
and comfort the sentiment of mastery.
Of course that does not apply to everyone.
Seeing the ugliness leaking from the cracks in the sandbox walls,
an opportunity is given for some of its inhabitants to migrate to other territories,
gather strength and seize that moment to live through their own techno-legal assisted *Aufklärung* [@Kant:enlightenment] by claiming their own sandbox,
as a mark of self-management,
maturation,
and maybe progress.
It is also more than a static schism or rupture,
but as with the lift of the forking taboo [@FullerGoffeyMackenzieMillsSharples:bigdiff],
it amplifies the constant gradual and exploratory becoming of those that escape a sandbox,
making sandbox culture representative of a modernist *ontology of ourselves* [@Foucault:lumieres].
There is however a counterpart to this process,
in the way that it is a spectacle,[@Foucault:lumieres]
and may also exist as a radical and global alternative in itself [@Foucault:enlightenment].
^[Please note that the last two references are in fact two different texts from Foucault.]
There is therefore a possibility for those who are able to escape their sandbox to turn into the new rulers,
gurus,
benevolent dictators,
facilitators of a new sandbox,
that will in turn be populated by those spectators who followed such new leaders down the rabbit hole.
The slave can therefore also accept their past condition,
and move to other sandboxes in which they regain their status of master,
either through construction,
in the case of building a new sandbox for others,
or delusion,
in which case they migrate from one demystified system of belief to a new one.
In all situations,
the collapse of free cultural belief does not lead to its destruction,
but to a new generative force.
So in a way,
if openness facilitates the creation of iron cages [In reference to @Weber:protestant],
it also gives the democratic possibility for virtually anyone to create their own,
creating new social imaginaries as part of an agenda,
an exit strategy,
or simply to relieve a personal itch.
In that sense the notion of a sandbox culture is strongly linked to the idea that liberalism can accommodate a diversity of different models of social organisation and modes of production.
To be sure,
the system I describe does not offer true pluralism but a post-democratic emulation of it.
What is more,
it shows that the existence of such systems are symptomatic of the current political struggle that moves to the moral register---it's not "right vs left" but "right vs wrong" [@Mouffe:political]---hard coded into conflict defusing licenses that determine and dictate what is acceptable or not beyond any form of debate or evaluation.


The sandbox model gives a privileged position to techno-legal infrastructure,
their software and legal code,
as constituent element and source of power.
This was necessary for me to articulate it in such a way in order to explain two things:
First,
that the mechanism of discursivity in free culture,
that has always been analysed from the perspective of the movements that may animate it---like the opposition between free software and open source software [@Berry:copy, Chapter 5 The Contestation of Code]---happens at a much lower level,
which is that of the individuals and communities gathered around the production of free cultural objects;
but secondly,
sandboxes are in effect non-human normalising and subjugating surrogates,
or proxies,
for those who control or initiate them.
The consequence of these two points is that the existence of such sandboxes challenges the idea that systems of domination are only terminal forms of power [In reference to @Foucault:sexuality1, p. 92].
The sandbox provides an opposite model precisely because it demonstrates that it is possible to limit the multiplicity of force relations and their confrontation [See @Foucault:sexuality1, Chapter 2 Method].
However,
this does not mean that the emergence of *reverse* discourses [@Foucault:sexuality1, p. 101]---the reclaiming of free culture-related terms for other motives
^[
Here I must explain that the reference to reappropriation is not a stretch.
Of course,
*reverse* discourse was articulated by Foucault in the context of homosexuality,
however the free cultural model,
in which the control of terms such as freedom and openness,
follows the same patterns of incessant recontextualisation between dominant groups and minority groups.
This is not limited to free culture jargon,
offensive words are also reclaimed,
such as the Internet slang *freetard*,
originally coined to mock both free-as-in-gratis and free-as-in-free-speech free culture supporters,
and that would end being used nonetheless by the latter in some Internet forums and boards to reinforce their identity.
]---is impossible,
it is,
but the manifestation of reverse discourses happens at a higher level,
because inside the sandbox,
conflict does not translate into the opportunity to reprogram the social organisation of the system,
but triggers instead the necessity to exile towards other sandboxes or create new ones,
from scratch or by forking.


In a sense,
if making visible the decontextualised formal abstraction of the sandbox highlights its neglect for its inner discourse,
it also provides a mechanism to defuse its dominance.
The dominated and manipulated relations within the sandbox that cannot be changed from within can therefore be reconsidered from the outside,
creating a sort of meta-discourse,
and thus providing the development process needed for the existence of a discursive free culture.
What is more,
its techno-legal code based immaterial territoriality permits a limitless creation of new nations for every crisis.
If seen through the lens of the freedom theory proposed by twentieth German psychologist Erich Fromm [@Fromm:escape]---in which are discussed the relationship between emancipation and submission---the creation of new sandboxes serve as a parable where participants free themselves from their current working condition,
and end up in a new position where they are free to pursue an emancipating process in a new environment.
In this case the transition is nearly seamless as the replacement offered has been partly experienced already.
However,
and still following Fromm's concept,
it remains to be seen if this is a change for the better,
and also if this individual experience is unique,
or instead something that can be replicated for all the members of the virtual community.
Indeed,
the newly gained freedom of the participants,
can also be a factor revealing that what others had thought about their participation as an act of positive freedom,
was in fact a masquerade now that they are confronted with the positive freedom of others,
a freedom that is universally defined but singularly interpreted.
In the end,
all of Marcuse's one-dimensional men are able,
in a strange way,
to form a nonetheless strange and ever-changing multi-dimensional society,
and avoid,
again in a very indirect way,
any hegemonic dominance of its discourse.
And yet it is more than just a type of classic liberalism that is described here because this model,
by the means of the techno-legal template,
is able to provide the structural channels for pluralism to exist.


That said,
this pluralism is threatened whenever such practices have been filtered or rationalised with umbrella definitions for free culture,
or open content or knowledge,
which greatly limits the discursive scope of these sandboxes.
In fact the free cultural process becomes much more interesting in terms of cultural diversity when it is *undefined* and tries to attack the immutable foundations of these sandboxes,
by creating incompatible situations,
as I have suggested earlier with,
for example,
non-military statements.
For instance,
the way license forking operates is indeed both effective and simple:
isolate an issue that is not compatible with a mode of production,
a creative process,
a belief,
or an ethical code,
and then manipulate the terms of the permission in a way that will make this issue visible through the way the work is being shared and published.
The goal is to be explicit about a point of conflict by encoding the divergence in the legal apparatus of these new sandboxes.
For instance the GPL is mutating into the eGPL,
the Exception GPL,
which allows the exclusion of certain groups or organisations from using the licensed material [@EGPL2DRAFT7]. 
Another mutant is John Magyar and Dmytri Kleiner's PPL,
the Peer Production License,
forked from the Creative Commons' BY-NC-SA license and that 
privileges work-owned businesses and collectives in which the different financial gains are distributed among work-owners. [@Kleiner:teleko, p. 5]
The life expectancy of such licenses is always difficult to predict,
but their existence proves the possibility of taming the free cultural normalisation and rationalisation,
and the possibility to revert to the healthier and more diverse agonistic pluralism of the proto-free culture era.


Ultimately,
the whole mechanism of forking sandboxes reveal the true greyness of a defined culture freedom,
in migratory movements that keeps it alive.
It all boils down to realising that these sandboxes have became a *home-sweet-home* for many,
and the denial of the issues attached to them is not lack of compassion or empathy,
but simply,
as I discussed earlier,
a matter of survival.
However,
every now and then exile is the only way out.
Exile was actually the most fundamental argument for the first public justification of free software.
As discussed in Chapter 1,
this justification was very much linked to the need of departing from an original home spoiled by the computer industry,
and set into motion a partly voluntary,
partly imposed,
journey towards new operating systems to populate.
If understanding the legal aspect of free software is a useful method for extrapolating its influence on digital culture,
it should always be examined in relation to the conditions of departure of those who participate in these practices.
Within the realm of software,
software migration is often depicted as a wilfull process in which one is moved from one technological environment to,
hopefully,
a better one.
While the software and hardware upgrade cycles have become a well accepted mass consumer phenomena,
the root of free software is,
on the contrary,
based on the refusal to take for granted that such improvements and migration is invariably positive.
In that sense free software is the strongest example of software exile.
Similarly,
artists, 
designers, 
musicians and writers that are interested in free culture licenses,
are responding in the form of a broader cultural exile,
and because of the techno-legal template of free software,
such cultural exile can be repeated endlessly. 


It is also striking that this cultural exile,
from one sandbox to another,
exhibits similar characteristics to physical exile.
Exile,
regardless of the tangibility of its context,
always comes at a price of solitude and loss.
Because the latter must be compensated,
the exile often reinforces a sharper,
stronger,
black and white vision of the world with an "exaggerated sense of group solidarity,
and a passionate hostility to outsiders,
even those who may in fact be in the same predicament as you." [@Said:exile, Reflections on Exile, p. 141]
Therefore it is no surprise to witness anger,
paranoia,
aggressiveness,
manias and overall quite emotional responses in the way members of these communities interact with each other and their sandboxed home.
In fact,
it does not matter what the object of tension is:
a license,
a program,
a piece of hardware,
an operating system,
a metaphor,
or even a compiler flag.
Anything goes.
At the same time,
this tension is a very powerful creative energy,
which is why it must be sustained at any cost.


Palestinian American literary theorist and critic Edward Saïd quotes Richard Ellmann to illustrate this point.
He explains how James Joyce maintained by all means his quarrel with Ireland,
in order to feed a state of creative loneliness in exile.
An exile that he specifically chose "to give force to his artistic vocation" [@Said:exile, Reflections on Exile, p. 145].
It does not take much effort to see a similar pattern,
in the way some artists and designers sustain a certain negative emotion towards the proprietary tools they used to work with,
and transform this energy in the development of a tailored technical craft,
through which their individuation is realised.
^[
A similar point is made by Mouffe in her effort to develop the friend-enemy model from German jurist and political theorist Carl Schmitt.
She refers in particular to the *constitutive outside*, a term coined by Henry Staten,
and itself based on several ideas from Derrida.
Mouffe uses this concept so as to develop a relational model of difference between identities and,
unlike Schmitt's model,
one that is compatible with democratic pluralism. See [@Mouffe:political, pp. 14-15, p. 19].
]
So like exile,
software exile is indeed not an event in time,
it is a time on its own,
yet that is synced with the computer clock of the homeland.
Alternatives proposed by proto-free and free cultural systems can quickly become places of dogmatic behaviour that is nurtured and nursed in this new home.
The latter becomes a substrate for the creation of a liberated set of tools,
licenses and practices.
To paraphrase Saïd,
and once again transpose his reflections on exile to reflections on software exile,
such behaviour finds its root in a discontinuous state of being that leads exiles to see themselves as belonging to a triumphant ideology,
in order to reconstitute their broken lives,
and of course the broken home directories of their operating system.


But most importantly,
there is another facet to software exile,
a more positive one,
which is the detachment it can create in relation not just to one sandbox,
but to every sandbox.
Saïd,
again,
quotes Theodor Adorno:
"it is part of morality not to be at home in one's home."
Here I am tempted to substitute the word home with the system variables that represent home directories in all the popular multi-user operating systems,
\texttt{\%UserProfile\%},
\texttt{\$HOME},
and \texttt{\~},
precisely to highlight the need of detachment from one's tools,
a detachment that can be quickly forgotten in the rush and excitement of conquering the sandbox blank files and canvases.
Thus,
possibly the biggest force of free software,
and by extension free culture,
does not lie in moving to a better home directory,
or to embrace a digital and globalist cosmopolitan city crowd,
but rather to reveal the ecosystem of these many sandboxes and their subculture,
to understand,
through detachment and distance,
the context from which they emerged and how they can create different modes of inquiry.
It is about actively observing patterns and not just passively generate them like a flip-flop stuck in an electronic sandbox.
Ultimately it is about gaining awareness that this plurality and proliferation is not a fault but,
again going back to the notion of agonistic pluralism,
it is a democratic foundation to make sure these sandboxes can sustain conflictual positions and oppose their different hegemonic vision.
Finally,
even though nomadic and purposeful migratory networks can bridge these different sandboxes,
their existence is nothing other than a higher level form of sandboxing,
itself driven by,
and a manifestation of other ideological pursuits.
There is no escape,
and it's fine like that.

\newpage

