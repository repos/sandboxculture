Abstract {.unnumbered}
========

\setlength{\parindent}{1em}

In partial response to the inability of intellectual property laws to adapt to data-sharing over computer networks,
several initiatives have proposed techno-legal alternatives to encourage the free circulation and transformation of digital works.
These alternatives have shaped part of contemporary digital culture for more than three decades and are today often associated with the "free culture" movement.
The different strands of this movement are essentially derived from a narrower concept of software freedom developed in the nineteen-eighties,
and which is enforced within free and open source software communities.
This principle was the first significant effort to articulate a reusable techno-legal template to work around the limitations of intellectual property laws.
It also offered a vision of network culture where community participation and sharing was structural.

From alternate tools and workflow systems,
artist-run servers,
network publishing experiments,
open data and design lobbies,
cooperative and collaborative frameworks,
but also novel copyright licensing used by both non-profit organisations and for-profit corporations,
the impact on cultural production of practices developed in relation to the ideas of free and open source software has been both influential and broadly applied.
However,
if it is true that free and open source software has indeed succeeded in becoming a theoretical and practical model for the transformation of art and culture,
the question remains at which ways it has provided such a model,
how it has been effectively appropriated across different groups and contexts and in what ways these overlap or differ.

Using the image of the sandbox,
where code becomes a constituent device for different communities to experience varying ideologies and practices,
this dissertation aims to map the consequent levels of divergence in interpreting and appropriating the free and open source techno-legal template.
This thesis identifies the paradoxes,
conflicts,
and contradictions within free culture discourse.
It explores the tensions between the wish to provide a theoretical universal definition of cultural freedom,
and the disorderly reality of its practice and interpretation.
However,
despite the different layers of cultural diffusion,
appropriation,
misunderstanding and miscommunication that together form the fabric of free culture,
this dissertation argues that,
even though feared,
fought,
and criticised,
these issues are not signs of dysfunctionality but are instead the evidence of cultural diversity within free culture.
This dissertation will also demonstrate that conflicts between and within these sandboxes create a democratic process that permits the constant transformation of the free and open source discourse,
and is therefore something that should be embraced and neither resisted nor substituted for a universal approach to cultural production.

\newpage
