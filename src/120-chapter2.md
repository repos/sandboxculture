In Search of Pluralism
======================

Diffusion and Appropriation
---------------------------


The first attempts to apply the free software model into a non-software context,
came naturally from fields where free software ideas had been circulated,
namely computer science.
One of the earliest example that I could find is the 1994 Free Music Philosophy (FMP),
by musician and computational biologist Ram Samudrala,
who then defined the project as following:


> What is the Free Music Philosophy (FMP)?
> 
> It is an anarchistic grass-roots,
> but high tech,
> system of spreading music:
> the idea that creating,
> copying,
> and distributing music must be as unrestricted as breathing air,
> plucking a blade of grass,
> or basking in the rays of the sun.
> 
> What does it mean to use the term "Free Music"?
> 
> The idea is similar to the notion of Free Software,
> and like with Free Software,
> the word "free" refers to freedom, not price.
> Specifically,
> Free Music means that any individual has the freedom of copying,
> distributing,
> and modifying music for personal,
> noncommercial purposes.
> Free Music does not mean that musicians cannot charge for records,
> tapes,
> CDs,
> or DATs.[@Ram:freemusic]


As for the distribution terms,
they are quite crude but partly mimic free software licensing,
except for the commercial use:


> Permission to copy,
> modify,
> and distribute the musical compositions and sound recordings on this album,
> provided this notice is included with every copy that is made,
> is given for noncommercial use.
> If you obtained this by making a copy,
> and if you find value in this music and wish to support it,
> please send a donation based on whatever you thought the music was worth to the address given on this notice [@Ram:freemusic].


The other important example of such appropriation was with Michael Stutz,
one of the first writers and journalists reporting on Linux and open source,
who in the mid nineties published his entire website including his clip art gallery under the GPL [@Stutz:clipart],
and was,
also as early as 1994,
the first to use the GPL outside the scope of software [@Moreau:phd, p. 473].
He explained that anyone deserved the freedom provided by the copyleft license,
and that it represented a "resource for all artists and scientists who work with digital information" [@Stutz:compgnu].
In his short 1997 electronic essay *Applying Copyleft To Non-Software Information*,
he justified his choice by saying that "certain restrictions of copyright - such as distribution and modification - are not very useful to 'cyberia,'
the 'free,
apolitical,
democratic community' that constitutes the internetworked digital world" [@Stutz:nonsoftware].
At the time he believed that the GPL provided the answer to the issue for software matters
and noted that "it appears that the same License can be easily applied to non-software information" [@Stutz:nonsoftware].


But if the GPL seemed adequate at first,
as the diffusion of free and open source software licensing progressed,
the need for the licensing of other things than software became more prominent.
So 1998 saw the birth of another effort to provide a more articulated licensing option for non-software works. 
In that year,
with the help of Stallman and Raymond,
David A. Wiley,
who was at that time working on a doctoral degree in Instructional Psychology and Technology at the Brigham Young University,
tweaked the GPL and released the *OpenContent License*.
The incentive for Wiley to release this license stemmed from his personal desire to share his teaching material,
so they can be reused by others,
circulated for free,
and also be properly attributed and responsibly modified [@Grossman:licenses].
The idea to create a general license that made the bridge between the free software philosophy beyond software itself was a novelty,
and was one step further from the fist landmark established with the FMP terms in 1994.
Lev Grossman who interviewed Wiley for Time magazine concluded his column wondering whether or not free content,
"open-source [sic]" novels and free concept albums would one day take over the world.


But Wiley's effort were not isolated.
In fact as early as 1998,
each in their respective domains,
artists,
musicians,
designers,
activists,
scientists,
had started to write their own licenses,
for works distributed most of the time on a border-less Internet,
yet attached to localised concerns and jurisdictions.
For instance the following licenses reflected on the ideas of freedom and openness within their own practice,
often with a growing distance from the free and open source software context:


\singlespacing

* the OpenContent License (1998);
* the Licence Publique Audiovisuelle (1998);
* the Licence Association des Bibliophiles Universels (1999);
* the Comprehensive Open Licence (1999);
* the Counter Copyright notice (1999);
* the Design Science License (1999);
* the Free Document Dissemination Licence (1999);
* the GNU Free Documentation License (1999);
* the IDGB Open Book Open Content License (1999);
* the License Publique Multimedia (1999);
* the Linux Documentation Project Copying License (1999);
* the Open Publication License (1999);
* the Open Directory License (1999);
* the Open Resources Magazine License (1999);
* the W3C Document Notice (1999);
* the Ethymonics Free Music Licence (2000);
* the Free Art License (2000);
* the Freedom CPU Charter (2000);
* the GNU Free Documentation License (2000);
* the Licence ludique générale (2000);
* the Licence pour Documents Libres (2000);
* the Licence Publique de Traduction (2000);
* the Open Game License (2000);
* the Trackers Public License (2000);
* the Common Documentation License (2001);
* the EFF Open Audio License (2001);
* the HyperNietzsche Licenses (2001);
* the Open Music Licenses (2001);
* the Simputer General Public License (2001);
* the Academic Free License (2002);
* the CopID notice (2002);
* the Mnémosyne Free Dissemination License (2002).
^[
For the full text of these licenses,
as well as a short explanation about the selection,
see Appendix: Selection of Proto-Free Culture Licenses.
]

\doublespacing

The amount of novelty licenses created just within four years,
shows the highly active cultural diffusion occurring at the time.
Stutz,
who defended the use of GPL for non-software,
will also eventually abandon the emblematic FSF license and write his own Design and Science License in 1999 [See @Stutz:beyondsoftware].
The peculiarity of all these endeavours,
is in the fact that they are all driven by different understandings of what freedom and openness means in the context of culture and knowledge
^[
Not to mention its commercial and non-commercial implication,
which is another can of worms I will briefly open and then attempt to close in the second part of the thesis.
].
Even though it would be quite a daunting effort to precisely analyse each of these in order to understand these differences,
in Chapter 3 I will take the 2000 Free Art License (FAL) as a case study,
in order to show how the cultural depth and the ramification of the community template of free and open source software actually works when it is claimed by other groups,
ideologies and practices.
So essentially,
all these licenses are efforts to claim a semantic territory,
a particular definition of cultural freedom and the words that can be used to articulate it.
Ultimately,
this snowball effect demonstrates the victory of Stallman to transform how licensing is perceived:
the inhibitory aspect of the license now becomes an expressive tool to empower and materialise various ideologies.
As a result though,
the sudden growth in cultural scope,
urge the need to guide,
make sense of,
and help navigate within all these new free and open groups and efforts [For instance @Liang:guide].


As pointed out with early and later critiques of license proliferation [@Majerus:proliferation],
the noise created from all these subcultural groups is not necessarily a positive mechanism of semantic disorder [@Hebdige:subculture, p. 90].
However,
all these licenses become effectively new *language-games* with accidental *family resemblances*,
^[
In reference to the concepts from Austrian-British philosopher Ludwig Wittgenstein.
See [@Wittgenstein:philo].
]
and help enrich discussion around cultural freedom.
Said differently,
beyond the apparent common universality that seem to connect them under the umbrella of openness and freedom,
they each have their distinctive features,
as a result of adapting to their needs the free and source software template. 
So if such a pluralistic approach to cultural freedom and openness appears to mimic the dynamics of liberal democracy,
its discursive mechanism as a whole does not belong however to the principle of aggregation,
where voting is linked to free market economics by giving the ability to the individual to choose for societal matters [@Downs:democracy; @Schumpeter:capitalism],
neither it fits with the principle of deliberation,
that gives preference to discussion and debate in the form of public discourse ethics [See @Habermas:norms; @Rawls:justice].


In fact,
and I will return to this point several time throughout this thesis,
this particular phenomena could be best explained under the model of radical democracy,
coined by political theorists Ernesto Laclau and Chantal Mouffe [@LaclauMouffe:hegemony],
and more precisely the model of *agonistic pluralism* [@Mouffe:agomodel].
By this,
and rephrasing Mouffe's description in the context of this apparent balkanisation of licensing,
I mean to say that through the lens of agonistic pluralism,
the sudden proliferation of licenses is not a by-product of competition,
but instead the emergence of identity politics within the not so diverse cultural context of free and open source communities.
By rallying under several new licenses,
these different groups have been able to cohabit,
and as a whole,
all these endeavours should therefore be understood as the interaction between several political adversaries,
treating each other,
and this is very important,
as legitimate opponents on the common ground that is cultural liberty and equality,
and yet disagreeing on the way to implement it [@Mouffe:agomodel, p. 203].
What is more,
and to be sure,
such passionate disagreements cannot be resolved with deliberation and rationale discussion [@Mouffe:agomodel, p. 203],
and this is fine and indispensable,
as according to this model,
democratic systems depends on the multiplication of discourse,
and the diversity of language-games and their matching organisations,
collectives,
institutions,
which are illustrated in this sub-section and later on in this thesis.


Under such a model,
I want to stress that it becomes therefore questionable that critiques of license proliferation and incompatibility between these documents,
are universally representative attempts to protect cultural freedom and openness as a whole.
The same can be said more generally of the *copyright atomism* that results from the ever increasing proliferation,
distribution,
and fragmentation of copyright [See @VanHouweling:atomism].
Instead,
these critiques should be best understood as the expression of threatened hegemonic forces.



Prototyping Free Culture
------------------------


In 2002,
an important change is about to happen.
Even though both the impressive legal literacy acquired [@Coleman:speech, p. 433],
and the collective intelligence produced,
by all the participants of the rapidly expanding field of all things free and open,
has allowed for the writing of all sorts of licenses,
the *real professionals* of the law are about to step into these communities of practices,
thereby threatening not only the existence of such communities,
but also the ability to establish common questions and reflect collectively about these [@Stengers:temps, p. 119, p. 177].


The professional argument is that even though anyone is free to write their own license,
it is a whole different story to make sure the license is actually a legally sound document,
which could effectively be useful if ever challenged in relation to intellectual property laws.
So in this logic and given the growing jungle of licenses,
the documents that would come from the work and research of lawyers and law scholars should have in theory a better chance of receiving public attention.
This is the claim and the bet taken in 2002 by the San Francisco based Creative Commons (CC) nonprofit organisation, 
which was started to provide a more generic approach to the issue of openness in culture.
Unlike the free software model in which the GNU Manifesto,
had set the ethical tone and direction for software freedoms and which eventually led to the creation of the GPL,
CC further embraced the strategy of economics by providing,
without substantial explanation,
a collection of licenses to fit,
according to them,
every purpose.
In regard to license proliferation to which such action clearly contributes,
CC did not acknowledge any other effort but that of the FSF,
and positioned itself as a complementary effort,
not a competitive one,
that would focus on scholarship,
film,
literature,
music,
photography,
and other kinds of creative works [@CC:faq2002],
basically all the domains in which free and open source software licenses,
and derivatives,
had been embraced since 1998.


There is of course a paradox in acknowledging on the one hand the pluralistic nature of licensing,
and on the other hand ignoring four years of the effective agonistic pluralism described earlier.
As a result,
the question of identifying and organising family resemblances across all these communities stopped being accidental,
and became instead a necessary means of survival,
for those who could not identify themselves with the aggregative model offered by CC.
As a matter of fact,
this meta discursivity operating on top of licenses,
had already started in 2001 with the concept of Open Source Intelligence (OSI)---not to be misunderstood with the Open Source Initiave (OSI) mentioned previously---which connected the free and open source software collaborative framework in the broader context of net culture:


> In the world of spies and spooks,
> Open Source Intelligence (OSI) signifies useful information gleaned from public sources,
> such as newspapers,
> phone books and price lists.
> We use the term differently.
> For us, OSI is the application of collaborative principles developed by the Open Source Software movement to the gathering and analysis of information.
> These principles include:
> peer review,
> reputation- rather than sanctions-based authority,
> the free sharing of products,
> and flexible levels of involvement and responsibility.
> [...]
> Projects like the Nettime e-mail list,
> Wikipedia and the NoLogo.org website each have distinct history that led them to develop different technical and social strategies,
> and to realize some or all of the open source collaborative principles. [@StalderHirsh:OSI]


The same year,
the community behind the *Manifesto de Hipatia*,
who would also go beyond the original scope of user freedom and cooperation to link the free software philosophy to social and political activism through the value of knowledge access:


> We propose the creation of a world-wide,
> popular,
> democratic organisation to promote the adoption of public policies combined with human and social behaviour that favour the free availability and sustainability of,
> and social access to,
> technology and knowledge;
> their use for the common good;
> and the viability of the economic model which creates them,
> in terms of the equality and inclusion of all human beings and all peoples of the world [@TeSaGeGo2001].


Eventually several initiatives offered their own proto-free culture definitions.
For example,
the 2003 "four kinds of free knowledge" by Spanish scholar Ismael Peña-López attempted to make a direct transposition between software freedom and knowledge:


> * The freedom to use the knowledge, for any purpose (freedom 0).
> * The freedom to study how the knowledge applies, and adapt it to your needs (freedom 1). Access to the source information is a precondition for this.
> * The freedom to redistribute knowledge so you can help your neighbour (freedom 2).
> * The freedom to improve the knowledge, and release your improvements to the public, so that the whole community benefits (freedom 3). Access to the source information is a precondition for this [@PenaLopez2003].


Another effort focussed instead on the idea of openness:
the Open Knowledge Definition (OKD).
The later is one of the projects of the Open Knowledge Foundation (OKF),
a nonprofit organisation founded in 2004 by Rufus Pollock,
Martin Keegan,
and Jo Walsh.
It was created to promote "the openness of knowledge in all its forms,
in the belief that greater access to information will have far-reaching social and economic benefits" [@OKF:home2005].
Their approach was originally based on what they call *the three meanings of open*:
legally open,
socially open,
and technologically open.
Unlike other initiatives that proudly exhibited their wishful affiliation with the FSF,
the OKF instead affiliated itself with the OSI and the Open Access movement.
And,
just like the other groups critical of proliferation,
it is on a mission to set the record straight when it comes to openness:


> The concept of openness has already started to spread rapidly beyond its original roots in academia and software.
> We already have 'open access' journals,
> open genetics,
> open geodata,
> open content etc.
> As the concept spreads so we are seeing a proliferation of licenses and a potential blurring of what is open and what is not.
> 
> In such circumstances it is important to preserve compatibility,
> guard against dilution of the concept,
> and provide a common thread to this multitude of activities across a variety of disciplines.
> The definition,
> by providing clear set of criteria for openness,
> is an essential tool in achieving these ends [@OKF:about2006].


As might be expected,
the OKF itself is thus directly derived from Perens' Open Source definition.
The first version,
v0.1,
was drafted in August 2005 and v1.0 was released in July 2006.
For the OKF to decide if a work is open or not, the latter must respect the following definition:


> 1. Access
> 2. Redistribution
> 3. Re-Use
> 4. Absence of Technological Restriction
> 5. Attribution
> 6. Integrity
> 7. No Discrimination Against Persons or Groups
> 8. No Discrimination Against Fields of Endeavor
> 9. Distribution of License
> 10. License Must Not Be Specific to a Package
> 11. License Must Not Restrict the Distribution of Other Works [@OKF:wiki2005]


At the time,
the OKF definition, or OKD, "sets forth principles by which to judge whether a knowledge license is open" and "does not seek to provide
or recommend specific licenses" [@OKF:home2006].
However they did mention that their wiki contained a license survey,
and before the end of 2006 a new entry was added to the project website: "Conformant Licenses"
^[
In 2006,
the licenses that could qualify as Open Knowledge licenses were:
the GNU Free Documentation License,
the Free Art License,
the Creative Commons Attribution License,
the Creative Commons Attribution Share-Alike and the Design Science License.
].


Later on,
in 2007,
another adaptation of software freedom, the Free/Libre Knowledge definition,
is released by the Free Knowledge Foundation (FKF),
yet another group that clearly stands on a different ground from the one claimed by the OKF. 


> (0) use the work for any purpose
> (1) study its mechanisms, to be able to modify and adapt it to their own needs
> (2) make and distribute copies, in whole or in part
> (3) enhance and/or extend the work and share the result [@FKF2007].


Also worth mentioning was the definition from Willey in 2007,
who had previously authored the OpenContent license.
In this attempt,
Wiley made a stronger distinction between rework and remix.
It is also a twist on the four software freedoms,
and in this case it has been renamed to the "4Rs Framework:"


> Reuse – Use the work verbatim, just exactly as you found it\
> Revise – Alter or transform the work so that it better meets your needs\
> Remix – Combine the (verbatim or altered) work with other works to better meet your needs\
> Redistribute – Share the verbatim work, the reworked work, or the remixed work with others [@Willey2007]


Next to the multiplication of definitions,
the cultural diffusion discussed in this section shows that different readings of the free software template are possible.
An important point of divergence,
and close to the spirit of the *Manifesto de Hipatia* and the Open Source Intelligence concept,
is to interprete the free software template as a model for large-scale productive social relations where generous collaboration can take place [@Balvedi:studios],
and not just a more effective and liberal form of efficient production and sharing.
As early as 2002,
projects such as the Brazillian network *MetaReciclagem* put forth the materialisation of critical appropriation of technologies for social change [@Fonseca:repair] in which DIY,
copyleft,
and consensus-based decision-making,
helped approach free and open source software as a "cultural and critical take on the pervasiveness of relationships mediated only by economic values" [@Fonseca:repair].
Similarly,
this social dimension was also a deciding element in the creation of the *Estúdio Livre* project in 2005,
a collaborative Brazilian Portuguese speaking network with a focus on the "breaking down of barriers between producer and consumer as an example of collective intelligence as well as of changes in aesthetic,
economic and social paradigms in contemporary society". [@Balvedi:studios, p. 263]
Generally speaking,
this proto-free culture era,
saw the emergence of what Chilean sound artist Alejandra Maria Perez Nuñez called the *southern time* of free and open source software [@ale:south].
Inspired by the *Rhythmanalysis* collection of essays from French Marxist philosopher Henri Lefebvre [@Lefebvre:rhythmanalysis],
she expressed the role of free and open source software in forging a culture that goes beyond software and exist outside of the "economical time of unlimited profit" [@ale:south, p. 281];
where new ways of learning,
creating,
and participating,
offer an alternative to a dominant productive model of time.
To be sure,
and as noted by the artist,
this southern time was "not so much about geographical locations as about frames of mind [...] that determines what is conceived as south" [@ale:south, p. 281],
and this approach to free culture was thereby also shared in European hacklabs,
art collectives and argumented critically in the context of network politics [@Fonseca:repair].
Towards the end of the noughthies,
free culture was therefore more than a chaotic collection of definitions and licenses,
it was also the concrete manifestation of different ideas about society,
structured and grounded by the free software template.




Defining Free Culture and the Decay of Pluralism
------------------------------------------------


Today’s most recognised definition is not to be found in any of the efforts listed in the previous section.
It is in fact the last one released in this stream of prototyping: 
the 2008 definition for *Free Cultural Works*,
but which nonetheless found its infancy in discussions started three years earlier.
Indeed back in 2005,
yet before the official release of the OKD,
and in this context of growing concerns about the lack of uniformity for the freedom of non-software things,
free software activist Benjamin Mako Hill started to openly criticise the definition-free approach offered by the “hodge-podge of pick-and-choose” features of CC licensing,
indirectly addressing the limits of the undefined forms of engagement found in CC co-founder Lawrence Lessig's 2004 book, *Free Culture*,
that I mentioned in the introduction of this thesis.


> [D]espite CC's stated desire to learn from and build upon the example of the free software movement,
> CC sets no defined limits and promises no freedoms,
> no rights,
> and no fixed qualities.
> Free software's success is built upon an ethical position.
> CC sets no such standard. [@Hill:standard].


As a self-fulfilling prophecy,
this intention is carried on in a 2006 announcement from German freelance journalist Erik Möller and Hill himself,
to work on such a missing definition:


> In the free software world, the two primary definitions - the Free Software Definition and the Open Source Definition - are both fairly clear about what uses must be allowed.
> Free software can be freely copied,
> modified,
> modified and copied,
> sold,
> taken apart and put back together.
> However, no similar standard exists in the sphere of free content and free expressions.
> 
> We believe that the highest standard of freedom should be sought for as many works as possible.
> And we seek to define this standard of freedom clearly.
> We call this definition the "Free Content and Expression Definition",
> and we call works which are covered by this definition "free content" or "free expressions" [@Freedomdefined:ann].


This definition is written by several authors [@Freedomdefined:authors] using a wiki
^[
The OKD was also drafted on a wiki.
],
a MediaWiki installation to be precise,
from the Wikipedia fame,
and a powerful symbol of online collaborative writing dear to free and open source software communities [See @Reagle:wikipedia].
In particular,
the deliberative process follows a system put in place by Möller,
and relies on a model loosely inspired from software production where a development branch co-exists with a released branch.
An invitation-only moderating group monitors the changes made by the wiki users on a page where an *unstable* version of the definition resides,
and when consensus is felt to be reached on a point,
the particular change is applied at the discretion of the moderators to the stable version of the definition. [For the latest list of moderators, see @Freedomdefined:mod]
As the name already implies,
this definition is a transposition of the free software definition.
According to their Frequently Asked Questions (FAQ),
the definition applies to "works of the human mind (and craft)":


> - the freedom to use the work and enjoy the benefits of using it
> - the freedom to study the work and to apply knowledge acquired from it
> - the freedom to make and redistribute copies, in whole or in part, of
> the information or expression
> - the freedom to make changes and improvements, and to distribute
> derivative works [@FreedomDefined2007]


Similar to the OKD,
the free culture definition is introduced as being different from a license [@Freedomdefined:FAQ2006].
Instead it is presented as "a list of conditions under which a work must be available in order to be considered 'free' [and] a way to classify existing licenses" [@Freedomdefined:FAQ2006].
Next to distinguish itself from licenses,
the project also distances itself from the concept of manifesto,
a form they qualify as 
"vague,
broad,
and
very encompassing".
The project aimed instead to provide a fixed reference point to free culture,
one that could not be interpreted too freely,
one that had to be restricted in order to build a common language and set a landmark,
yet not as formal as legal code,
hence the project name behind the definition: *freedom defined*.
And just like the OKD,
the free cultural works definition had no specific licenses to offer,
but instead pointed to several already existing licenses that allowed the application of the four freedoms to the licensed work or expression.
Similar to the licenses filtered by the FSF and the OSI,
the overlap between freedom defined approved and OKF approved licenses is quite spectacular
^[
In 2006,
the licenses that were considered fitting the creation of free cultural works are:
Against DRM,
Creative Commons Attribution,
Creative Commons Attribution ShareAlike,
Design Science License,
Free Art License,
FreeBSD Documentation License,
GNU Free Documentation License,
GNU General Public License,
MIT License.
Of the seven Creative Commons licenses at the time,
only two qualified as free cultural licenses.
].
This should not come as a surprise.
Just as a piece of GPL'ed source code can be independently articulated as either free or open source software,
the *same* double inflection is carried with free culture and open knowledge.
Hill,
^[
Email to author, October 9, 2015.
]
who did not know about the OKD when he started to work with Möller,
told me that there was some brief discussions about merging the projects,
but there was a few barriers to do that.
First,
the specific naming and content of the free culture definition had been extensively discussed with Stallman and the FSF,
Lessig and CC,
and Wikimedia,
to make sure they would endorse the project,
and if they had called it *open knowledge definition* Hill believed that would have most likely lost some,
probably all,
of their support.
Second,
Between the two projects,
there were too much structural and scope differences,
with the OKF lacking in particular a model for being responsive to a broader community interested in free open cultural concerns.




I previously argued that the state of free culture in its early undefined and unnamed days,
was neither of aggregative,
nor deliberative nature,
but had instead the potential to illustrate a successful model of agonistic pluralism and radical democracy in which conflict is not seen negatively,
and where consensus is not blindly pursued.
Borrowing the words from law scholar Lawrence Liang,
the licenses of the proto-free culture era were more than legal documents, they were also "speech act[s]" [@Liang2005, p. 57].
However,
this situation changed completely with the rise of CC and the free culture definition,
which suddenly permitted the two classical liberal democratic models to become once again dominant.
Indeed,
CC approached the licensing from an economic perspective,
proposing their own broad free market of different in-house *professional* licenses from which copyright owners can choose,
thereby building up some commons in an aggregative way where voting and Darwinist survival mechanism are put forth.
At the opposite,
the free culture definition built upon the meritocratic position of its initiators and experts turned moderators,
to create a sort of Habermassian deliberative open platform for the public to contribute,
and eventually establish a list of licenses the selection of which is based on ethical concerns.
But in *both* cases,
the notion of consensus that was not a primary concern in the proto free culture era,
now becomes a tool for,
respectively,
an economic reform for immaterial property on the one hand,
and on the other hand a contribution to the democratic narrative of the multitude,
in which *the common* is constructed by spreading out singularities and where conflict is believed to become increasingly unnatural [@HardtNegri:multitude].
This process came at a cost however,
which is the exclusion of all the groups the work of which did not match or did not matter for these federative platforms,
as well as the disempowering of practicing communities,
now guided by experts from the techno-legal field.



The Political Denial of Open Everything
---------------------------------------


> Free software movement was started in a capitalist society and has always existed in a capitalist society,
> there is no incompatibility between free software and capitalism. [...]
> We do not need to get rid of capitalism.
> Free software combines capitalist ideas, and socialist ideas, and anarchist ideas.
> It does not fit into any of those camps. [@RT:rms]



It should become clear by now that free and open source software movements and initiatives,
as well as the free culture phenomenon as a whole,
are symptomatic of contemporary politics in which the ideas of cultural freedom and openness are stretched between on the one hand the post-political need to embrace a sort of consensus driven liberal democracy
^[
Which manifests itself with the aggregation of all these licenses and their respective communities under diverse acronyms,
such as Free/Libre and Open Source Software (FLOSS),
see [@GhoshGlot:survey],
as well as labels and novel organisations related to all things open.
],
and on the other hand the diverging language games and family resemblances of the groups which constitute these movements
^[
Which are manifestation of all sorts of ideologies and attempts to contribute to a certain social order,
with some of these visions sometimes compatible, sometimes not.
].
It is this particular stretch that makes me wonder how to best approach the questions of commons and common ground,
which has been made implicit or explicit in all these projects.
The point that I want to investigate here is the consequence of a situation in which local disconnected singularities claim,
or pretend,
or assume,
or believe to belong to the same universality.
In particular do these efforts create a common ground because they are perceived to be able to fit within a universal consensus,
or do they generate series of commons because they can be appropriated for radically plural purposes?
Either way,
the question of the political cannot be easily dismissed,
because it is the fundamental basis to articulate such differences.
Unlike the political agnosticism noted by American anthropologist Gabriella Coleman,
to refer to the political denial that is both informed and reinforced by the cultural liberalism and the technological pragmatism of the free and open source software history [@Coleman:agno],
I believe it is essential to acknowledge the direct political dimension of free and open source things,
and not fall into the trap of seeing them as operating at a different level,
or similarly,
disconnected from passionate irrational motives.
Free and open source software,
and hacking in general,
*is* political [@Soderberg:marx].
Sometimes the apolitical,
or neutrality illusion is made stronger by the groups themselves.
In this case any discussion about the context of such free and open things reinforces the idea that the political dimension is unproductive noise.
For instance,
in software developer mailing lists and forums,
the term *semantics* is often used in a derogative way to describe any potentially conflictual discussion,
that is not articulated in a purely techno-rational fashion.
Similarly,
while Raymond disagrees with Stallman's tactics and rhetoric of free software,
their opposition meets up again in their extreme unclear positions,
with Stallman unable to articulate a meaningful political interpretation for software freedom,
and Raymond's desire to simplify the free and open source software discourse to a neutral technological debate [see @Raymond:shutup] and yet is hardly able to disguise his libertarian agenda [@Raymond:bezroukov].


To articulate my argument,
I borrow from Mouffe,
the definitions and distinctions made by her between *politics* and *the political*
^[
Mouffe makes the distinction as follow: *the political* refers to the constitutive antagonist magnitude of human societies,
while *politics* are the set of practices and institutions through which human order and organisation are founded in the context of conflictuality provided by *the political*. [@Mouffe:political, p. 9].
],
and I will argue that the denial and the refusal to consider the politics within the different groups that constitute the ever expanding universe of free and open things,
has the tragic consequence of denying access to the political antagonism that,
and still following Mouffe,
is the essential constituent of democracy.
In particular there is an indispensable difference to be made between a model of pluralism where a common bond exists,
yet in which conflictual collective identities can construct themselves by specific differentiation---which was the case with the messy unstructured but very rich proto free culture---and a model where pluralism is consensually simulated because the common bond chosen is a one-size-fit-all collective identity---which is the case for all the different efforts made to define free cultural practices,
to aggregate,
and to normalise them
^[
To be sure,
the pluralism offered by CC also falls into this category as its palette of licenses is nothing more but an ersatz of diversity.
]. 
The problem with the second approach is two fold:
first,
it can only favour one type of hegemony which will contribute to the shaping of such rationalisation and where pluralism is just another word for competition;
and second,
which is also a consequence of the first point,
it makes vulnerable,
fragile,
and open to exploitation the cultural diversity and social practices that end up unknowingly mixed in the deceptive cultural blender of consensus.


To illustrate my argument I will now take a look at open design.
I chose this field precisely to give an example of how the notion of consensus in free culture can backfire,
and how the idea of a common public space and language can prevent the we/they construction,
which in turn leads to deception for the actors of the less represented ideologies.


So what is open design precisely?
In fact,
it is precisely vague.
Open design is a term generally accepted to describe the open development of tangible objects [@Balka:opendesign].
In the free culture family tree,
it is one of its latest and distant branches.
It is a particularly interesting one for my demonstration because of the overlaps it has with several other family trees not necessarily linked to free culture itself.
First of all, 
as the name might suggest,
open design is a concept derived from free and open source software practices,
in the sense that it is an attempt to assimilate and integrate some aspects of these practices,
not for the making of digital things but for the production of physical goods.
Next to that,
open design is also connected to a long history of participatory design practices,
and is therefore influenced by broad research and applications around users and communities in the fields of urban,
architecture,
product,
and graphic design.
^[
In that sense,
from the viewpoint of participatory design,
free and open source software is not the ultimate form of collaboration,
but one of many different types of processes and procedures that can be deployed for designing things.
]
This multi-disciplinary assemblage has in fact been instrumental in allowing the come-back of crafting and tinkering in art and design,
via most notably the use of digital fabrication and rapid prototyping workshops,
most commonly branded under the name FabLab [@WalterHerrmannBuching:fab],
as well as allowing the co-habitation of concepts such user empowerment and entrepreneurship,
which are both mixed under the so-called maker movement [@Anderson:makers],
but also connect back to the seventies Do It Yourself (DIY) scene
^[
This is particularly true for the countercultural dimension of DIY.
For instance there is an interesting connection between the "Free Furniture" design ideas from the 1971 *Steal this Book* work by American political and social activist Abbie Hoffman,
and the "free (as in freedom)" 2012 *uH bench* open source public bench by Belgian designer Julien Deswaef,
developed in the context of "urban hacking" and the reclaiming of public space.
While each of these approaches deal with the notion of freedom differently,
the overlap in methods and activist intention is striking and cannot be dismissed as coincidental.
See [@Hoffman:steal, pp. 23-25] and see [@Deswaef:bench].
].


As a consequence,
open design ends up overlapping with all sorts of social,
autonomous,
and commercial practices.
Indeed,
the principle of designing and co-creating things that can be freely digitally distributed on online platforms and networks,
then improved or modified by others,
and eventually made tangible by their quick making with hobbyist and semi-industrial machinery,
can give the idea that it can be applied for a wide range of applications:
its modularity and fast deployment is ideal for strategic humanist projects and interventions;
the making of unique tools and technical methodologies close to the ethos of manual labour is relevant for design practices in line with the Arts and Crafts movement but also meaningful for designers in search of novel forms of production;
and of course the idea of a network of small generic manufacturing units has a bearing with businesses relying on prosumerism.
It should not take long to realise that to be able to articulate such differences and approaches,
a whole new subset of terms,
definitions and licenses will be needed.
While it is not within the scope of this thesis to look into this aspect specifically,
I will mention that it is very much like zooming into a fractal set,
because there is virtually no difference between the current state of open design where there is no clear consensus about a specific definition,
and the state of proto free culture some years ago when several visions co-existed.
At the time of writing this text,
open design is represented by an assortment of definitions,
scopes,
perspectives,
and of course several new licenses to implement these definitions [See @OSHWA:history],
as well as taking into account the physical dimension of some of these designs
^[
Most notably,
the notion of free and open source hardware has opened several cans of worms given the need to take more carefully into account the distinction between copyright and patent laws.
See [@Peterson:cleanroom]
].


Practically speaking,
nobody knows at this point,
or known since its infancy,
if open design will in the end manage to converge towards a shared model of production,
or if it will instead diverge into "a plethora of different models that embrace various aspects of commons-based peer production,
with users switching between different models as appropriate" [@Troxler:lib].
Meanwhile,
the main voices from the free and open source communities are not quite sure how to articulate their understanding of these recent transformations.
Even though Stallman would eventually reconsider [@Stallman:freehardware2015] his previous discouraging views [@Stallmand:freehardware1999] on the validity of free hardware,
it seems that these questions are out of the FSF scope which tends so far to limit itself to only recommending hardware that does not use one bit of non-free software with no concern about its means of production [@Gay:freedomhardware];
similarly,
the OSI does not review any licenses specific to open source hardware design,
making room in practice for the emergence and competition of new institutions and organisation more apt to deal with the issue. [See @OSHWA:history]
But if open design,
as in the era of proto-free culture,
is currently showing some healthy signs of pluralism from the outside,
it is not by design,
but as with free culture,
it is the result of the assumption that everyone is heading towards the same goal.
Unfortunately in recent years some cracks in the wall have started to appear,
which highlight the limits of unspoken consensual definitions in these groups. 
This is peculiarly noticeable with 3D printing,
which has became emblematic of the open design versatility,
and the ultimate mascot of the maker movement.



The Liberal Democratic Industries of Freedom and Openness
---------------------------------------------------------


3D printing is in fact modern alchemy.
Of course,
it does not focus on the transmutation of common metals into gold.
Instead,
it deals with the transmutation of digital information into tangible objects.
An interesting aspect of this process is the transmutation of the alchemists themselves.
This analogy is not innocent.
Swiss psychiatrist Carl Gustav Jung used alchemy to exemplify the process of individuation of the alchemist,
a process of psychological development towards the Self [@Jung:alchemy].
3D printing operates similarly,
and its impact goes well beyond the local production of objects pulled from a recombining library of digital models.
In fact,
drawn into the symbolism of direct-digital manufacturing,
big data,
the Internet,
and just-in-time practices,
with 3D printing,
open designers are being transmuted into the true replicable yet indefinitely customised Self:
the co-creating Tofflerian prosumer. [In reference to @Toffler:third]
With this remark,
I make a connection with the individuation of the programmer and their relationship with source code as psychopomp,
discussed in Chapter 1.
On a more general view,
French philosopher Bernard Stiegler also saw in free software a process of individuation.
For Stiegler,
it is more particularly defined as the process that substitutes the duality of consumer/producer with an infrastructure made of active contributors,
and according to him this individuation permits the transformation and the questioning of the self,
as well as enabling the sharing and responsibility of what is made [@noirfalisse:stiegler].
While I believe that the exact same principles of individuation can be applied to open designers and 3D printing,
in both cases it should not be treated without its existentialist counterpart which is the question of authenticity within open design.
Said differently,
what happens to this very individuation when it is faced with inconspicuous deceptive mechanisms?


When makers,
fabbers,
and open designers start to use 3D printers,
the technology is not new [For a brief historical overview, see @ChuaLeong:3D, 1.1 Development of AM].
What is novel however is accessibility to the technology with simpler,
cheaper components,
and the recursive consequences of using GPL licensing for these:
this is the story of the RepRap,
a machine that should eventually be able to print itself entirely.
The consequences of such a,
still theoretical,
economical fork bomb are multiple,
but most importantly according to the RepRap project inventor,
British engineer and mathematician Adrian Bowyer,
such a replicating manufacturing unit could "allow the world's poorest people easily to put a foot on the first rung of the manufacturing ladder that has made the rest of us rich" [@Mason:pirate, p. 30],
a comment that is symptomatic of the delusional enthusiasm that often accompanies the arrival of a new technology [@Winner:alchemy].
Even if in the following years Bowyer would significantly tone down his moral and political motives---with the adoption of the pragmatic rationalism from the open source narrative and by putting upfront the notion of evolutionary game theory as the common ground for those wishing to contribute to the project [@Bowyer:policy]---the subtext of the project and its early introduction as an apparatus that would bring down global capitalism [@Randerson:santa],
made it popular in circles where free culture could not be decoupled from social concerns
^[
As seen in a previous section.
],
and those for who free and open source technologies,
and their legal framework,
are unconditionally linked to the shaping of a better society [see @Mason:postcap, Chapter 5, Chapter 10].


But of course,
and linking back to the many roots of the open design field,
such an invention also resonated strongly for those who interpreted openness as yet another tool for capitalist manoeuvres.
Even though initially started by academic groups of users-developers,
it soon moved to a mix of users from different backgrounds,
as well as paid and unpaid developers.
The RepRap project also helped to create a distributed and networked assembly line of open source products sold by a few for-profit companies.
While the anti-global capitalist tone of the early days of the project were facing the reality of sustainability for its most involved participants,
it also became an open door for exploitative strategies that did not give anything back to the RepRap community.


In particular 
this can be seen in the way a company such as MakerBot Industries,
used the community and the research on the RepRap project,
to develop and refine a series of products while accumulating experience and customer feedback in the process.
As I will further develop in Part 2 of the dissertation,
from a licensing perspective there is absolutely nothing wrong in deriving a commercial product here,
and in that sense MakerBot Industries was no different from any other parts of the commercial open source hardware ecosystem that had formed around the RepRap.
However in this case only the first product from this company was made open source.
Starting in 2012,
the following versions were closed source.
Alongside this,
and despite their bad reputation in free and open source circles [@Lucarini:patent] some patents were also filled by MakerBot Industries [For a lits of all patents from Makerbot Industries, 53 at time of writing, see @google:patentbots] who had coincidently received venture capital a few months before the release of their closed source product [@Pettis:invest].
The immediate result is not an economical fork bomb,
but a violent blow to the community,
with some of the smaller companies and freelancers deeply involved with the idea of open source hardware describing "RepRap and especially 3D printing [...] now full of bullshit" [@Prusa:meaning].
The same sentiments came from one of the three MakerBot Industries founders who was,
according to him,
forced out around the time of this new product release,
and who described the "move to closed source as the ultimate betrayal" with the company adopting "a load of corporate double-speak bullshit" [@Smith:perspective].


Next to this,
other wall fissures in the open design world appeared during the rest of the year on the Thingiverse website,
the leading online platform owned by MakerBot Industries where open source hardware designs can be shared under GPL and CC licenses.
Two incidents in particular are relevant,
and both originate from changes in the terms of service (TOS) of the platform.
While Thingiverse was initially relatively illustrative of the messy pluralistic nature of open design,
where all sorts of things could be uploaded and licensed with an arbitrary collection of free and non-free licenses,
two decisions were made that would reduce this pluralism to a narrower subset aligned with the ethical and economical concerns of the company running the site.


The first decision was the purging of gun parts designs because the company's "focus is to empower the creative process and make things for good".
But this decision eventually led to the creation of the DEFCAD project in 2012,
an open source search engine for 3D models of all sorts and purposes,
without any restriction.
The project initiated by American free-market anarchist and crypto-anarchist Cody Wilson,
is part of the larger agenda of the Defense Distributed [@defdist:defcad] online organisation he founded,
and focussed on the development of 3D printed weapons accessible to anyone.
This particular approach to openness which can be understood from the perspective of libertarianism,
avoids the smokescreen of ethical debate and directly responds to disenchantment with politics.
It is made clear on the DEFCAD website with the following headlines the day of its launch:
"Google can't, MakerBot won't, Politicians say don't, We the People, we will". [@defdist:defcadwill]


The second incident is not of ethical nature but has to do with the economic dimension of open design.
So far in this thesis I have presented licenses as documents that can be used by an intellectual property owner to form novel ways of distribution and transformations while still,
for the most part,
relying on existing copyright laws.
However,
these documents do not exclude each others.
I will come back to the issue of dual-, 
triple-,
and n-,
licensing in the next chapters,
but for now I will simply say that the copyright owner is free to apply,
or agree to apply,
any and all licenses if they like,
and be free to agree on all sorts of non-exclusive licensing and agreements concerning their property.
So an open source hardware design can be both licensed under the public domain friendly CC0 license,
and the non commercial non derivative CC BY-NC-ND,
while being at the same time the subject of a special commercial exploitation contract with a third-party.
That would not make any sense at all,
^[
I'm excluding here the potential *trolling* effect of such an approach to free culture licensing.
]
but it's perfectly possible to do so.
This contradiction can nevertheless be forced upon open designers via the TOS of the platform they use to distribute their work,
which is what happened when Thingiverse introduced in its terms the requirement to give "to the Company and its affiliates and partners,
an irrevocable,
non exclusive,
royalty-free and fully paid,
worldwide license to reproduce,
distribute,
publicly display and perform,
prepare derivative works of,
incorporate into other works,
and otherwise use [their] User Content,
and to grant sublicenses of the foregoing,
solely for the purposes of including [their] User Content in the Site and Services" [@Makerbot:tos2012],
and made its contributors "irrevocably waive (and cause to be waived) any claims and assertions of moral rights or attribution with respect to [their] User Content" [@Makerbot:tos2012].
The decision was followed by a virtual protest under the form of an "Occupy Thingiverse" action in 2012,
but this part of the TOS is still unchanged at the time of writing.
Even if this is perceived by the protestants as yet another betrayal of the community ethos,
the terms work like a non-negotiable contract on top of the license
^[
For a more detailed analysis and other intellectual property issues related to Thingiverse,
see [@Moilanen:thingiverse].
],
and is so pervasive that it is nothing but a transfer of copyright in disguise.


Simulating pluralism via some unspoken or unclear consensus made around terms that are loosely defined cannot be sustainable.
^[
There should be no doubt that such stories are not exceptional.
Open design is a minefield for the beginner and specialist alike,
and things only get messier when taking into account the international cultural diversity of hardware manufacturing.
This is visible,
to give an example,
in the clash between classic western IP models and the Chinese *shanzhai* tradition,
which in the context of hardware manufacturing,
has enabled the creation of the tech mashup *open BOM* culture.
The latter is an umbrella term to drescribe practices of sharing bills of materials and other design materials between Chinese small and mid-size manufacturers,
and policed alone by community word of mouth.
This practice in particular has recently been given the name "gongkai" (公开) by US hacker Andrew Huang.
See [@Huang:gonkai]; [@Huang:openbom]; [@Wenning:han].
]
Sooner or later the balance will shift dramatically at the expense of others,
surprised to be suddenly precipitated into an alien monoculture.
Instead of being able to develop strategies of adversarial politics,
the pluralistic nature of open design,
the maker movement and others,
are increasingly dominated by the aggregation of groups whose ideologies are compatible or overlapping,
which in the present situation are essentially linked to the roadmap of so-called smart industries and the next industrial revolution
^[
See [@Rifkin:zero].
Incidentally, the 2015 careers page of MakerBot Industries' website was opening up with the large headline "JOIN THE NEXT INDUSTRIAL REVOLUTION", see [@MakerBot:revolution2015].
].
Of course,
things are not static,
and such dominance can also be overruled,
but for this to happen the notion of common cannot be decoupled from the political.
Because open design was never understood as a place of conflict,
because it was not understood as a political struggle,
or was simply denied a political dimension,
and was instead reduced to its technological apparatus,
its economics,
or its ethics under an assumed consensus,
there could only be deception further down the road,
and an invalidation of the identity politics that emerged in the proto-free culture era of the late nineties and early noughties.


Critical making [@Ratto:making] and engineering [@Parikka:engineered] are not enough,
explicit political making is lacking in today's free and open source practices.
Regardless of the morality of a project such as DEFCAD,
it has been one,
perhaps the only one,
concrete response to this particular process of subjugation,
because it triggered an hegemonic versus counter-hegemonic dynamic,
a public discussion about learning and reflecting outside of the techno-legal niche of the professionals of free and open source production.
What I can see today is that without a strong political ground,
the current state of open design can be interpreted very pessimisticly with little hope for change [@Rone:hardware].
Worse still,
actors of its early connection to a wider cultural context---for instance with the attempt to consider the approach of Brazilian *gambiarra* as a counter to industrial born prototyping practices [@Menotti:gambiarra]---are now leaving a ship they believe is sinking.
Instead of providing the necessary counter-hegemonic communities,
they seem to have lost their energy and grown increasingly cynical towards the makers movement they first thought they were connected to [@Fonseca:repair],
suggesting today the need to set-up an exodus towards other matters and concerns.
But by doing so,
they are ultimately leaving the way free for the hegemonic practices they fought so far.

\newpage
