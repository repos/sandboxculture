Conclusion {.unnumbered}
==========

\newpage

\setcounter{footnote}{0}


Summary of the Argument {.unnumbered}
-----------------------


In this thesis,
I have explored what impact free and open source software principles and practices has on the making of art,
and cultural production in general.
My main research question was:
in which practical and theoretical ways has free and open source software licensing provided a model of transformation for art and cultural production?


Such a question is timely and important for several reasons.
First,
as I show in the dissertation,
there has been almost two decades of writing,
experiments,
and various attempts to adapt free and open source principles and practices beyond the realm of software engineering.
Concretely,
it means that there is now enough material,
efforts and works to start discussing them and,
to assess,
for the first time,
the viability of making non-software works and cultural expressions,
using techno-legal systems similar to those found in free and open source software production.
Second,
today only a very few pseudo-free and free cultural licenses,
like those from Creative Commons,
are being used as alternatives to standard copyright protection.
Do they represent all the possible alternatives for cultural freedom or only a very tiny sample?
Of course,
I explain why some of these legal documents became dominant,
but I also discuss the plethora of other licenses available,
and that cultural producers are also *free* to come up with their own terms,
which is a crucial strategy for the license to effectively work as a paratextual artist's statement,
but also an economic strategy,
or the manifestation of a counter-hegemonic effort by cultural minorities.
There is therefore a need to question the dominance of a few licenses that have became omnipresent and found their way,
very often unquestioned,
into all sorts of places from clauses in cultural institution contracts that artists must sign for a commission,
to open access academic journals,
as well as web platforms legal framework for users to contribute or share content,
but also as methods taught in academies and universities.
In this situation,
culture producers are not encouraged to articulate their work by the means of designing their own techno-legal media,
but are instead asked to choose amongst limited and pre-fabricated options.
As a result,
these documents,
which are inscribed in a discourse of transparency and openness,
can also become smoke screens,
which means it is not always clear who benefits from this free circulation of information.
Third,
the techno-legal models of social organisation introduced by free and open source principles and practices have been an overlooked annunciator of issues found today in so-called algorithmic societies and other environments where "code is law" [In reference to @Lessig:code],
a motto that was for instance recently revived with the rise of cryptocurrencies.
In particular the shortcomings of such codification could already be perceived in free and open source communities where the political was either denied,
or ignored,
in favour of techno-legal assisted systems that claimed to replace human mediation in order to solve issues of power and control,
a trend that has been more broadly generalised recently as *technology solutionism* [@Morozov:click].
As I have illustrated in the first part of this thesis,
if on the one hand free software had enabled the empowering constitution and organisation of code-centric---software and legal code---communities,
it is also to this date the biggest self-applied experiment in cybernetics,
given the important role given to technology and regulatory processes to treat culture as a system that can be fully controlled.
This came with several strings attached,
as was discussed throughout the thesis,
but notably in Chapter 6,
the problem of reducing and simplifying culture to only a few specific processes and products.


To answer my main research question,
I divided the thesis into three parts,
named after Stallman's famous attempt to contextualise software freedom in his own terms: free as in speech [@Stallman2001].
In the first part *Free as in... Culture*,
I discuss what makes free and open source software principles and practices relevant to cultural production.
This is articulated over two chapters.
I argue in Chapter 1 that free software was not so much a paradigm shift in terms of software production,
but what was exceptional about it was the techno-legal template it provided to help constitute and regiment communities.
In Chapter 2,
I show that such a template was able to provide an abstraction of the subcultural hero rhetoric,
one reason why it was appropriated so widely by groups and individual outside of the original context of free and open source software culture.
However,
using the concept of agonistic pluralism from Mouffe,
I argue that these appropriations eventually became subject to the aggregative and deliberative normalisation process from Creative Commons and Freedom Defined projects respectively,
in which the desire to develop a consensual approach to cultural freedom,
came at the price of excluding other principles and practices of cultural freedom that conflicted with these dominant projects.


In the second part,
*Free as in... Art*,
I look into the overlaps and difference between the discourse from free and open source software,
its artistic adaption known as free art, 
and the free culture generalisation.
This discussion is carried over three chapters.
In Chapter 3,
I use free art as an example of the appropriation of the free software techno-legal template in art,
and I explain that despite the free culture umbrella,
there exists irreconcilable differences between the different communities that are presented as the same movement.
In Chapter 4,
I argue that next to these discrepancies there was also a second stage cultural appropriation happening when other artists and practitioners started to use free cultural techno-legal systems in their work yet without engaging with the individuals or groups from which such systems originated.
I then conclude the second part with Chapter 5,
explaining how these different levels of cultural diffusion have resulted to misunderstandings and faulty generalisation,
both by practitioners but also in their theoretical analyses.
However,
taking copyleft as an example,
I have also shown that misunderstanding can also happen at another level,
that is to say in the lack of recognition of how free culture exists at the cross-roads of different fields and historical contexts,
making it difficult to provide an absolute reasoning of why artists engage in free cultural practices.


Finally,
in the third and last part of the thesis,
*Free as in... Trapped*,
I discuss what kind of techno-legal and social systems are created by free and open source practices.
In Chapter 6,
I argue that so far the approach consisting of strictly defining free culture,
struggles to offer more than a system in which culture is limited to the sharing of files over the Internet.
With such an approach,
I explain that potentiality and accessibility of information---could I get these files now and what could I do with them---became the dominant form of criteria to determine the value of a work or cultural expression.
I also explain that the impossibility of precisely defining free cultural sources prevented a complete transposition of free software principles to free non-software works and cultural expressions.
In Chapter 7,
I then explain that the free cultural discourse of potentiality and accessibility distracts from discussing the systems in which free cultural information is used.
To give a name to such systems,
I suggest using the metaphor of the sandbox to describe the techno-legal frameworks in which free and open source *things* are shared and produced,
I also explain how sandboxing was historically introduced in the mix between operating systems and social systems.
To conclude,
in Chapter 8,
I explain how the sandbox model could be used to understand the different mechanism of replication existing within free culture,
notably taking software and license forking as an example.
I also argue that this model explains why free and open source communities give the impression of constantly becoming,
and that it also permits an indirect democratic process from which new discourses and counter-hegemonic efforts can emerge,
but at the same time are threatened by free cultural normalisation which impoverishes their discursivity and tends to exclude practices rather than allowing cultural diversity.



A Model for the Transformation of Art and Cultural Production? {.unnumbered}
--------------------------------------------------------------


As noted in the introduction,
the impact on cultural production of practices developed in relation to the ideas of free and open source software has been both influential and broadly applied.
However,
what this research highlights is that a line must be drawn between what free culture believes it is doing to culture,
and what happens when practitioners actually engage with cultural freedom,
that is to say,
when they want to liberate their practice from so-called tools of the trade and established workflows,
or when they want to free their work from traditional means of publishing,
distribution,
or appropriation.
One is clear,
orderly and rigid,
while the other is messy,
chaotic and adaptive.
British scholar John Clarke refers to *bricolage*,
that is to say,
the juxtaposition, re-ordering, rearrangement of previously unconnected objects to produce new meanings,
so as to illustrate the generative process of subcultural styles and cultural identities [@Clarke:style].
What my research shows is that a situation of bricolage is almost unavoidable once practitioners are engaged with free culture.
Regardless of whether this situation is accidental or conscious,
it leads to the creation of many new codes of meaning.
If artists,
designers,
writers,
and musicians,
who started to use and write free software as part of their practice have been in effect bricoleurs sharing a common starting point using directly,
or inspired by,
the free software techno-legal template,
I argue these bricoleurs did not however necessarily share anything from a cultural or political point of view,
and were likely to engage,
right from the start,
in incompatible or conflicting ideological pursuits.
And this is very fine.


This is where the line between the two kinds of free culture on which I attempted to draw earlier becomes crucial.
Free culture as a messy collection of hacked identities and bricolage should not be mixed up with free culture as a universal model for cultural production.
In the former there are various levels of cultural appropriation happening:
from anarchism,
rastafarianism,
punk,
hippysm,
transhumanism,
and more,
as found in the early free software and art field and today’s free-range free culture practitioners.
With their broken laptops,
DIY software and unstable media art,
together they form a rich *supermarket* of styles [@Polhemus:supermarket],
in which participants collectively think about societal issues,
and derive new utopias from the original free society of Stallman.
In this version of free culture,
cultural freedom is not an end,
but a means.
But, by contrast, in the defined and tamed free cultural model,
there is nothing but the uniform and normalised style of a creative industry working class, 
an efficient lifestyle with liberated software that *just works*,
professional interfaces to a culture where the political is denied, 
and the only thing that matters is a productive free circulation of information.
In this free culture,
acknowledging the existence of diverging forces weakens the community or movement,
license fragmentation and incompatibility is a problem,
and specialists are the ones crafting techno-legal human readable tools for the masses to use.
Here,
cultural freedom becomes akin to a technocratic policy,
where we are advised to buy into the label *free culture* without ever questioning what it means.


The strongest advantage of free culture is the opportunity it offers to claim back territories of knowledge and think collectively,
as small-scale reflective groups,
and not as obedient cooperative hordes.
So when it becomes a universal ready-made solution and end in itself,
free culture offers nothing but a variation of the systems it is thought to be an alternative to.
In that sense,
the history of free and open source software production and its several transformations should also work as a warning for practitioners willing to apply its universal productive apparatus to culture without critical assessment.


With that said,
the rise of artistic and cultural interest in free and open source software principles and practices is everything but an anecdote.
It is the embodiment of several elements that have announced important changes in artistic and cultural production at the turn of the millennia,
which are ultimately more crucial than the inflated generalisation of openness and the use of digital commons in an artistic setting,
implied by the deliberative or aggregative umbrella of free culture and Creative Commons licensing.
Such elements are:
the call to turn legal and technological rules into a novel system to make art;
the reflection on the nature of alienation and authorship;
the access and distribution of culture outside of official institutions and channels;
the democratic dimension of art-making liberated from elites;
the living archaeology of the creative process by bringing traceability and transparency;
and most importantly,
the mark of an age of intellectual property and bureaucratic exaltation,
which is pushing artists to develop their practice within the administrative structure of society,
and further embed it in their creative process,
even if,
in a paradoxical manner,
it is sometimes done to object to this very machinery.


There is so much confusion and misunderstanding about all these elements because they manifest and materialise differently at several levels,
via a process of rationalisation that leads to the fragmentation of cultural freedom into new codes of meaning,
the ideological and emotional nature of which can be contradictory to or incompatible with each other.
As a consequence,
free culture ends up being simply many different things at once:


* A toolkit for artists to expand their practice and free themselves from consumerist workflows;
* A template for political statements against authorities of any kind;
* A novel creative legal and technical framework to interface with and support existing copyright law practices;
* A lifestyle, and sometimes fashionable statement to go along with the marketing of all things free and open;
* An economic model that tries to reconcile the legacy of radical anti-property art practice with the reformist nature of social critique;
* An aesthetic in the sense of an audiovisual language, like meme culture, but also a number of novelty appropriative frameworks ranging from semionauts to circulationism.

In practice it is possible for a practitioner,
and their audience,
to cherry-pick or only see one of these properties,
and either ignore or not be aware of the others,
making cultural freedom a series of multidimensionally one-dimensional ambiguous objects,
open to different interpretations,
just like the codes they were drafted in.


And yet,
with all their imperfections,
these appropriations are very important because they force cultural production outside of the path set by mainstream culture and ideologies.
They push practices both into a corner and into strange places,
where practitioners are forced to challenge the dominant *handy* productive apparatus of their field. 
For instance by forcing an animator to question what animation is,
to force a movie maker to rethink what cinema is and the networked creation and distribution of moving images,
to create a constraint in which the tools for graphic design and audiovisual performance must be reinvented,
to help the writer reconnects with a forgotten legacy of self-publishing strategies,
etc.
In that sense the use of free and open source principles and practices in the context of art and cultural production relates closely to avant-garde practices,
where different aesthetics of resistance are articulated.
What is more,
the collaborative aspect of free software production,
and the free cultural licensing approach to the publishing and distribution of works,
resonates strongly with politically engaged practices in which the communication of intentions and the need to rally together,
becomes a call to collectively think about things,
to take action and not passively accept the formalisation of issues and their solutions by specialists and professionals.
This is why the abstraction of the subcultural hero rhetoric found in the free software techno-legal template,
even if flawed,
is an essential inspiration for empowerment and cultural diversity.


Remark on Sandbox Culture and Future Research {.unnumbered}
---------------------------------------------


In this thesis I have introduced a discussion on sandboxing,
and it would be interesting to see how it could be articulated beyond the free and open source context---something I have only slightly touched upon in Chapter 8---because it is a model that is very relevant to how social systems are constructed within network cultures.


Sandbox culture is a term that is sometimes used in connection with open-ended games or virtual world platforms,
to describe the different activities happening within these environments.
However,
as discussed in this thesis,
the idea of a sandbox culture can go far beyond the boundaries of software-rendered virtual realities.
Or,
to be more precise, 
this virtualisation can be articulated differently at every level of its different layers.
For instance it rarely occurs that relationships and transactions within a sandbox can also be sandboxed at another level.
Technology and its legal apparatus,
by the means of manipulation or misunderstandings,
generates a new imaginary,
a magical thinking that inspires novel forms of organisation and production,
which in return calls for the creation of more technology to support the newly bootstrapped culture.
It is both fascinating and worrying to see how this affects our relationship with others and with said technology,
and how the cohesive rules of a community inside a sandbox provide the *anchoring* needed to cope with an existence otherwise uprooted in global social,
cultural,
economic,
and political systems.
On their own,
all these sandboxes have the potential to be perfect friction-less standalone universes,
scaled down to a particular belief or lifestyle.
Being able to detect our sandboxing is far from being trivial.
To be sure,
a sandbox culture is not just another term to describe filter bubbles [@Pariser:bubble],
fields [@BourdieuWacquant:champs],
or subculture [@Hebdige:subculture],
but the precise techno-legal governance that shapes our vision of the world,
and that it is purposefully designed by others or ourselves to do so.
What is more,
a sandbox is not purely metaphorical because its existence can be tracked down to its exact codification:
software source code and code as enforced system of rules.


Sandboxing becomes tangible through the tensions and conflicts that arise from the inability to reconcile the messiness of human interactions with the programmatic rationalism of the rules,
the codes,
that are inscribed as the governance model within the sandbox.
Its extreme form of cybernetic thinking hides behind the utopia they claim to offer.
In a society that is increasingly programmed and scripted for efficiency and productivity,
the binary nature of the software apparatus gives less and less room for negotiation,
hesitation and reflection,
no room for trust to be explored and grow at a human pace once it is outsourced and mediated by techno-legal infrastructures.
Further research is needed to establish why this issue of trust seems to never be directly addressed,
why it is always deflected with more sandboxing and more codification.
Looking at the social component of proto-,
past and current conflation between operating systems and social systems,
it is clear that in the process of building digital infrastructures to inhabit,
fundamental social mechanisms have been moved into these architectures,
to the point where they became indistinguishable.
We should not be surprised if today we are at their mercy.

\newpage
